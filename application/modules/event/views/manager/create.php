<?php
    $id               = isset($item["id"]) ? $item["id"] : "";
    $judul            = isset($item["judul"]) ? $item["judul"] : "";
    $image_url        = isset($item["image_url"]) ? $item["image_url"] : "";
    $popup_image_url  = isset($item["popup_image_url"]) ? $item["popup_image_url"] : "";
    $alamat           = isset($item["alamat"]) ? $item["alamat"] : "";
    $content          = isset($item["content"]) ? $item["content"] : "";
    $periode_start    = isset($item["periode_start"]) ? date("d/m/Y", strtotime($item["periode_start"])) : "";
    $periode_end      = isset($item["periode_end"]) ? date("d/m/Y", strtotime($item["periode_end"])) : "";
    $latitude         = isset($item["latitude"]) ? $item["latitude"] : 0;
    $longitude        = isset($item["longitude"]) ? $item["longitude"] : 0;
    $province_id      = isset($item["province_id"]) ? $item["province_id"] : "";
    $prov_name        = isset($item["prov_name"]) ? $item["prov_name"] : "";
    $member_id        = isset($item["member_id"]) ? $item["member_id"] : "";
    $is_from_avian    = isset($item["is_from_avian"]) ? $item["is_from_avian"] : "";
    $is_show_as_popup = isset($item["is_show_as_popup"]) ? $item["is_show_as_popup"] : "";
    $is_show          = isset($item["is_show"]) ? $item["is_show"] : 1;
    $created_date     = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date     = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>

<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/event/lists';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/event/process-form" method="POST">
                            <header>Event form</header>
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Judul</label>
                                        <label class="input">
                                            <input name="judul" id="judul" type="text"  class="form-control" placeholder="Judul" value="<?php echo $judul; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Image View (640x256 px)</label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image">
                                                <?php if($image_url): ?>
                                                    <img src="<?= $image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="add-image" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Popup Image View (640x640 px)</label>
                                        <div class="input">
                                            <div class="add-image-popup-preview" id="preview-image-popup">
                                                <?php if($popup_image_url): ?>
                                                    <img src="<?= $popup_image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="add-image-popup" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($popup_image_url != "") ? "Change" : "Add" ?> Image Popup</button>
                                        </div>
                                    </section>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Periode Start</label>
                                            <label class="input">
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input name="periode_start" type="text" id="periode_start"  class="form-control" placeholder="Periode Start" value="<?php echo $periode_start; ?>" readonly/>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Periode End</label>
                                            <label class="input">
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input name="periode_end" type="text" id="periode_end"  class="form-control" placeholder="Periode End" value="<?php echo $periode_end; ?>" readonly/>
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Content</label>
                                        <label class="textarea">
                                            <textarea name="content" id="content" class="form-control tinymce" rows="1"><?= $content; ?></textarea>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?php echo select_is_show('is_show', $is_show, 'id="is_show"'); ?>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">From Avian</label>
                                        <label class="input">
                                            <?php echo select_is_from_avian('is_from_avian', $is_from_avian); ?>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Show as Popup</label>
                                        <label class="input">
                                            <?php echo select_is_show_as_popup('is_show_as_popup', $is_show_as_popup); ?>
                                        </label>
                                    </section>
                                </fieldset>

                                <header>Detail of explanatory event location</header>
                                <fieldset>
                                    <div id="map" style="height:450px"></div><br>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Alamat</label>
                                            <label class="textarea">
                                                <textarea name="alamat" id="alamat" class="form-control" rows="4"><?= $alamat; ?></textarea>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Provinsi</label>
                                            <select class="sel2form valid-this provinsi-select" name="province" style="width:100%">
                                                <?php if($province_id != ""): ?>
                                                    <option value="<?= $province_id ?>"><?= $prov_name ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Latitude</label>
                                            <label class="input">
                                                <input type="number" name="latitude" id="latitude" value="<?= $latitude ?>" >
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Longitude</label>
                                            <label class="input">
                                                <input type="number" name="longitude" id="longitude" value="<?= $longitude ?>" >
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <?php if ($created_date != ""): ?>
                                    <section>
                                        <label class="label">Created date </label>
                                        <label class="input">
                                            <?php echo $created_date; ?>
                                        </label>
                                    </section>
                                    <?php endif; ?>

                                    <?php if ($updated_date != ""): ?>
                                    <section>
                                        <label class="label">Update date </label>
                                        <label class="input">
                                            <?php echo $updated_date; ?>
                                        </label>
                                    </section>
                                    <?php endif; ?>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
