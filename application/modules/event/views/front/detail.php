<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="section-title tabgroup targrouptaba targettaba0 section-title-top" targid="taba0" targroup="taba"><?php echo $event["judul"] ?></div>
            <div class="event_detail">
                <div class="event_detail_pic">
                    <img src="<?php echo $event['image_url'] ?>">
                </div>
                <div class="event_detail_content">
                    <div class="event_detail_info">
                        <div>Periode Start : <span><?php echo $event["periode_start"] ?></span></div>
                        <div>Periode End : <span><?php echo $event["periode_end"] ?></span></div>
                        <div>Alamat : <span><?php echo $event["alamat"] ?></span></div>
                        <div>Provinsi : <span><?php echo $event["prov_name"] ?></span></div>
                    </div>
                    <?php echo $event["content"] ?>
                </div>
            </div>
        </div>
    </div>
</div>