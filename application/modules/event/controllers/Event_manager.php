<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Event Controller.
 */
class Event_manager extends Baseadmin_Controller
{
    private $_title         = "Event";
    private $_title_page    = '<i class="fa-fw fa fa-star"></i> Event ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "event";
    private $_back          = "/manager/event/lists";
    private $_js_path       = "/js/pages/event/manager/";
    private $_view_folder   = "event/manager/";
    private $_table         = "mst_event";
    private $_table_aliases = "evn";
    private $_pk            = "evn.id";

    /**
     * constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List event
     */
    public function lists()
    {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Event</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Event</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Create event
     */
    public function create()
    {

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Event</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Event</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "script_http" => array(
                "https://maps.googleapis.com/maps/api/js?key=". GOOGLE_API_KEY ."&libraries=geometry",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an event
     */
    public function edit($id = null)
    {
        if (empty($id)) {
            show_404();
        }

        $this->_breadcrumb .= '<li><a href="/manager/event/lists/">Event</a></li>';

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => array("evn.*", "pro.name AS prov_name"),
            "left_joined"     => array(
                "mtb_province pro" => array("pro.id" => "evn.province_id"),
                "mtb_country con"  => array("con.id" => "pro.country_id")
            ),
            "conditions" => array("evn.status" => STATUS_ACTIVE),
            "find_by_pk" => array($id),
            "row_array"  => true
        ))['datas'];

        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Event</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Event</li>',
            "back"          => "/manager/event/lists",
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "script_http" => array(
                "https://maps.googleapis.com/maps/api/js?key=". GOOGLE_API_KEY ."&libraries=geometry",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("judul", "Judul", "required");
        $this->form_validation->set_rules("content", "Content", "required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get event
     */
    public function list_all_data()
    {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

        $select = array("evn.id", "judul", "image_url", "popup_image_url", "periode_start", "periode_end", "evn.is_show", "pro.name as prov_name");

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['evn.id'] = $value;
                        }
                        break;

                    case 'judul':
                        if ($value != "") {
                            $data_filters['judul'] = $value;
                        }
                        break;

                    case 'prov_name':
                        if ($value != "") {
                            $data_filters['pro.name'] = $value;
                        }
                        break;

                    case 'start':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_start as date) <="] = $date['end'];
                            $conditions["cast(periode_start as date) >="] = $date['start'];
                        }
                        break;

                    case 'end':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_end as date) <="] = $date['end'];
                            $conditions["cast(periode_end as date) >="] = $date['start'];
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['evn.is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'          => $select,
            'left_joined'     => array(
                "mtb_province pro" => array("pro.id" => "evn.province_id"),
                "mtb_country con"  => array("con.id" => "pro.country_id")
            ),
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            "count_all_first" => true,
            'status' => STATUS_ACTIVE,
        ));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post - not ready.
     */
    public function process_form()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => false,
            "is_version" => true,
            "ordering"   => false
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $data_image       = $this->input->post('data-image');
        $data_image_popup = $this->input->post('data-image-popup');
        $periode_start    = validate_date_input($this->input->post('periode_start'));
        $periode_end      = validate_date_input($this->input->post('periode_end'));
        $judul            = $this->input->post('judul');
        $alamat           = $this->input->post('alamat');
        $content          = $this->input->post('content');
        $latitude         = $this->input->post('latitude');
        $longitude        = $this->input->post('longitude');
        $province         = $this->input->post('province');
        $is_show          = $this->input->post('is_show');
        $is_from_avian    = $this->input->post('is_from_avian');
        $is_show_as_popup = $this->input->post('is_show_as_popup');

        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            $arrayToDB = array(
                "judul"            => $judul,
                "alamat"           => $alamat,
                "content"          => $content,
                "periode_start"    => $periode_start,
                "periode_end"      => $periode_end,
                "latitude"         => $latitude,
                "longitude"        => $longitude,
                "province_id"      => $province,
                "is_show"          => $is_show,
                "is_from_avian"    => $is_from_avian,
                "is_show_as_popup" => $is_show_as_popup
            );

            $image = $this->upload_image("image", "upload/event", "image-file", $data_image, 640, 256, $id);
            $popup_image = $this->upload_image("popup", "upload/event/popup", "image-popup-file", $data_image_popup, 640, 640, $id);

            // Begin transaction.
            $this->db->trans_begin();

            // Insert or update?
            if ($id == "") {
                if (!empty($image)) {
                    $arrayToDB['image_url'] = $image;
                }

                if (!empty($popup_image)) {
                    $arrayToDB['popup_image_url'] = $popup_image;
                }

                // Insert to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                    $arrayToDB,
                    $extra_param
                );

            } else {

                // Condition for update.
                $condition = array("id" => $id);
                $get_img = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(
                    array(
                        "select"     => array("image_url","popup_image_url"),
                        "conditions" => $condition,
                        "row_array"  => true
                    )
                )["datas"];

                // update.
                if (!empty($image) && isset($get_img['image_url']) && !empty($get_img['image_url'])) {
                    unlink(FCPATH . $get_img['image_url']);
                }

                if (!empty($image)) {
                    $arrayToDB['image_url'] = $image;
                }

                if (!empty($popup_image) && isset($get_img['popup_image_url']) && !empty($get_img['popup_image_url'])) {
                    unlink(FCPATH . $get_img['popup_image_url']);
                }

                if (!empty($popup_image)) {
                    $arrayToDB['popup_image_url'] = $popup_image;
                }

                // Update to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    $arrayToDB,
                    $condition,
                    $extra_param
                );
            }

            // End transaction.
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "Event has been added.";

                    //push ke org org sesuai dengan provinsi yang di pilih
                    //push ke semua orang
                    $this->load->library("FirebaseFcm");
                    $firebase = new FirebaseFcm();

                    if (!empty($province)) {
                        //get token user
                        $tokens = $this->_dm->set_model("dtb_push_device", "dpd", "id")->get_all_data(array(
                            "conditions" => array(
                                "province_id" => $province
                            ),
                        ))['datas'];

                        $config = array(
                            "mode"          => "single",
                            "target"        => "ios",
                            "priority"      => "PRIORITY_HIGH",
                            "sound"         => "default",
                            "badge"         => 1,
                            "notif_title"   => "Event Baru Avian Brands",
                            "notif_content" => $judul,
                        );

                        if (is_array($tokens) && count($tokens) > 0) {
                            foreach ($tokens as $token) {
                                if (!empty($token['device_token']) && $token['push_event'] == 1) {
                                    $config['target'] = $token['device_token'];

                                    if ($token['type'] == 1) {
                                        $firebase->firepush_ios($config);
                                    }
                                    else {
                                        $firebase->firepush_android($config);
                                    }

                                }
                            }
                        }
                    }
                    else {
                        $config = array(
                            "mode"          => "topic",
                            "target"        => "ios_event",
                            "priority"      => "PRIORITY_HIGH",
                            "sound"         => "default",
                            "badge"         => 1,
                            "notif_title"   => "Event Baru Avian Brands",
                            "notif_content" => $judul,
                        );
                        $firebase->firepush_ios($config);

                        $config = array(
                            "mode"          => "topic",
                            "target"        => "android_event",
                            "priority"      => "PRIORITY_HIGH",
                            "notif_title"   => "Event Baru Avian Brands",
                            "notif_content" => $judul,
                        );
                        $firebase->firepush_android($config);
                    }



                } else {
                    $message['notif_message'] = "Event has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/event/lists";
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an event.
     */
    public function delete()
    {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data admin
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => true
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $delete = $_model->delete(
                    array("id" => $id),
                    array(
                        "is_version" => true,
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Event has been delete.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * List of Province.
     */
    public function get_list_provinsi()
    {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //get ajax query and page.
        $select_q = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;

        //sanitazion.
        $select_q = sanitize_str_input($select_q);

        //page must numeric.
        $select_page = is_numeric($select_page) ? $select_page : 1;

        //for paging, calculate start.
        $limit = 10;
        $start = ($limit * ($select_page - 1));

        //filters.
        $filters = array();
        if ($select_q != "") {
            $filters["lower(id)"] = strtolower($select_q);
            $filters["lower(name)"] = strtolower($select_q);
        }

        //conditions.
        $conditions = array( );

        //get data.
        $datas = $this->_dm->set_model('mtb_province', 'pro', 'id')->get_all_data(array(
            "select"          => array("pro.id", "pro.name", "IFNULL(total_device,0) as total_device"),
            "conditions"      => $conditions,
            "filter_or"       => $filters,
            "count_all_first" => true,
            "limit"           => $limit,
            "start"           => $start,
            "left_joined"     => array(
                "(
                    select province_id, count(id) as total_device
                    from dtb_push_device
                    group by province_id
                ) dpd" => array("dpd.province_id" => "pro.id")
            )
        ));

        //prepare returns.
        $message["page"]        = $select_page;
        $message["total_data"]  = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"]       = $datas['datas'];

        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
