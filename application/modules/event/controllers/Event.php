<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Event Controller.
 */
class Event extends Basepublic_Controller  {

    private $_view_folder   = "event/front/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    public function index(){

        redirect(base_url('articles'));
    }

    /**
     * Detail event on front-end
     */
    public function detail() {
        //get header page
        // $page = get_page_detail('event-detail');

        // $header = array(
        //     'header' => $page,
        // );

        // // param from url
        // $event_url = $this->uri->segment(3, 0);

        // // conditions
        // if(empty($event_url)) {
        //     show_404();
        // }

        // // Get all event data
        // $event = $this->_dm->set_model("mst_event", "evn", "evn.id")->get_all_data(array(
        //     "select"      => array("evn.*", "pro.name AS prov_name"),
        //     "conditions"  => array("evn.id" => $event_url),
        //     "left_joined" => array(
        //         "mtb_province pro" => array("pro.id" => "evn.province_id")
        //     ),
        //     "row_array" => TRUE
        // ))['datas'];

        // // conditions
        // if(empty($event)) {
        //     show_404();
        // }

        // $data['event'] = $event;

        // //load the views.
        // $this->load->view(FRONT_HEADER, $header);
        // $this->load->view($this->_view_folder . 'detail', $data);
        // $this->load->view(FRONT_FOOTER);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    ////////////////////////////// AJAX CALL ////////////////////////////////////

}
