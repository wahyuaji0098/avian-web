<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_colour extends Basepublic_Controller {
    private $_view_folder = "custom_colour/front/";

    function __construct() {
        parent::__construct();

    }

    public function index() {
        //get header page
        // $page = get_page_detail('custom-colours');

        // $header = array(
        //     'header'            => $page,
        // );

        // $footer = array(
        //     "script" => array(
        //         "/js/front/custom-colour.js",
        //     )
        // );

        // //load the views.
        // $this->load->view(FRONT_HEADER, $header);
        // $this->load->view($this->_view_folder . 'index');
        // $this->load->view(FRONT_FOOTER, $footer);
    	redirect(base_url('color'));

	}

	public function send () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$message = array();
		$message['is_error'] = false;
		$message['error_count'] = 0;
		$message['id'] = 0;
		$data = array();

		//check if user already login
		if (!isset($this->data_login_user['id'])) {
			$message['is_error'] = true;
			$message['error_count'] = 1;
			$data = "Please Login First to Save to Likebox.";
		} else {
			$r 	= $this->input->post('r');
			$g 	= $this->input->post('g');
			$b 	= $this->input->post('b');
			$name 	= $this->input->post('name');
			$hex_code 	= rgb2hex(array($r,$g,$b));

			$data_insert = array(
				"name" => $name,
				"red" => $r,
				"green" => $g,
				"blue" => $b,
				"hex_code" => $hex_code,
				"member_id" => $this->data_login_user['id'],
			);

            $this->db->trans_begin();

			//insert to likebox
			$insert = $this->_dm->set_model("dtb_custom_color", "dcc", "id")->insert($data_insert);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data = "database operation failed.";

                $message['is_error'] = true;
				$message['error_count'] = 1;
				$data = "Something Went Wrong..";
				$message['id'] = 0;

            } else {
                $this->db->trans_commit();

				$message['id'] = $insert;
            }
		}


		$message['data'] = $data;
        echo json_encode($message);

        exit;
	}
}
