<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/colours">WARNA</a>
                <span>KUSTOM WARNA</span>
            </div>
            <div class="halfpages">
                <div class="halfpage halfpagepadded">
                    <div class="halfpage_title"><h2>KUSTOM WARNA</h2></div>
                    <img src="/img/lists/colours/custom-color.jpg"/>
                    <div class="halfpage_section">
                        <p class="subcontent">
                            Buat warna Anda sendiri dengan slider ini, dan menyimpannya ke kotak favorit Anda.
                            Bawa data warna Anda ke toko Avian terdekat, dan mereka akan membuatnya untuk Anda!
                        </p>
                    </div>
                </div>
                <div class="halfpage shadowed">
                    <div class="grey_form grey_form-nofootpad">
                        <div class="grey_form_name">KOMPOSISI WARNA</div>
                        <p>
                            Aturlah persentase Merah, Hijau dan Biru untuk menciptakan warna yang Anda inginkan.
                        </p>
                    </div>
                    <div class="colourmixer" id="colourmixer" defcol="000000">
                        <div class="colourmixbtns">
                            <div class="colourmix">
                                <div class="colourmixsamp autopalette" colhex="EF5E5E"></div>
                                <div class="colourmixname">RED</div>
                                <div class="colourmixslide">
                                    <div class="cmxs_btn" targ="cmxs1" inc="10" pos="0" mixtarg="cmixresult">-10</div>
                                    <div class="cmxs_btn" targ="cmxs1" inc="1" pos="0" mixtarg="cmixresult">-1</div>
                                    <div class="cmxs_val" id="cmxs1" val="0">0%</div>
                                    <div class="cmxs_btn" targ="cmxs1" inc="1" pos="1" mixtarg="cmixresult">+1</div>
                                    <div class="cmxs_btn" targ="cmxs1" inc="10" pos="1" mixtarg="cmixresult">+10</div>
                                </div>
                                <div class="lead"></div>
                            </div>
                            <div class="colourmix">
                                <div class="colourmixsamp autopalette" colhex="44B244"></div>
                                <div class="colourmixname">GREEN</div>
                                <div class="colourmixslide">
                                    <div class="cmxs_btn" targ="cmxs2" inc="10" pos="0" mixtarg="cmixresult">-10</div>
                                    <div class="cmxs_btn" targ="cmxs2" inc="1" pos="0" mixtarg="cmixresult">-1</div>
                                    <div class="cmxs_val" id="cmxs2" val="0">0%</div>
                                    <div class="cmxs_btn" targ="cmxs2" inc="1" pos="1" mixtarg="cmixresult">+1</div>
                                    <div class="cmxs_btn" targ="cmxs2" inc="10" pos="1" mixtarg="cmixresult">+10</div>
                                </div>
                                <div class="lead"></div>
                            </div>
                            <div class="colourmix">
                                <div class="colourmixsamp autopalette" colhex="3BA7D1"></div>
                                <div class="colourmixname">BLUE</div>
                                <div class="colourmixslide">
                                    <div class="cmxs_btn" targ="cmxs3" inc="10" pos="0" mixtarg="cmixresult">-10</div>
                                    <div class="cmxs_btn" targ="cmxs3" inc="1" pos="0" mixtarg="cmixresult">-1</div>
                                    <div class="cmxs_val" id="cmxs3" val="0">0%</div>
                                    <div class="cmxs_btn" targ="cmxs3" inc="1" pos="1" mixtarg="cmixresult">+1</div>
                                    <div class="cmxs_btn" targ="cmxs3" inc="10" pos="1" mixtarg="cmixresult">+10</div>
                                </div>
                                <div class="lead"></div>
                            </div>
                        </div>
                        <div class="cmixresult">
                            <div class="cmixbreakdown">
                                <div class="cmixelement autopalette" colhex="EF5E5E">
                                    <span id="outcmxs1">0%</span>
                                </div>
                                <div class="cmixelement autopalette" colhex="44B244">
                                    <span id="outcmxs2">0%</span>
                                </div>
                                <div class="cmixelement autopalette" colhex="3BA7D1">
                                    <span id="outcmxs3">0%</span>
                                </div>
                            </div>
                            <div class="cmixresultbox" id="cmixresult">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
