<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Article Controller.
 */
class Article_manager extends Baseadmin_Controller  {

    private $_title = "Article";
    private $_title_page = '<i class="fa-fw fa fa-file-o"></i> Article ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "article";
    private $_back = "/manager/article";
    private $_js_path = "/js/pages/article/manager/";
    private $_view_folder = "article/manager/";

    private $_table = "dtb_article";
    private $_table_aliases = "dar";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List article
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Article</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Article</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an article
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/article">Article</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Article</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Article</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an article
     */
    public function edit ($article_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/article">Article</a></li>';

        //load the model.
        $data['item'] = null;

        //validate ID and check for data.
        if ( $article_id === null || !is_numeric($article_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $article_id));
        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Article</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Article</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("title", "title", "required");
        $this->form_validation->set_rules("pretty_url", "pretty_url", "required|callback_check_pretty_url[$id]");
        $this->form_validation->set_rules("full_content", "full content", "required");
        $this->form_validation->set_rules("is_show", "is show", "required");
        $this->form_validation->set_rules("enable_push", "Enable push", "required");

    }

    public function check_pretty_url($string,$id) {
        if (!$this->_secure) {
            show_404();
        }

        $conditions = array(
            "pretty_url" => $string,
        );

        if ($id != 0) {
            $conditions['id != '] = $id;
        }

        //check page title if exist
        $article = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "conditions" => $conditions
        ))['datas'];

        if ($article) {
            $this->form_validation->set_message('check_pretty_url', 'Pretty Url already exist, Pretty Url must be unique.');
			return FALSE;
        }

        return TRUE;
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','title','type', 'is_show', 'sticky_flag');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'title':
                        if ($value != "") {
                            $data_filters['lower(title)'] = $value;
                        }
                        break;

                    case 'type':
                        if ($value != "") {
                            $conditions['type'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "" && $value != "all") {
                            $conditions['is_show'] = ($value == "show") ? STATUS_SHOW : STATUS_HIDE;
                        }
                        break;

                    case 'sticky':
                        if ($value != "" && $value != "all") {
                            $conditions['sticky_flag'] = ($value == "yes") ? STATUS_SHOW : STATUS_HIDE;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
            'status' => STATUS_ACTIVE,
		));

        //get total rows
        $total_rows = $datas['total'];

        // echo "<pre>";
        // print_r($datas);


        // exit();
        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $extra_param = array(
            "is_version" => true,
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id                = sanitize_str_input($this->input->post('id'));
        $type              = sanitize_str_input($this->input->post('type'));
        $title             = sanitize_str_input($this->input->post('title'));
        $pretty_url        = sanitize_str_input($this->input->post('pretty_url'));
        $short_content     = sanitize_str_input($this->input->post('short_content'));
        $full_content      = $this->input->post('full_content');
        $status            = sanitize_str_input($this->input->post('status'));
        $is_show           = sanitize_str_input($this->input->post('is_show'));
        $meta_desc         = sanitize_str_input($this->input->post('meta_desc'));
        $meta_keys         = sanitize_str_input($this->input->post('meta_keys'));
        $tags              = sanitize_str_input($this->input->post('tags'));
        $enable_push       = sanitize_str_input($this->input->post('enable_push'));
        $push_date         = sanitize_str_input($this->input->post('push_date'));
        $push_time         = sanitize_str_input($this->input->post('push_time'));
        $sticky_flag       = sanitize_str_input($this->input->post('sticky_flag'));
        $file_pretty_name  = sanitize_str_input($this->input->post('file_pretty_name'));
        $data_image        = $this->input->post('data-image');
        $data_image_device = $this->input->post('data-image-device');

        $date= sanitize_str_input($this->input->post('date'));
        if ($date != "") $date = date('Y-m-d', strtotime($date));
        else $date = date('Y-m-d');

        if ($push_date) {
           if ($push_time) $push_date = date('Y-m-d H:i:s',strtotime($push_date ." ". $push_time.":00"));
           else $push_date = date('Y-m-d H:i:s',strtotime($push_date ." ". "00:00:00"));
        } else {
            $push_date = null;
        }

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $image = $this->upload_image($file_pretty_name, "upload/article", "image-file", $data_image, 740, 500, $id, array(
                "target_path"   =>  "upload/article/thumb",
                "filename"      =>  $file_pretty_name,
                "target_width"  =>  370,
                "target_height" =>  250,
                "crop_data"     =>  $data_image,
            ));

            $image_device = $this->upload_image("device", "upload/article/device", "image-file-device", $data_image_device, 640, 256, $id);

            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'type'          => $type,
                'title'         => $title,
                'pretty_url'    => $pretty_url,
                'short_content' => $short_content,
                'full_content'  => $full_content,
                'date'          => $date,
                'is_show'       => $is_show,
                'meta_desc'     => $meta_desc,
                'meta_keys'     => $meta_keys,
                'tags'          => $tags,
                'enable_push'   => $enable_push,
                'push_date'     => $push_date,
                'sticky_flag'   => $sticky_flag,
            );

            if (!empty($image)) {
                $arrayToDB['image_url'] = $image[0];
                $arrayToDB['image_thumb'] = $image[1];
                $arrayToDB['file_pretty_name'] = $file_pretty_name;
            }

            if (!empty($image_device)) {
                $arrayToDB['image_device'] = $image_device;
            }

            //insert or update?
            if ($id == "") {
                //insert to DB.
                $result = $model->insert($arrayToDB, $extra_param);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Article has been added.";

                    //push ke semua orang
                    $this->load->library("FirebaseFcm");
                    $firebase = new FirebaseFcm();

                    $config = array(
                        "mode"          => "topic",
                        "target"        => "ios_article",
                        "priority"      => "PRIORITY_HIGH",
                        "sound"         => "default",
                        "badge"         => 1,
                        "notif_title"   => "Artikel Baru",
                        "notif_content" => $title,
                    );
                    $firebase->firepush_ios($config);

                    $config = array(
                        "mode"          => "topic",
                        "target"        => "android_article",
                        "priority"      => "PRIORITY_HIGH",
                        "notif_title"   => "Artikel Baru",
                        "notif_content" => $title,
                    );
                    $firebase->firepush_android($config);

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/article";
                }

            } else {
                //condition for update.
                $condition = array("id" => $id);
                $result = $model->update($arrayToDB, $condition, $extra_param);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Article has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/article";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an member.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $extra_param = array(
            "is_version" => true,
        );

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data member
            $data = $model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $model->delete($condition, $extra_param);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Article has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
