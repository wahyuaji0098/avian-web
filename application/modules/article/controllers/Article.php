<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Article Controller.
 */
class Article extends Basepublic_Controller  {

    private $_view_folder = "article/front/";

    public function __construct() {
        parent::__construct();
    }

    public function index () {
        //ambil title, seo meta dari db untuk article
		$page = get_page_detail('article');

        redirect( base_url('articles') );

		// $limit = 6;
  //       $start = 0;
  //       $data = $this->_dm->set_model("view_article_csr", "va", "id")->get_all_data(array(
  //           "order_by" => array("date" => "desc"),
  //           "limit" => $limit,
  //           "start" => $start,
  //           "count_all_first" => true,
  //       ));

  //       $articles = $data['datas'];
  //       $total_page = ceil($data['total']/$limit);

		// //get all stickies
		// $stickies = $this->_dm->set_model("dtb_article", "da", "id")->get_all_data(array(
  //           "order_by" => array("date" => "desc"),
  //           "conditions" => array(
  //               "is_show" => SHOW,
  //               "sticky_flag" => STICKIE_FLAG_YES,
  //           ),
  //           "status" => STATUS_ACTIVE,
  //       ))['datas'];

  //       $header = array(
  //           'header'        => $page,
  //           'models'        => $articles,
  //           'stickies'      => $stickies,
		// 	'article_page'  => $total_page,
  //       );

  //       $footer = array(
  //           "script" => array(
  //               "/js/front/article.js",
  //           ),
  //       );

  //       //load the views.
  //       $this->load->view(FRONT_HEADER, $header);
  //       $this->load->view($this->_view_folder . 'index');
  //       $this->load->view(FRONT_FOOTER, $footer);
    }

    public function detail () {
        $art_url = $this->uri->segment(3, 0);

        //union csr ke artikel
        if (strtolower($art_url) == "csr") {
            $art_url = $this->uri->segment(4, 0);
        }

        if (empty($art_url)) {
            show_404('page');
        }

        //check store url
        $article = $this->_dm->set_model("view_article_csr ", "va", "id")->get_all_data(array(
            "conditions" => array(
                "pretty_url" => $art_url,
            ),
            "row_array" => true,
        ))['datas'];


		if (!$article) {
			//return error not found
			show_404('page');
		}


        redirect(base_url('articles/detail/'.$art_url));
  //       //get 4 random article beside this id
  //       $r_article = $this->_dm->set_model("view_article_csr", "va", "id")->get_all_data(array(
  //           "conditions" => array(
  //               "pretty_url != " => $art_url,
  //           ),
  //           "limit" => 4,
  //           "order_by" => array("rand()" => "")
  //       ))['datas'];

  //       $page = array(
		// 	"title" => $article['title'],
		// 	"meta_desc" => $article['meta_desc'],
		// 	"meta_keys" => $article['meta_keys'],
		// );

		// $header = array(
		// 	"title"      => $article['title'],
		// 	"meta_desc"  => $article['meta_desc'],
		// 	"meta_keys"  => $article['meta_keys'],
  //           'model'      => $article,
  //           'r_article'  => $r_article,
  //           'header'     => $page,
		// );

  //       $footer = array(
  //           "script" => array(
  //               "/js/front/article_detail.js",
  //               "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ae74c4dc6f7454b",
  //           ),
  //           "css" => array(
  //           )
  //       );

  //       //load the views.
  //       $this->load->view(FRONT_HEADER, $header);
  //       $this->load->view($this->_view_folder . 'detail');
  //       $this->load->view(FRONT_FOOTER, $footer);
    }

    ////////////////////////AJAX CALL/////////////////////////////

    public function loadmore () {
        //check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $page = sanitize_str_input($this->input->post("page"), "numeric");
        $limit = sanitize_str_input($this->input->post("limit"), "numeric");
        $from = sanitize_str_input($this->input->post("from"));

        if (empty($page)) $page = 1;
        $start = ($limit * ($page - 1));

        if ($from == "list") {
            $article = $this->_dm->set_model("view_article_csr ", "va", "id")->get_all_data(array(
                "order_by" => array("date" => "desc"),
                "limit" => $limit,
                "start" => $start,
                "count_all_first" => true,
            ));
        } else {
            $article = $this->_dm->set_model("dtb_article ", "da", "id")->get_all_data(array(
                "order_by" => array("date" => "desc"),
                "limit" => $limit,
                "start" => $start,
                "conditions" => array(
                    "is_show" => SHOW,
                ),
                "status" => STATUS_ACTIVE,
                "count_all_first" => true,
            ));
        }

        $models = array();
        foreach ($article['datas'] as $model) {
            $model['content_formated'] = ($model['short_content']) ? trimstr(strip_tags($model['short_content']), 150, 'WORDS', '...') : trimstr(strip_tags($model['full_content']), 200, 'WORDS', '...');
            unset($model['full_content']);
            array_push ($models,$model);
        }

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "result"        => "OK",
            "datas"         => $models,
            "total_page"    => ceil($article['total']/$limit),
        ));
		exit;
    }
}
