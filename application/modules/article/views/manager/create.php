<?php
    $id               = isset($item["id"]) ? $item["id"] : "";
    $type             = isset($item["type"]) ? $item["type"] : "";
    $pretty_url       = isset($item["pretty_url"]) ? $item["pretty_url"] : "";
    $title            = isset($item["title"]) ? $item["title"] : "";
    $image_device     = isset($item["image_device"]) ? $item["image_device"] : "";
    $short_content    = isset($item["short_content"]) ? $item["short_content"] : "";
    $full_content     = isset($item["full_content"]) ? $item["full_content"] : "";
    $date             = isset($item["date"]) ? date('Y-m-d', strtotime($item["date"])) : "";
    $status           = isset($item["status"]) ? $item["status"] : "";
    $status_name      = isset($item["status_name"]) ? $item["status_name"] : "";
    $is_show          = isset($item["is_show"]) ? $item["is_show"] : 1;
    $meta_desc        = isset($item["meta_desc"]) ? $item["meta_desc"] : "";
    $meta_keys        = isset($item["meta_keys"]) ? $item["meta_keys"] : "";
    $tags             = isset($item["tags"]) ? $item["tags"] : "";
    $enable_push      = isset($item["enable_push"]) ? $item["enable_push"] : 1;
    $push_date        = isset($item["push_date"]) ? date('Y-m-d',strtotime($item["push_date"])) : "";
    $push_time        = isset($item["push_date"]) ? date('H:i',strtotime($item["push_date"])) : "";
    $sticky_flag      = isset($item["sticky_flag"]) ? $item["sticky_flag"] : "";
    $image_url        = isset($item["image_url"]) ? $item["image_url"] : "";
    $image_thumb      = isset($item["image_thumb"]) ? $item["image_thumb"] : "";
    $file_pretty_name = isset($item["file_pretty_name"]) ? $item["file_pretty_name"] : "";
    $created_date     = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date     = isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date     = isset($item["deleted_date"]) ? $item["deleted_date"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Article</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/article/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Type <sup class="color-red">*</sup></label>
                                            <label class="select">
                                                <?= article_type_select('type', $type, 'id="type"'); ?>
                                                <i></i>
                                            </label>
                                        </section><!-- 
                                        <section class="col col-6">
                                            <label class="label">Type <sup class="color-red">*</sup></label>
                                            <label class="select">
                                                <select name="type" id="type" class="valid" aria-invalid="false">
                                                    <option value="" selected="selected">Pilih Article Type</option>
                                                    <option <?= ($type == 1) ? 'selected' : ''; ?> value="1">Tips</option>
                                                    <option <?= ($type == 2) ? 'selected' : ''; ?> value="2">Article</option>
                                                    <option <?= ($type == 3) ? 'selected' : ''; ?> value="3">News</option>
                                                    <option <?= ($type == 4) ? 'selected' : ''; ?> value="4">Siaran Pers</option>
                                                </select>
                                                <i></i>
                                            </label>
                                        </section> -->

                                        <section class="col col-6">
                                            <label class="label">Sticky <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                <?= select_yes_no('sticky_flag', $sticky_flag, 'id="sticky_flag"'); ?>
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Title <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="title" id="title" type="text"  class="form-control" placeholder="Judul Tips / Article" value="<?= $title; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Pretty Url <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="pretty_url" id="pretty_url" type="text"  class="form-control" placeholder="url untuk Tips / Article" value="<?= $pretty_url; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Short content</label>
                                        <label class="textarea">
                                            <textarea name="short_content" id="short_content" type="text"  class="form-control" placeholder="Cuplikan singkat dari isi Tips / Article" /><?= $short_content; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <div class="add-image-preview" id="preview-image">
                                                <?php if($image_thumb): ?>
                                                <img src="<?= $image_thumb ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_thumb != "") ? "Change" : "Add" ?> Image</button>

                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image For Device (640x256) <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <div class="add-image-preview" id="preview-image-device">
                                                <?php if($image_device): ?>
                                                <img src="<?= $image_device ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimagedevice" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_device != "") ? "Change" : "Add" ?> Image</button>

                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image filename</label>
                                        <label class="input">
                                            <input name="file_pretty_name" id="file_pretty_name" type="text"  class="form-control" placeholder="Image Filename" value="<?= $file_pretty_name; ?>" />
                                        </label>
                                    </section>


                                    <section>
                                        <label class="label">Full content <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="full_content" id="full_content" class="form-control tinymce" rows="10"><?= $full_content; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Date of Tips / Article </label>
                                        <label class="input">
                                            <i class="icon-append fa fa-calendar"></i>
                                            <input name="date" id="date" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Tanggal Tips / Article" value="<?= $date; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Meta description </label>
                                        <label class="textarea">
                                            <textarea name="meta_desc" id="meta_desc" class="form-control" placeholder="untuk SEO, deskripsi dari Tips / Article, sebaiknya 70 karakter minimum."><?= $meta_desc; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Meta keywords </label>
                                        <label class="textarea">
                                            <textarea name="meta_keys" id="meta_keys" class="form-control" placeholder="Kata kunci untuk SEO dari Tips / Article"><?= $meta_keys; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Show / Hide <sup class="color-red">*</sup> </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show, 'id="is_show"'); ?>
                                        </label>
                                    </section>

                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Enable Push <sup class="color-red">*</sup> </label>
                                            <label class="input">
                                                <?= select_yes_no('enable_push', $enable_push, 'id="enable_push"'); ?>
                                            </label>
                                        </section>

                                        <section class="col col-6">
                                            <label class="label">Push Date </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input name="push_date" id="push_date" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Tanggal push" value="<?= $push_date; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <section>
                                    <label class="label">Active</label>
                                    <label class="have_data">
                                        <div><?= $status_name ?></div>
                                    </label>
                                </section>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Deleted Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($deleted_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
