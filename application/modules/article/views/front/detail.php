<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/article">TIPS & ARTIKEL</a>
                <span><?= strtoupper($model['title']) ?></span>
            </div>
            <div class="articlepage">

                <div class="article">
                    <div class="article-type"><?= ($model['type'] == "csr") ? "CSR" : Dynamic_model::$article_type[$model['type']] ?></div>
                    <div class="article-date"><?= date('d F Y', strtotime($model['date'])) ?></div>
                    <div class="article-title"><h2><?= $model['title'] ?></h2></div>
                    <!-- <div class="article-subtitle">By Mr. McAuthor</div> -->

                    <!-- share with plugin -->
                    <div class="addthis_inline_share_toolbox"></div>

                    <div class="article-contents">
                        <img src="<?= $model['image_url'] ?>" width="100%" />
                        <?= html_entity_decode($model['full_content']) ?>
                    </div>
                </div>
                <div class="article-sidebar">
                    <div class="article-sidebar-title">ARTIKEL LAINNYA</div>
                    <div class="article-sidebar-list">
                        <?php if(count($r_article) > 0): foreach($r_article as $art): ?>
                        <a href="<?= ($art['type'] == "csr") ? "/article/detail/csr/".$art['pretty_url'] : "/article/detail/".$art['pretty_url'] ?>" class="article-sidebar-item">
                            <span class="article-sidebar-item-pic"><img src="<?= $art['image_thumb'] ?>" /></span>
                            <span class="article-sidebar-item-name"><?= $art['title'] ?></span>
                            <span class="article-sidebar-item-date"><?= date('d F Y', strtotime($art['date'])) ?></span>
                        </a>
                        <?php endforeach; endif; ?>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
