<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>TIPS & ARTIKEL</span>
            </div>
            <?php if(count($stickies) > 0): ?>
            <div class="horizontal-scroller horizontal-scroller-wide scrollbarcust avian-scroll">
                <div class="horizontal-scroller-content">
                    <?php foreach ($stickies as $stick): ?>
                    <a href="/article/detail/<?= $stick['pretty_url'] ?>" class="horizontal-scroller-item">
                        <img src="<?= $stick['image_thumb'] ?>" alt="<?= $stick['title'] ?>"/>
                        <span class="horizontal-scroller-itemname"><?= $stick['title'] ?></span>
                        <span><?= date('d F Y', strtotime($stick['date'])) ?></span>
                    </a>
                    <?php endforeach; ?>
                    <div class="lead"></div>
                </div>
            </div>
            <?php endif; ?>
            <div class="thirds thirdsfixheight" id="article-container">
                <?php if(count($models) > 0): foreach ($models as $model): ?>
                <div class="athird athirdpictxt" ><a href="/article/detail/<?= $model['pretty_url'] ?>">
                    <div class="athirdpic"><img src="<?= $model['image_thumb'] ?>" alt="<?= $model['title'] ?>" /></div>
                    <span class="athirdtitle"><?= $model['title'] ?></span>
                    <span class="athirdtxt"><?= ($model['short_content']) ? trimstr(strip_tags($model['short_content']), 150, 'WORDS', '...') : trimstr(strip_tags($model['full_content']), 200, 'WORDS', '...'); ?></span>
                </a></div>
                <?php endforeach; endif; ?>
                <div class="lead"></div>
                <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
            </div>
        </div>
    </div>
</div>
