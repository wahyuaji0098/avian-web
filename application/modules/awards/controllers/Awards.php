<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Awards Controller.
 */
class Awards extends Basepublic_Controller  {

    private $_view_folder = "awards/front/";

    public function __construct() {
        parent::__construct();

        $this->load->model("Awards_model");
        $this->load->model("Awards_image_model");
    }


    public function index() {
        //get header page

        redirect(base_url('award'));
  //       $page = get_page_detail('awards');

  //       //get awards list
  //       $model_awards = new Awards_model();

  //       $lists = $model_awards->getAllAwardsAndImages ();
		// //get avail product awards
		// $prod_awd = $this->Awards_image_model->getAllProductAwards();
		// $prod_awards = array();

		// if (count($prod_awd) > 0) {
		// 	foreach ($prod_awd as $model) {
		// 		if ($model['product_id'] == 3) {
		// 			$model['name'] = "Avitex";
		// 		}

		// 		if ($model['product_id'] != 6) array_push($prod_awards,$model);
		// 	}
		// }

		// $year_awards = $this->Awards_image_model->getMinMaxYear();

  //       $header = array(
  //           'header'            => $page,
  //           'models'            => $lists,
  //           'prod_awards'       => $prod_awards,
  //           'year_awards'       => $year_awards,
  //       );

  //       $footer = array(
  //           "script" => array(
  //               "/js/front/awards.js",
  //           ),
  //       );

  //       //load the views.
  //       $this->load->view(FRONT_HEADER, $header);
  //       $this->load->view($this->_view_folder . 'index');
  //       $this->load->view(FRONT_FOOTER, $footer);
	}

    public function search() {
        //check if ajax request
  //       if (!$this->input->is_ajax_request()) {
		// 	exit('No direct script access allowed');
		// }  

        $this->db->save_queries = TRUE;

		$year = sanitize_str_input($this->input->post("year"));
		$filter = $this->input->post("filter");

        $conditions = array(
            "is_show" => SHOW,
        );

        if ($year != "") {
            $conditions['year'] = $year;
        }

        if (!empty($filter)) {
			if (array_search("3",$filter) !== FALSE) $filter[] = 6;
            $filter = implode("','", $filter);
            $conditions["product_id in ('" . $filter . "')"] = NULL;
            // $conditions["debug"] = true;
		}

        $model_awards = new Awards_model();

        //get ids of awards
        $ids = $this->_dm->set_model("dtb_awards_image", "dai", "id")->get_all_data(array(
            "select"        => array("awards_id"),
            "distinct"      => true,
            "conditions"    => $conditions,
            "status"        => STATUS_ACTIVE,
            // "debug"         =>  true,
        ))['datas'];


        $lists = array();
        // exit();

        if ($ids) {
            $ids = array_column($ids, 'awards_id');
            $lists = $model_awards->getAllAwardsAndImages ($ids,$conditions);
        }

        // echo $this->db->last_query();
        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "result" => "OK",
            "datas" => $lists,
            "ids" => $ids,
            'q' => $this->db->last_query()  
        ));
		exit;

    }

}
