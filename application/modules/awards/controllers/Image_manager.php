<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Image Controller.
 */
class Image_manager extends Baseadmin_Controller  {

    private $_title = "Awards Images";
    private $_title_page = '<i class="fa-fw fa fa-trophy"></i> Awards <span>> Images</span>';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li><li><a href='/manager/awards'>Awards</a></li>";
    private $_active_page = "awards";
    private $_back = "/manager/awards";
    private $_js_path = "/js/pages/awards/manager/image/";
    private $_view_folder = "awards/manager/image/";

    private $_table = "dtb_awards_image";
    private $_table_aliases = "dai";
    private $_pk = "dai.id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Awards
     */
    public function lists($id = null) {
        if (empty($id) && !is_numeric($id)) {
            show_404();
        }

        $this->load->model('Awards_model');
        $this->load->model('Awards_image_model');

        //get all ordering slider
        $data['awards'] = $this->Awards_model->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        if (!$data['awards']) {
            show_404();
        }

        $data['ordering_data'] = $this->Awards_image_model->getAllOrdering($id);


        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Image Awards '. $data['awards']['title'] .'</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li> List Image '. $data['awards']['title'] .'</li>',
            "back"          => $this->_back,
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/lightbox/js/lightbox.min.js",
                $this->_js_path . "list.js",
            ),
            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            )
        );



        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an awards
     */
    public function create ($id = null) {
        $this->load->model('Awards_model');

        //get all ordering slider
        $data['awards'] = $this->Awards_model->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        if (!$data['awards']) {
            show_404();
        }

        $this->_breadcrumb .= '<li><a href="/manager/awards/image/lists/'. $data['awards']['id']  .'">List Image '. $data['awards']['title'] .'</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Image Awards</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Image Awards</li>',
            "back"          => $this->_back . "/image/lists/" . $id,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                "/js/crop-master.js",
                $this->_js_path . "create.js",
            )
        );

        $data['awards_id'] = $id;

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an awards
     */
    public function edit ($awards_image_id = null) {

        //load the model.
        $this->load->model("Awards_image_model");
        $data['item'] = null;

        //validate ID and check for data.
        if ( $awards_image_id === null || !is_numeric($awards_image_id) ) {
            show_404();

        }

        $params = array(
            "select" => array("dai.*", "dp.name as product_name"),
            "row_array" => true,
            "conditions" => array("dai.id" => $awards_image_id),
            "left_joined" => array(
                "dtb_product dp" => array("dp.id" => "dai.product_id")
            ),
        );
        //get the data.
        $data['item'] = $this->Awards_image_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        $this->load->model('Awards_model');

        //get all ordering slider
        $data['awards'] = $this->Awards_model->get_all_data(array(
            "find_by_pk" => array($data['item']['awards_id']),
            "row_array" => true,
        ))['datas'];


        $this->_breadcrumb .= '<li><a href="/manager/awards/image/lists/'. $data['awards']['id']  .'">List Image '. $data['awards']['title'] .'</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Image Awards</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Image Awards</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                "/js/crop-master.js",
                $this->_js_path . "create.js",
            )
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("year", "Year", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('dai.id','dai.file_url','dai.year', 'dpr.name', 'dai.ordering', 'dai.is_show');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'aid':
                        if ($value != "") {
                            $conditions['awards_id'] = $value;
                        }
                        break;

                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(dai.id)'] = $value;
                        }
                        break;

                    case 'year':
                        if ($value != "") {
                            $data_filters['lower(dai.year)'] = $value;
                        }
                        break;

                    case 'produk':
                        if ($value != "") {
                            $data_filters['lower(dpr.name)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $conditions['dai.is_show'] = ($value == "show") ? STATUS_SHOW : STATUS_HIDE;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
			'select' => $select,
            'left_joined' => array(
                "dtb_product dpr" => array("dpr.id" => "dai.product_id")
            ),
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
            'status' => STATUS_ACTIVE,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        //load model
        $this->load->model("Awards_image_model");

        $model = $this->Awards_image_model;

        //sanitize input (id is primary key, if from edit, it has value).
        $id = sanitize_str_input($this->input->post('id'), "numeric");
        $aid = sanitize_str_input($this->input->post('aid'), "numeric");
        $year = sanitize_str_input($this->input->post('year'));
        $product_id = sanitize_str_input($this->input->post('product_id'));
        $file_alt_name = sanitize_str_input($this->input->post('file_alt_name'));
        $file_pretty_name = sanitize_str_input($this->input->post('file_pretty_name'));
        $is_show = sanitize_str_input($this->input->post('is_show'));
        $data_image = $this->input->post('data-image');

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            //upload image
            $image = $this->upload_image ($file_pretty_name, UPLOAD_PATH_AWARDS, "image-file", $data_image, 560, 400, $id);

            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'awards_id'         => $aid,
                'year'              => $year,
                'product_id'        => $product_id,
                'file_alt_name'     => $file_alt_name,
                'file_pretty_name'  => $file_pretty_name,
                'is_show'           => $is_show,
            );

            if (!empty($image)) {
                $arrayToDB['file_url'] = $image;
            }

            //insert or update?
            if ($id == "") {
                $ordering = $this->Awards_image_model->getLastOrdering($aid);
                $arrayToDB['ordering'] = $ordering+1;

                //insert to DB.
                $result = $model->insert($arrayToDB);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Awards Image has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/awards/image/lists/".$aid;
                }

            } else {
                //get current image data
                $curr_data = $this->Awards_image_model->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

                //update.
                if (!empty($image) && isset($curr_data['file_url']) && !empty($curr_data['file_url'])) {
                    unlink( FCPATH . $curr_data['file_url'] );
                }

                //condition for update.
                $condition = array("id" => $id);
                $result = $model->update($arrayToDB, $condition);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Awards Image has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/awards/image/lists/".$aid;
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an member.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Awards_image_model');
        $model = $this->Awards_image_model;

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data member
            $data = $model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $model->delete($condition, array("is_permanently" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();

                    if (isset($data['file_url']) && !empty($data['file_url'])) {
                        unlink( FCPATH . $data['file_url']);
                    }

                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Awards Image has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function ordering() {
    	//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $id = $this->input->post('id');
    	$order = $this->input->post('val');

        $this->load->model("Awards_image_model");

		$table = $this->Awards_image_model;
		$model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

		$datas = $table->getAllAwardsImage($model['awards_id']);

		$min = $table->getFirstOrdering($model['awards_id']);
		$max = $table->getLastOrdering($model['awards_id']);

		$newkey = searchForIdInt($order, $datas, 'ordering');
		$oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

		$oldOrdering = $model['ordering'];

        $this->db->trans_begin();

		if ($oldOrdering > $order) {
			//smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
			for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
					'ordering' =>  $datas[$i]['ordering']+1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));

		} else if ($oldOrdering < $order) {
			//smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
			for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
				$table->update(array(
					'ordering' =>  $datas[$i]['ordering']-1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));
		}

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title'] = "Done!";
            $message['notif_message'] = "Image Awards Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
