<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Awards Controller.
 */
class Awards_manager extends Baseadmin_Controller  {

    private $_title = "Awards";
    private $_title_page = '<i class="fa-fw fa fa-trophy"></i> Awards ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "awards";
    private $_back = "/manager/awards";
    private $_js_path = "/js/pages/awards/manager/";
    private $_view_folder = "awards/manager/";

    private $_table = "dtb_awards_image";
    private $_table_aliases = "dai";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Awards
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Awards</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Awards</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        $this->load->model('Awards_model');

        //get all ordering slider
        $data['ordering_data'] = $this->Awards_model->getAllOrdering();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an awards
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/awards">Awards</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Awards</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Awards</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => $this->_js_path . "create.js",
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an awards
     */
    public function edit ($awards_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/awards">Awards</a></li>';

        //load the model.
        $this->load->model("Awards_model");
        $data['item'] = null;

        //validate ID and check for data.
        if ( $awards_id === null || !is_numeric($awards_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $awards_id));
        //get the data.
        $data['item'] = $this->Awards_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //get all awards image
        $data['images'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'order_by' => array("ordering" => "asc"),
			'conditions' => array("awards_id" => $awards_id),
            'status' => STATUS_ACTIVE,
		));

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Awards</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Awards</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("title", "Title", "trim|required");
        $this->form_validation->set_rules("sub_title", "Subtitle", "trim|required");
        $this->form_validation->set_rules("description", "Description", "trim|required");
        $this->form_validation->set_rules("is_show", "Is Show", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //load the model
        $this->load->model("Awards_model");

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','title','sub_title', 'description', 'ordering', 'is_show');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'title':
                        if ($value != "") {
                            $data_filters['lower(title)'] = $value;
                        }
                        break;

                    case 'subtitle':
                        if ($value != "") {
                            $data_filters['lower(sub_title)'] = $value;
                        }
                        break;

                    case 'description':
                        if ($value != "") {
                            $data_filters['lower(description)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $conditions['is_show'] = ($value == "show") ? STATUS_SHOW : STATUS_HIDE;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Awards_model->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
            'status' => STATUS_ACTIVE,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //load model
        $this->load->model("Awards_model");

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        $model = $this->Awards_model;

        //sanitize input (id is primary key, if from edit, it has value).
        $id = sanitize_str_input($this->input->post('id'), "numeric");
        $title = sanitize_str_input($this->input->post('title'));
        $sub_title = sanitize_str_input($this->input->post('sub_title'));
        $description = sanitize_str_input($this->input->post('description'));
        $is_show = sanitize_str_input($this->input->post('is_show'), "numeric");

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'title'       => $title,
                'sub_title'   => $sub_title,
                'description' => $description,
                'is_show'     => $is_show,
            );

            //insert or update?
            if ($id == "") {
                $ordering = $model->getLastOrdering();
                $arrayToDB['ordering'] = $ordering+1;

                //insert to DB.
                $result = $model->insert($arrayToDB);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Awards has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/awards";
                }

            } else {
                //condition for update.
                $condition = array("id" => $id);
                $result = $model->update($arrayToDB, $condition);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Awards has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/awards";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an member.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Awards_model');
        $model = $this->Awards_model;

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data member
            $data = $model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $model->delete($condition);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Awards has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function ordering() {
    	//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $id = $this->input->post('id');
    	$order = $this->input->post('val');

        $this->load->model("Awards_model");

		$table = $this->Awards_model;
		$model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

		$datas = $table->getAllAwards();

		$min = $table->getFirstOrdering();
		$max = $table->getLastOrdering();

		$newkey = searchForIdInt($order, $datas, 'ordering');
		$oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

		$oldOrdering = $model['ordering'];

        $this->db->trans_begin();

		if ($oldOrdering > $order) {
			//smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
			for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
					'ordering' =>  $datas[$i]['ordering']+1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));

		} else if ($oldOrdering < $order) {
			//smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
			for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
				$table->update(array(
					'ordering' =>  $datas[$i]['ordering']-1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));
		}

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title'] = "Done!";
            $message['notif_message'] = "Awards Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
