<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>PENGHARGAAN</span>
            </div>
            <h2 class="section-title section-title-top">PENGHARGAAN</h2>
            <div class="content_sbar">
                <div class="sidebar">
                    <div class="sidebar-title">PILIHAN</div>
                    <div class="sidebar-group sidebar-group-wide">
                        <div class="sidebar-group-title">Tahun </div>
                        <div class="option-options">
                            <div class="option option-select">
                                <div class="select-cont">
                                    <select id="year_filter" name="year_filter">
                                        <option value="">Semua Tahun</option>
                                        <?php $min = $year_awards['min_year'];
                                        $max = $year_awards['max_year'];
                                        for($i = $max; $i >= $min; $i--): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                        <?php endfor; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sidebar-group">
                        <div class="sidebar-group-title">Produk </div>
                        <div class="option-options">
                            <?php if(count($prod_awards) > 0): foreach($prod_awards as $pawd): ?>
                            <div class="option option-checkbox">
                                <input type="checkbox" name="produk[]" value="<?= $pawd['product_id'] ?>" id="<?= str_replace(" ","",$pawd['name']) ?>"/>
                                <label for="<?= str_replace(" ","",$pawd['name']) ?>">
                                    <div class="cicon"></div>
                                    <span><?= $pawd['name'] ?></span>
                                </label>
                            </div>
                            <?php endforeach; endif; ?>
                        </div>
                    </div>
                    <div class="lead"></div>
                </div>
                <div class="sbarcontent awards-list">
                    <?php if(count($models) > 0): foreach($models as $model): ?>
                    <div class="award">
                        <div class="awardpic" id="awardpic1">
                            <?php if(count($model['images']) > 0 ): foreach($model['images'] as $image): ?>
                            <img src="<?= $image['file_url'] ?>" alt="<?= $image['file_alt_name'] ?>" />
                            <?php endforeach; endif; ?>
                        </div>
                        <div class="awardtext">
                            <div class="awardtitle">
                                <h3><?= $model['title'] ?></h3>
                                <span><?= $model['sub_title'] ?></span>
                            </div>
                            <div class="awardp">
                                <p><?= $model['description'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; endif; ?>
                    <div class="lead"></div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
