<?php
    $id= isset($item["id"]) ? $item["id"] : "";
    $awards_id= isset($awards_id) ? $awards_id : (isset($item["awards_id"]) ? $item["awards_id"] : "");
    $file_pretty_name = isset($item["file_pretty_name"]) ? $item["file_pretty_name"] : "";
    $file_alt_name = isset($item["file_alt_name"]) ? $item["file_alt_name"] : "";
    $year = isset($item["year"]) ? $item["year"] : "";
    $product_id = isset($item["product_id"]) ? $item["product_id"] : "";
    $file_url = isset($item["file_url"]) ? $item["file_url"] : "";
    $is_show= isset($item["is_show"]) ? $item["is_show"] : 1;
    $status= isset($item["status"]) ? $item["status"] : "";
    $ordering= isset($item["ordering"]) ? $item["ordering"] : "";
    $created_date= isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date= isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date= isset($item["deleted_date"]) ? $item["deleted_date"] : "";
    $product_name= isset($item["product_name"]) ? $item["product_name"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Awards Images</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/awards/image/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <?php if($awards_id != 0): ?>
                                    <input type="hidden" name="aid" value="<?= $awards_id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Year <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="year" type="text"  class="form-control" placeholder="Year" value="<?= $year; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Product</label>
                                        <label class="input">
                                            <select class="form-control select2 product-select" name="product_id" style="width:100%">
                                                <?php if($product_id != ""): ?>
                                                <option value="<?= $product_id ?>"><?= $product_name ?></option>
                                                <?php endif; ?>
                                            </select>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Alt Name</label>
                                        <label class="input">
                                            <input name="file_alt_name" type="text"  class="form-control" placeholder="Alt Name" value="<?= $file_alt_name; ?>" />
                                        </label>
                                        <div class="note">
                                            for seo purpose
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Image (560X400 px)<sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-user">
                                                <?php if($file_url): ?>
                                                <img src="<?= $file_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($file_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">File Name</label>
                                        <label class="input">
                                            <input name="file_pretty_name" type="text"  class="form-control" placeholder="File Name" value="<?= $file_pretty_name; ?>" />
                                        </label>
                                        <div class="note">
                                            rename untuk file nya
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Deleted Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($deleted_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
