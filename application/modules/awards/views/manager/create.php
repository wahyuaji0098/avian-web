<?php
    $id= isset($item["id"]) ? $item["id"] : "";
    $title= isset($item["title"]) ? $item["title"] : "";
    $sub_title= isset($item["sub_title"]) ? $item["sub_title"] : "";
    $description= isset($item["description"]) ? $item["description"] : "";
    $is_show= isset($item["is_show"]) ? $item["is_show"] : 1;
    $status= isset($item["status"]) ? $item["status"] : "";
    $ordering= isset($item["ordering"]) ? $item["ordering"] : "";
    $created_date= isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date= isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date= isset($item["deleted_date"]) ? $item["deleted_date"] : "";

    $images = isset($images) ? $images : array();

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Awards</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/awards/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Title <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="title" id="title" type="text"  class="form-control" placeholder="Title" value="<?= $title; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Sub title <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="sub_title" id="sub_title" type="text"  class="form-control" placeholder="Sub title" value="<?= $sub_title; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Description <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="description" id="description" type="text"  class="form-control" placeholder="Description" /><?= $description; ?></textarea>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Deleted Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($deleted_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
