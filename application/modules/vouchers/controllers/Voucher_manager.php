<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Voucher Controller.
 */
class Voucher_manager extends Baseadmin_Controller  {

    private $_title = "Voucher";
    private $_title_page = '<i class="fa-fw fa fa-credit-card"></i> Voucher ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "voucher";
    private $_back = "/manager/vouchers/voucher/list-voucher";
    private $_js_path = "/js/pages/voucher/";
    private $_view_folder = "vouchers/manager/";

    private $_table = "mst_voucher";
    private $_table_aliases = "mv";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Voucher
     */
    public function list_voucher() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Voucher</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Voucher</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/lightbox/js/lightbox.min.js",
                $this->_js_path . "list.js",
            ),

            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create Voucher
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/country">Voucher</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Voucher </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Voucher</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
    * Edit voucher
    */
    public function edit($id = "") {
        if(!$id) {
            show_404();
        }

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Country </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Country</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            ),
        );

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'conditions' => array("id" => $id),
            'row_array' => true
        ))['datas'];

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

     //////////////////////////////// RULES //////////////////////////////////////
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //special validations for when editing.
        $this->form_validation->set_rules('merchant_id', 'Merchant', "trim|required");
        $this->form_validation->set_rules('stok', 'Stok', 'trim|required');
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data voucher
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array('mv.id','mm.name as merchant', 'is_show', 'title', 'price', 'image_url', 'periode_start', 'periode_end', 'stok', 'summary');
        $joined = array("mst_merchant mm" => array("mm.id" => "mv.merchant_id"));
        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array("mv.status" => STATUS_ACTIVE);

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(merchant)'] = $value;
                        }
                        break;

                    case 'title':
                        if ($value != "") {
                            $data_filters['lower(title)'] = $value;
                        }
                        break;

                    case 'price':
                        if ($value != "") {
                            $data_filters['lower(price)'] = $value;
                        }
                        break;

                    case 'periode_start':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_start as date) <="] = $date['end'];
                            $conditions["cast(periode_start as date) >="] = $date['start'];

                        }
                        break;

                    case 'periode_end':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_end as date) <="] = $date['end'];
                            $conditions["cast(periode_end as date) >="] = $date['start'];

                        }
                        break;

                    case 'create_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(created_date as date) <="] = $date['end'];
                            $conditions["cast(created_date as date) >="] = $date['start'];

                        }
                        break;

                    case 'update_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(updated_date as date) <="] = $date['end'];
                            $conditions["cast(updated_date as date) >="] = $date['start'];

                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'          => $select,
            'joined'          => $joined,
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            "count_all_first" => true,
        ));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;
        
        //
        $this->load->library('form_validation');
        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id            = sanitize_str_input($this->input->post('id'), "numeric");
        $merchant_id   = sanitize_str_input($this->input->post('merchant_id'), "numeric");
        $title         = sanitize_str_input($this->input->post('title'));
        $price         = sanitize_str_input($this->input->post('price'), "numeric");
        $periode_start = sanitize_str_input($this->input->post('periode_start'));
        $periode_end   = sanitize_str_input($this->input->post('periode_end'));
        $stok          = sanitize_str_input($this->input->post('stok'));
        $summary       = $this->input->post('summary');
        $how_to        = $this->input->post('how_to');
        $terms         = $this->input->post('terms');
        $is_show       = sanitize_str_input($this->input->post('is_show'));
        $data_image    = $this->input->post('data-image');
        // pr($this->input->post());exit;
        $this->_set_rule_validation($id);

        if ($this->form_validation->run($this) == FALSE) {
            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            //load library uploader
            $this->load->library("Uploader");

            $image_url = $this->upload_image(false, "upload/voucher", "image-url", $data_image, 640, 256, $id);

            $this->db->trans_begin();
            //prepare insert
            $arrayToDB = array(
                "merchant_id"   => $merchant_id,
                "title"         => $title,
                "price"         => $price,
                "periode_start" => $periode_start,
                "periode_end"   => $periode_end,
                "stok"          => $stok,
                "summary"       => $summary,
                "how_to"        => $how_to,
                "terms"         => $terms,
                "is_show"       => $is_show,
            );

            if (!empty($image_url)) {
                $arrayToDB['image_url'] = $image_url;
            }

            //insert or update ?
            if($id == "") {
               
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert($arrayToDB, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Voucher has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/vouchers/voucher/list-voucher";
                }
            } else {

                //get current data
                $curr_dat = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array"  => true
                ))['datas'];

                if (!empty($image_url) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                    unlink( FCPATH . $curr_data['image_url'] );
                }

                //update
                //conditions for update
                $conditions = array("id" => $id);

                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update($arrayToDB, $conditions, array('is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Voucher has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/vouchers/voucher/list-voucher";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * delete voucher.
     */
    public function delete() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        //check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (!$data) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // deactivate the user
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(array("status" => 0),$condition, array('is_version' => true));

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Voucher has been Deleted.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}

/* End of file Voucher_manager.php */
/* Location: ./application/modules/voucher/controllers/Voucher_manager.php */