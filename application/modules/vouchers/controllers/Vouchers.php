<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Vouchers extends Basepublic_Controller {

    private $_view_folder = "vouchers/front/";

    function __construct() {
        parent::__construct();

    }

	public function index() {
        //get header page
		redirect(base_url());

        exit;
        $page = get_page_detail("vouchers");

        $limit = 3;
        $start = 0;
        $voc_data = $this->_dm->set_model("mst_voucher", "vo", "id")->get_all_data(array(
            "order_by" => array("created_date" => "desc"),
            "limit" => $limit,
            "start" => $start,
            "conditions" => array(
                "is_show" => SHOW,
                "periode_end >=" => date("Y-m-d")
            ),
            "status" => STATUS_ACTIVE,
            "count_all_first" => true,
        ));
        $voucher = $voc_data['datas'];
        $total = $voc_data['total'];
        $total_page = ceil($total/$limit);

        $header = array(
            "header"       => $page,
            "voucher"      => $voucher,
            "total_page"   => $total_page,
        );

        $footer = array(
            "script" => array(
                "/js/front/voucher.js"
            ),
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . "index");
        $this->load->view(FRONT_FOOTER, $footer);
	}

	public function detail($id) {
		//get header page
		$page = get_page_detail('vouchers');

        // Get data vouchers
        $data["vouchers"] = $this->_dm->set_model("mst_voucher")->get_all_data(array(
            "conditions" => array(
                "id" => $id,
                "periode_end >=" => date("Y-m-d"),
                "is_show" => 1, 
                "status"  => 1
            ),
            "row_array" => TRUE
        ))["datas"];

        if (empty($data["vouchers"])) {
            show_404();
        } 
        

        $header = array(
            'header' => $page,
        );

        $footer = array(
            "script" => array(
                "/js/front/voucher-detail.js",
                "/js/bootstrap.min.js",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'detail', $data);
        $this->load->view(FRONT_FOOTER, $footer);
	}

	public function loadmore () {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $page = sanitize_str_input($this->input->post("page"), "numeric");
        $limit = sanitize_str_input($this->input->post("limit"), "numeric");

        if (empty($page)) $page = 1;
        $start = ($limit * ($page - 1));

        $voc_data = $this->_dm->set_model("mst_voucher", "vo", "id")->get_all_data(array(
            "order_by" => array("created_date" => "desc"),
            "limit" => $limit,
            "start" => $start,
            "conditions" => array(
                "is_show" => SHOW,
                "periode_end >=" => date("Y-m-d")
            ),
            "status" => STATUS_ACTIVE,
            "count_all_first" => true,
        ));

        $models = $voc_data['datas'];
        
        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "result"        => "OK",
            "datas"         => $models,
            "total_page"    => ceil($voc_data['total']/$limit),
        ));
        exit;
    }

}
