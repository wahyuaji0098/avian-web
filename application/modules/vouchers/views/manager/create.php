<?php
    $id                 = isset($item["id"]) ? $item["id"] : "";
    $merchant_id        = isset($item["merchant_id"]) ? $item["merchant_id"] : "";
    $title              = isset($item["title"]) ? $item["title"] : "";
    $price              = isset($item['price']) ? $item['price'] : "";
    $image_url          = isset($item["image_url"]) ? $item["image_url"] : "";
    $periode_start      = isset($item["periode_start"]) ? date('Y-m-d', strtotime($item["periode_start"])) : "";
    $periode_end        = isset($item["periode_end"]) ? date('Y-m-d', strtotime($item["periode_end"])) : "";
    $stok               = isset($item["stok"]) ? $item['stok'] : "";
    $summary            = isset($item['summary']) ? $item['summary'] : "";
    $how_to             = isset($item['how_to']) ? $item['how_to'] : "";
    $terms              = isset($item['terms']) ? $item['terms'] : "";
    $is_show            = isset($item["is_show"]) ? $item["is_show"] : "";
    $created_date       = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date       = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg    = ($id == 0) ? "Create" : " Update";
    $title_msg  = ($id == 0) ? "Create" : " Update";
    $data_edit  = ($id == 0) ? 0 : 1;
    // pr($item);exit;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Voucher</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/vouchers/voucher/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label"> Merchant <sup class="color-red">*</sup></label>
                                        <label class="select">
                                            <?php echo select_merchant('merchant_id', $merchant_id , 'id="merchant_id" class="form-control"'); ?>
                                            <i></i>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Title</label>
                                        <label class="input">
                                                <input name="title" type="text"  class="form-control" placeholder="Title" value="<?= $title; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Price </label>
                                        <label class="input">
                                                <input name="price" id="name" type="text"  class="form-control" placeholder="Price" value="<?= $price; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image(640x256)</label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image">
                                                <?php if($image_url): ?>
                                                <img src="<?= $image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Periode Start </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input name="periode_start" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Periode Start" value="<?= $periode_start; ?>" />
                                            </label>
                                        </section>

                                        <section class="col col-6">
                                            <label class="label">Periode End </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-calendar"></i>
                                                <input name="periode_end" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Periode End" value="<?= $periode_end; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Stok <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <input name="stok" id="name" type="text"  class="form-control" placeholder="Stok" value="<?= $stok; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Summary </label>
                                        <label class="textarea">
                                            <textarea name="summary" id="full_content" class="form-control tinymce" rows="10"><?= $summary ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">How To </label>
                                        <label class="textarea">
                                            <textarea name="how_to" id="full_content" class="form-control tinymce" rows="10"><?= $how_to ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Terms </label>
                                        <label class="textarea">
                                            <textarea name="terms" id="full_content" class="form-control tinymce" rows="10"><?= $terms ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>

                                <fieldset>
                                    <?php if ($created_date != ""): ?>
                                    <section>
                                        <label class="label">Created date </label>
                                        <label class="input"> 
                                            <?php echo $created_date; ?>
                                        </label>
                                    </section> 
                                    <?php endif; ?>
                                    
                                    <?php if ($updated_date != ""): ?>
                                    <section>
                                        <label class="label">Update date </label>
                                        <label class="input"> 
                                            <?php echo $updated_date; ?>
                                        </label>
                                    </section> 
                                    <?php endif; ?>
                                </fieldset>
                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>
                <!-- end widget div -->
                </article>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
