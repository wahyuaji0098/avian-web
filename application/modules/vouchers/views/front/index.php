<script>
    var total_page_article = <?= $total_page ?>;
</script>               
<div class="container container_voucher targetcontainer_voucher">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>DAFTAR VOUCHER</span>
            </div>
        </div>
    </div>
    <div class="section section-smargin">
        <a href="/download" class="topmessage">
            <img class="topmessagebg" src="/img/ui/appbanner.jpg" />
            <div class="topmessagecontain">
                <div class="topmessagetext">Gunakan Avian Brand Mobile App Ver. 2 untuk mendapatkan voucher-voucher menarik</div>
                <div class="btn"><span></span>Download App</div>
            </div>
        </a>
    </div>
    <div class="thirds home" id="voucher-container">
        <div class="section gridds">
            <div class="section_content griddscontent col12">
                <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                <?php if(count($voucher) > 0): foreach($voucher as $vouchers): ?>
                <div class="vthird vthirdpictxt">
                    <a href="/vouchers/detail/<?= $vouchers["id"] ?>" class="griddsitem griddsitemvoucher gridcol width-4 height-3.5">
                        <div class="voucherbox">
                            <div class="voucherpic"><img src="<?= $vouchers["image_url"] ?>" /></div>
                            <div class="vouchername"><?= ucwords($vouchers["title"]) ?></div>
                            <div class="voucherpoin"><?= number_format($vouchers["price"]) ?><span>poin</span></div>
                            <div class="voucherdate"><span>Tersedia sampai:</span><?= date("d M Y", strtotime($vouchers["periode_end"])); ?></div>
                        </div>
                    </a>
                </div>
                <?php endforeach; endif; ?>
                <div class="lead"></div>
            </div>
        </div>
        <div class="section section-thirds">
            <div class="section_content">
                <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
            </div>
        </div>
    </div>
</div>