<div class="container container_voucherdetail">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/vouchers">DAFTAR VOUCHER</a>
                <span>DETAIL VOUCHER</span>
            </div>
        </div>
    </div>
    <div class="section section-smargin">
        <div class="voucherpic"><img src="<?= $vouchers["image_url"] ?>" /></div>
        <div class="row row_padded_mobile">
            <div class="col col-md-7 col-sm-12 vouchername"><?= ucwords($vouchers["title"]) ?></div>
            <div class="col col-md-5 col-sm-12 voucherinfo">
                <div class="row">
                    <div class="col-sm-7"><div class="voucherpoin"><?= number_format($vouchers["price"]) ?><span>poin</span></div></div>
                    <div class="col-sm-5"><div class="voucherdate"><span>Tersedia sampai:</span><?= date("d M Y", strtotime($vouchers["periode_end"])); ?></div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <ul class="linetabs nav nav-tabs">
            <li class="linetab active"><a data-toggle="tab" href="#ikhtisar">IKHTISAR</a></li>
            <li class="linetab"><a data-toggle="tab" href="#petunjuk">PETUNJUK PENGGUNAAN</a></li>
            <li class="linetab"><a data-toggle="tab" href="#syarat">SYARAT &amp; KETENTUAN</a></li>
        </ul>
        <div class="linetabscontent tab-content">
          <div id="ikhtisar" class="linetabcontent tab-pane fade in active">
            <?php echo $vouchers["summary"] ?>
          </div>
          <div id="petunjuk" class="linetabcontent tab-pane fade">
            <?php echo $vouchers["how_to"] ?>
          </div>
          <div id="syarat" class="linetabcontent tab-pane fade">
            <?php echo $vouchers["terms"] ?>
          </div>
        </div>
    </div>
</div>
