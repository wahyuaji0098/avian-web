<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Controller.
 * For Dashboard admin
 */
class Dashboard_manager extends Baseadmin_Controller  {
    private $_title = "Dashboard";
    private $_title_page = '<i class="fa-fw fa fa-home"></i> Dashboard ';
    private $_active_page = "dashboard";
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Dashboard</a></li>";
    private $_back = "";
    private $_js_path = "/js/pages/dashboard/manager/";
    private $_view_folder = "dashboard/manager/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> My Dashboard</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb,
        );

        $data = [];

        //get layout header footer snippet
        $data['snippet'] = $this->_dm->set_model("mst_layout", "ml", "id")->get_all_data(array(
            "row_array" => true
        ))['datas'];

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                $this->_js_path . "index.js"
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER,$header);
        $this->load->view($this->_view_folder . 'index',$data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation() {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("header_snippet", "Header", "trim");
        $this->form_validation->set_rules("footer_snippet", "Footer", "trim");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_snippet() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => TRUE
        );

        $header_snippet = $this->input->post('header_snippet');
        $footer_snippet = $this->input->post('footer_snippet');

        $arrayToDB = array(
            "header"    => $header_snippet,
            "footer"    => $footer_snippet,
        );

        // server side validation. not implement yet.
        $this->_set_rule_validation();

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            //check if mst_layout ada isinya tidak
            $exist_snippet = $this->_dm->set_model("mst_layout", "ml", "id")->get_all_data(array(
                "row_array" => true
            ))['datas'];

            if (empty($exist_snippet)) {
                //insert
                $result = $this->_dm->set_model("mst_layout", "ml", "id")->insert(
                    $arrayToDB,
                    $extra_param
                );
            }
            else {
                $result = $this->_dm->set_model("mst_layout", "ml", "id")->update(
                    $arrayToDB,
                    array("id" => $exist_snippet['id']),
                    $extra_param
                );
            }



            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                $message['notif_message'] = "Snippet has been updated.";
                // redirected.
                $message['redirect_to'] = "/manager";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
