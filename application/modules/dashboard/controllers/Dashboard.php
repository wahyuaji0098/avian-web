<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboard Controller.
 * For Dashboard front
 */
class Dashboard extends MX_Controller  {
    private $_title = "Dashboard";
    private $_title_page = '<i class="fa-fw fa fa-home"></i> Dashboard ';
    private $_active_page = "dashboard";
    private $_breadcrumb = '<li><a href="' . FRONT_HOME . '">Dashboard</a></li>';
    private $_back = "";
    private $_js = "/js/pages/dashboard/front/";
    private $_view_folder = "dashboard/front/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        redirect(base_url());

        // $header = array(
        //     "title"         => $this->_title,
        //     "title_page"    => $this->_title_page . '<span> My Dashboard</span>',
        //     "active_page"   => $this->_active_page,
        //     "breadcrumb"    => $this->_breadcrumb,
        // );

        // $footer = array(
        //     "script" => array(
        //         $this->_js . "dashboard.js",
        //     )
        // );

        // //load the views.
        // $this->load->view(FRONT_HEADER,$header);
        // $this->load->view($this->_view_folder . 'index');
        // $this->load->view(FRONT_FOOTER, $footer);
    }

}
