<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
    </div>

    <!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget jarviswidget-color-blueLight" id="wid-id-001"
					data-widget-editbutton="false"
					data-widget-deletebutton="false"
					data-widget-attstyle="jarviswidget-color-blueLight">

					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2>ADD SNIPPET HEADER FOOTER</h2>
					</header>
					<!-- widget div-->
					<div>
						<!-- widget content -->
						<div class="widget-body no-padding">
                            <form class="smart-form" id="create-form" action="/manager/dashboard/process-snippet" method="POST">
                                <header>HEADER FOOTER</header>
                                <fieldset>

                                    <section>
                                        <label class="label">HEADER</label>
                                        <label class="textarea">
                                            <textarea name="header_snippet" id="header_snippet" class="form-control" rows="15"><?php echo $snippet['header']; ?></textarea>
                                        </label>
                                    </section>
                                    
                                    <section>
                                        <label class="label">FOOTER</label>
                                        <label class="textarea">
                                            <textarea name="footer_snippet" id="footer_snippet" class="form-control" rows="15"><?php echo $snippet['footer']; ?></textarea>
                                        </label>
                                    </section>

                                    <button type="button" class="btn btn-lg btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    					<i class="fa fa-floppy-o fa-lg"></i> Submit
                    				</button>

                                </fieldset>
                            </form>
						</div> <!-- end widget content -->
					</div> <!-- end widget div -->
				</div> <!-- end widget -->
			</article> <!-- WIDGET END -->
		</div> <!-- end row -->
	</section> <!-- end widget grid -->

</div>
