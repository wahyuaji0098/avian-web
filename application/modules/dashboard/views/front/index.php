<div class="aw_gmap">
    <iframe class="gmap_iframe" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d126748.56347863862!2d107.57311668399257!3d-6.903444341628322!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1508070718361" frameborder="0" style="border:0"></iframe>
</div>
<div class="dashboard_uis" id="dui">
    <div class="dashboard_header">
        <div class="dh_part dh_left">
            <a href="/login" class="dh_btn dh_btn_exit">KELUAR</a>
        </div>
        <div class="dh_part dh_center">
            <div class="dh_logo"></div>
        </div>
        <div class="dh_part dh_right">
            <a class="dh_lang">ENGLISH</a>
            <a class="dh_btn dh_btn_profile"></a>
        </div>
    </div>
    <div class="dashboard_topmenu" id="dtm">
        <div class="dtm_contain">
            <ul class="dtm_tabs nav nav-tabs">
                <li class="dtm_tab active"><a class="toggler_alt" data-class-toggle="displayed" data-id-target="#righttab_a" data-class-target=".toggler_alt_target" data-toggle="tab" href="#tab_a">Cabang, pelanggan &amp; pabrik</a></li>
                <li class="dtm_tab"><a class="toggler_alt" data-class-toggle="displayed" data-id-target="#righttab_b" data-class-target=".toggler_alt_target" data-toggle="tab" href="#tab_b">Pelacakan</a></li>
            </ul>
            <div class="dtm_tabcontents tab-content">
              <div id="tab_a" class="dtm_tabcontent tab-pane fade in active">
                  <div class="dtm_leftcolumn">
                      <div class="aw_input">
                          <label for="mode" class="aw_input_name">Pilih mode</label>
                          <select id="mode" class="customselect form-control">
                              <option>Cabang</option>
                              <option>Pelanggan</option>
                              <option>Pabrik</option>
                          </select>
                      </div>
                      <div class="btn_aw btn_aw-grey btn_aw-reset">
                          Reset
                      </div>
                  </div>
                  <div class="dtm_columns">
                      <div class="row row_tight">
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="cabang" class="aw_input_name">Pilih cabang</label>
                                  <select id="cabang" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Cabang 1</option>
                                      <option>Cabang 2</option>
                                      <option>Cabang 3</option>
                                      <option>Cabang 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="area" class="aw_input_name">Pilih area</label>
                                  <select id="area" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Area 1</option>
                                      <option>Area 2</option>
                                      <option>Area 3</option>
                                      <option>Area 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="kategori" class="aw_input_name">Pilih kategori</label>
                                  <select id="kategori" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Kategori 1</option>
                                      <option>Kategori 2</option>
                                      <option>Kategori 3</option>
                                      <option>Kategori 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-4">
                              <div class="aw_input">
                                  <label for="mesin" class="aw_input_name">Mesin campur warna</label>
                                  <select id="mesin" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Mesin 1</option>
                                      <option>Mesin 2</option>
                                      <option>Mesin 3</option>
                                      <option>Mesin 4</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="row row_tight">
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="kabupaten" class="aw_input_name">Pilih kabupaten</label>
                                  <select id="kabupaten" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Kabupaten 1</option>
                                      <option>Kabupaten 2</option>
                                      <option>Kabupaten 3</option>
                                      <option>Kabupaten 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="kecamatan" class="aw_input_name">Pilih kecamatan</label>
                                  <select id="kecamatan" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Kecamatan 1</option>
                                      <option>Kecamatan 2</option>
                                      <option>Kecamatan 3</option>
                                      <option>Kecamatan 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-2">
                              <div class="aw_input">
                                  <label for="produk" class="aw_input_name">Kategori produk</label>
                                  <select id="produk" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Produk 1</option>
                                      <option>Produk 2</option>
                                      <option>Produk 3</option>
                                      <option>Produk 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-4">
                              <div class="aw_input">
                                  <label for="omset" class="aw_input_name">Jangkauan omset</label>
                                  <div class="inputgroup">
                                      <input class="custominput" type="text" />
                                      <span>~</span>
                                      <input class="custominput" type="text" />
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div id="tab_b" class="dtm_tabcontent tab-pane fade">
                  <div class="dtm_leftcolumn">
                      <div class="aw_input">
                          <label for="mode" class="aw_input_name">Pilih mode</label>
                          <select id="mode" class="customselect form-control">
                              <option>Salesman</option>
                              <option>Driver</option>
                          </select>
                      </div>
                      <div class="btn_aw btn_aw-grey btn_aw-reset">
                          Reset
                      </div>
                  </div>
                  <div class="dtm_columns">
                      <div class="row row_tight">
                          <div class="tcol tcol-sm-5 tcol-md-5">
                              <div class="aw_input">
                                  <label for="cabang" class="aw_input_name">Pilih cabang</label>
                                  <select id="cabang" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Cabang 1</option>
                                      <option>Cabang 2</option>
                                      <option>Cabang 3</option>
                                      <option>Cabang 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-5">
                              <div class="aw_input">
                                  <label for="area" class="aw_input_name">Pilih area</label>
                                  <select id="area" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Area 1</option>
                                      <option>Area 2</option>
                                      <option>Area 3</option>
                                      <option>Area 4</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="row row_tight">
                          <div class="tcol tcol-sm-5 tcol-md-5">
                              <div class="aw_input">
                                  <label for="kabupaten" class="aw_input_name">Pilih kabupaten</label>
                                  <select id="kabupaten" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Kabupaten 1</option>
                                      <option>Kabupaten 2</option>
                                      <option>Kabupaten 3</option>
                                      <option>Kabupaten 4</option>
                                  </select>
                              </div>
                          </div>
                          <div class="tcol tcol-sm-5 tcol-md-5">
                              <div class="aw_input">
                                  <label for="kecamatan" class="aw_input_name">Pilih kecamatan</label>
                                  <select id="kecamatan" class="customselect form-control">
                                      <option>Semua</option>
                                      <option>Kecamatan 1</option>
                                      <option>Kecamatan 2</option>
                                      <option>Kecamatan 3</option>
                                      <option>Kecamatan 4</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <div class="dtm_tackon">
            <div class="dtm_closepin toggler" data-class-toggle="tclosed" data-id-target="#dui"></div>
        </div>
    </div>
    <div class="dashboard_rightmenu" id="drm">
        <div class="drm_tackon">
            <div class="drm_closepin toggler" data-class-toggle="rclosed" data-id-target="#dui"></div>
        </div>
        <div class="drm_contain">
            <div class="drm_tabcontents tab-content">
                <div id="righttab_a" class="drm_tabcontent toggler_alt_target tab-pane fade in active displayed">
                    <div class="aw_acc">
                        <div class="aw_acc_head">
                            <a data-toggle="collapse" href="#collapse_a1">Cabang</a>
                        </div>
                        <div id="collapse_a1" class="aw_acc_body collapse in">
                            <div class="aw_acc_content">
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a1_1">Jumlah cabang <span>70</span></a>
                                    </div>
                                    <div id="collapse_a1_1" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>Banyumanik</li>
                                                <li>Candisari</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a1_2">Kategori produk <span>40,000</span></a>
                                    </div>
                                    <div id="collapse_a1_2" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>C04_Avitex Emulsion <span>2,377</span></li>
                                                <li>C05_Avem Tinting<span>1,343</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="aw_input">
                                  <label for="area" class="aw_input_name">Pembagian berdasarkan:</label>
                                  <select id="area" class="customselect form-control">
                                      <option>Status cabang</option>
                                      <option>Tempat parkir</option>
                                      <option>Rating</option>
                                  </select>
                                </div>
                                <div class="drm_checkboxes">
                                    <div class="aw_checkbox aw_checkbox_green">
                                        <input name="checkbox_1" id="checkbox_1a" value="opened" type="checkbox" />
                                        <label for="checkbox_1a">
                                            Sudah buka
                                            <span>32,142</span>
                                        </label>
                                    </div>
                                    <div class="aw_checkbox aw_checkbox_blue">
                                        <input name="checkbox_1" id="checkbox_1b" value="not_opened" type="checkbox" />
                                        <label for="checkbox_1b">
                                            Akan datang
                                            <span>12,552</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aw_acc">
                        <div class="aw_acc_head">
                            <a class="collapsed" data-toggle="collapse" href="#collapse_a2">Pelanggan</a>
                        </div>
                        <div id="collapse_a2" class="aw_acc_body collapse">
                            <div class="aw_acc_content">
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a2_1">Jumlah cabang <span>70</span></a>
                                    </div>
                                    <div id="collapse_a2_1" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>Banyumanik</li>
                                                <li>Candisari</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a2_2">Kategori produk <span>40,000</span></a>
                                    </div>
                                    <div id="collapse_a2_2" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>C04_Avitex Emulsion <span>2,377</span></li>
                                                <li>C05_Avem Tinting<span>1,343</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="aw_acc">
                        <div class="aw_acc_head">
                            <a class="collapsed" data-toggle="collapse" href="#collapse_a3">Pabrik</a>
                        </div>
                        <div id="collapse_a3" class="aw_acc_body collapse">
                            <div class="aw_acc_content">
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a3_1">Jumlah cabang <span>70</span></a>
                                    </div>
                                    <div id="collapse_a3_1" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>Banyumanik</li>
                                                <li>Candisari</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_a3_2">Kategori produk <span>40,000</span></a>
                                    </div>
                                    <div id="collapse_a3_2" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <ul class="plainlist">
                                                <li>C04_Avitex Emulsion <span>2,377</span></li>
                                                <li>C05_Avem Tinting<span>1,343</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="righttab_b" class="drm_tabcontent toggler_alt_target tab-pane">
                    <div class="aw_acc">
                        <div class="aw_acc_head">
                            <a data-toggle="collapse" href="#collapse_b1">Statistik</a>
                        </div>
                        <div id="collapse_b1" class="aw_acc_body collapse in">
                            <div class="aw_acc_content">
                                <div class="aw_acc_sml">
                                    <div class="aw_acc_sml_head">
                                        <a data-toggle="collapse" href="#collapse_b1_1">Jumlah user <span>15,242</span></a>
                                    </div>
                                    <div id="collapse_b1_1" class="aw_acc_sml_body collapse in">
                                        <div class="aw_acc_sml_content">
                                            <div class="drm_checkboxes">
                                                <div class="aw_checkbox aw_checkbox_purple">
                                                    <input name="checkbox_b1" id="checkbox_b1a" value="Salesman" type="checkbox" />
                                                    <label for="checkbox_b1a">
                                                        Salesman
                                                        <span>13,241</span>
                                                    </label>
                                                </div>
                                                <div class="aw_checkbox aw_checkbox_darkblue">
                                                    <input name="checkbox_b1" id="checkbox_b1b" value="Supir" type="checkbox" />
                                                    <label for="checkbox_b1b">
                                                        Supir
                                                        <span>532</span>
                                                    </label>
                                                </div>
                                                <div class="aw_checkbox aw_checkbox_green">
                                                    <input name="checkbox_b1" id="checkbox_b1c" value="Teknisi" type="checkbox" />
                                                    <label for="checkbox_b1c">
                                                        Teknisi
                                                        <span>310</span>
                                                    </label>
                                                </div>
                                                <div class="aw_checkbox aw_checkbox_red">
                                                    <input name="checkbox_b1" id="checkbox_b1d" value="SPV" type="checkbox" />
                                                    <label for="checkbox_b1d">
                                                        SPV
                                                        <span>121</span>
                                                    </label>
                                                </div>
                                                <div class="aw_checkbox aw_checkbox_orange">
                                                    <input name="checkbox_b1" id="checkbox_b1e" value="BM" type="checkbox" />
                                                    <label for="checkbox_b1e">
                                                        BM
                                                        <span>78</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
