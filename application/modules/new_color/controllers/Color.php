<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Index Controller.
 */
class Color extends Basepublic_Controller  {

    private $_view_folder = "new_color/front/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }


    public function index() {
        //get header page
        $page = get_page_detail('colours');

        $condition  =   [
            'status'  => 1,
            'is_show' => 1,
            // 'debug' => true
        ];

        $colour      = $this->_dm->set_model("dtb_color", "c" , "id")->get_all_data(['conditions'=>$condition])['datas'];

        $header = array(
            'header' => $page,
            'color'  => $colour,
            'scroll_hide'   => true,
        );

        //load the views.
        $this->load->view(FRONT_HEADER_2, $header);
        $this->load->view($this->_view_folder . 'index' );
        $this->load->view(FRONT_FOOTER_2);
    }

    public function detail() {
    
        $page = get_page_detail('colours');
        $detail      = $this->_dm->set_model("dtb_color", "c" , "id")->get_all_data(
            [
               "conditions" => ['id' => $_GET['color_id'] ],
               'row_array'  => true
            ]
        )['datas'];
        $palette     = $this->_dm->set_model("mst_palette_color", "d" , "id")->get_all_data( 
            [
                "conditions" => [ 'color_id' => $_GET['color_id'] ], 
                'row_array'  => false,
            ]
        )['datas'];

        $return_data = [];
        foreach ($palette as $key) {
            $product     = $this->_dm->set_model("mst_palette_product", "e" , "id")->get_all_data( 
                [
                    "conditions" => ['palette_id' => $key['palette_id'] , 'is_show' => SHOW],
                    "left_joined" => [
                        "dtb_product pr" => array("e.product_id" => "pr.id"),
                    ],
                    "order_by" => array("e.id" => "asc"),
                ]
            )['datas'];

            $i = 0;
            foreach ($product as $dey) {
                $return_data[$i]['product_id']   = $dey['id'];
                $return_data[$i]['product_name'] = $dey['name'];
                $return_data[$i]['pretty_url']   = $dey['pretty_url'];
                $return_data[$i]['image_url']    = $dey['image_url'];
                $return_data[$i]['desc']         = $dey['description'];
                $i++;
            }
        }
 

        $header = array(
            'header' => $page,
            'detail' => $detail,
            'data'   => $return_data,
        );

        //load the views.
        $this->load->view(FRONT_HEADER_2, $header);
        $this->load->view($this->_view_folder . 'detail' );
        $this->load->view(FRONT_FOOTER_2);
    }

    public function search(){

        // print_r($_GET['filter']);

        $page = get_page_detail('colours');
        

        if (isset($_GET['keyword'])) {
           $search    = $_GET['keyword'];
           $filter_or = array("name" => $search,"code" => $search);
           // exit($search);
        } else {
           $filter_or = '';
        }


        
        $conditions  =   [
            'd.status'  => 1,
            'd.is_show' => 1,
        ];

        if (isset($_GET['type_colour'])) {
            $conditions['d.jenis_warna'] = $_GET['type_colour'] ; 
        }  

        if (isset($_GET['filter'])) {
            
            $count  = count($_GET['filter']);
            for ($i=0; $i < $count  ; $i++) { 
               $conditions_same_name['data'][$i] =  $_GET['filter'][$i];
            }
            $conditions_same_name['name'] =  'c.name';

        }  else {
            $conditions_same_name = "";
        }


        $palette      = $this->_dm->set_model("mst_palette_product", "a" , "id")->get_all_data([
            // 'conditions' => $condition,
            "select" => ['a.*','b.name as product_name','b.product_category_id','c.id as category_id','c.name' , 'd.name as palette_name' , 'd.jenis_warna'],
            "conditions_same_name"=> $conditions_same_name,
            "joined" => array(
                "dtb_product b"    => array("a.product_id" => "b.id"),
                "dtb_product_category c"  => array("b.product_category_id" => "c.id"),
                "dtb_pallete d"  => array("a.palette_id" => "d.id")
            ),
            "conditions" => $conditions,
            "group_by"=> 'a.palette_id',
            // "order_by" => array('id' =>'asc'),
            // 'debug' => true
        ])['datas'];

        if ($palette) {
            // menyeleksi , per palette ada berapa color 
            $num = 0;
            foreach ($palette as $k) {
                $colour      = $this->_dm->set_model("mst_palette_color", "a" , "id")->get_all_data([
                    'conditions' => ['a.palette_id' => $k['palette_id'] ],
                    "select" => ['a.color_id','a.palette_id','b.name as color_name','b.code','b.red','b.green','b.blue'],
                    "joined" => array(
                        "dtb_color b"    => array("a.color_id" => "b.id"),
                    ),
                    'order_by' => ['a.color_id' => 'desc'],
                    'filter_or' => $filter_or,
                ])['datas'];

                foreach ($colour as $x) {
                    $datas[$num]['color'] = $x['color_name'];
                    $datas[$num]['code'] = $x['code'];
                    $datas[$num]['r'] = $x['red'];
                    $datas[$num]['g'] = $x['green'];
                    $datas[$num]['b'] = $x['blue'];
                    $datas[$num]['color_id'] = $x['color_id'];
                    $num++;
                }
            }
        }


        $header = array(
            'num'      => $num,
            'header' => $page,
            'color'  => $datas,
            'scroll_hide'   => true,
        );

        //load the views.
        $this->load->view(FRONT_HEADER_2, $header);
        $this->load->view($this->_view_folder . 'color_filter_result' );
        $this->load->view(FRONT_FOOTER_2);


    }
	
}
