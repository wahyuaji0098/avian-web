<div id="content">

  <div class="h-col fw bg-light-green warna">

    <div class="mr">

      <div class="row">

        <div class="col-md-10 col-xs-6">

          <h2 class="font-sofia-bold font-md font-green">

            Warna Sesuai Filter

          </h2>

        </div>

        <div class="col-md-2 col-xs-6 text-right">

          <div class="dropdown-warna">

            <button class="btn btn-outline-success"><span class="margin font-sofia-bold font-sofia-md font-green">+ Filter</span></button>

            <div class="dropdown-content-warna">

              <form action="<?= base_url() ?>new_color/Color/search?" method="get">

              <ul class="text-left">

                <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Jenis Cat</span></a></li>

                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input name="filter[]" value="Cat Tembok" type="checkbox" 

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Cat Tembok') { echo "checked"; } ?> >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Tembok</p></div>

                  </div>

                </li>



                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" name="filter[]" value="Cat Pelapis Anti Bocor" 

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Cat Pelapis Anti Bocor') { echo "checked"; } ?>  >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Pelapis Anti Bocor</p></div>

                  </div>

                </li>



                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" value="Cat Kayu dan Besi" name="filter[]"

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Cat Kayu dan Besi') { echo "checked"; } ?> >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Kayu dan Besi</p></div>

                  </div>

                </li>



                <!-- <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Beton</p></div>

                  </div>

                </li> -->



                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" value="Cat Genteng / Seng" name="filter[]" 

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Cat Genteng / Seng') { echo "checked"; } ?> >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Genteng dan Seng</p></div>

                  </div>

                </li>



                 <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" value="Cat Duco & Spray" name="filter[]" 

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Cat Duco & Spray') { echo "checked"; } ?>  >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Duco dan Spary</p></div>

                  </div>

                </li>



                 <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="checkbox" value="Lain - Lain" name="filter[]" 

                        <?php if(isset($_GET['filter']) && $_GET['filter'] == 'Lain - Lain') { echo "checked"; } ?> >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Lain-lain</p></div>

                  </div>

                </li>



                <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Ketersediaan</span></a></li>



                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input t type="radio" name="type_colour" value="ready_mix" >

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Ready Mix</p></div>

                  </div>

                </li>



                <li>

                  <div class="row rowcheck">

                    <div class="col-md-2 col-xs-2 padcheck">

                      <label class="content-check">

                        <input type="radio" type="radio" name="type_colour" value="tinting">

                        <span class="checkmark"></span>

                      </label>

                    </div>

                    <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Tinting</p></div>

                  </div>

                </li>

                

                <li  class="button-drop">

                  <div class="row">

                    <div class="col-md-7 col-xs-6">

                      <button  type='submit' class="btn btn-outline-success-nav btn-block"><span class="font-sofia-bold font-sofia-md font-green">Terapkan</span></button>

                    </div>

                    <div class="col-md-5 col-xs-6">

                      <button type="button" class="btn btn-outline-success-nav btn-block"><span class="font-sofia-bold font-sofia-md font-green">Hapus</span></button>

                    </div>

                  </div>

                </li>

                </form>

              </ul>

            </div>

          </div>

        </div>

      </div>

      <div class="boxround">

         <div class="col-md-6 col-xs-12">
            <p class="font-sofia-bold font-md font-green">
              Warna Populer <span class="font-sofia-light font-sm font-green">Lihat Semua(<?= count($color) ?>)</span>
            </p>
         </div>
        <div class="col-md-12 text-right" style="padding: 0px 50px;">
          <input type="text" class="input-form font-sm" id="searchbar" name="cari" placeholder="Tekan enter untuk search" >
        </div>

        

        <div class="row box-color nol-padmarg">

            

         <?php //echo "<pre>"; print_r($color); exit(); ?>



      <?php if (count($color) > 0){ ?>

          <?php for($i = 0 ; $i < count($color) ; $i++){ ?>

             <a href="<?= base_url() ?>color/detail?color_id=<?= $color[$i]['color_id'] ?>">

              <div class="col-md-2 col-xs-4 box " style="background-color: rgb(<?= $color[$i]['r'] ?>,<?=  $color[$i]['g'] ?> ,<?=  $color[$i]['b'] ?>) "> <?=  $color[$i]['color'] ?><br><?= strtoupper($color[$i]['code']) ?>

             </div>

           </a>

         <?php } ?>

       <?php }else{ ?>

         

            <center>Maaf belum ada Warna </center>

          <?php } ?>

        </div>

        

      </div>

    </div>

  </div>

</div>
<script type="text/javascript">
  $(document).on('keypress',function(e) {
      if(e.which == 13) {

        $val = $("#searchbar").val();
        window.location.href="/new_color/Color/search?keyword="+$val;

          // alert('You pressed enter!');
      }
  });
</script>