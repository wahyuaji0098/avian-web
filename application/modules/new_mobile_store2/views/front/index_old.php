<style type="text/css">
  .map-icon-label .map-icon {
    font-size: 24px;
    color: #FFFFFF;
    line-height: 48px;
    text-align: center;
    white-space: nowrap;
  }
</style>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div id="content" class="mr horizontal-wrapper pemasaran" data-aos="fade">
    <div class="kantor-map map-box not-hscroll">
      <div class="map not-hscroll" id="gmap_object"></div>
    </div>

    <div class="map-filter row bg-light-green" id="tokoFilter">
      <div class="col-md-12 col-xs-9">
        <h3 class="font-sofia-bold font-md  font-green">Cari Toko</h3>
      </div>
      <div class="col-xs-3 desktop-hide text-right">
        <img src="/avian_new/images/search.png" id="search-trigger">
      </div>
      <div class="col-md-8 col-xs-12 search-hideshow">
        <div class="form-group">
          <select class="input-form" id="search-by">
            <option <?= ($ss_search['search_by'] == "1") ? "selected = 'selected' " : "" ?> value="1">Berdasarkan Lokasi</option>
            <option <?= ($ss_search['search_by'] == "2") ? "selected = 'selected' " : "" ?> value="2">Berdasarkan Nama Toko</option>
            <option <?= ($ss_search['search_by'] == "3") ? "selected = 'selected' " : "" ?> value="3">Berdasarkan Nama Produk</option>
          </select>
          <input type="text" class="input-form" id="search-box" value="<?= ($ss_search['search']) ? $ss_search['search'] : "" ?>">
        </div>
      </div>
      <!-- <input class="btn" type="button" value="Hapus" id="clear-btn"/> -->
      <input type="hidden" id="f_lat" value="<?= ($ss_search['lat']) ? $ss_search['lat'] : 0 ?>" />
      <input type="hidden" id="f_long" value="<?= ($ss_search['long']) ? $ss_search['long'] : 0 ?>" />
      <input type="hidden" id="f_user_lat" value="<?= ($ss_search['lat_user']) ? $ss_search['lat_user'] : 0 ?>" />
      <input type="hidden" id="f_user_long" value="<?= ($ss_search['long_user']) ? $ss_search['long_user'] : 0 ?>" />
      <input type="hidden" id="f_search_by" value="<?= ($ss_search['search_by']) ? $ss_search['search_by'] : 1 ?>" />
      <input type="hidden" id="f_zoom" value="<?= ($ss_search['zoom']) ? $ss_search['zoom'] : 15 ?>" />
      <input type="hidden" id="f_cari_toko_terdekat" value="<?= ( isset($_GET['nearest_store'])) ? 1 : 0 ?>" />
      <div class="col-md-2 col-xs-12 search-hideshow">
        <button id="search-store" class="button button-rounded button-block btn-inline-green font-green" type="button">Cari</button>
      </div>
      <div class="col-md-2 col-xs-12 search-hideshow">
        <button class="button button-rounded button-block btn-inline-green font-green btn-mfilter" data-container="body" data-toggle="popover" data-placement="bottom" data-popover-content="#popoverContent">
          + Filter
        </button>

        <div style="display: none;">
          <form action="" method="get">
          <div id="popoverContent">
            <div class="row">
              <div class="col-sm-6 col-xs-12">
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input onchange="change_parent(this.id)" class="p_category_2" type="checkbox" name="parent_category[]" value="2" id="CatTembokPremium">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><h4 class="font-sofia-bold">Cat Tembok Premium</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="11" id="Sunguard">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Sunguard</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="13" id="Everglo" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Everglo</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="12" id="Supersilk">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Supersilk</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="1" id="NoOdor" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">No Odor</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="14" id="Aquamatt" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Aquamatt</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_2" onchange="change_input(this.id)" name="category[]" value="80" id="SupersilkAntiNoda" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Supersilk Anti Noda</p></div>
                    </div>
                  </li>

                </ul>
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input onchange="change_parent(this.id)" class="p_category_1" type="checkbox" name="parent_category[]" value="1" id="CatTembok">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><h4 class="font-sofia-bold">Cat Tembok </h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_1" onchange="change_input(this.id)" name="category[]" value="3" id="Avitex">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Avitex</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_1" onchange="change_input(this.id)" name="category[]" value="9" id="AriesGold" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Aries Gold</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_1" onchange="change_input(this.id)" name="category[]" value="10" id="Aries">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Aries</p></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="col-sm-6 col-xs-12">
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input onchange="change_parent(this.id)" class="p_category_3" type="checkbox" name="parent_category[]" value="3" id="CatKayudanBesi" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><h4 class="font-sofia-bold">Cat Kayu Besi</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_3" onchange="change_input(this.id)" name="category[]" value="52" id="AvianCatCepatKering" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Avian Cat  Kayu & Besi</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_3" onchange="change_input(this.id)" name="category[]" value="53" id="Yoko" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Yoko</p></div>
                    </div>
                  </li>
                  <li style="display: none;">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_3" onchange="change_input(this.id)" name="category[]" value="53" id="Yoko" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Yoko</p></div>
                    </div>
                  </li>
                  
          
                </ul>
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input onchange="change_parent(this.id)" class="p_category_4" type="checkbox" name="parent_category[]" value="4" id="CatPelapisAntiBocor" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><h4 class="font-sofia-bold">Cat Pelapis Anti Bocor</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" class="category_4" onchange="change_input(this.id)" name="category[]" value="5" id="NoDrop" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">No Drop</p></div>
                    </div>
                  </li>
                  <!-- <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 col-xs-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox"  onchange="change_input(this.id)" name="category[]" value="suzuka">
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 col-xs-9 textcheck"><h4 class="font-sofia-bold">Suzuka Cat Auto Refinish</h4></div>
                    </div>
                  </li> -->
                  
          
                </ul>
              </div>
            </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    

    
  </div>
<style type="text/css">  
  .popover{
    margin-left: 20px !important;
    overflow: hidden !important;
  }
  @media (min-width: 320px) and (max-width: 480px) {
    .popover{
      min-width: auto !important;
    }
  }
</style>  

<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAdfB-1tzijt8NQRVY6SLNft9_JwxWxu1s&libraries=geometry'
  type="text/javascript"></script>
<script src="/js/plugins/infobox.js"></script>
<script src="/js/plugins/markerclusterer.js"></script>
<script src="/js/plugins/markerwithlabel.min.js"></script>
<script src="/avian_new/js/store-location.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.matchHeight-min.js"></script>