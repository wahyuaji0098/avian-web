<div class="container container_viszer targetcontainer_viszer">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>VISUALISASI WARNA</span>
            </div>
        </div>
    </div>
    <div class="section section-smargin">
        <div class="section_content">
            <div class="searchcontain">
                <div class="searchbox">
                    <input type="text" placeholder="Cari Nama Visualisasi Warna" id="search-visual" value="<?= $vis_search ?>"/>
                </div>
                <button class="btn" type="button" id="search-btn">Cari</button>
                <button class="btn btn-sml" type="button" id="clear-btn">Hapus</button>
            </div>
            <div class="mbtncontain btncontainframed <?= (!isset($login_user['id'])) ? "needloginvisual" : "" ?>">
                <a href="<?= (!isset($login_user['id'])) ? "javascript:void(0)" : "/visualize/choose-template" ?>" class="btn">Buat Rancangan Baru</a>
            </div>
        </div>
    </div>
    <div class="section gridds">
        <div class="section_content griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <?php if(count($models) > 0): foreach($models as $model): ?>
            <a href="/visualize/detail/<?= url_title($model['name']."-".$model['id']) ?>" class="griddsitem griddsitemtheme gridcol width-2 height-3">
                <div class="themebox">
                    <div class="themepic"><img src="<?= ($model['result_image_url']) ? $model['result_image_url'] : $model['cover_image'] ?>"/></div>
                    <div class="themecollist">
                        <?php if (count($model['colors']) > 0): foreach($model['colors'] as $col): ?>
                        <div class="themecol autopalette" colhex="<?= $col ?>"></div>
                        <?php endforeach; endif; ?>
                    </div>
                    <div class="themename"><?= $model['name'] ?></div>
                    <div class="themeauth">By: <?= $model['member_name'] ?></div>
                </div>
            </a>
            <?php endforeach; endif; ?>
        </div>
    </div>
    <div class="section section-thirds">
        <div class="section_content">
            <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
        </div>
    </div>
</div>
