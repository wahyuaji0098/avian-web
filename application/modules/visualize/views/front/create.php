<div class="container container_viszer targetcontainer_viszer">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/visualize">VISUALISASI WARNA</a>
                <a href="/visualize/choose-template">PILIH TEMPAT</a>
                <span><?= strtoupper($location['name']); ?></span>
            </div>
        </div>
    </div>
    <div id="png-container">
    </div>
    <div class="section section_viszer">
        <div class="section_content">
            <div class="viszer">
                <div class="flscrnbtn trigger" targid="container_viszer">Toggle Fullscreen</div>
                <div class="showlabel trigger" targid="pallabels">Tampilkan/Sembunyikan Label</div>
                <div class="viszer_disp">
                    <div class="viszer_cont">
                        <canvas id="canvas"></canvas>
                        <?php for($i = 0; $i < $total_vl; $i++): ?>
                        <?php if ($i == 0) { ?>
                        <div class="viszer_bg"><img class="la_s" src="<?= $vl_detail[$i]['image_url'] ?>"/></div>
                        <?php } else if($i > 0 && $i < $total_vl-1) { if($vl_detail[$i]['colorable'] == 1) { ?>
                        <div class="viszer_la" id="la<?= $vl_detail[$i]['id'] ?>" defcol="">
                            <?= $vl_detail[$i]['svg'] ?>
                        </div>
                        <?php } } else { ?>
                        <div class="viszer_sh"><img src="<?= $vl_detail[$i]['image_url'] ?>"/></div>
                        <?php }  ?>
                        <?php endfor; ?>
                        <div class="pallabels targetpallabels">
                            <?php $nomor = 1; for($i = 0; $i < $total_vl; $i++):
                                    if ($i > 0 && $i < $total_vl-1 && $vl_detail[$i]['colorable'] == 1): ?>
                            <div class="pallabel pallabel<?= $vl_detail[$i]['id'] ?> viszer_part active" part="<?= $vl_detail[$i]['id'] ?>" style="top: <?= $vl_detail[$i]['top'] ?>%; left:<?= $vl_detail[$i]['left'] ?>%;"><?= $nomor ?></div>
                            <?php $nomor++; endif; endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="viszer_controls">
                    <div class="viszer_parts scrollbarcust scrollbarcust_simple">
                        <?php $nomor = 1; for($i = 0; $i < $total_vl; $i++): ?>
                        <?php if ($i > 0 && $i < $total_vl-1) { if($vl_detail[$i]['colorable'] == 1) { ?>
                        <div class="viszer_part_con" id="vpc_<?= $vl_detail[$i]['id'] ?>">
                            <input type="radio" id="vp_<?= $vl_detail[$i]['id'] ?>" name="viszer_part" <?= ($i == 1) ? 'checked="checked"' : '' ?> selpalid="" selcolid="" value="<?= $vl_detail[$i]['id'] ?>"/>
                            <label class="viszer_part" for="vp_<?= $vl_detail[$i]['id'] ?>" part="<?= $vl_detail[$i]['id'] ?>">
                                <div class="viszer_part_samp autopalette" id="samp_<?= $vl_detail[$i]['id'] ?>" colhex=""><span class="pallabellist"><?= $nomor ?></span></div>
                                <span class="viszer_part_name"><?= $vl_detail[$i]['name'] ?></span>
                            </label>
                        </div>
                        <?php $nomor++;} }  ?>
                        <?php endfor; ?>
                    </div>
                    <div class="viszer_palls">
                        <div class="viszer_pall_group active">
                            <div class="viszer_pall_selector">
                                <div class="option option-select">
                                    <span>Kartu Warna</span>
                                    <div class="select-cont">
                                        <select id="pallete_val">
                                            <?php if(count($pallete) > 0): foreach($pallete as $pall):  ?>
                                            <option value="a<?= $pall['id'] ?>"><?= $pall['name'] ?></option>
                                            <?php endforeach; endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <?php $i_pal = 0; foreach($pallete as $pall):  ?>
                            <div class="viszer_pallscroll scrollbarcust scrollbarcust_simple gpals_a pals_a<?= $pall['id'] ?> <?= ($i_pal == 0) ? "active" : '' ?>">
                            </div>
                            <?php $i_pal++; endforeach; ?>
                            <div class="viszer_pallscroll scrollbarcust scrollbarcust_simple gpals_a pals_a0">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="viszer_saveform">
                    <div class="viszer_fieldcol viszer_fieldcol_title">
                        <div class="viszer_field">
                            <input type="text" id="visual_name" placeholder="Masukan Nama Visualize..." />
                        </div>
                    </div>
                    <div class="viszer_fieldcol viszer_fieldcol_mid">
                        <input type="hidden" id="vis_id" value="<?= $location_id ?>"/>
                        <div class="viszer_field option-radio">
                            <input type="radio" id="unpublished" name="viszer_publishstate" value="0"/>
                            <label class="viszer_publishstate" for="unpublished"><span class="cicon"></span>Jangan Publish</label>
                        </div>
                        <div class="viszer_field option-radio">
                            <input type="radio" id="published" name="viszer_publishstate" value="1"/>
                            <label class="viszer_publishstate" for="published"><span class="cicon"></span>Publish</label>
                        </div>
                    </div>
                    <div class="viszer_fieldcol">
                        <div class="viszer_field"><button class="btn" type="button" onclick="saveVisual()">Simpan Visualize</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
