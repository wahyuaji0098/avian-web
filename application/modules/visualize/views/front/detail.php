<div class="container container_viszer targetcontainer_viszer">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/visualize">VISUALISASI WARNA</a>
                <span><?= strtoupper($result_location['name']); ?></span>
            </div>
        </div>
    </div>
    <div class="section section_viszer section-smargin">
        <div class="section_content">
            <div class="viszer">
                <div class="flscrnbtn trigger" targid="container_viszer">Toggle Fullscreen</div>
                <div class="viszer_disp">
                    <div class="viszer_cont viszer_result">
                        <canvas id="canvas"></canvas>
                        <?php for($i = 0; $i < $total_vl; $i++): ?>
                        <?php if ($i == 0) { ?>
                        <div class="viszer_bg"><img class="la_s" src="<?= $vl_detail[$i]['image_url'] ?>"/></div>
                        <?php } else if($i > 0 && $i < $total_vl-1) { ?>
                        <div class="viszer_la" id="la<?= $vl_detail[$i]['id'] ?>" defcol="<?= isset($result_detail[$vl_detail[$i]['id']]['color']['hex_code']) ? $result_detail[$vl_detail[$i]['id']]['color']['hex_code'] : "" ?>">
                            <?= $vl_detail[$i]['svg'] ?>
                        </div>
                        <?php } else { ?>
                        <div class="viszer_sh"><img src="<?= $vl_detail[$i]['image_url'] ?>"/></div>
                        <?php }  ?>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
            <div class="viszer_info">
                <div class="viszer_id scrollbarcust scrollbarcust_simple">
                    <div class="themename"><?= $result_location['name'] ?></div>
                    <div class="creator">By: <?= $result_location['member_name'] ?></div>
                    <div class="date"><?= date('d F Y', strtotime($result_location['publish_date'])) ?></div>
                    <div class="addthis_inline_share_toolbox"></div>
                </div>
                <div class="viszer_collist scrollbarcust scrollbarcust_simple">
                    <?php for($i = 0; $i < $total_vl; $i++): ?>
                    <?php if ($i > 0 && $i < $total_vl-1 && $vl_detail[$i]['colorable'] == 1) { ?>
                    <div class="viszer_collist_item">
                        <div class="viszer_collist_item_partname"><?= $vl_detail[$i]['name'] ?></div>
                        <div class="viszer_collist_item_colour">
                            <div class="viszer_collist_item_samp autopalette" colhex="<?= isset($result_detail[$vl_detail[$i]['id']]['color']['hex_code']) ? $result_detail[$vl_detail[$i]['id']]['color']['hex_code'] : "" ?>"></div>
                            <span><?= isset($result_detail[$vl_detail[$i]['id']]['color']['name']) ? $result_detail[$vl_detail[$i]['id']]['color']['name'] : "" ?></span>
                        </div>
                    </div>
                    <?php }  ?>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section">
        <div class="section_content">
            <div class="rbtncontain">
                <?php if(isset($login_user['id']) && ($login_user['id'] == $result_location['member_id']) && (date('Y-m-d', strtotime($result_location['created_date'])) >= "2016-12-14")) : ?>
                <a href="/visualize/edit/<?= url_title($result_location['name']."-".$result_location['id']) ?>" class="btn">Ubah Rancangan</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
