<div class="container container_viszer targetcontainer_viszer">
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/visualize">VISUALISASI WARNA</a>
                <span>PILIH TEMPAT</span>
            </div>
        </div>
    </div>
    <div class="section section-crumb section-smargin mobilenogap">
        <div class="section_content">
            <div class="section-title">Pilih Template</div>
        </div>
    </div>
    <div class="section gridds">
        <div class="section_content griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <?php if (count($models) > 0): foreach($models as $model): ?>
            <a href="/visualize/create/<?= url_title($model['name']."-".$model['id']) ?>" class="griddsitem griddsitemtheme gridcol width-5 height-5p5">
                <div class="tempbox">
                    <div class="temppic"><img src="<?= $model['image_url'] ?>"/></div>
                    <div class="tempname"><?= $model['name'] ?></div>
                </div>
            </a>
            <?php endforeach; endif; ?>
        </div>
    </div>
    <div class="section section-smargin">
        <div class="section_content">
            <div class="mbtncontain btncontainframed">
                <a href="/visualize" class="btn">Batal</a>
            </div>
        </div>
    </div>
</div>
