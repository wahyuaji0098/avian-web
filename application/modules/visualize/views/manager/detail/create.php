<?php
    $location_id = isset($location_data["id"]) ? $location_data["id"] : "";

    $x = isset($update_data["x"]) ? $update_data["x"] : "";
    $y = isset($update_data["y"]) ? $update_data["y"] : "";
    $id = isset($update_data["id"]) ? $update_data["id"] : "";
    $svg = isset($update_data["svg"]) ? $update_data["svg"] : "";
    $name = isset($update_data["name"]) ? $update_data["name"] : "";
    $layer = isset($update_data["layer"]) ? $update_data["layer"] : "";
    $is_show = isset($update_data["is_show"]) ? $update_data["is_show"] : "";
    $image_url = isset($update_data["image_url"]) ? $update_data["image_url"] : "";
    $colorable = isset($update_data["colorable"]) ? $update_data["colorable"] : "";
    $created_date = isset($update_data["created_date"]) ? $update_data["created_date"] : "";
    $updated_date = isset($update_data["updated_date"]) ? $update_data["updated_date"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/visualize/detail/lists/<?= $location_id ?>';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/visualize/detail/process-form" method="POST" enctype="multipart/form-data">
                            <header>Visualizer location detail form</header>
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <input type="hidden" name="location_id" value="<?= $location_id ?>" />
                                <fieldset>
                                    <section>
                                        <label class="label">Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?php echo $name; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">PNG <sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div>
                                                <?php if($image_url): ?>
                                                <img src="<?= $image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <input name="image-file" id="image-file" type="file"  class="form-control" />
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Svg </label>
                                        <label class="textarea">
                                                <textarea name="svg" id="svg" type="text"  class="form-control" placeholder="Svg" rows="8"/><?php echo $svg; ?></textarea>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Colorable </label>
                                        <label class="input">
                                            <?php echo select_yes_no('colorable', $colorable); ?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Posisi tombol (X) </label>
                                        <label class="input">
                                            <input name="x" id="x" type="text"  class="form-control" placeholder="Posisi Tombol X" value="<?php echo $x; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Posisi tombol (Y) </label>
                                        <label class="input">
                                            <input name="y" id="y" type="text"  class="form-control" placeholder="Posisi Tombol Y" value="<?php echo $y; ?>" />
                                        </label>
                                    </section>
                                    <section id="assign_palette">
                                        <label class="label">Assign Palette </label>
                                        <div class="row">
                                        <?php
                                            $last_number = 0;
                                            for ($i=0; $i < 3; $i++): ?>
                                            <div class="col col-4">
                                                <?php
                                                    for($j = $last_number; $j< $total_pallete+$last_number; $j++):
                                                        if (isset($pallete[$j])):
                                                            ?>
                                                <label class="checkbox">
                                                    <input type="checkbox" class="colors" name="pallete_id[]" <?= (in_array($pallete[$j]['id'], $pallete_ids)) ? "checked" : ""; ?> value="<?= $pallete[$j]['id'] ?>" />
                                                    <i></i><?= $pallete[$j]['name'] ?></label>
                                                <?php endif;  endfor; $last_number = $j;?>
                                            </div>
                                        <?php endfor; ?>
                                        </div>
                                    </section>

                                    <section id="assign_product">
                                        <label class="label">Assign Product </label>
                                        <div class="row">
                                        <?php
                                            $last_number = 0;
                                            for ($i=0; $i < 3; $i++): ?>
                                            <div class="col col-4">
                                                <?php
                                                    for($j = $last_number; $j< $total_product+$last_number; $j++):
                                                        if (isset($products[$j])):
                                                            ?>
                                                <label class="checkbox">
                                                    <input type="checkbox" class="colors" name="products[]" <?= (in_array($products[$j]['id'], $product_ids)) ? "checked" : ""; ?> value="<?= $products[$j]['id'] ?>" />
                                                    <i></i><?= $products[$j]['name'] . "( ".$products[$j]['category_name']." )" ?></label>
                                                <?php endif;  endfor; $last_number = $j;?>
                                            </div>
                                        <?php endfor; ?>
                                        </div>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
