<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Location_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'dtb_visual_location';
        $this->_table_alias = 'ds';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {
        //need to extend something?
    }

    public function get_location_data($id) {
        return $this->get_all_data(array(
            "conditions" => array("is_show" => 1),
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];
    }

    //get the first order number.
	public function getFirstOrdering() {
		$this->db->select("min(ordering) AS ordering");
        $this->db->from($this->_table);

        return $this->db->get()->row()->ordering;
	}

    //get last number of ordering
    public function getLastOrdering () {
        $this->db->select("max(ordering) AS ordering");
        $this->db->from($this->_table);

        return $this->db->get()->row()->ordering;
    }

    public function getAllOrdering () {
        $this->db->select('GROUP_CONCAT( DISTINCT ordering ORDER BY ordering SEPARATOR ",") as ordering');
        $this->db->from($this->_table);

        return $this->db->get()->row()->ordering;
    }

    public function getAllSlider () {
        return $this->get_all_data(array(
            "order_by" => array("ordering" => "asc"),
        ))['datas'];
    }

    public function getCoverImage ($location_id) {
        $this->db->select("image_url");
        $this->db->from($this->_table);
        $this->db->where('id', $location_id);
        $query = $this->db->get();
        $result = $query->row_array();

        return $result['image_url'];
    }

}
