<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Result_detail_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'dtb_visual_result_detail';
        $this->_table_alias = 'dvrd';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {
        //need to extend something?
    }

    public function getAllColorWithDetail ($result_id) {
        $this->load->model("Dynamic_model");

        $this->db->where("visual_result_id",$result_id);
        $this->db->order_by('visual_location_detail_id','asc');

        $result = $this->db->get($this->_table)->result_array();

		$models = array();

		//get color name and hex
		foreach ($result as $res) {
			if($res['pallete_id'] == 0) {
                //find color in custom color
                $color = $this->Dynamic_model->set_model("dtb_custom_color", "dcc", "id")->get_all_data(array(
                    "find_by_pk" => array($res['color_id']),
                    "row_array" => true,
                ))['datas'];
            } else {
                $color = $this->Dynamic_model->set_model("dtb_color", "dc", "id")->get_all_data(array(
                    "find_by_pk" => array($res['color_id']),
                    "row_array" => true,
                ))['datas'];
            }
			$models[$color['id']] = $color['hex_code'];
		}

		return $models;
    }

    public function get_full_detail ($conditions = false) {
		if ($conditions) {
            $this->db->where($conditions);
        }

		$this->db->order_by('visual_location_detail_id','asc');

        $result = $this->db->get($this->_table)->result_array();

		$models = array();

		//get color name and hex
		foreach ($result as $res) {
            if($res['pallete_id'] == 0) {
                //find color in custom color
                $color = $this->Dynamic_model->set_model("dtb_custom_color", "dcc", "id")->get_all_data(array(
                    "find_by_pk" => array($res['color_id']),
                    "row_array" => true,
                ))['datas'];
            } else {
                $color = $this->Dynamic_model->set_model("dtb_color", "dc", "id")->get_all_data(array(
                    "find_by_pk" => array($res['color_id']),
                    "row_array" => true,
                ))['datas'];
            }
			$res['color'] = $color;
            $models[$res['visual_location_detail_id']] = $res;
		}

		return $models;
	}

    /**------------------------------------------------------------------------------------------
     * API FUNCTION
     *------------------------------------------------------------------------------------------*/

    public function getVisualDetail($visual_result_id) {
        $this->db->select("t_dresult.id, t_dresult.color_id, t_dresult.pallete_id, IF(t_dresult.pallete_id != 0, t_color.red, t_ccolor.red) as r, IF(t_dresult.pallete_id != 0, t_color.green, t_ccolor.green) as g , IF(t_dresult.pallete_id != 0, t_color.blue, t_ccolor.blue) as b, IF(t_dresult.pallete_id != 0, t_color.name, t_ccolor.name) as color_name, t_dresult.visual_location_detail_id as layer_id, t_vldetail.layer , t_vldetail.name, t_vldetail.image_url, '' as svg, t_vldetail.colorable, t_vldetail.is_show", false);
        $this->db->from('dtb_visual_result_detail as t_dresult');
        $this->db->join('dtb_color as t_color', 't_dresult.color_id = t_color.id', 'LEFT');
        $this->db->join('dtb_custom_color as t_ccolor', 't_dresult.color_id = t_ccolor.id', 'LEFT');
        $this->db->join('dtb_visual_location_detail as t_vldetail', 't_dresult.visual_location_detail_id = t_vldetail.id', 'LEFT');
        $this->db->where("t_dresult.visual_result_id", $visual_result_id);

        return $this->db->get()->result_array();
    }

}
