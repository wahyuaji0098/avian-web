<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Result_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'dtb_visual_result';
        $this->_table_alias = 'dvr';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {
        //need to extend something?
    }

    public function getVisualResultWithDetailByID($id) {
        $this->load->model("Dynamic_model");

        $v_result = $this->Dynamic_model->set_model("dtb_visual_result", "dvr", "id")->get_all_data(array(
            "select" => "dvr.id , dvr.member_id, dm.name as member_name, dvr.location_id, dvl.name as location_name, dvr.result_image_url, dvr.result_image_large, dvr.name , dvr.is_published , publish_date, dvr.version, dvr.is_show, dvr.status",
            "left_joined" => array(
                "dtb_visual_location as dvl" => array("dvr.location_id" => "dvl.id"),
                "dtb_member dm" => array("dvr.member_id" => "dm.id"),
            ),
            "conditions" => array(
                "dvr.id" => $id
            ),
            "row_array" => true,
        ))['datas'];

        $v_result['layers'] = $this->Dynamic_model->set_model("dtb_visual_result_detail", "dvrd", "id")->get_all_data(array(
            "select" => "dvrd.id, dvrd.color_id, dvrd.pallete_id, IF(dvrd.pallete_id != 0, dc.red, dcc.red) as r, IF(dvrd.pallete_id != 0, dc.green, dcc.green) as g , IF(dvrd.pallete_id != 0, dc.blue, dcc.blue) as b, IF(dvrd.pallete_id != 0, dc.name, dcc.name) as color_name, dvrd.visual_location_detail_id as layer_id, dvld.layer , dvld.name, dvld.image_url, '' as svg, dvld.colorable, dvld.is_show, dvrd.product_id",
            "left_joined" => array(
                "dtb_color as dc" => array("dc.id" => "dvrd.color_id"),
                "dtb_custom_color as dcc" => array("dcc.id" => "dvrd.color_id"),
                "dtb_visual_location_detail as dvld" => array("dvld.id" => "dvrd.visual_location_detail_id"),
            ),
            "conditions" => array(
                "dvrd.visual_result_id" => $id
            )
        ))['datas'];

        return $v_result;
    }

    function findByPkForLikeBox ($id){
        $this->load->model("visualize/Result_detail_model");
        $this->load->model("visualize/Location_model");

        if ($id) {
			$this->db->select("dtb_visual_result.*, dm.name as member_name");
			$this->db->from($this->_table );
            $this->db->join("dtb_member dm" , "dm.id = dtb_visual_result.member_id", "left");

			$this->db->where("dtb_visual_result." . $this->_pk_field,$id);
			$this->db->where("dtb_visual_result.is_show",SHOW);
			$this->db->where("dtb_visual_result.status",STATUS_ACTIVE);
			$this->db->where("dtb_visual_result.is_published",SHOW);
			$this->db->limit(1);
			$query = $this->db->get();
			$result = $query->row_array();

			//get image
			$vlmodel = new Location_model();
			$image = $vlmodel->getCoverImage ($result['location_id']);
			$result['cover_image'] = $image;

			//get location detail colors with hex.
			$vmodel = new Result_detail_model();
			$color = $vmodel->getAllColorWithDetail ($result['id']);
			$result['colors'] = $color;
		} else {
			$result = array();
		}

        return $result;
    }

    public function get_all_list_data($sort_field = "id",$sortby = "DESC",$limit,$start,$search = "") {
        $this->load->model("visualize/Result_detail_model");
        $this->load->model("visualize/Location_model");

        $data = array('dtb_visual_result.id','location_id','member_id','result_image_url','dtb_visual_result.name','publish_date', 'dm.name as member_name');
        $sort_field = $sort_field;
        $this->db->select($data);
        $this->db->from($this->_table);
        $this->db->join("dtb_member dm" , "dm.id = dtb_visual_result.member_id", "left");

        if(!empty($search)){
            $search = $this->db->escape_str($search);
            $this->db->group_start();
            $this->db->like('lower(dtb_visual_result.name)', strtolower($search));
            $this->db->group_end();
        }

        $this->db->where("dtb_visual_result.is_show",SHOW);
        $this->db->where("dtb_visual_result.status",STATUS_ACTIVE);

        if(!isset($this->session->sess_login_user['id'])) {
        	$this->db->where("is_published",SHOW);
        } else {
        	$this->db->where("is_published",SHOW);
        	$this->db->or_where("dtb_visual_result.id in (select dtb_visual_result.id from dtb_visual_result where is_published = 0 and is_show = 1 and status = 1 and member_id = ".$this->session->sess_login_user['id'].") ", NULL, false);
        }

        $this->db->order_by($sort_field,$sortby);
        $this->db->limit($limit,$start);
        $query = $this->db->get();

        $result = $query->result_array();

        $models = array();

        $vmodel = new Result_detail_model();
        $vlmodel = new Location_model();

        if (count($result) > 0) {
            foreach ($result as $model) {
                //get image
                $image = $vlmodel->getCoverImage ($model['location_id']);
                $model['cover_image'] = $image;

                //get location detail colors with hex.
                $color = $vmodel->getAllColorWithDetail ($model['id']);
                $model['colors'] = $color;

                array_push($models,$model);
            }
        }

        return $models;
    }

    function count_all_rows_for_list($search="") {
        $this->db->select("COUNT(*) AS numrows");
        $this->db->from($this->_table);

        if(!empty($search)){
            $search = $this->db->escape_str($search);
            $this->db->group_start();
            $this->db->like('name', $search);
            $this->db->group_end();
        }

        $this->db->where("is_show",SHOW);
        $this->db->where("status",STATUS_ACTIVE);
        if(!isset($this->session->sess_login_user['id'])) {
            $this->db->where("is_published",SHOW);
        } else {
            $this->db->where("is_published",SHOW);
            $this->db->or_where("id in (select id from dtb_visual_result where is_published = 0 and is_show = 1 and status = 1 and member_id = ".$this->session->sess_login_user['id'].") ", NULL, false);
        }

        return $this->db->get()->row()->numrows;
    }


}
