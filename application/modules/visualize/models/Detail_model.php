<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Detail_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'dtb_visual_location_detail';
        $this->_table_alias = 'ds';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {
        //need to extend something?
    }

    // public function insert($datas) {
    //     //add create date and update date
    //     $datas['created_date'] = $datas['updated_date'] = date('Y-m-d H:i:s');
	//
	// 	//get last version
    //     $last_ver = $this->getLastVersion();
    //     $datas['version'] = $last_ver+1;
	//
	// 	//update last version vlocation model
	// 	$this->Vlocation_model->addVersion($datas['location_id']);
	//
	// 	//add layer
    //     $layer = $this->getLastLayer($datas['location_id']);
    //     $datas['layer'] = $layer+1;
	//
    //     $this->db->insert($this->_table, $datas);
    //
    //     return $this->db->insert_id();
    // }

    public function getAllVdetail ($vlocation_id) {
        $col = array('id','location_id','name','layer','image_url','svg','colorable','is_show','version','created_date','updated_date');
        $this->db->select("*");
        $this->db->from($this->_table);
        $this->db->where('location_id', $vlocation_id);
        $this->db->order_by('layer','ASC');

        return $this->db->get()->result_array();
    }

    // get the first layer.
    public function getFirstLayer($id) {
        $this->db->select("min(layer) AS layer");
        $this->db->from($this->_table);
        $this->db->where('location_id', $id);

        return $this->db->get()->row()->layer;
    }

    // get last layer
    public function getLastLayer($id) {
        $this->db->select("max(layer) AS layer");
        $this->db->from($this->_table);
        $this->db->where('location_id', $id);

        return $this->db->get()->row()->layer;
    }

    // get all layer
    public function getAllLayer ($id) {
        $this->db->select('GROUP_CONCAT( DISTINCT layer ORDER BY layer SEPARATOR ",") as layer');
        $this->db->from($this->_table);
        $this->db->where('location_id', $id);

        return $this->db->get()->row()->layer;
    }

    public function getAllLayering ($id) {
        return $this->get_all_data(array(
            "conditions" => array('location_id' => $id, ),
            "order_by"  => array("layer" => "asc"),
        ))['datas'];
    }

    public function get_vl_detail_web ($conditions = false) {
        if (!empty($conditions)) {
            $this->db->where($conditions);
        }
        $this->db->order_by('layer','ASC');

        $result = $this->db->get($this->_table)->result_array();

        $models = array();

        if (count($result) > 0) {
            foreach ($result as $model) {
                if ($model['image_url']) {
                    $size = getimagesize (FCPATH . $model['image_url']);
                    $model["image_width"] = $size[0];
                    $model["image_height"] = $size[1];
                    $model["top"] = ($model['y']/$size[1])*100;
                    $model["left"] = ($model['x']/$size[0])*100;
                } else {
                    $model["image_width"] = 0;
                    $model["image_height"] = 0;
                    $model["top"] = 0;
                    $model["left"] = 0;
                }
                array_push($models, $model);
            }
        }

        return $models;
    }

    /**
     * API FUNCTION
     */

    public function getAllVdetailAPI ($vlocation_id) {
        $col = array('id','name','layer','image_url','products as svg','colorable','is_show', 'ifnull(x,0) as x', 'ifnull(y,0) as y','products');
        $this->db->select($col);
        $this->db->from($this->_table);
        $this->db->where('location_id', $vlocation_id);
        $this->db->order_by('layer','ASC');

        $result = $this->db->get()->result_array();

        $models = array();

        if (count($result) > 0) {
            foreach ($result as $model) {
                if ($model['image_url']) {
                    $size = getimagesize (FCPATH . $model['image_url']);
                    $model["image_width"] = $size[0];
                    $model["image_height"] = $size[1];
                } else {
                    $model["image_width"] = 0;
                    $model["image_height"] = 0;
                }
                array_push($models, $model);
            }
        }

        return $models;
    }

}
