<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Visual_palette_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'dtb_visual_pallete';
        $this->_table_alias = 'dvp';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {
        //need to extend something?
    }


    function get_pallete_detail_by_layer($id) {
        $this->db->select("vpal.pallete_id as id , pal.name");
        $this->db->from('dtb_visual_pallete as vpal');
        $this->db->join('dtb_pallete as pal', 'pal.id = vpal.pallete_id', 'LEFT');

        $this->db->where('layer_id',$id);
        $this->db->where("vpal.status",STATUS_ACTIVE);
        $this->db->order_by("pal.name", "ASC");

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

}
