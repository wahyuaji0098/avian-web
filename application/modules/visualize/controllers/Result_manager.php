<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Visualizer Result Controller.
 */
class Result_manager extends Baseadmin_Controller  {

    private $_title         = "Visualizer Result";
    private $_title_page    = '<i class="fa-fw fa fa-files-o"></i> Visualizer Result ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "vresult";
    private $_back          = "/manager/visualize/result/lists";
    private $_js_path       = "/js/pages/visualize/manager/result/";
    private $_view_folder   = "visualize/manager/result/";
    private $_table         = "dtb_visual_result";
    private $_table_detail  = "dtb_visual_result_detail";
    private $_table_aliases = "vres";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List visualizer_result
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Visualizer Result</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Visualizer Result</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////


    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get visualizer_result
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('vres.id','vloc.name AS location_name','mbr.name AS member_name','vres.name','publish_date','result_image_url','result_image_large','is_published','vres.is_show');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['vres.id'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['vres.name'] = $value;
                        }
                        break;

                    case 'publish_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(publish_date as date) <="] = $date['end'];
                            $conditions["cast(publish_date as date) >="] = $date['start'];
                        }
                        break;

                    case 'location':
                        if ($value != "") {
                            $data_filters['vloc.name'] = $value;
                        }
                        break;

                    case 'member':
                        if ($value != "") {
                            $data_filters['mbr.name'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select' => $select,
            'left_joined' => array(
                "dtb_member mbr" => array("mbr.id" => "vres.member_id"),
                "dtb_visual_location vloc" => array("vloc.id" => "vres.location_id")
            ),
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Delete an visualizer_result.
     */
    public function delete() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data result visualizer
            $data = $model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {
                //begin transaction
                $this->db->trans_begin();
                // delete the data
                $condition = array("id" => $id);
                $delete = $model->delete($condition, array("is_permanently" => true));
                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = FALSE;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Visualizer Result has been deleted.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function hide_show() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(TRUE) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //initial.
        $message['is_error']    = TRUE;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        $extra_param = array(
            "is_direct" => TRUE,
        );

        $pid = sanitize_str_input($this->input->post('id'), "numeric");
        $type = $this->input->post("type");

        if (!empty($pid)) {
            //get data result visualizer
            $data = $model->get_all_data(array(
                "find_by_pk" => array($pid),
                "row_array" => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {
                //begin transaction
                $this->db->trans_begin();
                // delete the data
                $condition = array("id" => $pid);
                $delete    = $model->update(array('is_show' => $type), $condition, $extra_param);
                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = FALSE;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Result show has been change.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;

    }

}
