<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Visualize extends Basepublic_Controller {

    private $_view_folder = "visualize/front/";

    function __construct() {
        parent::__construct();

    }

	public function index() {
        //get header page
		$page = get_page_detail('visualize');
        redirect(base_url('color'));

        exit;
        //load model visualize result
        $this->load->model("Result_model");

		$m_vis_res = new Result_model ();
		$limit = 20;
		$start = 0;
        $search = $this->session->userdata('visual_search');
        $sort_by = "dtb_visual_result.created_date";
        $sort = "desc";

		$lists = $m_vis_res->get_all_list_data($sort_by,$sort,$limit,$start,$search);
		$total = $m_vis_res->count_all_rows_for_list($search);
        $total_page = ceil($total/$limit);

        $header = array(
            'header'        => $page,
            'models'    => $lists,
			'vis_page'   => $total_page,
			'vis_search'   => $search,
        );

        $footer = array(
            "script" => array(
                "/js/front/visual.js",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER, $footer);


	}

	public function choose_template () {
		if (!isset($this->data_login_user['id'])) {
			show_404('page');
		}

		$page = get_page_detail('visualize');

		//get all location
		$lists = $this->_dm->set_model("dtb_visual_location", "dvl", "id")->get_all_data(array(
            "conditions" => array("is_show" => SHOW),
            "order_by" => array("ordering" => "asc"),
        ))['datas'];

        $header = array(
            'header'        => $page,
            'models'    => $lists,
        );
        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'choose-template');
        $this->load->view(FRONT_FOOTER);
	}

	public function create() {
        if (!isset($this->data_login_user['id'])) {
			show_404('page');
		}

		$this->load->model('visualize/Visual_palette_model');
		$this->load->model('visualize/Detail_model');

		$url = $this->uri->segment(3, 0);

        if (empty($url)) {
            show_404();
        }

		$url = explode("-",$url);

		$id = end($url);

		//check if id is exist
		$location = $this->_dm->set_model("dtb_visual_location", "dvl", "id")->get_all_data(array(
            "conditions" => array('id' => $id, 'is_show' => SHOW),
            "row_array" => true,
        ))['datas'];

		if (!$location) {
			//return error not found
			show_404('page');
		}

		//get vizualiser detail with id location $id
		$vl_detail = $this->Detail_model->get_vl_detail_web(array('location_id' => $id, 'is_show' => SHOW));

		//get color pallete + color
		$pallete = $this->Visual_palette_model->get_pallete_detail_by_layer($vl_detail[1]['id']);

        //get header page
		$page = get_page_detail('visualize');

        $header = array(
            'header'            => $page,
            'location_id'       => $id,
			'location'          => $location,
			'vl_detail'         => $vl_detail,
			'total_vl'          => count($vl_detail),
			'pallete'           => $pallete,
        );

        $footer = array(
            "script" => array(
                "/js/front/visual-create.js",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(FRONT_FOOTER, $footer);


	}

	public function detail() {
        $this->load->model("visualize/Result_model");
        $this->load->model("visualize/Result_detail_model");
        $this->load->model("visualize/Detail_model");

		$url = $this->uri->segment(3, 0);

        if (empty($url)) {
            show_404();
        }

		$url = explode("-",$url);

		$id = end($url);

		$result_location = $this->Result_model->get_all_data(array(
            "select" => "dvr.* , dm.name as member_name",
            "conditions" => array('dvr.id' => $id, 'dvr.is_show' => SHOW, 'dvr.status' => STATUS_ACTIVE),
            "row_array" => true,
            "left_joined" => array(
                "dtb_member dm" => array("dm.id" => "dvr.member_id"),
            )
        ))['datas'];

		if (!$result_location) {
			//return error not found
			show_404('page');
		}

		//get location detail
		$vl_detail = $this->Detail_model->get_all_data(array(
            "conditions" => array('location_id' => $result_location['location_id'], 'is_show' => SHOW),
            "order_by" => array("layer" => "asc"),
        ))['datas'];

		//result detail
		$result_detail = $this->Result_detail_model->get_full_detail(array('visual_result_id' => $result_location['id']));

        //get visual likebox
        if (isset($this->data_login_user['id'])) {
            $is_like = find_like_by_member ($this->data_login_user['id'],LIKEBOX_VISUALIZER,$result_location['id']);
            $visual_like = ($is_like) ? 1 : 0;
        } else {
            $visual_like = 0;
        }

		//get header page
		$page = get_page_detail('visualize');

        $header = array(
            'header'            => $page,
            'location_id'       => $id,
			'vl_detail'         => $vl_detail,
			'total_vl'          => count($vl_detail),
			'result_detail'     => $result_detail,
			'result_location'   => $result_location,
			'visual_like'       => $visual_like,
        );

        $footer = array(
            "script" => array(
                "/js/front/visual.js",
                "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ae74c4dc6f7454b",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'detail');
        $this->load->view(FRONT_FOOTER, $footer);
	}

    public function edit() {
        if (!isset($this->data_login_user['id'])) {
			show_404('page');
		}

        $this->load->model("visualize/Location_model");
        $this->load->model("visualize/Detail_model");
        $this->load->model("visualize/Result_model");
        $this->load->model("visualize/Result_detail_model");

		$url = $this->uri->segment(3, 0);

        if (empty($url)) {
            show_404();
        }

		$url = explode("-",$url);

		$id = end($url);

        //check if user login
        if (!isset($this->data_login_user['id'])) {
            show_404('page');
        }

        //check if result is exist
        $result_location = $this->Result_model->get_all_data(array(
            "conditions" => array(
                'id' => $id,
                "member_id" => $this->data_login_user['id'],
                'is_show' => SHOW,
                'status' => STATUS_ACTIVE
            ),
            "row_array" => true,
        ))['datas'];

		if (empty($result_location)) {
			//return error not found
			show_404('page');
		}

        if (date('Y-m-d', strtotime($result_location['created_date'])) < "2016-12-14") {
            show_404('page');
        }

        //result detail
		$result_detail = $this->Result_detail_model->get_full_detail(array('visual_result_id' => $result_location['id']));

		//check if location id is exist
		$location = $this->Location_model->get_all_data(array(
            "conditions" => array('id' => $result_location['location_id'], 'is_show' => SHOW),
            "row_array" => true,
        ))['datas'];

		if (empty($location)) {
			//return error not found
			show_404('page');
		}

		//get vizualiser detail with id location $id
		$vl_detail = $this->Detail_model->get_vl_detail_web(array('location_id' => $result_location['location_id'], 'is_show' => SHOW));

		//get pallete
		$pallete = $this->_dm->set_model("dtb_pallete", "dp", "id")->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
                "color_ids != " => "",
            ),
            "status" => STATUS_ACTIVE,
            "group_by" => "name"
        ))['datas'];

        //get header page
		$page = get_page_detail('visualize');

        $header = array(
            'header'            => $page,
            'location_id'       => $id,
			'location'          => $location,
			'vl_detail'         => $vl_detail,
			'total_vl'          => count($vl_detail),
			'pallete'           => $pallete,
			'result_location'   => $result_location,
            'result_detail'     => $result_detail,
        );

        $footer = array(
            "script" => array(
                "/js/front/visual-create.js",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'edit');
        $this->load->view(FRONT_FOOTER, $footer);

	}

	/**
	 * AJAX FUNCTION
	 */

	public function getpallete () {
		//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $this->load->model('palette/Palette_model');

        $pall_id = $this->input->post("pallete");

		$pall_id = str_replace('a','',$pall_id);

        if ($pall_id == 0) {
            //get custom color
            $pallete['id'] = 0;
            $pallete = $this->_dm->set_model("dtb_visual_location", "dvl", "id")->get_all_data(array(
                "conditions" => array(
                    "member_id" => $this->data_login_user['id'],
                ),
                "status" => STATUS_ACTIVE,
            ))['datas'];
        } else {
            //get color pallete + color
            $pallete = $this->Palette_model->getPalleteWithColorById($pall_id);
        }

		echo json_encode(array(
            "result" => "OK",
			"is_error" => false,
            "pallete" => $pallete,
        ));

		exit;
	}

	public function getlayerpallete () {
		//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$this->load->model('visualize/Visual_palette_model');

        $vis_id = $this->input->post("vis_id");

        $pallete = $this->Visual_palette_model->get_pallete_detail_by_layer($vis_id);

		echo json_encode(array(
            "result" => "OK",
			"is_error" => false,
            "pallete" => $pallete,
        ));

		exit;
	}

	public function save_visualize () {
		//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $message['is_error'] = true;
        $message['result'] = "NG";
        $message['redirect'] = "";


        $this->load->model('visualize/Result_model');
        $this->load->model('visualize/Result_detail_model');

		$name = $this->input->post("name");
		$publish = $this->input->post("publish");
		$detail = $this->input->post("detail");
		$vid = $this->input->post("vid");

        $this->db->trans_begin();

		//insert to visual result
		$data_insert = array(
			"location_id" => $vid,
			"member_id" => $this->data_login_user['id'],
			"name" => $name,
			"publish_date" => date('Y-m-d H:i:s'),
			"is_published" => $publish,
		);

		$result_id = $this->Result_model->insert($data_insert, array("is_version" => true));

		$models = array();

		foreach ($detail as $det) {
			$pallete_id = str_replace('a','',$det['pal_id']);
			$field = array(
				"visual_result_id" => $result_id,
				"color_id" => str_replace('pall_a'.$pallete_id,'',$det['col_id']),
				"pallete_id" => $pallete_id,
				"visual_location_detail_id" => str_replace('vp_','',$det['id']),
			);

			array_push($models,$field);
		}

		//insert to visual result detail
		$visual_detail = $this->Result_detail_model->insert($models, array("is_batch" => true, "is_version" => true, "is_direct" => true));

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            $message['error_msg'] = "Visualisasi telah di simpan.";
            // redirected.
            $message['redirect'] = '/visualize/detail/'.url_title($name."-".$result_id);
        }

		echo json_encode($message);

		exit;

	}

    public function update_visualize () {
		//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $this->load->model('visualize/Result_model');
        $this->load->model('visualize/Result_detail_model');

        $message['is_error'] = true;
        $message['result'] = "NG";
        $message['redirect'] = "";

		$name = $this->input->post("name");
		$publish = $this->input->post("publish");
		$detail = $this->input->post("detail");
		$rid = $this->input->post("rid");

        $this->db->trans_begin();

        //update vresult model
        $result_id = $this->Result_model->update(array(
            'name' => $name,
            'is_published' => $publish,
        ), array("id" => $rid), array("is_version" => true));

        foreach ($detail as $det) {
            //update detail color
            $pallete_id = str_replace('a','',$det['pal_id']);
            $color_id = str_replace('pall_a'.$pallete_id,'',$det['col_id']);

            if ($pallete_id != "" && $color_id != "") {
                //check if exist , if not , insert
                $is_exist = $this->Result_detail_model->get_all_data(array(
                    "conditions" => array(
                        'visual_result_id' => $rid,
                        "visual_location_detail_id" => str_replace('vp_','',$det['id']),
                    ),
                    "row_array" => true,
                ))['datas'];

                if (!$is_exist) {
                    //insert
                    $this->Result_detail_model->insert(array(
                        "visual_result_id" => $rid,
                        "color_id" => $color_id,
                        "pallete_id" => $pallete_id,
                        "visual_location_detail_id" => str_replace('vp_','',$det['id']),
                    ), array("is_direct" => true, "is_version" => true));
                } else {
                    //update
                    $this->Result_detail_model->update(array(
                        "color_id" => $color_id,
                        "pallete_id" => $pallete_id,
                    ),array(
                        'visual_result_id' => $rid,
                        "visual_location_detail_id" => str_replace('vp_','',$det['id']),
                    ), array("is_direct" => true, "is_version" => true) );
                }
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            $message['error_msg'] = "Visualisasi telah di simpan.";
            // redirected.
            $message['redirect'] = '/visualize/detail/'.url_title($name."-".$rid);
        }

		echo json_encode($message);

		exit;

	}

	public function loadmore() {
		//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $this->load->model("Result_model");

		$page = $this->input->post("page");
		$search = $this->input->post("search");

		$m_vis = new Result_model ();
		$limit = 20;
		$start = ($limit * ($page - 1));
		$sort_by = "dtb_visual_result.created_date";
        $sort = "desc";

        $this->session->set_userdata('visual_search', $search);

        if ($search == "") {
            $this->session->unset_userdata('visual_search');
        }

		$visuals = $m_vis->get_all_list_data($sort_by,$sort,$limit,$start,$search);
        $total = $m_vis->count_all_rows_for_list($search);

        $total_page = ceil($total/$limit);

		$models = array();

		if (count($visuals) > 0) {
			foreach($visuals as $model) {
				$model['url'] = url_title($model['name']."-".$model['id']);
				$model['image'] = (($model['result_image_url']) ? $model['result_image_url'] : $model['cover_image']);

				array_push($models,$model);
			}
		}

		echo json_encode(array(
            "result" => "OK",
            "datas" => $models,
            "total_page" => $total_page,
        ));

		exit;
	}


}
