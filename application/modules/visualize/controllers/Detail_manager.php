<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Visualizer Location Detail Detail Controller.
 */
class Detail_manager extends Baseadmin_Controller  {

    private $_title         = "Visualizer Location Detail";
    private $_title_page    = '<i class="fa-fw fa fa-files-o"></i> Visualizer Location Detail ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "vlocation";
    private $_js_path       = "/js/pages/visualize/manager/detail/";
    private $_view_folder   = "visualize/manager/detail/";

    private $_table         = "dtb_visual_location_detail";
    private $_table_aliases = "vdt";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Visualizer Location Detail
     */
    public function lists($id = 0) {
        // set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Visualizer Location Detail</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Visualizer Location Detail</li>',
        );

        $this->load->model('Detail_model');

        $model = $this->_dm->set_model("dtb_visual_location", "dvis", "id");
        $data['location_data'] = $model->get_all_data(array(
                "conditions" => array("is_show" => 1),
                "find_by_pk" => array($id),
                "row_array"  => TRUE,
            ))['datas'];

        // get all layer
        $data['layer_data'] = $this->Detail_model->getAllLayer($id);
        #pr($data['layer_data']);exit;

        // set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        // load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an visualizer_location_detail
     */
    public function create ($id = 0) {
        if (empty($id)) {
            show_404();
        }

        //load model
        $this->load->model("visualize/Location_model");
        $this->load->model("palette/Palette_model");

        $this->_breadcrumb .= '<li><a href="/manager/visualize/detail/lists/"'.$id.'>Visualizer Location Detail</a></li>';

        $data['id'] = $id;
        $data['location_data'] = $this->Location_model->get_location_data($id);

        $data['pallete'] = $this->Palette_model->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
                "status"  => STATUS_ACTIVE,
                "id in (select distinct(palette_id) from mst_palette_color)" => NULL,
            ),
        ))['datas'];

        $data['pallete_ids']   = array();
        $data['total_pallete'] = ceil(count($data['pallete'])/3);

        $data['products'] = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
            "conditions" => array(
                "dp.is_show" => SHOW,
                "dp.status"  => STATUS_ACTIVE,
                "dc.is_show" => SHOW,
            ),
            "left_joined" => array(
                "dtb_product_category dc" => array("dp.product_category_id" => "dc.id")
            ),
            "select" => array("dp.*", "dc.name as category_name"),
            "order_by" => array(
                "dc.ordering" => "asc",
                "dp.ordering" => "asc"
            )
        ))['datas'];

        $data['product_ids']   = array();
        $data['total_product'] = ceil(count($data['products'])/3);

        // Prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Visualizer Location Detail</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Visualizer Location Detail</li>',
            "back"          => "/manager/visualize/detail/lists/$id",
        );

        $footer = array(
            "script" => $this->_js_path . "create.js",
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an visualizer_location_detail
     */
    public function edit ($id = null) {
        if ($id == 0) show_error('Page is not existing', 404);
        $this->_breadcrumb .= '<li><a href="/manager/visualize/detail/lists/"'.$id.'>Visualizer Location Detail</a></li>';

        $data['update_data'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "conditions" => array("is_show" => 1),
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];

        $data['location_data'] = $this->_dm->set_model("dtb_visual_location", "dvis", "id")->get_all_data(array(
            "conditions" => array("is_show" => 1),
            "find_by_pk" => array($data['update_data']["location_id"]),
            "row_array"  => TRUE
        ))['datas'];

        $data['pallete'] = $this->_dm->set_model("dtb_pallete", "pal", "id")->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
                "status"  => STATUS_ACTIVE,
                "id in (select distinct(palette_id) from mst_palette_color)" => NULL,
            ),
        ))['datas'];

        $data['pallete_ids'] = $this->_dm->set_model("dtb_visual_pallete", "pal", "id")->get_all_data(array(
            "select" => array("pallete_id"),
            "conditions" => array(
                "layer_id" => $data['update_data']["id"],
                "status"  => 1
            ),
        ))['datas'];

        $data['pallete_ids'] = array_column($data['pallete_ids'], "pallete_id");
        $data['total_pallete'] = ceil(count($data['pallete'])/3);

        $data['products'] = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
            "conditions" => array(
                "dp.is_show" => SHOW,
                "dp.status"  => STATUS_ACTIVE,
                "dc.is_show" => SHOW,
            ),
            "left_joined" => array(
                "dtb_product_category dc" => array("dp.product_category_id" => "dc.id")
            ),
            "select" => array("dp.*", "dc.name as category_name"),
            "order_by" => array(
                "dc.ordering" => "asc",
                "dp.ordering" => "asc"
            )
        ))['datas'];


        $data['product_ids']   = explode(",",$data['update_data']['products']);
        $data['total_product'] = ceil(count($data['products'])/3);


        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Visualizer Location Detail</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Visualizer Location Detail</li>',
            "back"          => "/manager/visualize/detail/lists/$id",
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation($id) {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data visualizer_location_detail
     */
    public function list_all_data($id = 0) {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('id','name','layer','image_url','colorable','location_id');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array(
            "location_id" => $id,
            "is_show" => 1
        );

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => $select,
            "order_by"        => array($column_sort => $sort_dir),
            "limit"           => $limit,
            "start"           => $start,
            "conditions"      => $conditions,
            "filter"          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas["total"];

        $output = array(
            "data"            => $datas["datas"],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type("application/json");
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_version" => TRUE,
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $location_id = $this->input->post('location_id');
        $name        = sanitize_str_input($this->input->post('name'));
        $svg         = $this->input->post('svg');
        $data_image  = $this->input->post('data-image');
        $colorable   = $this->input->post('colorable');
        $x           = (!empty($this->input->post('x'))) ? $this->input->post('x') : null;
        $y           = (!empty($this->input->post('y'))) ? $this->input->post('y') : null;

        $pallete_id = $this->input->post('pallete_id');
        $products = $this->input->post('products');

        $this->load->model("Detail_model");

        $table = $this->Detail_model;

        $arrayToDB = array(
            'location_id' => $location_id,
            'name'        => $name,
            'svg'         => $svg,
            'colorable'   => $colorable,
            'x'           => $x,
            'y'           => $y,
        );

        if (!empty($products)) {
            $arrayToDB['products'] = implode(",",$products);
        }

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $image = $this->upload_file("image-file", "", false, "upload/vizualiser/$location_id", $id);

            //begin transaction.
            $this->db->trans_begin();

            // insert or update?
            if ($id == "") {

                if (isset($image['uploaded_path'])) {
                    $arrayToDB['image_url'] = $image['uploaded_path'];
                }

                $arrayToDB['layer'] = $table->getLastLayer($location_id) + 1;

                // insert to DB.
                $result = $table->insert($arrayToDB, $extra_param);

                // all pallete data prepare for batch insert
                $array_pallete = array();
                if (count($pallete_id) > 0) {
                    foreach ($pallete_id as $pall) {
                        // Prepare data to DB
                        $array_pallete[] = array(
                            'layer_id'   => $result,
                            'pallete_id' => $pall
                        );
                    }

                    // Insert Visual Pallete.
                    if (!empty($array_pallete)) {
                        $result_pallete = $this->Dynamic_model->set_model("dtb_visual_pallete","pal", "id")->insert(
                            $array_pallete,
                            array(
                                "is_direct"   => true,
                                "is_batch"   => true,
                                "is_version" => true,
                            )
                        );
                    }
                }

            } else {
                // condition for update.
                $condition = array("id" => $id);
                $get_img = $table->get_all_data(array(
                        "select"     => array("image_url","version"),
                        "conditions" => $condition,
                        "row_array"  => TRUE
                    )
                )["datas"];

                //update.
                if (isset($image['uploaded_path']) && isset($get_img['image_url']) && !empty($get_img['image_url'])) {
                    unlink( FCPATH . $get_img['image_url'] );
                }

                if (isset($image['uploaded_path'])) {
                    $arrayToDB['image_url'] = $image['uploaded_path'];
                }

                $result = $table->update(
                    $arrayToDB,
                    $condition,
                    $extra_param
                );

                // Insert all pallete id
                $array_pallete_insert = array();
                if (count($pallete_id) > 0) {
                    //delete all visual palette
                    $this->Dynamic_model->set_model("dtb_visual_pallete","pal", "id")->delete(array("layer_id" => $id), $extra_param);

                    //cek apakah ada palette_id di dalam visual palette
                    $list_visual_palette = $this->Dynamic_model->set_model("dtb_visual_pallete","pal", "id")->get_all_data(array(
                        "conditions" => array(
                            'layer_id'   => $id,
                        ),
                    ))['datas'];

                    if (!empty($list_visual_palette)) {
                        $list_visual_palette = array_column($list_visual_palette, "pallete_id", "id");
                    }

                    foreach ($pallete_id as $pall) {
                        $key = array_search($pall, $list_visual_palette);
                        if ($key !== false) {

                            $this->Dynamic_model->set_model("dtb_visual_pallete","pal", "id")->update(
                                array(
                                    'layer_id'   => $id,
                                    'pallete_id' => $pall,
                                    "status" => STATUS_ACTIVE,
                                ),
                                array(
                                    "id" => $key,
                                ),
                                array(
                                    "is_direct" => true,
                                    "is_version" => true,
                                )
                            );
                        } else {
                            // Prepare data to DB
                            $array_pallete_insert[] = array(
                                'layer_id'   => $id,
                                'pallete_id' => $pall,
                            );
                        }

                    }

                    // Insert Visual Pallete.
                    if (!empty($array_pallete_insert)) {
                        $result_pallete = $this->Dynamic_model->set_model("dtb_visual_pallete","pal", "id")->insert(
                            $array_pallete_insert,
                            array(
                                "is_batch"   => true,
                                "is_direct" => true,
                                "is_version" => true,
                            )
                        );
                    }

                }

            }

            // change version for visualizer_location
            $location = $this->_dm->set_model("dtb_visual_location", "vloc", "id")->update(
                array(),
                array( "id" => $location_id ),
                array( "is_version" => true, )
            );

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "New Visualizer Location Detail has been added.";
                } else {
                    $message['notif_message'] = "New Visualizer Location Detail has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/visualize/detail/lists/$location_id";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an visualizer_location_detail.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        $extra_param = array(
            "is_version" => TRUE,
        );

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data detail
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            // no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();

                // deactivate for visualizer_location_detail
                $detail = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array(
                        "is_show" => HIDE,
                        "layer" => null,
                    ),
                    array(
                        "id" => $id
                    ),
                    $extra_param
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Visualizer Location Detail has been delete.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    // bug //
    public function layering() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id    = $this->input->post('id');
        $layer = $this->input->post('val');
        $loc_id = $this->input->post('loc_id');

        $this->load->model("Detail_model");

        $table = $this->Detail_model;
        $model = $table->get_all_data(array(
            "conditions" => array("id" => $id),
            "row_array" => true,
        ))['datas'];

        $datas = $table->getAllVdetail($loc_id);

        $min = $table->getFirstLayer($loc_id);
        $max = $table->getLastLayer($loc_id);

        $newkey = searchForIdInt($layer, $datas, 'layer');
        $oldkey = searchForIdInt($model['layer'], $datas, 'layer');
        $oldLayering = $model['layer'];

        $this->db->trans_begin();

        if ($oldLayering > $layer) {
            for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(
                    array(
                        'layer' =>  $datas[$i]['layer'] + 1,
                    ),
                    array(
                        'id' => $datas[$i]['id']
                    ), array("is_version" => true)
                );
            }
            $table->update(
                array(
                    'layer' => $layer,
                ),
                array(
                    'id' => $datas[$oldkey]['id']
                ), array("is_version" => true)
            );
        } else if ($oldLayering < $layer) {
            for ($i = $oldkey + 1 ; $i <= $newkey ; $i++) {
                $table->update(
                    array(
                        'layer' =>  $datas[$i]['layer'] - 1,
                    ),
                    array(
                        'id' => $datas[$i]['id']
                    ), array("is_version" => true)
                );
            }
            $table->update(
                array(
                    'layer' =>  $layer,
                ),
                array('id' => $datas[$oldkey]['id']), array("is_version" => true)
            );
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();
            $message["is_error"] = false;
            $message['notif_title']   = "Done!";
            $message['notif_message'] = "Visualizer Location Detail Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }

}
