<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Visualizer Location Controller.
 */
class Location_manager extends Baseadmin_Controller  {

    private $_title         = "Visualizer Location";
    private $_title_page    = '<i class="fa-fw fa fa-files-o"></i> Visualizer Location ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "vlocation";
    private $_back          = "/manager/visualize/location/lists";
    private $_js_path       = "/js/pages/visualize/manager/location/";
    private $_view_folder   = "visualize/manager/location/";

    private $_table         = "dtb_visual_location";
    private $_table_aliases = "vloc";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Visualizer Location
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Visualizer Location</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Visualizer Location</li>',
        );

        $this->load->model('visualize/Location_model');

        //get all ordering
        $data['ordering_data'] = $this->Location_model->getAllOrdering();

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an visualizer_location
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/visualize/location/lists">Visualizer Location</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Visualizer Location</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Visualizer Location</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => $this->_js_path . "create.js",
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an visualizer_location
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/visualize/location/lists">Visualizer Location</a></li>';

        //load the model.
        $data['item'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $id));
        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Visualizer Location</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Visualizer Location</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data visualizer_location
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('id','name','is_show','is_interior','ordering');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => $select,
            "order_by"        => array($column_sort => $sort_dir),
            "limit"           => $limit,
            "start"           => $start,
            "conditions"      => $conditions,
            "filter"          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas["total"];

        $output = array(
            "data"            => $datas["datas"],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type("application/json");
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_version" => true,
            "ordering"   => true
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id          = sanitize_str_input($this->input->post('id'), "numeric");
        $name        = $this->input->post('name');
        $is_show     = $this->input->post('is_show');
        $data_image  = $this->input->post('data-image');
        $is_interior = $this->input->post('is_interior');

        $arrayToDB = array(
            'name'        => $name,
            'is_show'     => $is_show,
            'is_interior' => $is_interior,
        );


        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $image = $this->upload_image("", "upload/visual/location", "image-file", $data_image, 640, 400, $id);

            //begin transaction.
            $this->db->trans_begin();

            // insert or update?
            if ($id == "") {

                if (!empty($image)) {
                    $arrayToDB['image_url'] = $image;
                }

                // insert to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert($arrayToDB, $extra_param);

            } else {
                // condition for update.
                $condition = array("id" => $id);
                $get_img = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                        "select"     => array("image_url"),
                        "conditions" => $condition,
                        "row_array"  => TRUE
                    )
                )["datas"];

                //update.
                if (!empty($image) && isset($get_img['image_url']) && !empty($get_img['image_url'])) {
                    unlink( FCPATH . $get_img['image_url'] );
                }

                if (!empty($image)) {
                    $arrayToDB['image_url'] = $image;
                }

                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update($arrayToDB, $condition, $extra_param);
            }

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "New Visualizer Location has been added.";
                } else {
                    $message['notif_message'] = "New Visualizer Location has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/visualize/location/lists";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function ordering() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id    = $this->input->post('id');
        $order = $this->input->post('val');

        $this->load->model("visualize/Location_model");

        $table = $this->Location_model;
        $model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        $datas = $table->getAllSlider();

        $min = $table->getFirstOrdering();
        $max = $table->getLastOrdering();

        $newkey = searchForIdInt($order, $datas, 'ordering');
        $oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

        $oldOrdering = $model['ordering'];

        $this->db->trans_begin();

        if ($oldOrdering > $order) {
            //smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
            for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']+1,
                ),array('id' => $datas[$i]['id']), array("is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array("is_version" => true));

        } else if ($oldOrdering < $order) {
            //smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
            for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']-1,
                ),array('id' => $datas[$i]['id']), array("is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array("is_version" => true));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title']   = "Done!";
            $message['notif_message'] = "Visualizer Location Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
