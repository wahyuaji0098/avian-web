<div class="container">
    <div class="section section-nocrumb section-thirds gridds gridwidth8">
        <div class="section_content griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <div class="griddsitem smallsquare grid_type_a"></div>
            <div class="griddsitem smallsquare grid_type_d nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_b nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_e nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_e nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_b"></div>
            <div class="griddsitem smallsquare grid_type_c nomob-3"></div>
            <div class="griddsitem fouronesquare grid_type_f squareplaintext">
                <span>
                    MAAF, HALAMAN YANG ANDA CARI TIDAK DITEMUKAN
                </span>
            </div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob"></div>
            <div class="griddsitem medsquare grid_type_a error">
                <div class="code">404</div>
                <div class="desc">HALAMAN TIDAK DI TEMUKAN</div>
            </div>
            <div class="griddsitem smallsquare grid_type_e nomob"></div>
            <div class="griddsitem smallsquare grid_type_b"></div>
            <div class="griddsitem smallsquare grid_type_c"></div>
            <div class="griddsitem smallsquare grid_type_e nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_c nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_a nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_d nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_d nomob-3"></div>
            <a href="/" class="griddsitem twonesquare grid_type_f squareplaintext">
                <span>
                    KEMBALI KE BERANDA
                </span>
            </a>
            <a href="/" class="griddsitem medsquare grid_type_b logolink"></a>
            <div class="griddsitem smallsquare grid_type_e"></div>
            <div class="griddsitem smallsquare grid_type_a nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_c nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_b"></div>
            <div class="griddsitem smallsquare grid_type_b"></div>
            <div class="griddsitem smallsquare grid_type_e"></div>
            <div class="griddsitem smallsquare grid_type_c nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_d nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_e nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_c nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-2"></div>
            <div class="griddsitem smallsquare grid_type_c"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
            <div class="griddsitem smallsquare grid_type_f nomob-3"></div>
        </div>
    </div>
</div>
