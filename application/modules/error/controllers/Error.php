<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends Basepublic_Controller  {

    /**
	 * constructor.
	 */
    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
        $this->output->set_status_header('404');

        $page = $this->uri->segment(1,0);

        if ($page == "manager") {
            $header = array(
                "title" => "Error 404",
                "breadcrumb" => "<li>Home</li><li>Error</li>",
            );

            $footer = array("css" => '/css/error_manager.css');

            if ($this->session->has_userdata(ADMIN_SESSION)) {
                $this->load->view(MANAGER_HEADER,$header);
                $this->load->view('error/manager/error_manager');
                $this->load->view(MANAGER_FOOTER,$footer);
            } else {
                $this->load->view(MANAGER_HEADER_SIGNIN,$header);
                $this->load->view('error/manager/error_manager');
                $this->load->view(MANAGER_FOOTER_SIGNIN,$footer);
            }
        } else {
            $header = array(
                "header" => array(
                    "title" => "Error 404",
                    "meta_desc" => "Page Not Found",
                    "meta_keys" => "Error"
                ),
            );

            $this->load->view(FRONT_HEADER,$header);
            $this->load->view('error/front/error');
            $this->load->view(FRONT_FOOTER);
        }


    }
}
