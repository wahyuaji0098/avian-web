<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 016
     * logout
     * format , api_key
     */
    public function index_post () {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $member = $this->check_api_key("post");

        $this->load->model("member/Device_model");

        //delete api key
        $this->Device_model->delete(array("id" => $member['device_id']), array("is_permanently" => true));

        $this->response($this->_result_OK (), REST_Controller::HTTP_OK);

    }


}
