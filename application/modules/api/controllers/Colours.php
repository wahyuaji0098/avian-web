<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Colours extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 008
     * get all pallete list and colours
     * format , pallete_version , color_version
     */
    public function index_get () {
        $pallete_version = $this->get("pallete_version") ? $this->get("pallete_version") : 0;
        $color_versions = $this->get("color_version") ? $this->get("color_version") : 0;

        //get all colors
        $colors = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data(array(
            "select" => array("id as color_id" , "name as color_name", "code as color_code", "red as r", "green as g", "blue as b", "hex_code" , "version", "status" , "is_show"),
            "conditions" => array(
                "version > " => $color_versions,
            ),
        ))['datas'];

		//get all pallete
		$pallete = $this->_dm->set_model("dtb_pallete", "dp", "id")->get_all_data(array(
            "select" => "id as pallete_id , name as pallete_name, pallete_for as pallete_to_product_id, color_ids, version, status , is_show, visualizer_type",
            "conditions" => array(
                "version > " => $pallete_version,
                "color_ids != " => "",
            ),
        ))['datas'];

        $this->response($this->_result_OK (array(
            "palletes" 	=> $pallete,
            "colors" 	=> $colors,
        )), REST_Controller::HTTP_OK);

    }

}
