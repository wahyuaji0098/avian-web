<?php defined('BASEPATH') or exit('No direct script access allowed');

class Visualizer extends Baseapi_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 017
     * get all visualizer, location
     * format, visualizer_version , location_version
     */
    public function index_get()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        $visualizer_version = $this->get("visualizer_version") ? $this->get("visualizer_version") : 0;
        $location_version = $this->get("location_version") ? $this->get("location_version") : 0;
        $palette_layer_version = $this->get("palette_layer_version") ? $this->get("palette_layer_version") : 0;

        $this->load->model("visualize/Detail_model");

        //get visual location result
        $v_result = $this->_dm->set_model("dtb_visual_result", "dvr", "dvr.id")->get_all_data(array(
            "select" => array(
                "dvr.id as visualizer_id", "dvr.member_id", "(select name from dtb_member where dtb_member.id = dvr.member_id) as member_name", "dvr.location_id", "dvl.name as location_name",
                "(ifnull(dvr.result_image_url,dvl.image_url)) as result_image_url", "dvr.result_image_large",
                "dvr.name", "dvr.is_published", "publish_date", "dvr.version", "dvr.is_show", "dvr.status"
            ),
            "left_joined" => array(
                "dtb_visual_location dvl" => array("dvl.id" => "dvr.location_id"),
            ),
            "conditions" => array(
                "dvr.version > " => $visualizer_version
            )
        ))['datas'];

        //get location and detail location
        $v_location = $this->_dm->set_model("dtb_visual_location", "dvl", "id")->get_all_data(array(
            "select" => "id,name,image_url,is_show,is_interior,version,ordering",
            "conditions" => array(
                "version > " => $location_version
            )
        ))['datas'];

        if (count($v_location) > 0) {
            foreach ($v_location as $key => $model) {
                $v_location[$key]['layers'] = $this->Detail_model->getAllVdetailAPI($model['id']);
            }
        }

        //get pallete layer
        $v_pallete = $this->_dm->set_model("dtb_visual_pallete", "dvp", "id")->get_all_data(array(
            "conditions" => array(
                "version > " => $palette_layer_version
            )
        ))['datas'];

        $this->response($this->_result_OK(array(
            "lists"    => $v_result,
            "locations" => $v_location,
            "palette_layer" => $v_pallete,
        )), 200);
    }
}
