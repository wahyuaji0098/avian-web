<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Store extends Baseapi_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * API - 012
     * get all store list and detail
     * format , store_version , category_version
     */
    public function index_get () {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $store_version = $this->get("store_version") ? $this->get("store_version") : 0;
        $store_version_all = $this->get("store_version_all") ? $this->get("store_version_all") : 0;


        //get current store version all
		$current_store_version_all = $this->_dm->set_model("dtb_version", "dv", "id")->get_all_data(array(
            "select" => array("IFNULL(version,0) as version"),
            "conditions" => array("name" => "store"),
            "row_array" => true,
        ))['datas']['version'];

        //get all store list and detail
        if ($store_version_all > $current_store_version_all) {
            $stores = array();
        } else {
            $conditions = array();

            if ($store_version_all == $current_store_version_all) {
                $conditions = array(
                    "version > " => $store_version,
                );
            }

            $stores = $this->_dm->set_model("dtb_store")->get_all_data(array(
                "select" => "id as store_id , name as store_name, latitude, longitude, address as short_address , full_address, phone, website, email, fax, opening_hour as opening_hours, store_photo as store_photo_url, description as store_description, can_custom_color, avail_products as sells_products, version, status , is_show",
                "conditions" => $conditions,
            ))['datas'];
        }

		$deleted = ($store_version_all < $current_store_version_all) ? 1 : 0;

        //load model product category

        $this->load->model("product/Category_model");

        //get categories product
        $categories = $this->Category_model->getCategoryListFilter();

        $this->response($this->_result_OK (array(
            "stores" => $stores,
            "products" => $categories,
            "store_version_all" => $current_store_version_all,
            "deleted" => $deleted,
        )), 200);
    }

}
