<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 002
     * api for login
     */

    function index_post() {
        $parameter_invalid = $this->_result_NG (sprintf(ERROR_CODE_12,""),12);

        $email = $this->post("email") ? $this->post("email") : "";
        $username = $this->post("username") ? $this->post("username") : "";
        $password = $this->post("password") ? $this->post("password") : "";
        $device_token = $this->post("device_token") ? $this->post("device_token") : "";
        $is_password = $this->post("is_forgot_password") ? $this->post("is_forgot_password") : 0;
        $from = $this->post("from") ? $this->post("from") : "1";
        $fb_id = $this->post("fb_id") ? $this->post("fb_id") : "";
        $fb_name = $this->post("fb_name") ? $this->post("fb_name") : "";

        $this->load->model("member/Member_model");
        $this->load->model("member/Device_model");

		if ($fb_id != "") {
			if ($fb_name == "" || $from == "") {
				$this->response($parameter_invalid, 200);
			}

			//check if member is deactivate
			$member = $this->Member_model->get_all_data (array(
                "conditions" => array(
                    "fb_id" => $fb_id,
                ),
                "row_array" => true
            ))['datas'];

			if (isset($member['status']) && $member['status'] == STATUS_DELETE) {
				$member_not_exist = $this->_result_NG (ERROR_CODE_14,14);
				$this->response($member_not_exist, 200);
			} else {
				//if member not exist yet.
				if (!$member) {
					//insert to member
					$insert = $this->Member_model->insert(array (
						"name" 		=> $fb_name,
						"fb_id" 	=> $fb_id,
						"email" 	=> $email,
					));

					$member = $this->Member_model->get_all_data (array(
                        "find_by_pk" => array($insert),
                        "row_array" => true
                    ))['datas'];
				}
			}

		} else {

			//if flag is_password is not send
			if ($is_password === "") {
				$this->response($parameter_invalid, 200);
			}

			//if from forgot password
			if ($is_password == "1") {
				if (!$email) {
					$this->response($parameter_invalid, 200);
				}
				$password = false;

				//check member if exist
				$member = $this->Member_model->get_all_data(array(
                    "conditions" => array(
                        "email" => $email
                    ),
                    "row_array" => true,
                    "status" => STATUS_ACTIVE,
                ))['datas'];
			} else {
				if (!$username || !$password) {
					$this->response($parameter_invalid, 200);
				}

				//check member if exist
				$member = $this->Member_model->check_login ($username,$password);
			}

			if (!$member) {
				$member_not_exist = $this->_result_NG (ERROR_CODE_13,13);
				$this->response($member_not_exist, 200);
			}
		}

        //if forgot password , then send email
        if ($is_password == "1") {

            $this->Member_model->sendResetPassword($member,$member['id']);

            //return ok
            $this->response($this->_result_OK (), 200);

        } else {
            //generate api key, and login
            $api_key = $this->Device_model->generate_api_key();

            //update to device
            $this->Device_model->insert(array(
                "api_key" => $api_key,
                "member_id" => $member['id'],
                "device_token" => $device_token,
                "type" => $from,
                "push_setting" => (($device_token == "") ? 2 : 1),
            ), array("is_direct" => true));

            //set login time
            $this->Member_model->update (array(
                "lastlogin_date" => date("Y-m-d H:i:s"),
                "unique_code" => null,
            ), array(
                "id" => $member['id']
            ));

            //return ok
            $this->response($this->_result_OK (array(
                "api_key" => $api_key,
                "user_id" => $member['id'],
                "name" => $member['name'],
            )), 200);
        }
    }

}
