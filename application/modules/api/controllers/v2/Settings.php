<?php defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends Baseapi_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 034
     * get push setting
     * format, api_key
     */
    public function index_get()
    {
        $member = $this->check_api_key("get");

        $this->response($this->_result_OK(array(
            "push_new_article_is_on"                    => $member['push_new_article_is_on'],
            "push_new_event_is_on"                      => $member['push_new_event_is_on'],
            "push_new_promo_is_on"                      => $member['push_new_promo_is_on'],
            "push_new_voucher_is_on"                    => $member['push_new_voucher_is_on'],
            "push_almost_expired_voucher_reminder_is_on"=> $member['push_almost_expired_voucher_reminder_is_on'],
            "device_token"                              => $member['device_token'],
        )), 200);
    }

    /**
     * API - 035
     * update push settings
     * format, api_key, push_new_article_is_on, push_new_voucher_is_on, push_new_event_is_on, push_new_promo_is_on, push_almost_expired_voucher_reminder_is_on
     */

    public function index_post()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        $push_new_article_is_on = $this->post("push_new_article_is_on") ? $this->post("push_new_article_is_on") : "0";
        $push_new_voucher_is_on = $this->post("push_new_voucher_is_on") ? $this->post("push_new_voucher_is_on") : "0";
        $push_new_event_is_on = $this->post("push_new_event_is_on") ? $this->post("push_new_event_is_on") : "0";
        $push_new_promo_is_on = $this->post("push_new_promo_is_on") ? $this->post("push_new_promo_is_on") : "0";
        $push_almost_expired_voucher_reminder_is_on = $this->post("push_almost_expired_voucher_reminder_is_on") ? $this->post("push_almost_expired_voucher_reminder_is_on") : "0";

        $device_token = $this->post("device_token") ?  $this->post("device_token") : "";

        //check if all param had been sent
        $member = $this->check_api_key("post");

        //check if push notif on not send
        if ($push_new_article_is_on != "0" && $push_new_article_is_on != "1") {
            $this->parameter_invalid("push_new_article_is_on");
        }
        if ($push_new_voucher_is_on != "0" && $push_new_voucher_is_on != "1") {
            $this->parameter_invalid("push_new_voucher_is_on");
        }
        if ($push_new_event_is_on != "0" && $push_new_event_is_on != "1") {
            $this->parameter_invalid("push_new_event_is_on");
        }
        if ($push_new_promo_is_on != "0" && $push_new_promo_is_on != "1") {
            $this->parameter_invalid("push_new_promo_is_on");
        }
        if ($push_almost_expired_voucher_reminder_is_on != "0" && $push_almost_expired_voucher_reminder_is_on != "1") {
            $this->parameter_invalid("push_almost_expired_voucher_reminder_is_on");
        }

        if ($push_new_article_is_on == 0
            && $push_new_event_is_on == 0 ) {
            $push_setting = 2;
        }
        else {
            $push_setting = 1;
        }

        $datas_update = array(
            "push_article"  => $push_new_article_is_on,
            "push_event"    => $push_new_event_is_on,
            "push_promo"    => $push_new_promo_is_on,
            "push_voucher"  => $push_new_voucher_is_on,
            "push_reminder" => $push_almost_expired_voucher_reminder_is_on,
            "push_setting" => $push_setting,
        );

        if ($device_token != "") {
            $datas_update['device_token'] = $device_token;
        }

        $this->load->model("member/Device_model");

        //update push notif setting
        $this->Device_model->update($datas_update, array("api_key" => $member['api_key']), array("is_direct" => true));

        //recheck and update modified settings.
        $member = $this->check_api_key("post");

        //return !
        $this->response($this->_result_OK(array(
            "push_new_article_is_on"                    => $member['push_new_article_is_on'],
            "push_new_event_is_on"                      => $member['push_new_event_is_on'],
            "push_new_promo_is_on"                      => $member['push_new_promo_is_on'],
            "push_new_voucher_is_on"                    => $member['push_new_voucher_is_on'],
            "push_almost_expired_voucher_reminder_is_on"=> $member['push_almost_expired_voucher_reminder_is_on'],
            "device_token"                              => $member['device_token'],
        )), 200);
    }
}
