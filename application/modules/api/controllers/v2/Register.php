<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Register extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 010
     * register new member
     */

    function index_post() {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $from = $this->post("from") ? $this->post("from") : "";
        $username = $this->post("username") ? $this->post("username") : "";
        $name = $this->post("fullname") ? $this->post("fullname") : "";
        $email = $this->post("email") ? $this->post("email") : "";
        $password = $this->post("password") ? $this->post("password") : "";
        $device_token = $this->post("device_token") ? $this->post("device_token") : "";

        $this->load->model("member/Member_model");
        $this->load->model("member/Device_model");

		//check if all param had been sent
        if (trim($username) == "" || trim($email) == "" || trim($password) == "" || trim($from) == "") {
            $this->parameter_invalid("Username, email, password dan from tidak boleh kosong.");
        }

        //check email member if exist
        $member = $this->Member_model->get_all_data(array(
            "conditions" => array(
                "email" => $email
            ),
            "row_array" => true,
            "status" => STATUS_ACTIVE,
        ))['datas'];

        //if email is already exist in database
        if ($member) {
            //if member status deleted , return error should activated by admin
            if ($member['status'] == STATUS_DELETE) {
                $this->response($this->_result_NG (ERROR_CODE_14,14), 200);
            } else if ($member['status'] == STATUS_ACTIVE) {
                //if member is active, return email cannot be used
                $this->response($this->_result_NG (ERROR_CODE_15,15), 200);
            }
        }

        //check username if exist
        if ($username != "") {
            //check username
            $uname_exist = $this->Member_model->get_all_data(array(
                "conditions" => array(
                    "username" => $username
                ),
                "row_array" => true,
                "status" => STATUS_ACTIVE,
            ))['datas'];

            if ($uname_exist) {
                if ($uname_exist['status'] == STATUS_DELETE) {
                    $this->response($this->_result_NG (ERROR_CODE_14,14), 200);
                } else if ($uname_exist['status'] == STATUS_ACTIVE) {
                    //if member is active, return email cannot be used
                    $this->response($this->_result_NG (ERROR_CODE_16,16), 200);
                }
            }
        }

        $datas = array(
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'username' => $username,
        );

        //create new member
        $member_id = $this->Member_model->insert($datas);

        //send email registration
        //$this->Member_model->sendmailRegister($datas);
 
        // Get POINT after register
        $add_point = $this->Dynamic_model->set_model("trs_member_point","point","id")->insert(
            array(
                "member_id"      => $member_id,
                "point_mutation" => POINT_REGISTER,
                "activity_code"  => CODE_REGISTER,
                "description"    => DESC_REGISTER
            )
        );

        //generate api key
        $api_key = $this->Device_model->generate_api_key();

        //update to dtb device
        $this->Device_model->insert(array(
            "api_key" => $api_key,
            "member_id" => $member_id,
            "device_token" => $device_token,
            "type" => $from,
			"push_setting" => (($device_token == "") ? 2 : 1),
        ), array("is_direct" => true));

        $member = $this->Member_model->get_all_data (array(
            "find_by_pk" => array($member_id),
            "row_array" => true
        ))['datas'];

        //set login time
        $this->Member_model->update (array(
            "lastlogin_date" => date("Y-m-d H:i:s"),
            "unique_code" => null,
        ), array(
            "id" => $member['id']
        ));

        // Ambil point member
        $member_point = $this->_dm->set_model('trs_member_point')->get_all_data(array(
            "select" => array("SUM(point_mutation) as total_point"),
            "conditions" => array("member_id" => $member['id']),
            "row_array" => true
        ))['datas'];

        //return ok
        $this->response($this->_result_OK (array(
            "api_key" => $api_key,
            "user_id" => $member['id'],
            "poin" => $member_point['total_point'],
            "username" => $member['username'],
            "email" => $member['email'],
            "fullname" => $member['name'],
        )), 200);

    }

}
