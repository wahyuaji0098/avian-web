<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("member/Member_model");
    }

    /**
     * API - 024
     * get all member vouchers list
     */
    public function voucher_get () {
    	// Check for api_key valid or not valid or not.
        $api = $this->check_api_key("get");

        $data = $this->_dm->set_model("trs_voucher_member", "tvm", "tvm.id")->get_all_data(array(
            "select" => "tvm.id, tvm.voucher_id, tvm.status_claimed, tvm.unique_code, tvm.bought_date, tvm.claimed_date, mv.title, mv.price, mv.image_url, mv.periode_start, mv.periode_end, mv.stok, mv.summary, mv.how_to, mv.terms",
            "joined" => array(
                "mst_voucher mv" => array("mv.id" => "tvm.voucher_id")
            ),
            "conditions" => array(
                "member_id" => $api['member_id']
            ),
        ))['datas'];

        $models = array();

        if (count($data) > 0) {
            foreach ($data as $model) {
                //replace spaces
                $url = str_replace(' ', '-', $model['title']);
                //convert url
                $model['share_url'] = base_url(). '' . 'vouchers/detail/'. $model['voucher_id'] . '-' .$url;
                array_push($models, $model);
            }
        }

        $this->response($this->_result_OK (array(
            "vouchers" => $models
        )), 200);
    }

    /**
     * API - 025
     * change profile
     */
    public function changeprofile_post () {
        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        $fullname = $this->post("fullname") ? $this->post("fullname") : "";
        $email = $this->post("email") ? $this->post("email") : "";
        $profile_picture = (isset($_FILES["profile_picture"])) ? $_FILES["profile_picture"] : "";

        // Validasi email required
        if (trim($email) == "") {
            $this->parameter_invalid("Email is required");
        }
        else {
            // Cek email nya apakah sudah ada di db
            $check_email = $this->_dm->set_model('dtb_member')->get_all_data(array(
                "conditions" => array('email' => $email),
                "row_array" => true
            ))['datas'];

            // Email ada di db dan bukan punya diri sendiri
            if($check_email && $check_email['email'] != $api['email']){
                $this->response($this->_result_NG (ERROR_CODE_15,15), 200);
            }
        }

        $arrayToDB = array(
            'name' => $fullname,
            'email' => $email,
        );

        $conditions = array("id" => $api['id']);

        // Upload profile picture
        if (isset($profile_picture['size']) && $profile_picture['size'] > 0) {
            $result_upload_file = $this->upload_file ("profile_picture", url_title($fullname, 'underscore')."_".strtotime("now"), false, 'upload/member/profile');

            //if image not uploaded.
            if (!isset($result_upload_file['uploaded_path'])) {
                $this->response($this->_result_NG(ERROR_CODE_19, 19), 200);
            }
            else {
                $arrayToDB["profile_picture_url"] = $result_upload_file['uploaded_path'];
            }
        }

        $this->db->trans_begin();
        $this->_dm->set_model('dtb_member')->update($arrayToDB, $conditions);

        // Ambil data member yang baru diupdate
        $updated_member = $this->_dm->set_model('dtb_member')->get_all_data(array(
            'row_array' => true,
            'conditions' => array('id' => $api['id'])
        ))['datas'];

        // Ambil total point untuk member ini
        $member_point = $this->_dm->set_model('trs_member_point')->get_all_data(array(
            "select" => array("SUM(point_mutation) as total_point"),
            "conditions" => array("member_id" => $api['id']),
            "row_array" => true
        ))['datas'];

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
        }
        else {
            $this->db->trans_commit();
            $this->response($this->_result_OK(array(
                "fullname"    => $updated_member['name'],
                "poin" => $member_point['total_point'],
                "username"    => $updated_member['username'],
                "email"    => $updated_member['email'],
                "profile_picture_url"    => $updated_member['profile_picture_url'],
            )), 200);
        }
    }

    /**
     * API - 026
     * userinfo
     */
    public function userinfo_get () {
        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("get");

        // Ambil total point untuk member ini
        $member_point = $this->_dm->set_model('trs_member_point')->get_all_data(array(
            "select" => array("SUM(point_mutation) as total_point"),
            "conditions" => array("member_id" => $api['id']),
            "row_array" => true
        ))['datas'];

        $this->response($this->_result_OK(array(
            "user_id"    => $api['id'],
            "poin"    => $member_point['total_point'],
            "username"    => $api['username'],
            "email"    => $api['email'],
            "fullname"    => $api['name'],
            "profile_picture_url"    => $api['profile_picture_url'],
        )), 200);
    }

    /**
     * API - 027
     * checkin
     */
    public function checkin_post () {
        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        $today = date('Y m d');
        $last_login_date = date('Y m d', strtotime($api['lastlogin_date']));

        $point = $this->Member_model->checkin($api['member_id']);

        if(!$point) {
            $this->_dm->set_model('trs_member_point')->insert(array(
                "member_id"      => $api['member_id'],
                "point_mutation" => POINT_LOGIN_DAILY,
                "activity_code"  => CODE_LOGIN_DAILY,
                "description"    => DESC_LOGIN_DAILY
            ));
        }

        // Ambil point member
        $member_point = $this->_dm->set_model('trs_member_point')->get_all_data(array(
            "select" => array("SUM(point_mutation) as total_point"),
            "conditions" => array("member_id" => $api['id']),
            "row_array" => true
        ))['datas'];

        //return ok
        $this->response($this->_result_OK(array(
            "user_id"    => $api['id'],
            "poin"    => $member_point['total_point'],
            "username"    => $api['username'],
            "email"    => $api['email'],
            "fullname"    => $api['name'],
            "profile_picture_url"    => $api['profile_picture_url'],
        )), 200);
    }

    /**
     * API - 028
     * changepassword
     */
    public function changepassword_post () {
        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        $old_pass = trim($this->post("old_pass")) ? $this->post("old_pass") : "";
        $new_pass = trim($this->post("new_pass")) ? $this->post("new_pass") : "";

        if($old_pass == "" || $new_pass == "") {
            $this->response($this->_result_NG (ERROR_CODE_12,12), 200);
        }

        // Cek password lama
        $check_old_pass = $this->Member_model->check_password($api['password'], $old_pass);

        if(!$check_old_pass) {
            $this->response($this->_result_NG (ERROR_CODE_27,27), 200);
        }
        else {
            $arrayToDB = array("password" => $new_pass);
            $conditions = array("id" => $api['member_id']);

            $this->db->trans_begin();
            $this->Member_model->update($arrayToDB, $conditions);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
            }
            else {
                $this->db->trans_commit();

                $this->response($this->_result_OK(array(
                    "datas"    => 'Change password success.',
                )), 200);
            }
        }
    }

    /**
     * API - 030
     * member point history
     */
    public function pointhistory_get () {
        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("get");

        $result = $this->_dm->set_model('trs_member_point')->get_all_data(array(
            'select'     => array('created_date', 'description', 'activity_code', 'point_mutation'),
            'conditions' => array('member_id'=> $api['member_id']),
            'order_by'   => array('created_date' => 'desc'),
            'debug'      => false,
        ))['datas'];

        $this->response($this->_result_OK(
            $result
        ), 200);
    }

    /**
     * API - 033
     * member voucher detail.
     */
    public function voucherdetail_post () {

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");
        $member_voucher_id = sanitize_str_input($this->post("member_voucher_id"));

        //check voucher dan kode merchant.
        if (empty($member_voucher_id)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, "Voucher"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        //query to get voucher information.
        $data = $this->_dm->set_model("mst_voucher", "mv", "mv.id")->get_all_data(array(
            "select"        => "mv.id as voucher_id, tvm.id as id, title, price, image_url, periode_start, periode_end, stok, summary, how_to, terms, tvm.status_claimed, tvm.unique_code as voucher_kode, tvm.bought_date, tvm.claimed_date",
            "joined"        => array(
                "trs_voucher_member tvm" => array("tvm.voucher_id" => "mv.id"),
            ),
            "conditions"    => array(
                "tvm.id"    => $member_voucher_id,
                "tvm.member_id" => $api['member_id'],
            ),
            "row_array"     => true,
            "debug"         => false,
        ))['datas'];

        if (empty($data)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, "Voucher"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        //replace spaces
        $url = str_replace(' ', '-', $data['title']);

        //convert url
        $data['share_url']       = base_url(). '' . 'vouchers/detail/'. $data['voucher_id'] . '-' .$url;

        //response done.
        $this->response($this->_result_OK(
            $data
        ), 200);
    }
}
