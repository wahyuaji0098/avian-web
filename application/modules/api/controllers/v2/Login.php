<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 009
     * api for login
     */

    function index_post() {

        $email = $this->post("email") ? $this->post("email") : "";
        $username = $this->post("username") ? $this->post("username") : "";
        $password = $this->post("password") ? $this->post("password") : "";
        $device_token = $this->post("device_token") ? $this->post("device_token") : "";
        $is_password = $this->post("is_forgot_password") ? $this->post("is_forgot_password") : 0;
        $from = $this->post("from") ? $this->post("from") : "1";
        $fb_id = $this->post("fb_id") ? $this->post("fb_id") : "";
        $fb_name = $this->post("fb_name") ? $this->post("fb_name") : "";
        $google_id = $this->post("google_id") ? $this->post("google_id") : "";
        $google_name = $this->post("google_name") ? $this->post("google_name") : "";
        $profile_picture_url = $this->post("profile_picture_url") ? $this->post("profile_picture_url") : "";

        $this->load->model("member/Member_model");
        $this->load->model("member/Device_model");

        // Validasi parameter yang required
        if ($from == "") {
            $this->parameter_invalid("From parameter is required");
        }
        else if ($is_password === "") {
            $this->parameter_invalid("Forgot Password parameter is required");
        }

        // Validasi login dengan sosmed
		if ($fb_id != "") {
			if ($fb_name == "" || $email == "") {
				$this->parameter_invalid("Facebook name and email is required");
			}

			//check if member is deactivate
			$member = $this->Member_model->get_all_data (array(
                "conditions" => array(
                    "fb_id" => $fb_id,
                ),
                "row_array" => true
            ))['datas'];

			if (isset($member['status']) && $member['status'] == STATUS_DELETE) {
				$member_not_exist = $this->_result_NG (ERROR_CODE_14,14);
				$this->response($member_not_exist, 200);
			} else {
				//if member not exist yet.
				if (!$member) {
                    // Cek email sudah terpakai belum
                    $check_email = $this->Member_model->get_all_data (array(
                        "conditions" => array(
                            "email" => $email,
                        ),
                        "row_array" => true
                    ))['datas'];

                    if($check_email) {
                        // Email sudah terpakai
                        $this->response($this->_result_NG (ERROR_CODE_15,15), 200);
                    }
                    else {
                        //insert to member
                        $insert = $this->Member_model->insert(array (
                            "name"      => $fb_name,
                            "fb_id"     => $fb_id,
                            "email"     => $email,
                            "profile_picture_url"     => $profile_picture_url,
                        ));

                        // Get POINT after register
                        $this->_dm->set_model("trs_member_point","point","id")->insert(
                            array(
                                "member_id"      => $insert,
                                "point_mutation" => POINT_REGISTER,
                                "activity_code"  => CODE_REGISTER,
                                "description"    => DESC_REGISTER
                            )
                        );

                        $member = $this->Member_model->get_all_data (array(
                            "find_by_pk"    => array($insert),
                            "row_array"     => true
                        ))['datas'];
                    }
				} else {
                    //jika sudah ada fb id nya, update datanya
                    $this->Member_model->update (array(
                        "profile_picture_url"       => $profile_picture_url,
                        "username"                  => $username,
                        "name"                      => $fb_name,
                        "email"                     => $email,
                    ), array(
                        "id" => $member['id']
                    ));
                }
			}

		}
        else if ($google_id != "") {
            if ($google_name == "" || $email == "") {
                $this->parameter_invalid("Google name and email is required");
            }

            //check if member is deactivate
            $member = $this->Member_model->get_all_data (array(
                "conditions" => array(
                    "google_id" => $google_id,
                ),
                "row_array" => true
            ))['datas'];

            if (isset($member['status']) && $member['status'] == STATUS_DELETE) {
                $member_not_exist = $this->_result_NG (ERROR_CODE_14,14);
                $this->response($member_not_exist, 200);
            } else {
                //if member not exist yet.
                if (!$member) {
                    // Cek email sudah terpakai belum
                    $check_email = $this->Member_model->get_all_data (array(
                        "conditions" => array(
                            "email" => $email,
                        ),
                        "row_array" => true
                    ))['datas'];

                    if($check_email) {
                        // Email sudah terpakai
                        $this->response($this->_result_NG (ERROR_CODE_15,15), 200);
                    }
                    else {
                        //insert to member
                        $insert = $this->Member_model->insert(array (
                            "name"          => $google_name,
                            "google_id"     => $google_id,
                            "email"         => $email,
                            "profile_picture_url"         => $profile_picture_url,
                        ));

                        // Get POINT after register
                        $this->_dm->set_model("trs_member_point","point","id")->insert(
                            array(
                                "member_id"      => $insert,
                                "point_mutation" => POINT_REGISTER,
                                "activity_code"  => CODE_REGISTER,
                                "description"    => DESC_REGISTER
                            )
                        );

                        $member = $this->Member_model->get_all_data (array(
                            "find_by_pk" => array($insert),
                            "row_array" => true
                        ))['datas'];
                    }
                } else {
                    //jika sudah pernah login di google, maka update datanya
                    $this->Member_model->update (array(
                        "profile_picture_url"       => $profile_picture_url,
                        "username"                  => $username,
                        "name"                      => $google_name,
                        "email"                     => $email,
                    ), array(
                        "id" => $member['id']
                    ));
                }
            }

        }
        else {
			//if flag is_password is not send
			//if from forgot password
			if ($is_password == "1") {
				if (!$email) {
					$this->parameter_invalid("Email is required");
				}

				$password = false;

				//check member if exist
				$member = $this->Member_model->get_all_data(array(
                    "conditions" => array(
                        "email" => $email
                    ),
                    "row_array" => true,
                    "status" => STATUS_ACTIVE,
                ))['datas'];
			} else {
				//check member if exist
                if(trim($username) != "") {
                    $member = $this->Member_model->check_login ($username,$password);
                }
                else {
                    if(trim($email) != "") {
                        $member = $this->Member_model->check_login_by_email ($email,$password);
                    }
                    else {
                        $this->parameter_invalid("Username/email is required");
                    }
                }
			}

			if (!$member) {
				$member_not_exist = $this->_result_NG (ERROR_CODE_13,13);
				$this->response($member_not_exist, 200);
			}
		}

        //if forgot password , then send email
        if ($is_password == "1") {
            $this->Member_model->sendResetPassword($member,$member['id']);

            //return ok
            $this->response($this->_result_OK (), 200);

        } else {
            //cek lokasi user
            $user_ip = get_client_ip();
            $region_user = get_client_province_by_ip($user_ip);

            // get provinsi id
            $data_provinsi = $this->_dm->set_model('mtb_province')->get_all_data(array(
                "conditions" => array(
                    "name" => $region_user
                ),
                "row_array" => true
            ))['datas'];

            //generate api key, and login
            $api_key = $this->Device_model->generate_api_key();

            //update to device
            $this->Device_model->insert(array(
                "api_key" => $api_key,
                "member_id" => $member['id'],
                "device_token" => $device_token,
                "type" => $from,
                "push_setting" => (($device_token == "") ? 2 : 1),
                "lokasi" => $region_user,
                "province_id" => (isset($data_provinsi['id']) ? $data_provinsi['id'] : ""),
            ), array("is_direct" => true));

            //set login time
            $this->Member_model->update (array(
                "lastlogin_date"            => date("Y-m-d H:i:s"),
                "unique_code"               => null,
            ), array(
                "id" => $member['id']
            ));

            // Ambil point member
            $member_point = $this->_dm->set_model('trs_member_point')->get_all_data(array(
                "select" => array("SUM(point_mutation) as total_point"),
                "conditions" => array("member_id" => $member['id']),
                "row_array" => true
            ))['datas'];

            //check if member is deactivate
			$member = $this->Member_model->get_all_data (array(
                "conditions" => array(
                    "id" => $member['id'],
                ),
                "row_array" => true
            ))['datas'];

            //return ok
            $this->response($this->_result_OK (array(
                "api_key" => $api_key,
                "user_id" => $member['id'],
                "poin" => $member_point['total_point'],
                "username" => $member['username'],
                "email" => $member['email'],
                "fullname" => $member['name'],
                "profile_picture_url" => $member['profile_picture_url'],
            )), 200);
        }
    }

}
