<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 011
     * get all promo list
     */
    public function list_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        $data = $this->_dm->set_model("mst_promo")->get_all_data(array(
            "select" => "id, judul, image_url, popup_image_url, content, periode_start, periode_end, province_id, is_from_avian, is_show_as_popup, is_show, version, judul as share_url",
            "conditions" => array(
                "version > " => $version,
            )
        ))['datas'];

        $models = array();

        if (count($data) > 0) {
            foreach ($data as $model) {
                //replace spaces
                $url = str_replace(' ', '-', $model['share_url']);
                //convert url
                $model['share_url']       = base_url(). '' . 'promo/detail/'. $model['id'] . '-' .$url;
                array_push($models, $model);
            }
        }

        $this->response($this->_result_OK (array(
            "datas" => $models,
        )), 200);
    }
}
