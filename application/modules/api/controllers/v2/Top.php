<?php defined("BASEPATH") OR exit("No direct script access allowed");

class Top extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 006
     * Get all top data
     */
    public function data_get () {
        $country_version = sanitize_str_input($this->get("country_version"), "numeric");
        $province_version = sanitize_str_input($this->get("province_version"), "numeric");

        $unique_id = $this->get("device_identifier");

        $user_latitude = $this->get("user_latitude");
        $user_longitude = $this->get("user_longitude");

        if (empty($country_version)) $country_version = 0;
        if (empty($province_version)) $province_version = 0;

        $slider_front = $this->_dm->set_model("dtb_slider_mobile")->get_all_data(array(
            "select"     => "id, url, image_url_device, popup_image_url, is_show_as_popup",
            "conditions" => array("is_show" => 1),
            "order_by"   => array("ordering" => "asc"),
        ))["datas"];

        $articles = $this->_dm->set_model("dtb_article")->get_all_data(array(
            "select" => "id as article_id , title as article_title, type as article_type, image_url as article_image_url, image_device as article_device_image_url,  sticky_flag as is_sticky , date , version, status , is_show, pretty_url as share_url",
            "conditions" => array(
                "is_show" => 1,
            ),
            "order_by" => array(
                "created_date" => "desc",
            ),
            "limit"     => 5,
        ))["datas"];

        if (count($articles) > 0) {
            foreach ($articles as $key => $model) {
            //    $articles[$key]['article_content'] = preg_replace("/[\n\r]/","",$model['article_content']);
              //  $articles[$key]['article_content'] = str_replace('src="../../../upload/','src="'.base_url().'upload/',$model['article_content']);
                //$articles[$key]['article_content'] = str_replace('src="/upload/','src="'.base_url().'upload/',$model['article_content']);
                $articles[$key]['share_url']       = base_url(). '' . 'articles/detailM/'.$model['share_url'];
            }
        }
        //get all csr
        // $csr = $this->_dm->set_model("mst_csr", "mc", "mc.id")->get_all_data(array(
        //     "select" => "mc.id,  mc.judul, mc.content, mc.content_device, mc.is_show, mc.version, mc.image_landing, mc.ordering, mc.judul as share_url",
        //     "conditions" => array(
        //         "is_show" => SHOW,
                
        //     ),
            
          
        //     "order_by" => array("ordering" => "desc"),
        //     "limit"    => 5,
        // ))['datas'];



        //get all csr
        $csr = $this->_dm->set_model("mst_csr", "mc", "mc.id")->get_all_data(array(
            "select" => "mc.id,  mc.judul, mc.content, mc.content_device, mc.is_show, mc.version, mc.image_landing, mc.ordering, mc.judul as share_url",
            //,mca.pretty_url",
            "conditions" => array(
                "is_show" => SHOW,
                
            ),
            
          
            "order_by" => array("ordering" => "desc"),
            //"left_joined" => array("mst_csr_artikel mca" => array("mca.id" => "mc.artikel_csr_id"),),
            "limit"    => 5,
        ))['datas'];
        
        $csr_list = $this->_dm->set_model("mst_csr", "mc", "mc.id")->get_all_data(array(
            "select" => "
                 
                mca.pretty_url as share_url",
                
            
            "order_by" => array("ordering" => "desc"),
            "count_all_first" => true,
            "left_joined" => array(
                "mst_csr_artikel mca" => array("mca.id" => "mc.artikel_csr_id"),
            ),
            "conditions" => array("mc.is_show" => 1),
            "limit"    => 5,
        ))['datas'];
        
        if (count($csr_list) > 0) {
            foreach ($csr_list as $key => $model) {
            $csr_list[$key]['share_url']       = base_url(). '' . 'peduli/detailM/'.$model['share_url'];
            }
        }
        
        

        if (count($csr) > 0) {
            foreach($csr as $key => $value) {
                $csr_id = $value['id'];
                $url    = str_replace(' ', '-', $value['share_url']);
                //$csr[$key]['share_url'] = base_url(). 'peduli/detailM/'. $csr_id .'-'.$url;
                $csr[$key]['share_url'] = base_url(). 'peduli/detailM/'.$url;

                $slider = $this->_dm->set_model("trs_csr_slider")->get_all_data(array(
                    "select" => array("id", "image_slider", "ordering"),
                    "conditions" => array("csr_id" => $csr_id),
                    "order_by" => array("ordering" => "asc")
                ))['datas'];

                $csr[$key]['image_details'] = $slider;
                $csr[$key]['content_device'] = preg_replace("/[\n\r]/", "", $value['content_device']);
                $csr[$key]['content_device'] = str_replace('src="../../../upload/', 'src="'.base_url().'upload/', $value['content_device']);
                $csr[$key]['content_device'] = str_replace('src="../../upload/', 'src="'.base_url().'upload/', $value['content_device']);
                $csr[$key]['content_device'] = str_replace('src="/upload/', 'src="'.base_url().'upload/', $value['content_device']);

             }
        }
        
        //NEWCSR
        $articlesCSR = $this->_dm->set_model("mst_csr_artikel")->get_all_data(array(
            "select" => "pretty_url as share_url",
            "conditions" => array(
                "is_show" => 1,
            ),
            "order_by" => array(
                "created_date" => "desc",
            ),
            "limit"     => 5,
        ))["datas"];
        
        if (count($articlesCSR) > 0) {
            foreach ($articlesCSR as $key => $model) {
            $articlesCSR[$key]['share_url']       = base_url(). '' . 'peduli/detailM/'.$model['share_url'];
            }
        }
        
       
        
        
        
        
        
        
        
        
        
        

        $country = $this->_dm->set_model("mtb_country")->get_all_data(array(
            "select" => "id as country_id, name as country_name, is_show, version",
            "conditions" => array("version > " => $country_version),
        ))["datas"];

        $province = $this->_dm->set_model("mtb_province")->get_all_data(array(
            "select" => "id as province_id, name as province_name, country_id, is_show, version",
            "conditions" => array("version > " => $province_version),
        ))["datas"];

        // get 3 closest store.
        $this->load->model("store/Store_model");
		$m_store = new Store_model();
        $closest_store = $m_store->getClosestStore($user_latitude, $user_longitude, 10);

        // polling
        $poll = $this->_dm->set_model("dtb_polling", "dp", "dp.id_polling")->get_all_data(array(
            "select" => "
                dp.id_polling,
                end_date,
                question,
                (CASE WHEN v_dpd.device_identifier IS NULL THEN false ELSE true END) as pernah_jawab
            ",
            "left_joined" => array(
                "(
                    SELECT *
                    FROM dtb_polling_device dpd
                    WHERE
                        dpd.device_identifier = '".$unique_id."'
                ) v_dpd" => ["v_dpd.id_polling" => "dp.id_polling"],
            ),
            "conditions" => array(
                "is_show" => 1,
                "start_date <= now()" => null,
                "end_date >= now()" => null,
            ),
        ))["datas"];

        // ambil jawaban si polling.
        foreach ($poll as $key => $value) {
            $id_poll = $value['id_polling'];

            $poll[$key]['answers'] = $this->_dm->set_model("dtb_polling_answer")->get_all_data(array(
                "select" => "id_polling_answer, answer, score",
                "conditions" => array(
                    "id_polling" => $id_poll,
                ),
            ))["datas"];
        }

        // return
        $this->response($this->_result_OK (array(
            "polling" => $poll,
            "article" => $articles,
            "articlesCSR" => $articlesCSR,
            "csr_list"=>$csr_list,
            "slider" => $slider_front,
            "country" => $country,
            "province" => $province,
            "csr" => $csr,
            "store" => $closest_store,
        )), 200);

    }

    /**
     * kirim pilihan jawaban polling.
     * POST method.
     */
    public function polling_post () {
        $unique_id = $this->post("device_identifier");
        $id_poll = $this->post("id_polling");
        $id_answer = $this->post("id_polling_answer");

        if (empty($id_poll) || empty($unique_id) || empty($id_answer)) {
            $this->response($this->_result_NG(ERROR_CODE_30, 30), REST_Controller::HTTP_OK);
        }

        $check_id_poll = $this->_dm->set_model("dtb_polling", "dp", "dp.id_polling")->get_all_data(array(
            "select" => "dp.id_polling, v_dpd.device_identifier",
            "left_joined" => array(
                "(
                    SELECT *
                    FROM dtb_polling_device dpd
                    WHERE
                        dpd.device_identifier = '".$unique_id."'
                ) v_dpd" => ["v_dpd.id_polling" => "dp.id_polling"],
            ),
            "conditions" => array(
                "dp.id_polling" => $id_poll,
                "is_show" => 1,
                "start_date <= now()" => null,
                "end_date >= now()" => null,
                "v_dpd.device_identifier IS NOT NULL" => null,
            ),
            "row_array" => true,
        ))["datas"];

        if (!empty($check_id_poll)) {
            $this->response($this->_result_NG("Polling invalid atau sudah pernah dipilih sebelumnya atau sudah berlalu.", 999), REST_Controller::HTTP_OK);
        }

        $check_id_answer = $this->_dm->set_model("dtb_polling_answer", "dpa", "id_polling_answer")->get_all_data(array(
            "select" => "*",
            "conditions" => array(
                "id_polling" => $id_poll,
                "id_polling_answer" => $id_answer,
            ),
            "row_array" => true,
        ))["datas"];

        if (empty($check_id_answer)) {
            $this->response($this->_result_NG("Pilihan jawaban yang dipilih invalid.", 999), REST_Controller::HTTP_OK);
        }

        // tambahkan score.
        $score = $check_id_answer['score'] + 1;

        // update score.
        $update_score = $this->_dm->set_model("dtb_polling_answer")->update(
            array("score" => $score),
            array("id_polling_answer" => $id_answer, "id_polling" => $id_poll),
            array("is_direct" => true)
        );

        // insert dtb_polling_device.
        $insert_device = $this->_dm->set_model("dtb_polling_device")->insert(
            array("id_polling" => $id_poll, "device_identifier" => $unique_id),
            array("is_direct" => true)
        );

        // ambil data lagi buat dibalikin.
        // polling
        $poll = $this->_dm->set_model("dtb_polling", "dp", "dp.id_polling")->get_all_data(array(
            "select" => "
                dp.id_polling,
                end_date,
                question,
                (CASE WHEN v_dpd.device_identifier IS NULL THEN false ELSE true END) as pernah_jawab
            ",
            "left_joined" => array(
                "(
                    SELECT *
                    FROM dtb_polling_device dpd
                    WHERE
                        dpd.device_identifier = '".$unique_id."'
                ) v_dpd" => ["v_dpd.id_polling" => "dp.id_polling"],
            ),
            "conditions" => array(
                "is_show" => 1,
                "start_date <= now()" => null,
                "end_date >= now()" => null,
            ),
        ))["datas"];

        // ambil jawaban si polling.
        foreach ($poll as $key => $value) {
            $id_poll = $value['id_polling'];

            $poll[$key]['answers'] = $this->_dm->set_model("dtb_polling_answer")->get_all_data(array(
                "select" => "id_polling_answer, answer, score",
                "conditions" => array(
                    "id_polling" => $id_poll,
                ),
            ))["datas"];
        }

        // return
        $this->response($this->_result_OK (array(
            "polling" => $poll
        )), 200);

    }

    /**
     * Mechanism MEMBER POINT on TOP
     *     The mechanism of checking API_KEY if true means the user is logged in. if the user request, will get POINT,
     *     but if the request again in the same day,
     *     will not add another point (must be different day).
     */
    private function _set_rule_point() {

        // check api_key
        $parameter = $this->check_api_key("get");

        // api_key == TRUE
        if ($parameter) {
            // Check member, already get point from daily_login
            $check = $this->_dm->set_model("trs_member_point")->get_all_data(array(
                "conditions" => array(
                                    "member_id" => $parameter["member_id"],
                                    "DAY(created_date)" => date('d')
                                )
            ))["datas"];
            // if data check == empty then ... add_point else nothing
            if (empty($check)) {
                // Add point to trs_member_point
                $add_point = $this->Dynamic_model->set_model("trs_member_point","point","id")->insert(
                    array(
                        "member_id"      => $parameter["member_id"],
                        "point_mutation" => POINT_LOGIN_DAILY,
                        "activity_code"  => CODE_LOGIN_DAILY,
                        "description"    => DESC_LOGIN_DAILY
                    )
                );
            }
        }

    }

}
