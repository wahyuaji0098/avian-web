<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Colours extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 019
     * get all pallete list and colours
     * format , pallete_version , color_version
     */
    public function list_get () {
        $pallete_version = $this->get("pallete_version") ? $this->get("pallete_version") : 0;
        $color_version = $this->get("color_version") ? $this->get("color_version") : 0;

		//get all pallete
		$pallete = $this->_dm->set_model("dtb_pallete")->get_all_data(array(
            "select" => "id, name, code, status , is_show, image_url as pallete_cover_image_url, version",
            "conditions" => array(
                "version > " => $pallete_version,
            ),
        ))['datas'];

        $pallete_id = array_column($pallete, "id");
        $pallete_id = implode(",", $pallete_id);

        //get all pallete images for these selected palletes.
        $pallete_images = $this->_dm->set_model("dtb_pallete_images")->get_all_data(array(
            "select" => "id, image_url, pallete_id",
            "conditions" => array(
                "pallete_id in ($pallete_id)" => null,
            ),
        ))['datas'];

        //get all colors for these selected palletes.
        $color_ids = $this->_dm->set_model("mst_palette_color")->get_all_data(array(
            "select" => "id, color_id, palette_id as pallete_id",
            "conditions" => array(
                "palette_id in ($pallete_id)" => null,
            ),
        ))['datas'];

        //get all products for these selected palletes.
        $product_ids = $this->_dm->set_model("mst_palette_product")->get_all_data(array(
            "select" => "id, product_id, palette_id as pallete_id",
            "conditions" => array(
                "palette_id in ($pallete_id)" => null,
            ),
        ))['datas'];

        //get all colors by color version.
        $colors = $this->_dm->set_model("dtb_color")->get_all_data(array(
            "select" => "id, name, code, red, green, blue, hex_code as hex, status, is_show, version, CONCAT('".WEB_BASE_URL."/colours/list?q=', id) as share_url",
            "conditions" => array(
                "version > " => $color_version,
            ),
        ))['datas'];

        $this->response($this->_result_OK (array(
            "palletes" 	=> $pallete,
            "pallete_images" 	=> $pallete_images,
            "color_ids"  => $color_ids,
            "product_ids"  => $product_ids,
            "colors" => $colors
        )), REST_Controller::HTTP_OK);

    }

}
