<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 012
     * get all event list
     */
    public function list_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        $data = $this->_dm->set_model("mst_event")->get_all_data(array(
            "select" => "id, judul, image_url, popup_image_url, alamat, content, periode_start, periode_end, latitude, longitude, province_id, member_id, is_from_avian, is_show_as_popup, CONCAT('".WEB_BASE_URL."/event/detail/', id) as share_url, is_show, version",
            "conditions" => array(
                "version > " => $version,
            )
        ))['datas'];

        $this->response($this->_result_OK (array(
            "datas" => $data,
        )), 200);
    }
}
