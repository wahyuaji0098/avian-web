<?php defined('BASEPATH') or exit('No direct script access allowed');

class Voucher extends Baseapi_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 018
     * get all vouchers list.
     */
    public function list_get()
    {
        $version = $this->get("version") ? $this->get("version") : 0;

        $data = $this->_dm->set_model("mst_voucher")->get_all_data(array(
            "select" => "id, title, price, image_url, periode_start, periode_end, stok, summary, how_to, terms, is_show, status, version, title as share_url",
            "conditions" => array(
                "version > " => $version,
            )
        ))['datas'];

        $models = array();

        if (count($data) > 0) {
            foreach ($data as $model) {
                //replace spaces
                $url = str_replace(' ', '-', $model['share_url']);
                //convert url
                $model['share_url']       = base_url(). '' . 'vouchers/detail/'. $model['id'] . '-' .$url;
                array_push($models, $model);
            }
        }

        $this->response($this->_result_OK(array(
            "vouchers" => $models,
        )), 200);
    }

    /**
     * API - 021
     * buy voucher.
     */
    public function buy_post()
    {
        $voucher_id = sanitize_str_input($this->post("voucher_id"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        // if empty parameters send to ERROR_CODE_12 = parameter invalid.
        if (empty($voucher_id)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, "Voucher ID"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        //verify voucher ID, bisa dibeli kaga, dan masih dalam periode bisa dibeli apa tidak.
        $data = $this->_dm->set_model("mst_voucher", "mv", "mv.id")->get_all_data(array(
            "select"        => "mv.id, mv.title, mv.price, mv.stok, sum(tmp.point_mutation) as my_point",
            "row_array"     => true,
            "from"          => "mst_voucher mv, trs_member_point tmp",
            "conditions"    => array(
                "mv.id"        => $voucher_id,
                "mv.stok > 0"  => null,
                "now() >= mv.periode_start" => null,
                "now() <= mv.periode_end"   => null,
                "mv.is_show"   => 1,
                "tmp.member_id" => $api['member_id'],
            ),
            "status"        => STATUS_ACTIVE,
            "debug"         => false,

        ))['datas'];

        if (empty($data) || $data['stok'] == 0) {
            //stok habis.
            $this->response(
                $this->_result_NG(
                    ERROR_CODE_26,
                    26
                ),
                REST_Controller::HTTP_OK
            );
        }

        //check if the my point is not enough to buy the voucher.
        if ($data['price'] > $data['my_point']) {
            $this->response(
                $this->_result_NG(
                    ERROR_CODE_24,
                    24
                ),
                REST_Controller::HTTP_OK
            );

        } else {

            //if success
            $arrayToDB = array(
                "member_id"      => $api['member_id'],
                "voucher_id"     => $data['id'],
                "bought_date"    => date("Y-m-d H:i:s"),
            );

            $result = $this->_dm->set_model("trs_voucher_member")->insert($arrayToDB, array('is_direct' => true));

            //then insert trs member point
            $this->_dm->set_model("trs_member_point")->insert(array(
                "member_id"              => $api['member_id'],
                "point_mutation"         => -$data['price'],
                "description"            => DESCRIPTION ." ". $data['title'],
                "voucher_transaction_id" => $result,
            ));

            //minus voucher stok.
            $this->_dm->set_model("mst_voucher", "mv", "id")->update(
                array(
                    "stok"  => ($data['stok'] - 1),
                ),
                array(
                    "id"    => $data['id'],
                ),
                array(
                    "is_version" => true,
                )
            );
        }

        // Check trans. commit or rollback
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            // return NG
            $this->response(
                $this->_result_NG(ERROR_CODE_20, 20),
                REST_Controller::HTTP_OK
            );
        } else {
            $this->db->trans_commit();
            // return OK
            $this->response(
                $this->_result_OK(
                    array(
                        "member_new_point" => ($data['my_point'] - $data['price']),
                    )
                ),
                REST_Controller::HTTP_OK
            );
        }
    }

    /**
     * API - 022
     * claim voucher.
     */
    public function claim_post()
    {
        $member_voucher_id        = sanitize_str_input($this->post("member_voucher_id"));
        $merchant_verifikasi_code = sanitize_str_input($this->post("merchant_verification_code"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        //check voucher dan kode merchant.
        if (empty($member_voucher_id)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, "Voucher"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        if (empty($merchant_verifikasi_code)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, "Kode Merchant"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        //check kode merchant betul kaga, sekalian cek voucher tsb ada kaga yang dimiliki oleh member terkait, DAN status vouchernya == 0.
        $select = "tvm.id";
        $joined = array(
            "mst_voucher mv"  => array("mv.id" => "tvm.voucher_id"),
            "mst_merchant mm" => array("mm.id" => "mv.merchant_id"),
        );
        $conditions = array(
            "tvm.status_claimed"    => STATUS_VOUCHER_BOUGHT,
            "mm.verification_code"  => $merchant_verifikasi_code,
            "tvm.member_id"         => $api['member_id'],
            "tvm.id"                => $member_voucher_id,
        );
        $check_voucher = $this->_dm->set_model("trs_voucher_member", "tvm", "tvm.id")->get_all_data(array(
            "select"        => $select,
            "conditions"    => $conditions,
            "joined"        => $joined,
            "debug"         => false,
            "row_array"     => true,
        ))['datas'];

        //balikan untuk kode voucher ini.
        $generated_unique_code = uniqid();

        if (empty($check_voucher)) {
            $this->response(
                $this->_result_NG(
                    ERROR_CODE_25,
                    25
                ),
                200
            );

        } else {
            //waktu lagi claim, yg di update 3 fields ini.
            $arrayToDB = array(
                "status_claimed" => STATUS_VOUCHER_CLAIMED,
                "unique_code"    => $generated_unique_code,
                "claimed_date"   => date("Y-m-d H:i:s")
            );
            $result = $this->_dm->set_model("trs_voucher_member")->update($arrayToDB, array("id" => $member_voucher_id), array("is_direct" => true));
        }

        //query to get voucher information.
        $data = $this->_dm->set_model("mst_voucher", "mv", "mv.id")->get_all_data(array(
            "select"        => "mv.id as voucher_id, tvm.id as my_voucher_id, title, price, image_url, periode_start, periode_end, stok, summary, how_to, terms, tvm.status_claimed, tvm.unique_code as voucher_kode, tvm.bought_date, tvm.claimed_date",
            "joined"        => array(
                "trs_voucher_member tvm" => array("tvm.voucher_id" => "mv.id"),
            ),
            "conditions"    => array(
                "tvm.id"    => $member_voucher_id,
                "tvm.member_id" => $api['member_id'],
            ),
            "row_array"     => true,
            "debug"         => false,
        ))['datas'];

        //replace spaces
        $url = str_replace(' ', '-', $data['title']);
        //convert url
        $data['share_url']       = base_url(). '' . 'vouchers/detail/'. $data['voucher_id'] . '-' .$url;

        // Check trans. commit or rollback
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            // return NG
            $this->response(
                $this->_result_NG(ERROR_CODE_20, 20),
                REST_Controller::HTTP_OK
            );
        } else {
            $this->db->trans_commit();
            // return OK
            $this->response(
                $this->_result_OK(
                    array(
                        "voucher_unique_code" => $generated_unique_code,
                        "affected_voucher"    => $data,
                    )
                ),
                REST_Controller::HTTP_OK
            );
        }
    }
}
