<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sns extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }
    
    /**
     * API - 023
     * get all share users
     */
    public function share_post () {

        $description  = sanitize_str_input($this->post("description"));
        // Check for api_key valid or not valid or not. 
        $api = $this->check_api_key("post");

        if(empty($description)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        } else {

            $this->db->trans_begin();

            $arrayToDB = array(
                "member_id"      => $api['member_id'],
                "activity_code"  => CODE_SNS_SHARE,
                "point_mutation" => POINT_SNS_SHARE,
                "description"    => $description,
            );
            $result = $this->_dm->set_model("trs_member_point")->insert($arrayToDB);
        }

        // Check trans. commit or rollback
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            // return NG
            $this->response(
                $this->_result_NG(ERROR_CODE_20, 20),
                REST_Controller::HTTP_OK
            );

        } else {
            $this->db->trans_commit();
            // return OK
            $this->response(
                $this->_result_OK(),
                REST_Controller::HTTP_OK
            );
        }
    }
}