<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Csr extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 008
     * get all csr list
     */
    public function list_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        //get all csr
        $csr = $this->_dm->set_model("mst_csr", "mc", "mc.id")->get_all_data(array(
            "select" => "mc.id, mc.judul, mca.content, mc.is_show, mc.version, mc.image_landing, mc.ordering, mca.pretty_url as share_url, mc.type, mc.latitude, mc.longitude, mc.lokasi",
            "conditions" => array(
                "mc.is_show" => SHOW,
                "version > " => $version,
            ),
            "order_by" => array("ordering" => "asc"),
            "left_joined" => array(
                "mst_csr_artikel mca" => array("mca.id" => "mc.artikel_csr_id"),
            )
        ))['datas'];

        $models = array();

        if ($csr) {
            foreach($csr as $key => $value) {
                $csr_id = $value['id'];
                $value['share_url'] = base_url(). 'csr/'. $value['share_url'];

                $slider = $this->_dm->set_model("trs_csr_slider")->get_all_data(array(
                    "select" => array("id", "image_slider", "ordering"),
                    "conditions" => array("csr_id" => $csr_id),
                    "order_by" => array("ordering" => "asc"),
					"limit" => 1,
                ))['datas'];

                $value['image_details'] = $slider;
                $value['content'] = preg_replace("/[\n\r]/", "", $value['content']);
                $value['content'] = str_replace('src="../../../../upload/', 'src="'.base_url().'upload/', $value['content']);
                $value['content'] = str_replace('src="../../../upload/', 'src="'.base_url().'upload/', $value['content']);
                $value['content'] = str_replace('src="../../upload/', 'src="'.base_url().'upload/', $value['content']);
                $value['content'] = str_replace('src="/upload/', 'src="'.base_url().'upload/', $value['content']);

                array_push($models, $value);
             }
        }

        $this->response($this->_result_OK (array(
            "datas" => $models,
        )), 200);
    }
}
