<?php defined('BASEPATH') or exit('No direct script access allowed');


class Store extends Baseapi_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function data_get()
    {
        $version = sanitize_str_input($this->get("version"), "numeric");
        $limiter = sanitize_str_input($this->get("limiter"), "numeric");

        if (empty($version)) {
            $version = 0;
        }
        if (empty($limiter)) {
            $limiter = "2000";
        }

        $store_last_version = get_last_ver("mst_store_addon");

        $store = $this->_dm->set_model("mst_store_addon", "msa", "msa.kode_customer")->get_all_data(array(
            "select" => array(
                'msa.kode_customer',
                'rating_avianbrands',
                'owner_member_id',
                "CONCAT('".WEB_BASE_URL."/store/detail/', pretty_url) as web_url",
                'opening_hour',
                'msa.phone',
                'msa.fax',
                'msa.email',
                'msa.website',
                'msa.is_show',
                'msa.version',
                'meta_keys',
                'meta_desc',
                "IF(msa.nama_customer IS NULL or msa.nama_customer ='', dst.nama_customer, msa.nama_customer) as nama_customer",
                "IF(msa.alamat IS NULL or msa.alamat ='', dst.alamat, msa.alamat) as alamat",
                "IF(msa.latitude IS NULL or msa.latitude ='', dst.latitude, msa.latitude) as latitude",
                "IF(msa.longitude IS NULL or msa.longitude ='', dst.longitude, msa.longitude) as longitude",
                "IF(msa.foto_toko_url IS NULL or msa.foto_toko_url ='', dst.foto_toko_url, msa.foto_toko_url) as foto_toko_url",
                "IF(msa.punya_mesin_tinting IS NULL or msa.punya_mesin_tinting ='', dst.punya_mesin_tinting, msa.punya_mesin_tinting) as punya_mesin_tinting",
                'dst.jual_pipa_power',
                'dst.jual_pipa_power_max',
                'dst.jual_fitting_power',
                'dst.jual_talang_power',
                'dst.jual_fitting_talang_power',
                'dst.jual_no_odor',
                'dst.jual_fres',
                'dst.jual_avitex_exterior',
                'dst.jual_boyo_plamir',
                'dst.jual_no_drop',
                'dst.jual_avitex',
                'dst.jual_aries_gold',
                'dst.jual_aries',
                'dst.jual_sunguard',
                'dst.jual_supersilk',
                'dst.jual_everglo',
                'dst.jual_aquamatt',
                'dst.jual_avitex_alkali_resisting_primer',
                'dst.jual_no_drop_107',
                'dst.jual_no_drop_100',
                'dst.jual_no_drop_bitumen_black',
                'dst.jual_absolute',
                'dst.jual_avitex_roof',
                'dst.jual_belmas_roof',
                'dst.jual_yoko_roof',
                'dst.jual_lem_putih_vip_pvac',
                'dst.jual_lem_putih_max_pvac',
                'dst.jual_avian_road_line_paint',
                'dst.jual_wood_eco_woodstain',
                'dst.jual_boyo_politur_vernis',
                'dst.jual_tan_politur',
                'dst.jual_avian_hammertone',
                'dst.jual_suzuka_lacquer',
                'dst.jual_viplas',
                'dst.jual_vip_paint_remover',
                'dst.jual_avian_anti_fouling',
                'dst.jual_avian_lem_epoxy',
                'dst.jual_avian_non_sag_epoxy',
                'dst.jual_thinner_a_avia',
                'dst.jual_lenkote_colorants',
                'dst.jual_giant_mortar_220',
                'dst.jual_giant_mortar_260',
                'dst.jual_giant_mortar_270',
                'dst.jual_giant_mortar_380',
                'dst.jual_giant_mortar_480',
                'dst.jual_platinum',
                'dst.jual_avian',
                'dst.jual_yoko',
                'dst.jual_belmas_zinchromate',
                'dst.jual_avian_zinchromate',
                'dst.jual_yoko_loodmeni',
                'dst.jual_thinner_a_special_avia',
                'dst.jual_yoko_yzermenie',
                'dst.jual_glovin',
                'dst.jual_no_odor_wall_putty',
                'dst.jual_lenkote_alkali_resisting_primer',
                'dst.jual_no_lumut_solvent_based',
                'dst.jual_no_drop_tinting',
                "IF(vsr.rating_avg IS NULL or vsr.rating_avg = '', 0, vsr.rating_avg) as user_rating_avg",
                "IF(vsr.total_reviews IS NULL or vsr.total_reviews = '', 0, vsr.total_reviews) as total_reviews",
                "IF(vsr.count_5_star IS NULL or vsr.count_5_star = '', 0, vsr.count_5_star) as count_5_star",
                "IF(vsr.count_4_star IS NULL or vsr.count_4_star = '', 0, vsr.count_4_star) as count_4_star",
                "IF(vsr.count_3_star IS NULL or vsr.count_3_star = '', 0, vsr.count_3_star) as count_3_star",
                "IF(vsr.count_2_star IS NULL or vsr.count_2_star = '', 0, vsr.count_2_star) as count_2_star",
                "IF(vsr.count_1_star IS NULL or vsr.count_1_star = '', 0, vsr.count_1_star) as count_1_star",
                "IF(vsr.count_0_star IS NULL or vsr.count_0_star = '', 0, vsr.count_0_star) as count_0_star",
                "IF(vsr.total_discussions IS NULL or vsr.total_discussions = '', 0, vsr.total_discussions) as total_discussions",
            ),
            "conditions" => array(
                "msa.version > " => $version,
            ),
            "limit" => $limiter,
            "joined" => array(
                "dtb_store_v2 dst" => array("dst.kode_customer" => "msa.kode_customer")
            ),
            "left_joined"   => array(
                "view_store_rating vsr" => array("vsr.foreign_key" => "msa.kode_customer"),
            ),
            "order_by" => array(
                "msa.version" => "asc",
            ),
        ))['datas'];

        $this->response($this->_result_OK(array(
            "store_latest_version" => $store_last_version,
            "stores" => $store,
        )), REST_Controller::HTTP_OK);
    }

    /**
     * API - 002
     * store/review_get
     * parameters :
     *     kode_customer, limit(default 3), page(default 0)
     */
    public function review_get()
    {
        // Parameters from get method
        $kode_customer = sanitize_str_input($this->get("kode_customer"));
        $page = sanitize_str_input($this->input->get("page"), "numeric");
        $limit = sanitize_str_input($this->get("limit"), "numeric");

        // if empty kode_customer = parameter invalid. (01A01010002)
        if (empty($kode_customer)) {
            $this->response(
                $this->_result_NG(
                    sprintf(ERROR_CODE_12, " Fill Required parameters"),
                    12
                ),
                REST_Controller::HTTP_OK
            );
        }

        // Rules Limit
        if (empty($limit)) {
            $limit = 3;
        } else {
            if (!is_numeric($limit)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Limit must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($limit < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Limit is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
            }
        }

        // Rules Page
        $start = 0;
        if (empty($page)) {
            $page = 0;
        } else {
            if (!is_numeric($page)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Page must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($page < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Page is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
                if ($page == 0) {
                    $page = 1;
                }
                $start = ($limit * ($page - 1));
            }
        }

        $reviews = $this->_dm->set_model("mst_store_addon", "msa", "msa.kode_customer")->get_all_data(array(
            "select" => array(
                "tsr.id",
                "tsr.review_datetime",
                "dm.id as member_id",
                "dm.name as member_name",
                "tsr.rating_score",
                "tsr.review_title",
                "tsr.review_content",
            ),
            "start" => $start,
            "limit" => $limit,
            "joined" => array(
                "trs_store_review tsr"      => array("tsr.foreign_key" => "msa.kode_customer"),
                "dtb_member dm"             => array("dm.id" => "tsr.member_id")
            ),
            "conditions" => array(
                "msa.kode_customer"         => $kode_customer,
                "tsr.status"                => 1,
            ),
            "order_by" => array(
                "tsr.review_datetime"       => "desc",
            ),
            "count_all_first" => true
        ));

        $this->response(
            $this->_result_OK(
                array(
                    "reviews"      => $reviews["datas"],
                    "total_page"   => ceil($reviews["total"] / $limit),
                    "limit"        => $limit,
                    "current_page" => $page,
                    "total_item"   => $reviews["total"],
                )
            ),
            REST_Controller::HTTP_OK
        );
    }

    /**
     * API - 003
     * store/discussion_get
     * parameters :
     *     kode_customer, limit(default 3), page(default 0),
     *     discussion_id(kalau tidak dikirim, maka maksudnya akan mengembalikan data topik discussionnya, kalau ada valuenya maka balikannya adalah
     *                     balasan dari discussion_id terkait.)
     */
    public function discussion_get()
    {
        // Parameters from get method
        $discussion_id = sanitize_str_input($this->get("discussion_id"), "numeric");
        $kode_customer = sanitize_str_input($this->get("kode_customer"));
        $page = sanitize_str_input($this->input->get("page"), "numeric");
        $limit = sanitize_str_input($this->get("limit"), "numeric");

        // if empty kode_customer = parameter invalid. (01A01010002)
        if (empty($kode_customer)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Rules Limit
        if (empty($limit)) {
            $limit = 3;
        } else {
            if (!is_numeric($limit)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Limit must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($limit < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Limit is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
            }
        }

        // Rules Page
        $start = 0;
        if (empty($page)) {
            $page = 0;
        } else {
            if (!is_numeric($page)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Page must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($page < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Page is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
                if ($page == 0) {
                    $page = 1;
                }
                $start = ($limit * ($page - 1));
            }
        }

        if (empty($discussion_id)) {
            //discussion TS doank.
            $discussion = $this->_dm->set_model("mst_store_addon", "msa", "msa.kode_customer")->get_all_data(array(
                "select" => array(
                    "tsd.id",
                    "tsd.discuss_datetime",
                    "dm.id as member_id",
                    "dm.name as member_name",
                    "tsd.discussion_content",
                    "tsd.is_from_store_owner",
                    "(select count(id) from trs_store_discussion tsd2 where tsd2.discussion_topic_id = tsd.id and tsd2.status = 1 and tsd2.type = 2) as total_replies",
                ),
                "start" => $start,
                "limit" => $limit,
                "joined" => array(
                    "trs_store_discussion tsd" => array("tsd.foreign_key" => "msa.kode_customer"),
                    "dtb_member dm" => array("dm.id" => "tsd.member_id")
                ),
                "conditions" => array(
                    "tsd.discussion_topic_id"   => null,
                    "tsd.foreign_key"           => $kode_customer,
                    "tsd.status"                => 1,
                    "tsd.type"                  => 1,
                ),
                "order_by" => array(
                    "tsd.discuss_datetime"      => "desc",
                ),
                "count_all_first" => true
            ));

            // This response!
            $this->response(
                $this->_result_OK(
                    array(
                        "discussion"   => $discussion["datas"],
                        "total_page"   => ceil($discussion["total"] / $limit),
                        "limit"        => $limit,
                        "current_page" => $page,
                        "total_item"   => $discussion["total"],
                    )
                ),
                REST_Controller::HTTP_OK
            );

        } else {
            //replies doank.
            $discussion = $this->_dm->set_model("mst_store_addon", "msa", "msa.kode_customer")->get_all_data(array(
                "select" => array(
                    "tsd.id",
                    "tsd.discussion_topic_id",
                    "tsd.discuss_datetime",
                    "dm.id as member_id",
                    "dm.name as member_name",
                    "tsd.discussion_content",
                    "tsd.is_from_store_owner",
                ),
                "start" => $start,
                "limit" => $limit,
                "joined" => array(
                    "trs_store_discussion tsd" => array("tsd.foreign_key" => "msa.kode_customer"),
                    "dtb_member dm" => array("dm.id" => "tsd.member_id")
                ),
                "conditions" => array(
                    "tsd.discussion_topic_id"   => $discussion_id,
                    "tsd.foreign_key"           => $kode_customer,
                    "tsd.status"                => 1,
                ),
                "order_by" => array(
                    "tsd.discuss_datetime"      => "desc",
                ),
                "count_all_first" => true
            ));

            // This response!
            $this->response(
                $this->_result_OK(
                    array(
                        "discussion"   => $discussion["datas"],
                        "total_page"   => ceil($discussion["total"] / $limit),
                        "limit"        => $limit,
                        "current_page" => $page,
                        "total_item"   => $discussion["total"],
                    )
                ),
                REST_Controller::HTTP_OK
            );
        }
    }

    /**
     * API - 004
     * store/review_post
     * parameters :
     *     api_key(the api_key from login API), kode_customer, score, title, content
     */
    public function review_post()
    {
        // Parameters from post method
        $kode_customer = sanitize_str_input($this->post("kode_customer")); // 01A01010002
        $api_key =  sanitize_str_input($this->post("api_key")); // 57449452b74191.00742088
        $score   =  sanitize_str_input($this->post("score"));
        $title   = sanitize_str_input($this->post("title"));
        $content = sanitize_str_input($this->post("content"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        // if empty parameters send to ERROR_CODE_12 = parameter invalid.
        if (empty($kode_customer) || empty($api_key) || empty($score) || empty($title)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Max. Score = 5, Min. Score = 0
        if ($score < 0 || $score > 5) {
            $this->response(
                $this->_result_NG(ERROR_CODE_23, 23),
                REST_Controller::HTTP_OK
            );
        }

        // Get customer(store) data.
        $get_customer = $this->Dynamic_model->set_model("mst_store_addon", "msa", "kode_customer")->get_all_data(array(
            "row_array" => true,
            "conditions" => array("kode_customer" => $kode_customer),
        ))["datas"];

        // Check customer exist or not.
        if (!empty($get_customer)) {

            // Prepare data review
            $data_review = array(
                "foreign_key"     => $kode_customer,
                "review_datetime" => date("Y-m-d H:i:s"),
                "member_id"       => $api["member_id"],
                "rating_score"    => $score,
                "review_title"    => $title,
                "review_content"  => $content
            );

            // Params for execute
            $params = array("is_direct" => true);

            // Conditions for Execute!
            $conditions = array(
                "foreign_key" => $kode_customer,
                "member_id" => $api["member_id"]
            );

            // Only 1 review per member in 1 store.
            $check_customer = $this->Dynamic_model->set_model("trs_store_review", "tsr", "id")->get_all_data(array(
                "conditions" => $conditions,
                "row_array"  => true,
                "row_array"  => true,
                "count_all_first" => true
            ))["total"];

            // Trans begin.
            $this->db->trans_begin();

            // Checker for insert or update.
            if ($check_customer == 0) {
                $data_review['status'] = 0;
                // Execute for insert review!
                $result_review = $this->Dynamic_model->set_model("trs_store_review", "tsr", "id")->insert(
                    $data_review,
                    $params
                );
            } else {
                $data_review['status'] = 3;
                // Execute for update review!
                $result_review = $this->Dynamic_model->set_model("trs_store_review", "tsr", "id")->update(
                    $data_review,
                    $conditions,
                    $params
                );
            }

            // Check trans. commit or rollback
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                // return NG
                $this->response(
                    $this->_result_NG(ERROR_CODE_20, 20),
                    REST_Controller::HTTP_OK
                );
            } else {
                $this->db->trans_commit();
                // return OK
                $this->response(
                    $this->_result_OK(),
                    REST_Controller::HTTP_OK
                );
            }
        } else {
            $this->response(
                $this->_result_NG(ERROR_CODE_21, 21),
                REST_Controller::HTTP_OK
            );
        }
    }

    /**
     * API - 005
     * store/discussion_post
     * parameters :
     *     api_key(the api_key from login API), kode_customer, discussion_id, content
     */
    public function discussion_post()
    {
        // Parameters from post method
        $kode_customer = sanitize_str_input($this->post("kode_customer")); // 01A01010002
        $api_key       =  sanitize_str_input($this->post("api_key")); // 57449452b74191.00742088
        $discussion_id =  sanitize_str_input($this->post("discussion_id"));
        $content       = sanitize_str_input($this->post("content"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        // if empty parameters send to ERROR_CODE_12 = parameter invalid.
        if (empty($kode_customer) || empty($api_key) || empty($content)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Get customer(store) data.
        $get_customer = $this->Dynamic_model->set_model("mst_store_addon", "msa", "kode_customer")->get_all_data(array(
            "row_array" => true,
            "conditions" => array("kode_customer" => $kode_customer),
        ))["datas"];

        // Check customer exist or not.
        if (!empty($get_customer)) {
            // Trans begin.
            $this->db->trans_begin();

            // Prepare primary data for trs_store_discussion
            $data_discuss = array(
                "foreign_key"         => $kode_customer,
                "discuss_datetime"    => date("Y-m-d H:i:s"),
                "member_id"           => $api["member_id"],
                "discussion_content"  => $content
            );

            // Prepare params for insert trs_store_discussion
            $params = array(
                "is_direct" => true
            );

            if (empty($discussion_id)) {
                // Extend another field
                $data_discuss["type"] = 1;

                // Execute for discuss!
                $result_discuss = $this->Dynamic_model->set_model("trs_store_discussion", "tsd", "id")->insert(
                    $data_discuss,
                    $params
                );
            } else {
                // Extend another field
                $data_discuss["type"] = 2;
                $data_discuss["discussion_topic_id"] = $discussion_id;

                // Execute for replies!
                $result_replies = $this->Dynamic_model->set_model("trs_store_discussion", "tsd", "id")->insert(
                    $data_discuss,
                    $params
                );
            }

            // Check trans. commit or rollback
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                // return NG
                $this->response(
                    $this->_result_NG(ERROR_CODE_20, 20),
                    REST_Controller::HTTP_OK
                );
            } else {
                $this->db->trans_commit();
                // return OK
                $this->response(
                    $this->_result_OK(),
                    REST_Controller::HTTP_OK
                );
            }
        } else {
            $this->response(
                $this->_result_NG(ERROR_CODE_21, 21),
                REST_Controller::HTTP_OK
            );
        }
    }
}
