<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Branch extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 020
     * get all branch list and detail
     * format , version
     */
    public function list_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        //get all branch list and detail
        $branch = $this->_dm->set_model("dtb_branch", "db", "id")->get_all_data(array(
            "select" => "id, name, province, address, postal_code, telephone, handphone, fax, latitude, longitude, email, website, status, is_show, version, type, CONCAT('".WEB_BASE_URL."/offices') as share_url",
            "conditions" => array(
                "version > " => $version,
            )
        ))['datas'];

        $this->response($this->_result_OK (array(
            "branch" => $branch,
        )), REST_Controller::HTTP_OK);

    }

}
