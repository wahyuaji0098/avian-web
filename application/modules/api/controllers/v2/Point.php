<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Point extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        $this->load->model("member/Member_model");
    }

    /**
     * API - 029
     * member point configuration
     */
    public function configuration_get () {
        $result = $this->_dm->set_model('mst_point_configuration')->get_all_data()['datas'];

        $this->response($this->_result_OK(array(
            $result,
        )), 200);    
    }
}