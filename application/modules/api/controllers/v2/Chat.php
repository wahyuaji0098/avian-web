<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Chat extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    private function _check_room_exist ($member) {
        $room_exist = $this->_dm->set_model("trs_chat_room", "tcr", "id")->get_all_data(array(
            "conditions" => array(
                "member_id" => $member['member_id'],
            ),
            "row_array" => true,
        ))['datas'];

        if (empty($room_exist)) {
            //get last version of trs_chat_room
            $last_version = $this->_dm->set_model("trs_chat_room", "tcr", "id")->get_all_data(array(
                "select" => "IFNULL(MAX(version),0) as last_ver",
                "row_array" => true,
            ))['datas']['last_ver'];

            // create room
            $room_id = $this->_dm->set_model("trs_chat_room", "tcr", "id")->insert(array(
                "member_id" => $member['member_id'],
                "admin_id" => 1,
                "version" => $last_version + 1,
                "created_date" => date("Y-m-d H:i:s"),
                "updated_date" => date("Y-m-d H:i:s"),
            ), array("is_direct" => true));
        }
        else {
            $room_id = $room_exist['id'];
        }

        return $room_id;
    }

    //API_v2_036 post a chat
    //define API_V2_036_POST (BASE_API_URL @"v2/chat/send")
    //define API_V2_036_PARAM @"format=%@&api_key=%@&chat_data=%@"

    //API_v2_037 get latest chat
    //define API_V2_037_GET (BASE_API_URL @"v2/chat?format=%@&api_key=%@&last_version=%@")

    public function index_get () {
        $api_key = $this->get("api_key") ? $this->get("api_key") : "";
        $version = $this->get("version") ? $this->get("version") : 0;

        //check if all param had been sent.
        if (empty($api_key) || trim($api_key) == "") {
            $this->response($this->_result_NG("API key harus dikirim.", 20), 200);
        }

        //check api key if valid
        $member = $this->check_api_key("get");

        $room_id = $this->_check_room_exist ($member);

        $datas = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->get_all_data(array(
            "select" => "IFNULL(admin_id, '') as admin_id, message, tanggal, version",
            "conditions" => array(
                "chat_room_id" => $room_id,
                "version > " => $version,
            ),
        ))['datas'];

        $this->response($this->_result_OK(array(
            "chat"    => $datas,
        )), 200);
    }

    public function send_post () {

        $api_key = $this->post("api_key") ? $this->post("api_key") : "";
        $chat_data = $this->post("chat_data") ? $this->post("chat_data") : "";

        //check if all param had been sent.
        if (empty($api_key) || trim($api_key) == "") {
            $this->response($this->_result_NG("API key harus dikirim.", 20), 200);
        }
        //check if all param had been sent.
        if (empty($chat_data)) {
            $this->response($this->_result_NG("Chat Data harus dikirim.", 20), 200);
        }

        //check api key if valid
        $member = $this->check_api_key("post");

        // trans begin
        $this->db->trans_begin();

        // check if room exist
        // kalau gak ada, dibuat dl roomnya
        $room_id = $this->_check_room_exist ($member);

        $last_version_chat_detail = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->get_all_data(array(
            "select" => "IFNULL(MAX(version),0) as last_ver",
            "conditions" => array(
                "chat_room_id" => $room_id
            ),
            "row_array" => true,
        ))['datas']['last_ver'];

        $data_insert = array();

        $chat_data = json_decode($chat_data, true);

        if (is_array($chat_data) && count($chat_data) > 0) {
            foreach ($chat_data as $cdata) {
                $last_version_chat_detail += 1;

                $msg = "";
                if (isset($cdata['pesan']) && !empty($cdata['pesan'])) {
                    $msg = $cdata['pesan'];
                } else {
                    $msg = $cdata['message'];
                }

                $tm = "";
                if (isset($cdata['waktu']) && !empty($cdata['waktu'])) {
                    $tm = validate_date_time_input($cdata['waktu']);
                } else {
                    $tm = validate_date_time_input($cdata['tanggal']);
                }

                // insert to visual result
                $data_insert[] = array(
                    "chat_room_id" => $room_id,
                    "message" => $msg,
                    "tanggal" => $tm,
                    "version" => $last_version_chat_detail,
                    "member_id" => $member['member_id'],
                    "is_read" => 1,
                );
            }

            if (count($data_insert) > 0) {
                //insert batch
                $this->_dm->set_model("trs_chat_detail", "tcd", "id")->insert($data_insert, array("is_direct" => true, "is_batch" => true));
            }
        }

        $this->_dm->set_model("trs_chat_room", "tcr", "id")->update (array(
            "updated_date" => date('Y-m-d H:i:s'),
            "is_read" => 1,
        ), array("id" => $room_id), array("is_direct" => true));

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
        } else {
            $this->db->trans_commit();

            $this->response($this->_result_OK(), 200);
        }

    }

}
