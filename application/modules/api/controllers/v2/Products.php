<?php defined('BASEPATH') or exit('No direct script access allowed');


class Products extends Baseapi_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * API - 013
     * get all product list and detail
     * format , product_version , category_version
     */
    public function index_get()
    {
        $product_version  = $this->get("product_version") ? $this->get("product_version") : 0;
        $category_version = $this->get("category_version") ? $this->get("category_version") : 0;
        $pslider_version  = $this->get("product_slider_version") ? $this->get("product_slider_version") : 0;

        $this->load->model("product/Product_model");

        //get all product list and detail
        $product = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
            "select"  => array(
                "id as product_id",
                "name as product_name",
                "code as product_code",
                "product_category_id as category_id",
                "description as product_desc",
                "usability_feature",
                "technical_data",
                "surface_prep",
                "how_to_use",
                "cleaning_tools",
                "how_to_store",
                "safety_info",
                "additional_information",
                "image_url as product_image_url",
                "packaging as sizes",
                "ordering",
                "version",
                "status",
                "is_show",
                "is_hot_item",
                "image_url_hot",
                "kualitas_rating",
                "harga_rating",
                "deskripsi_singkat as short_description",
                "kelebihan as advantages",
                "paling_cocok_untuk",
                "file_url as tds_url",
                "file_url_msds msds_url",
                "CONCAT('".WEB_BASE_URL."/product/detail/', pretty_url) as share_url",
                "satuan",
                "spread_rate",
                "bisa_tinting",
                "tags",
                "IF(vpr.rating_avg IS NULL or vpr.rating_avg = '', 0, vpr.rating_avg) as user_rating_avg",
                "IF(vpr.total_reviews IS NULL or vpr.total_reviews = '', 0, vpr.total_reviews) as total_reviews",
                "IF(vpr.count_5_star IS NULL or vpr.count_5_star = '', 0, vpr.count_5_star) as count_5_star",
                "IF(vpr.count_4_star IS NULL or vpr.count_4_star = '', 0, vpr.count_4_star) as count_4_star",
                "IF(vpr.count_3_star IS NULL or vpr.count_3_star = '', 0, vpr.count_3_star) as count_3_star",
                "IF(vpr.count_2_star IS NULL or vpr.count_2_star = '', 0, vpr.count_2_star) as count_2_star",
                "IF(vpr.count_1_star IS NULL or vpr.count_1_star = '', 0, vpr.count_1_star) as count_1_star",
                "IF(vpr.count_0_star IS NULL or vpr.count_0_star = '', 0, vpr.count_0_star) as count_0_star",
                "IF(vpr.total_discussions IS NULL or vpr.total_discussions = '', 0, vpr.total_discussions) as total_discussions",
            ),
            "left_joined"   => array(
                "view_product_rating vpr" => array("vpr.product_id" => "dp.id"),
            ),
            "conditions" => array(
                "version > " => $product_version,
            ),
        ))['datas'];

        //get product_slider
        $slider = $this->_dm->set_model("dtb_product_slider", "dps", "id")->get_all_data(array(
            "select" => array('id','title','image_url','image_big','ordering','is_show','category_id','version'),
            "conditions" => array(
                "version > " => $pslider_version,
            ),
        ))['datas'];
		
		//get product video 
        $video = $this->_dm->set_model("dtb_product_video", "dpv", "id")->get_all_data(array(
            "select" => array('id','product_id','url','image_url')
        ))['datas'];

        //get categories product
        $categories = $this->_dm->set_model("dtb_product_category", "dpc", "id")->get_all_data(array(
            "select" => "id as category_id, name as category_name, description as category_description, is_show, version, show_in_store_filter, ordering",
            "conditions" => array(
                "version > " => $category_version,
            ),
            "order_by" => array("ordering" , "asc")
        ))['datas'];

        //making guide.
        $guide = array(
            array(
                "question" => "Apa yang Anda butuhkan ?",
                "answer"   => array(
                    array(
                        "text" => "Saya butuh cat dinding untuk rumah baru saya.",
                        "tag"  => ["cat dinding"],
                        "next" => array(
                            array(
                                "question" => "Apa bahan / material dari dinding Anda ?",
                                "answer"   => array(
                                    array(
                                        "text" => "Beton.",
                                        "tag"  => ["beton"],
                                        "next" => array(
                                            array(
                                                "question" => "Untuk sisi dalam atau luar rumah ?",
                                                "answer"   => array(
                                                    array(
                                                        "text" => "Luar.",
                                                        "tag"  => ["exterior"],
                                                        "next" => array(
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang tahan cuaca ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Ya.",
                                                                        "tag"  => ["tahan cuaca"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak harus.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang mudah dibersihkan ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Ya.",
                                                                        "tag"  => ["mudah dibersihkan"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak harus.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                        ),
                                                    ),
                                                    array(
                                                        "text" => "Dalam.",
                                                        "tag"  => ["interior"],
                                                        "next" => array(
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang anti lumut & jamur ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Anti lumut.",
                                                                        "tag"  => ["anti lumut"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Anti jamur.",
                                                                        "tag"  => ["anti jamur"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Anti lumut dan jamur.",
                                                                        "tag"  => ["anti lumut", "anti jamur"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang tidak berbau ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Ya.",
                                                                        "tag"  => ["tidak bau"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak harus.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang mudah dibersihkan ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Ya.",
                                                                        "tag"  => ["mudah dibersihkan"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak harus.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                        ),
                                                    ),
                                                    array(
                                                        "text" => "Bebas.",
                                                        "tag"  => [],
                                                        "next" => array(),
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                    array(
                                        "text" => "Kayu.",
                                        "tag"  => ["kayu"],
                                        "next" => array(
                                            array(
                                                "question" => "Untuk sisi dalam atau luar rumah ?",
                                                "answer"   => array(
                                                    array(
                                                        "text" => "Luar.",
                                                        "tag"  => ["exterior"],
                                                        "next" => array(
                                                            array(
                                                                "question" => "Apakah dibutuhkan yang tahan cuaca ?",
                                                                "answer"   => array(
                                                                    array(
                                                                        "text" => "Ya.",
                                                                        "tag"  => ["tahan cuaca"],
                                                                        "next" => array(),
                                                                    ),
                                                                    array(
                                                                        "text" => "Tidak harus.",
                                                                        "tag"  => [],
                                                                        "next" => array(),
                                                                    ),
                                                                ),
                                                            ),
                                                        ),
                                                    ),
                                                    array(
                                                        "text" => "Dalam.",
                                                        "tag"  => ["interior"],
                                                        "next" => array(),
                                                    ),
                                                    array(
                                                        "text" => "Bebas.",
                                                        "tag"  => [],
                                                        "next" => array(),
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                    array(
                                        "text" => "Besi / baja / seng.",
                                        "tag"  => ["logam"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Bata ringan.",
                                        "tag"  => ["bata ringan"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Tampilkan semua jenis cat dinding.",
                                        "tag"  => [],
                                        "next" => array(),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    array(
                        "text" => "Saya butuh pengencer cat.",
                        "tag"  => ["solvent"],
                        "next" => array(),
                    ),
                    array(
                        "text" => "Saya butuh pelapis anti bocor.",
                        "tag"  => ["anti bocor"],
                        "next" => array(
                            array(
                                "question" => "Pelapis anti bocor diperlukan untuk pengaplikasian apa ?",
                                "answer"   => array(
                                    array(
                                        "text" => "Tembok sisi luar rumah yang sering terkena hujan.",
                                        "tag"  => ["anti retak", "elastis"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Dinding / lantai wc / kolam renang.",
                                        "tag"  => ["wc", "kolam renang"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Penampung air.",
                                        "tag"  => ["bak air"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Tampilkan semua pelapis anti bocor.",
                                        "tag"  => [],
                                        "next" => array(),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    array(
                        "text" => "Saya butuh perekat / lem yang kuat.",
                        "tag"  => ["lem"],
                        "next" => array(),
                    ),
                    array(
                        "text" => "Saya butuh semen instant yang mudah digunakan.",
                        "tag"  => ["semen instant"],
                        "next" => array(
                            array(
                                "question" => "Kebutuhan semen instant ini adalah untuk ?",
                                "answer"   => array(
                                    array(
                                        "text" => "Meratakan dan menghaluskan permukaan dinding.",
                                        "tag"  => ["pelamir"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Sebagai pengisi / perekat antar bata ringan.",
                                        "tag"  => ["bata ringan"],
                                        "next" => array(),
                                    ),
                                    array(
                                        "text" => "Sebagai perekat keramik, marmer, dll ke lantai / dinding.",
                                        "tag"  => ["keramik", "lantai"],
                                        "next" => array(),
                                    ),
                                ),
                            ),
                        ),
                    ),
                    array(
                        "text" => "Saya butuh cat untuk genteng.",
                        "tag"  => ["genteng"],
                        "next" => array(),
                    ),
                    array(
                        "text" => "Saya butuh cat untuk mebel kayu.",
                        "tag"  => ["kayu"],
                        "next" => array(),
                    ),
                ),
            ),
        );

        $this->response($this->_result_OK(array(
            "products"          => $product,
            "categories"        => $categories,
            "product_slider"    => $slider,
			"product_video"		=> $video,
            "guidance"          => $guide,
        )), 200);
    }

    /**
     * API - 014
     * products/review_get
     * parameters :
     *     product_id, limit(default 3), page(default 0)
     */
    public function review_get()
    {
        // Parameters from get method
        $product_id = sanitize_str_input($this->get("product_id"), "numeric");
        $limit = sanitize_str_input($this->get("limit"), "numeric");
        $page  = sanitize_str_input($this->input->get("page"), "numeric");

        // if empty product_id = parameter invalid. (6)
        if (empty($product_id)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Rules Limit
        if (empty($limit)) {
            $limit = 3;
        } else {
            if (!is_numeric($limit)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Limit must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($limit < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Limit is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
            }
        }

        // Rules Page
        $start = 0;
        if (empty($page)) {
            $page = 0;
        } else {
            if (!is_numeric($page)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Page must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($page < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Page is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
                if ($page == 0) {
                    $page = 1;
                }
                $start = ($limit * ($page - 1));
            }
        }

        $reviews = $this->_dm->set_model("dtb_product", "dp", "dp.id")->get_all_data(array(
            "select" => array(
                "tpr.id",
                "tpr.review_datetime",
                "dm.id as member_id",
                "dm.name as member_name",
                "tpr.rating_score",
                "tpr.review_title",
                "tpr.review_content",
            ),
            "start" => $start,
            "limit" => $limit,
            "joined" => array(
                "trs_product_review tpr"    => array("tpr.foreign_key" => "dp.id"),
                "dtb_member dm"             => array("dm.id" => "tpr.member_id")
            ),
            "conditions" => array(
                "dp.id"                     => $product_id,
                "tpr.status"                => 1,
            ),
            "order_by" => array(
                "tpr.review_datetime"       => "desc",
            ),
            "count_all_first" => true
        ));

        $this->response(
            $this->_result_OK(
                array(
                    "reviews"      => $reviews["datas"],
                    "total_page"   => ceil($reviews["total"] / $limit),
                    "limit"        => $limit,
                    "current_page" => $page,
                    "total_item"   => $reviews["total"],
                )
            ),
            REST_Controller::HTTP_OK
        );
    }

    /**
     * API - 015
     * products/discussion_get
     * parameters :
     *     product_id, limit(default 3), page(default 0),
     *     discussion_id(kalau tidak dikirim, maka maksudnya akan mengembalikan data topik discussionnya, kalau ada valuenya maka balikannya adalah
     *                     balasan dari discussion_id terkait.)
     */
    public function discussion_get()
    {
        // Parameters from get method
        $discussion_id = sanitize_str_input($this->get("discussion_id"), "numeric");
        $product_id = sanitize_str_input($this->get("product_id"), "numeric");
        $limit = sanitize_str_input($this->get("limit"), "numeric");
        $page  = sanitize_str_input($this->input->get("page"), "numeric");

        // if empty kode_customer = parameter invalid. (6)
        if (empty($product_id)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Rules Limit
        if (empty($limit)) {
            $limit = 3;
        } else {
            if (!is_numeric($limit)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Limit must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($limit < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Limit is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
            }
        }

        // Rules Page
        $start = 0;
        if (empty($page)) {
            $page = 0;
        } else {
            if (!is_numeric($page)) {
                $this->response(
                    $this->_result_NG(
                        sprintf(ERROR_CODE_12, " Page must a number."),
                        12
                    ),
                    REST_Controller::HTTP_OK
                );
            } else {
                if ($page < 0) {
                    $this->response(
                        $this->_result_NG(
                            sprintf(ERROR_CODE_12, " Minimal value for Page is 0."),
                            12
                        ),
                        REST_Controller::HTTP_OK
                    );
                }
                if ($page == 0) {
                    $page = 1;
                }
                $start = ($limit * ($page - 1));
            }
        }

        if (empty($discussion_id)) {

            //discussion TS only.
            $discussion = $this->_dm->set_model("dtb_product", "dp", "dp.id")->get_all_data(array(
                "select" => array(
                    "tsd.id",
                    "tsd.discuss_datetime",
                    "dm.id as member_id",
                    "dm.name as member_name",
                    "tsd.discussion_content",
                    "(select count(id) from trs_product_discussion tsd2 where tsd2.discussion_topic_id = tsd.id and tsd2.status = 1 and tsd2.type = 2) as total_replies",
                ),
                "start" => $start,
                "limit" => $limit,
                "joined" => array(
                    "trs_product_discussion tsd"    => array("tsd.foreign_key" => "dp.id"),
                    "dtb_member dm"                 => array("dm.id" => "tsd.member_id")
                ),
                "conditions" => array(
                    "tsd.discussion_topic_id"   => null,
                    "tsd.foreign_key"           => $product_id,
                    "tsd.status"                => 1,
                    "tsd.type"                  => 1,
                ),
                "order_by" => array(
                    "tsd.discuss_datetime"      => "desc",
                ),
                "count_all_first" => true
            ));

            // This response!
            $this->response(
                $this->_result_OK(
                    array(
                        "discussion"   => $discussion["datas"],
                        "total_page"   => ceil($discussion["total"] / $limit),
                        "limit"        => $limit,
                        "current_page" => $page,
                        "total_item"   => $discussion["total"],
                    )
                ),
                REST_Controller::HTTP_OK
            );

        } else {
            //replies only.
            $discussion = $this->_dm->set_model("dtb_product", "dp", "dp.id")->get_all_data(array(
                "select" => array(
                    "tsd.id",
                    "tsd.discussion_topic_id",
                    "tsd.discuss_datetime",
                    "dm.id as member_id",
                    "dm.name as member_name",
                    "tsd.discussion_content",
                    "tsd.is_from_store_owner",
                ),
                "start" => $start,
                "limit" => $limit,
                "joined" => array(
                    "trs_product_discussion tsd"    => array("tsd.foreign_key" => "dp.id"),
                    "dtb_member dm"                 => array("dm.id" => "tsd.member_id")
                ),
                "conditions" => array(
                    "tsd.discussion_topic_id"   => $discussion_id,
                    "tsd.foreign_key"           => $product_id,
                    "tsd.status"                => 1,
                    "tsd.type"                  => 2,
                ),
                "order_by" => array(
                    "tsd.discuss_datetime"      => "desc",
                ),
                "count_all_first" => true,
            ));

            // This response!
            $this->response(
                $this->_result_OK(
                    array(
                        "discussion"   => $discussion["datas"],
                        "total_page"   => ceil($discussion["total"] / $limit),
                        "limit"        => $limit,
                        "current_page" => $page,
                        "total_item"   => $discussion["total"],
                    )
                ),
                REST_Controller::HTTP_OK
            );
        }
    }

    /**
     * API - 016
     * products/review_post
     * parameters :
     *     api_key(the api_key from login API), product_id, score, title, content
     */
    public function review_post()
    {
        // Parameters from post method
        $product_id = sanitize_str_input($this->post("product_id")); // 6
        $api_key    =  sanitize_str_input($this->post("api_key")); // 57449452b74191.00742088
        $score      =  sanitize_str_input($this->post("score"));
        $title      = sanitize_str_input($this->post("title"));
        $content    = sanitize_str_input($this->post("content"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        // if empty parameters send to ERROR_CODE_12 = parameter invalid.
        if (empty($product_id) || empty($api_key) || empty($score) || empty($title)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Max. Score = 5, Min. Score = 0
        if ($score < 0 || $score > 5) {
            $this->response(
                $this->_result_NG(ERROR_CODE_23, 23),
                REST_Controller::HTTP_OK
            );
        }

        // Get product data.
        $get_product = $this->Dynamic_model->set_model("dtb_product", "dp", "id")->get_all_data(array(
            "row_array" => true,
            "conditions" => array("id" => $product_id),
        ))["datas"];

        // Check product exist or not.
        if (!empty($get_product)) {

            // Prepare data review
            $data_review = array(
                "foreign_key"     => $product_id,
                "review_datetime" => date("Y-m-d H:i:s"),
                "member_id"       => $api["member_id"],
                "rating_score"    => $score,
                "review_title"    => $title,
                "review_content"  => $content
            );

            // Params for execute
            $params = array("is_direct" => true);

            // Conditions for Execute!
            $conditions = array(
                "foreign_key" => $product_id,
                "member_id" => $api["member_id"]
            );

            // Only 1 review per member in 1 product.
            $check_product = $this->Dynamic_model->set_model("trs_product_review", "tsr", "id")->get_all_data(array(
                "conditions" => $conditions,
                "row_array"  => true,
                "row_array"  => true,
                "count_all_first" => true
            ))["total"];

            // Trans begin.
            $this->db->trans_begin();

            // Checker for insert or update.
            if ($check_product == 0) {
                $data_review['status'] = 0;
                // Execute for insert review!
                $result_review = $this->Dynamic_model->set_model("trs_product_review", "tpr", "id")->insert(
                    $data_review,
                    $params
                );
            } else {
                $data_review['status'] = 3;
                // Execute for update review!
                $result_review = $this->Dynamic_model->set_model("trs_product_review", "tpr", "id")->update(
                    $data_review,
                    $conditions,
                    $params
                );
            }

            // Check trans. commit or rollback
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                // return NG
                $this->response(
                    $this->_result_NG(ERROR_CODE_20, 20),
                    REST_Controller::HTTP_OK
                );
            } else {
                $this->db->trans_commit();
                // return OK
                $this->response(
                    $this->_result_OK(),
                    REST_Controller::HTTP_OK
                );
            }
        } else {
            $this->response(
                $this->_result_NG(ERROR_CODE_21, 21),
                REST_Controller::HTTP_OK
            );
        }
    }

    /**
     * API - 017
     * products/discussion_post
     * parameters :
     *     api_key(the api_key from login API), product_id, discussion_id, content
     */
    public function discussion_post()
    {
        // Parameters from post method
        $product_id    = sanitize_str_input($this->post("product_id")); // 6
        $api_key       =  sanitize_str_input($this->post("api_key")); // 57449452b74191.00742088
        $discussion_id =  sanitize_str_input($this->post("discussion_id"));
        $content       = sanitize_str_input($this->post("content"));

        // Check for api_key valid or not valid or not.
        $api = $this->check_api_key("post");

        // if empty parameters send to ERROR_CODE_12 = parameter invalid.
        if (empty($product_id) || empty($api_key) || empty($content)) {
            $this->response(
                sprintf(ERROR_CODE_12, " Fill Required parameters"),
                REST_Controller::HTTP_OK
            );
        }

        // Get product data.
        $get_product = $this->Dynamic_model->set_model("dtb_product", "dp", "id")->get_all_data(array(
            "row_array" => true,
            "conditions" => array("id" => $product_id),
        ))["datas"];

        // Check product exist or not.
        if (!empty($get_product)) {
            // Trans begin.
            $this->db->trans_begin();

            // Prepare primary data for trs_product_discussion
            $data_discuss = array(
                "foreign_key"         => $product_id,
                "discuss_datetime"    => date("Y-m-d H:i:s"),
                "member_id"           => $api["member_id"],
                "discussion_content"  => $content
            );

            // Prepare params for insert trs_product_discussion
            $params = array(
                "is_direct" => true
            );

            if (empty($discussion_id)) {
                // Extend another field
                $data_discuss["type"] = 1;

                // Execute for discuss!
                $result_discuss = $this->Dynamic_model->set_model("trs_product_discussion", "tpd", "id")->insert(
                    $data_discuss,
                    $params
                );
            } else {
                // Extend another field
                $data_discuss["type"] = 2;
                $data_discuss["discussion_topic_id"] = $discussion_id;

                // Execute for replies!
                $result_replies = $this->Dynamic_model->set_model("trs_product_discussion", "tpd", "id")->insert(
                    $data_discuss,
                    $params
                );
            }

            // Check trans. commit or rollback
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
                // return NG
                $this->response(
                    $this->_result_NG(ERROR_CODE_20, 20),
                    REST_Controller::HTTP_OK
                );
            } else {
                $this->db->trans_commit();
                // return OK
                $this->response(
                    $this->_result_OK(),
                    REST_Controller::HTTP_OK
                );
            }
        } else {
            $this->response(
                $this->_result_NG(ERROR_CODE_21, 21),
                REST_Controller::HTTP_OK
            );
        }
    }
}
