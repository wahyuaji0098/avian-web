<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Articles extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 014
     * get all Article
     * format
     */
    public function index_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        //get Latest article
        $articles = $this->_dm->set_model("dtb_article")->get_all_data(array(
            "select" => "id as article_id , title as article_title, type as article_type, image_url as article_image_url, full_content as article_content, sticky_flag as is_sticky , date , version, status , is_show",
            "conditions" => array(
                "version > " => $version
            )
        ))['datas'];

        $models = array();

        if(count($articles) > 0) {
            foreach($articles as $model) {
                $model['article_content'] = preg_replace("/[\n\r]/","",$model['article_content']);
                $model['article_content'] = str_replace('src="/upload/','src="'.base_url().'upload/',$model['article_content']);
                array_push($models, $model);
            }
        }

        $this->response($this->_result_OK (array(
            "articles" 	=> $models,
        )), REST_Controller::HTTP_OK);

    }

}
