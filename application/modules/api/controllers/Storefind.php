<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Storefind extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->library("Geolocation");
    }

    /**
     * API - 013
     * get all store list and detail
     * format , user_latitude, user_longitude, range , center_latitude, center_longitude, query, search_mode, can_custom_color_filter , product_filter , premium_filter
     * search mode : 1: nyari lokasi, 2: nyari toko, 3: nyari product, 0: ga nyari apa2 (default)
     */
    public function index_get () {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $user_latitude = $this->get("user_latitude") ? $this->get("user_latitude") : "";
        $user_longitude = $this->get("user_longitude") ? $this->get("user_longitude") : "";
        $range = $this->get("range") ? $this->get("range") : "";
        $center_latitude = $this->get("center_latitude") ? $this->get("center_latitude") : "";
        $center_longitude = $this->get("center_longitude") ?  $this->get("center_longitude") : "";
        $query = $this->get("query") ? $this->get("query") : "";
        $search_mode = $this->get("search_mode") ?  $this->get("search_mode") : "";
        $can_custom_color_filter = $this->get("can_custom_color_filter") ? $this->get("can_custom_color_filter") : "";
        $product_filter = $this->get("product_filter") ? $this->get("product_filter") : array();

        //check if all param had been sent
        if ($range == "" || $center_latitude == "" || $center_longitude == "" || $search_mode == "") {
            $this->response($parameter_invalid, 200);
        }

        if ($user_latitude == "") $user_latitude = 0;
        if ($user_longitude == "") $user_longitude = 0;

        //load models
        $this->load->model("store/Store_model");

		$m_store = new Store_model();

        $latitude_moved = "";
        $longitude_moved = "";
		$products = array();

        //category filter
        if (!empty($product_filter)) {
            $product_filter = explode("|",$product_filter);
        } else {
			$product_filter = array();
		}

        //search mode : 1: nyari lokasi, 2: nyari toko, 3: nyari product, 0: ga nyari apa2 (default)
        if ($search_mode == 1 && $center_latitude == 0 && $center_longitude == 0 && $query != "") {
            //find by location
			$geo = new Geolocation();
			$coor = $geo->getCoordinates(
				$query,
				"",
				"",
				"",
				"indonesia"
			);
			$center_latitude = (($coor['latitude']) ? $coor['latitude'] : "");
			$center_longitude = (($coor['longitude']) ? $coor['longitude'] : "");
        } else if ($search_mode == 1 && $center_latitude != 0 && $center_longitude != 0 && $query != "") {
            //find by location
        } else if ($search_mode == 2) {
            //find by store
			//get first store
			$_store = $m_store->getLatLongByNameLimit1 ($query,$product_filter,$user_latitude,$user_longitude);
			if (!empty($_store)) {
				$latitude_moved = $_store['latitude'];
				$longitude_moved = $_store['longitude'];
			}
        } else if ($search_mode == 3) {
            //find by product
            $products = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data (array(
                "select" => array("id"),
                "filter" => array(
                    "lower(name)"  => strtolower($query),
                ),
            ))['datas'];

			$_product = $m_store->getLatLongByProductNameLimit1_apiv1 ($products);

			if (!empty($_product)) {
				$latitude_moved = $_product['latitude'];
				$longitude_moved = $_product['longitude'];
			}

        } else {
            //default (not find anything)
            // $search_mode = "";
        }

        $range = $range/1000;


        $s_datas = $m_store->getStoreByRadiusAPI ($user_latitude,$user_longitude,$center_latitude,$center_longitude,$range,null,$can_custom_color_filter,$query,$search_mode,$product_filter,$products);

		// print_r($s_datas); exit;

        $models = array();

        $i = 0;
        if (count($s_datas) > 0) {
            foreach ($s_datas as $model) {

                array_push ($models, array(
                    "store_id" => $model['store_id'],
                    "distance" => $model['user_distance'],
                ));
                $i++;
            }
        }

        $datas = array(
            "stores" => $models,
        );

        if ($search_mode == 2 || $search_mode == 3) {
            $datas['latitude_to_move'] = $latitude_moved;
            $datas['longitude_to_move'] = $longitude_moved;
        }

		if ($search_mode == 1) {
			$datas['latitude_to_move'] = $center_latitude;
            $datas['longitude_to_move'] = $center_longitude;
		}

        $this->response($this->_result_OK ($datas), 200);

    }

}
