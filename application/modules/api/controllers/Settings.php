<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 005
     * get push setting
     * format , api_key
     */
    public function index_get () {

        $member = $this->check_api_key("get");

        $this->response($this->_result_OK (array(
            "push_is_on" 	=> $member['push_setting'],
        )), 200);

    }

    /**
     * API - 006
     * update push setting
     * format , api_key , push_notif_on
     */

    function index_put() {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $push_notif_on = $this->put("push_notif_on") ?  $this->put("push_notif_on") : "";
        $device_token = $this->put("device_token") ?  $this->put("device_token") : "";

        //check if all param had been sent
        $member = $this->check_api_key("put");

        //check if push notif on not send
        if ($push_notif_on != "1" && $push_notif_on != "2") {
            $this->response($parameter_invalid, 200);
        }

        $datas_update = array("push_setting" => $push_notif_on);

        if ($device_token != "") {
            $datas_update['device_token'] = $device_token;
        }

        $this->load->model("member/Device_model");

        //update push notif setting
        $this->Device_model->update($datas_update,array("api_key" => $member['api_key']),array("is_direct" => true));

        //return ok
        $this->response($this->_result_OK (array(
            "push_is_on" 	=> $push_notif_on,
        )), 200);

    }

}
