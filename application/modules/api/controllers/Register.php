<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Register extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 003
     * register new member
     */

    function index_post() {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $from = $this->post("from") ? $this->post("from") : "";
        $username = $this->post("username") ? $this->post("username") : "";
        $name = $this->post("fullname") ? $this->post("fullname") : "";
        $email = $this->post("email") ? $this->post("email") : "";
        $password = $this->post("password") ? $this->post("password") : "";
        $fb_id = $this->post("fb_id") ? $this->post("fb_id") : "";
        $fb_name = $this->post("fb_name") ? $this->post("fb_name") : "";
        $device_token = $this->post("device_token") ? $this->post("device_token") : "";

        $this->load->model("member/Member_model");
        $this->load->model("member/Device_model");

		if ($fb_id != "") {
			if ($fb_name == "" || $from == "") {
				$this->response($parameter_invalid, 200);
			}

			//check if member is already exist for this fb id
            $member = $this->Member_model->get_all_data (array(
                "conditions" => array(
                    "fb_id" => $fb_id,
                ),
                "row_array" => true
            ))['datas'];

			if (isset($member['status']) && $member['status'] == STATUS_DELETE) {
				$member_not_exist = $this->_result_NG (ERROR_CODE_14,14);
				$this->response($member_not_exist, 200);
			} else {
				//if member not exist yet.
				if (!$member) {
					//insert to member
					$member_id = $this->Member_model->insert(array (
						"name" 		=> $fb_name,
						"fb_id" 	=> $fb_id,
						"email" 	=> $email,
					));
				} else {
					$member_id = $member['id'];
				}
			}

		} else {
			//check if all param had been sent
			if (!$username || !$email || !$password || !$name || !$from
			|| trim($name) == "" || trim($email) == "" || trim($password) == "" || trim($username) == "" || trim($from) == "") {
				$this->response($parameter_invalid, 200);
			}

			//check email member if exist
			$member = $this->Member_model->get_all_data(array(
                "conditions" => array(
                    "email" => $email
                ),
                "row_array" => true,
                "status" => STATUS_ACTIVE,
            ))['datas'];

			//if email is already exist in database
			if ($member) {
				//if member status deleted , return error should activated by admin
				if ($member['status'] == STATUS_DELETE) {
					$this->response($this->_result_NG (ERROR_CODE_14,14), 200);
				} else if ($member['status'] == STATUS_ACTIVE) {
					//if member is active, return email cannot be used
					$this->response($this->_result_NG (ERROR_CODE_15,15), 200);
				}
			}

			//check username if exist
			if ($username != "") {
				//check username
				$uname_exist = $this->Member_model->get_all_data(array(
                    "conditions" => array(
                        "username" => $username
                    ),
                    "row_array" => true,
                    "status" => STATUS_ACTIVE,
                ))['datas'];

				if ($uname_exist) {
					if ($uname_exist['status'] == STATUS_DELETE) {
						$this->response($this->_result_NG (ERROR_CODE_14,14), 200);
					} else if ($uname_exist['status'] == STATUS_ACTIVE) {
						//if member is active, return email cannot be used
						$this->response($this->_result_NG (ERROR_CODE_16,16), 200);
					}
				}
			}

			$datas = array(
				'name' => $name,
				'email' => $email,
				'password' => $password,
				'username' => $username,
			);

			//create new member
			$member_id = $this->Member_model->insert($datas);

			//send email registration
			$this->Member_model->sendmailRegister($datas);
		}

        //generate api key
        $api_key = $this->Device_model->generate_api_key();

        //update to dtb device
        $this->Device_model->insert(array(
            "api_key" => $api_key,
            "member_id" => $member_id,
            "device_token" => $device_token,
            "type" => $from,
			"push_setting" => (($device_token == "") ? 2 : 1),
        ), array("is_direct" => true));

        $member = $this->Member_model->get_all_data (array(
            "find_by_pk" => array($member_id),
            "row_array" => true
        ))['datas'];

        //set login time
        $this->Member_model->update (array(
            "lastlogin_date" => date("Y-m-d H:i:s"),
            "unique_code" => null,
        ), array(
            "id" => $member['id']
        ));

        //return ok
        $this->response($this->_result_OK (array(
            "api_key" => $api_key,
            "user_id" => $member['id'],
            "name" => $member['name'],
        )), 200);

    }

}
