<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Branch extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 015
     * get all branch list and detail
     * format , version
     */
    public function index_get () {
        $version = $this->get("version") ? $this->get("version") : 0;

        //get all branch list and detail
        $branch = $this->_dm->set_model("dtb_branch", "db", "id")->get_all_data(array(
            "select" => array('id','name','province','address','postal_code','telephone','handphone','fax','status','is_show','version','type'),
            "conditions" => array(
                "version > " => $version,
            )
        ))['datas'];

        $this->response($this->_result_OK (array(
            "branch" => $branch,
        )), REST_Controller::HTTP_OK);

    }

}
