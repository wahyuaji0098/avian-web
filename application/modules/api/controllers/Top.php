<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Top extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 001
     * get all pallete list and colours
     * format
     */
    public function index_get () {
        //get Latest article
        $articles = $this->_dm->set_model("dtb_article", "da", "id")->get_all_data(array(
            "select" => "id as article_id , title as article_title, type as article_type, image_url as article_image_url",
            "conditions" => array(
                "is_show" => SHOW,
            ),
            "status" => STATUS_ACTIVE,
            "limit" => 5,
            "order_by" => array("date" => "desc"),
        ))['datas'];

        $this->response($this->_result_OK (array(
            "articles" 	=> $articles,
        )), 200);
    }

    /**
     * API - 006
     * get all data for top page
     */

    public function data_get () {
        $data['slider'] = $this->_dm->set_model("dtb_slider")->get_all_data(array(
            "select" => "id, url, image_url_device, popup_image_url, is_show_as_popup",
            "conditions" => array("is_show" => 1),
            "order_by" => array("ordering" => "asc")
        ))['datas'];

        $data['promo'] = $this->_dm->set_model("mst_promo")->get_all_data(array(
            "select" => "id, image_url, popup_image_url, is_show_as_popup, is_from_avian",
            "conditions" => array("is_show" => 1),
            "order_by" => array("created_date" => "desc"),
            "limit" => 4
        ))['datas'];

        $data['event'] = $this->_dm->set_model("mst_event")->get_all_data(array(
            "select" => "id, image_url, popup_image_url, is_show_as_popup, is_from_avian",
            "conditions" => array("is_show" => 1),
            "order_by" => array("created_date" => "desc"),
            "limit" => 4
        ))['datas'];

        $this->response($this->_result_OK (array(
            "slider"  => $data['slider'],
            "promo"  => $data['promo'],
            "event"  => $data['event'],
        )), 200);
    }
}
