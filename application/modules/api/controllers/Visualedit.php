<?php defined('BASEPATH') or exit('No direct script access allowed');

class Visualedit extends Baseapi_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * API - 020
     * edit visual result
     * format, api_key , visualizer_result_id, result_image, name, is_published, layers (layer_id,color_id,pallete_id)
     */
    public function index_post()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        $api_key = $this->post("api_key") ? $this->post("api_key") : "";
        $visualizer_result_id = $this->post("visualizer_result_id") ? $this->post("visualizer_result_id") : "";
        $result_image = (isset($_FILES["result_image"]['size'])) ? $_FILES["result_image"] : "";
        $result_image_large = (isset($_FILES["result_image_large"]['size'])) ? $_FILES["result_image_large"] : "";
        $name = $this->post("name") ? $this->post("name") : "";
        $is_published = ($this->post("is_published") !== "") ? $this->post("is_published") : "";
        $layers = $this->post("layers") ? $this->post("layers") : "";

        //check if all param had been sent
        if (!$api_key || trim($api_key) == "" || !$visualizer_result_id || $visualizer_result_id == "") {
            $this->response($parameter_invalid, 200);
        }

        //check api key if valid
        $member = $this->check_api_key("post");

        $this->load->model("visualize/Result_model");
        $this->load->model("visualize/Result_detail_model");

        //check if data is exist
        $v_result = $this->Result_model->get_all_data(array(
            "conditions" => array(
                "id" => $visualizer_result_id,
                "member_id" => $member['id']
            ),
            "row_array" => true,
        ))['datas'];

        if (empty($v_result)) {
            $this->response($this->_result_NG(ERROR_CODE_18, 18), 200);
        }

        //upload image
        $file_error = false;
        if (isset($result_image['size']) && $result_image['size'] > 0) {
            $result_upload_file = $this->upload_file("result_image", '', false, 'upload/visualizer_result/');

            //if image not uploaded.
            if (!isset($result_upload_file['uploaded_path'])) {
                $file_error = true;
                $data = "Failed to move file.";
            }
        }

        if (isset($result_image_large['size']) && $result_image_large['size'] > 0) {
            $result_upload_file_large = $this->upload_file("result_image_large", '', false, 'upload/visualizer_result/large/');

            //if image not uploaded.
            if (!isset($result_upload_file_large['uploaded_path'])) {
                $file_error = true;
                $data = "Failed to move file.";
            }
        }

        if ($file_error == true) {
            $this->response($this->_result_NG(ERROR_CODE_19, 19), 200);
        }

        $data_update = array();

        if ($name != "") {
            $data_update['name'] = $name;
        }
        if ($is_published != "") {
            $data_update['is_published'] = $is_published;
        }

        if (isset($result_upload_file['uploaded_path'])) {
            $data_update["result_image_url"] = $result_upload_file['uploaded_path'];
            if ($v_result['result_image_url']) {
                unlink(FCPATH . $v_result['result_image_url']);
            }
        }

        if (isset($result_upload_file_large['uploaded_path'])) {
            $data_update["result_image_large"] = $result_upload_file_large['uploaded_path'];
            if ($v_result['result_image_large']) {
                unlink(FCPATH . $v_result['result_image_large']);
            }
        }

        //trans begin
        $this->db->trans_begin();

        //update visual result
        $result_id = $this->Result_model->update($data_update, array("id" => $visualizer_result_id), array("is_version" => true));

        if ($layers != "") {
            $layers_data = json_decode($layers);

            //update to visual result detail
            foreach ($layers_data as $det) {
                //check layer if exist
                $layer_exist = $this->Result_detail_model->get_all_data(array(
                    "conditions" => array(
                        'visual_result_id' => $visualizer_result_id,
                        "visual_location_detail_id" => $det->layer_id,
                    ),
                    "row_array" => true,
                ))['datas'];

                if ($layer_exist) {
                    $this->Result_detail_model->update(array(
                        "color_id" => $det->color_id,
                        "pallete_id" => $det->pallete_id,
                        "product_id" => ((isset($det->product_id)) ? $det->product_id : 0),
                    ), array(
                        'visual_result_id' => $visualizer_result_id,
                        "visual_location_detail_id" => $det->layer_id,
                    ), array("is_version" => true, "is_direct" => true));
                } else {
                    $this->Result_detail_model->insert(array(
                        "visual_result_id" => $visualizer_result_id,
                        "color_id" => $det->color_id,
                        "pallete_id" => $det->pallete_id,
                        "visual_location_detail_id" => $det->layer_id,
                        "product_id" => ((isset($det->product_id)) ? $det->product_id : 0),
                    ), array("is_version" => true, "is_direct" => true));
                }
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
        } else {
            $this->db->trans_commit();

            $this->response($this->_result_OK(array(
                "id"    => $visualizer_result_id,
            )), 200);
        }
    }
}
