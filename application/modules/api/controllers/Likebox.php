<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Likebox extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 009
     * get all Article
     * format, api_key, likebox_version, likebox_item, cust_color_item, paint_calc_item, visualizer_item
     * likebox_item [id_device, id_web, type, id, version]
     * custom_color_item [id_device, id_web, name, red, green, blue, version]
     * paint_calc_item [id_device, id_web, req_text, result_text, version]
     * visualizer_item [id_device, id_web, location_id, name, is_published, version, result_image, result_image_large, layers {layer_id, color_id, pallete_id}]
     */
    public function index_post () {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $api_key = $this->post("api_key") ? $this->post("api_key") : "";
        $likebox_version = $this->post("likebox_version") ? $this->post("likebox_version") : 0;
        $likebox_item = $this->post("likebox_item") ? $this->post("likebox_item") : "";
        $cust_color_item = $this->post("custom_color_item") ? $this->post("custom_color_item") : "";
        $paint_calc_item = $this->post("paint_calc_item") ? $this->post("paint_calc_item") : "";
        $visualizer_item = $this->post("visualizer_item") ? $this->post("visualizer_item") : "";

        //check if all param had been sent
        $member = $this->check_api_key("post");

        $_arr_ccolor = array();
        $_arr_pcalc = array();
        $_arr_vis = array();
        $_arr_likebox = array();

        //get likebox by latest version
        $_arr_likebox = $this->_dm->set_model("dtb_likebox", "dl", "id")->get_all_data(array(
            "select" => array('id as id_web','"" as id_device','type', 'COALESCE(product_id,color_id,store_id,paint_calc_result_id,visualizer_result_id,custom_color_id) as id' ,'version','status'),
            "conditions" => array(
                "version > " => $likebox_version
            ),
        ))['datas'];

		$_arr_pcalc = $this->_dm->set_model("dtb_paint_calc", "dpc", "id")->get_all_data(array(
            "select" => "id as id_web, top as req_text, bottom as result_text, created_date, '' as id_device",
            "conditions" => array(
                "id IN (select paint_calc_result_id from dtb_likebox where version > {$likebox_version}) " => NULL
            ),
        ))['datas'];

		//custom color by id
        $_arr_ccolor = $this->_dm->set_model("dtb_custom_color", "dcc", "id")->get_all_data(array(
            "select" => "id as id_web, '' as id_device, name, red, green, blue",
            "conditions" => array(
                "id IN (select custom_color_id from dtb_likebox where version > {$likebox_version}) " => NULL
            ),
        ))['datas'];

        //insert custom color
        if ($cust_color_item != "") {
            $cust_color_item = json_decode($cust_color_item);

            foreach ($cust_color_item as $ccolor) {
                $hex_code 	= rgb2hex(array($ccolor->red,$ccolor->green,$ccolor->blue));

                //insert to custom color
                $id_ccolor = $this->_dm->set_model("dtb_custom_color", "dcc", "id")->insert (array(
                    'name' => $ccolor->name,
                    'member_id' => $device['member_id'],
                    'red' => $ccolor->red,
                    'green' => $ccolor->green,
                    'blue' => $ccolor->blue,
                    'hex_code' => $hex_code,
                ), array("is_version" => true));

                $_arr_ccolor[] = array(
                    'id_device' => $ccolor->id_device,
                    'id_web' => $id_ccolor,
                    'name' => $ccolor->name,
                    'red' => $ccolor->red,
                    'green' => $ccolor->green,
                    'blue' => $ccolor->blue,
                );
            }
        }

        //insert paint calc
        if ($paint_calc_item != "") {
            $paint_calc_item = json_decode($paint_calc_item);

            foreach ($paint_calc_item as $pcalc) {
                //insert to custom color
                $id_pcalc = $this->_dm->set_model("dtb_paint_calc", "dpc", "id")->insert (array(
                    'top'  => $pcalc->req_text,
                    'bottom' => $pcalc->result_text,
                    'created_date' => date('Y-m-d H:i:s', strtotime($pcalc->created_date)),
                    'updated_date' => date('Y-m-d H:i:s', strtotime($pcalc->created_date)),
                ), array("is_version" => true));

                $_arr_pcalc[] = array(
                    'id_device' => $pcalc->id_device,
                    'id_web' => $id_pcalc,
                    'req_text' => $pcalc->req_text,
                    'result_text' => $pcalc->result_text,
                    'created_date' => date('Y-m-d H:i:s', strtotime($pcalc->created_date)),
                );
            }
        }

        //insert visualizer
        if ($visualizer_item != "") {
            $this->load->model("visualize/Result_model");

            $visualizer_item = json_decode($visualizer_item);

            foreach ($visualizer_item as $vis) {
                //upload image
                $file_error = false;
                $result_image = (isset($_FILES["result_image_".$vis->id_device]['size'])) ? $_FILES["result_image_".$vis->id_device] : "";

                if (isset($result_image['size']) && $result_image['size'] > 0) {
                    $result_upload_file = $this->upload_file ("result_image_".$vis->id_device, '', false, 'upload/visualizer_result/');

                    //if image not uploaded.
                    if (!isset($result_upload_file['uploaded_path'])) {
                        $file_error = true;
                        $data = "Failed to move file.";
                    }
                }

                $result_image_large = (isset($_FILES["result_image_large_".$vis->id_device]['size'])) ? $_FILES["result_image_large_".$vis->id_device] : "";
                if (isset($result_image_large['size']) && $result_image_large['size'] > 0) {
                    $result_upload_file_large = $this->upload_file ("result_image_large_".$vis->id_device, '', false, 'upload/visualizer_result/large/');

                    //if image not uploaded.
                    if (!isset($result_upload_file_large['uploaded_path'])) {
                        $file_error = true;
                        $data = "Failed to move file.";
                    }
                }

                if ($file_error == false) {
                    //insert to db visualizer
                    if ($vis->id_web) {
                        //edit visualizer
                        $data_update = array(
                            "name" => $vis->name,
                            "is_published" => $vis->is_published,
                        );

                        //check if data is exist
                        $v_result = $this->_dm->set_model("dtb_visual_result", "dvr", "id")->get_all_data(array(
                            "conditions" => array(
                                "member_id" => $device['member_id'],
                                "id" => $vis->id_web,
                            ),
                            "row_array" => true,
                        ))['datas'];

                        if (isset($result_upload_file['uploaded_path'])) {
                            $data_update["result_image_url"] = $result_upload_file['uploaded_path'];
                            if(!empty($v_result['result_image_url'])) unlink (FCPATH . $v_result['result_image_url']);
                        }

                        if (isset($result_upload_file_large['uploaded_path'])) {
                            $data_update["result_image_large"] = $result_upload_file_large['uploaded_path'];
                            if(!empty($v_result['result_image_large'])) unlink (FCPATH . $v_result['result_image_large']);
                        }

                        $result = $this->_dm->set_model("dtb_visual_result", "dvr", "id")->update($data_update, array("id" => $vis->id_web), array("is_version" => true));

                        $layers_data = json_decode($vis->layers);

                        //update to visual result detail
                        foreach ($layers_data as $det) {
                            $this->_dm->set_model("dtb_visual_result_detail", "dvrd", "id")->update(array(
                                "color_id" => $det->color_id,
                                "pallete_id" => $det->pallete_id,
                            ),array(
                                'visual_result_id' => $vis->id_web,
                                "visual_location_detail_id" => $det->layer_id,
                            ), array("is_direct" => true, "is_version" => true));
                        }

                        $vis_id = $vis->id_web;

                    } else {
                        //create new visualizer
                        $data_insert = array(
                            "location_id" => $vis->location_id,
                            "member_id" => $device['member_id'],
                            "name" => $vis->name,
                            "publish_date" => date('Y-m-d H:i:s'),
                            "is_published" => $vis->is_published,
                        );

                        if (isset($result_upload_file['uploaded_path'])) {
                            $data_insert["result_image_url"] = $result_upload_file['uploaded_path'];
                        }

                        if (isset($result_upload_file_large['uploaded_path'])) {
                            $data_insert["result_image_large"] = $result_upload_file_large['uploaded_path'];
                        }

                        $vis_id = $this->_dm->set_model("dtb_visual_result", "dvr", "id")->insert($data_insert, array("is_version" => true));

                        //insert to visual result detail
                        if ($vis_id) {
                            $models = array();

                            $layers_data = $vis->layers;

                            foreach ($layers_data as $det) {
                                $field = array(
                                    "visual_result_id" => $vis_id,
                                    "color_id" => $det->color_id,
                                    "pallete_id" => $det->pallete_id,
                                    "visual_location_detail_id" => $det->layer_id,
                                );

                                array_push($models,$field);
                            }

                            //insert to visual result detail
                            $visual_detail = $this->_dm->set_model("dtb_visual_result_detail", "dvrd", "id")->insert($models, array("is_batch" => true, "is_version" => true));
                        }
                    }

                    $vis_res = $this->Result_model->getVisualResultWithDetailByID($vis_id);
                    $vis_res['id_device'] = $vis->id_device;
                    $vis_res['id_web'] = $vis_id;

                    $_arr_vis[] = $vis_res;
                }
            }
        }

        $type_arr = array(
			'1' => array("field" => "product_id","arr_res" => ""),
			'2' => array("field" => "color_id","arr_res" => ""),
			'3' => array("field" => "store_id","arr_res" => ""),
			'4' => array("field" => "paint_calc_result_id","arr_res" => $_arr_pcalc),
			'5' => array("field" => "visualizer_result_id","arr_res" => $_arr_vis),
			'6' => array("field" => "custom_color_id","arr_res" => $_arr_ccolor),
		);

        //insert likebox
        if ($likebox_item != "") {
            $likebox_item = json_decode($likebox_item);

            foreach ($likebox_item as $lbox) {
                if ($lbox->id_web) {
                    //update

                    $delete = $this->_dm->set_model("dtb_likebox", "dl", "id")->delete(array("id" => $lbox->id_web), array("is_version" => true));

                    //if from custom_color , delete custom_color
                    if ($lbox->type == "6") {
                        $this->_dm->set_model("dtb_custom_color", "dcc", "id")->delete(array("id" => $lbox->id), array("is_version" => true));
                    }

                    //if from paint_calc , delete custom_color
                    if ($lbox->type == "4") {
                        $this->_dm->set_model("dtb_paint_calc", "dpc", "id")->delete(array("id" => $lbox->id), array("is_version" => true));
                    }

                    $lbox_id = $lbox->id_web;

                } else {
                    //insert
                    $field = $type_arr[$lbox->type]['field'];
                    $array_search = $type_arr[$lbox->type]['arr_res'];

                    if ($array_search != "") {
						$key = searchForIdInt($lbox->id, $array_search, "id_device");
						$id = $array_search[$key]['id_web'];
					} else {
						$id = $lbox->id;
					}

                    $lbox_id = $this->_dm->set_model("dtb_likebox", "dl", "id")->insert(array(
                        "type" => $lbox->type,
                        "member_id" => $device['member_id'],
                        $field => $id,
                    ), array("is_version" => true));
                }

                $res_like = $this->_dm->set_model("dtb_likebox", "dl", "id")->get_all_data(array(
                    "select" => array('id as id_web','type', 'COALESCE(product_id,color_id,store_id,paint_calc_result_id,visualizer_result_id,custom_color_id) as id','version','status'),
                    "find_by_pk" => array($lbox_id),
                    "row_array" => true,
                ))['datas'];
                $res_like['id_device'] = $lbox->id_device;

                $_arr_likebox[] = $res_like;
            }
        }

        $this->response($this->_result_OK (array(
            "likebox_item" 	=> $_arr_likebox,
            "custom_color_item" 	=> $_arr_ccolor,
            "paint_calc_item" 	=> $_arr_pcalc,
            "visualizer_item" 	=> $_arr_vis,
        )), 200);

    }

}
