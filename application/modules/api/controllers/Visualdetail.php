<?php defined('BASEPATH') or exit('No direct script access allowed');

class Visualdetail extends Baseapi_Controller
{
    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 018
     * get detail of visual result
     * format, visualizer_version , location_version
     */
    public function index_get()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        $visual_result_id = $this->get("visual_result_id") ? $this->get("visual_result_id") : 0;

        $this->load->model("visualize/Result_model");

        //check if all param had been sent
        if (!$visual_result_id || $visual_result_id == 0) {
            $this->response($parameter_invalid, 200);
        }

        //get visual location result detail
        $v_result = $this->Result_model->getVisualResultWithDetailByID($visual_result_id);

        $this->response($this->_result_OK($v_result), 200);
    }
}
