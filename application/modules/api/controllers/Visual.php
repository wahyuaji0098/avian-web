<?php defined('BASEPATH') or exit('No direct script access allowed');

class Visual extends Baseapi_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * API - 019
     * create visual result
     * format, api_key , location_id, result_image, name, is_published, layers (layer_id,color_id,pallete_id)
     */
    public function index_post()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        $api_key = $this->post("api_key") ? $this->post("api_key") : "";
        $location_id = $this->post("location_id") ? $this->post("location_id") : "";
        $result_image = ($_FILES["result_image"]['size'] > 0) ? $_FILES["result_image"] : "";
        $result_image_large = ($_FILES["result_image_large"]['size'] > 0) ? $_FILES["result_image_large"] : "";
        $name = $this->post("name") ? $this->post("name") : "";
        $is_published = ($this->post("is_published") !== "") ? $this->post("is_published") : "";
        $layers = $this->post("layers") ? $this->post("layers") : "";

        //check if all param had been sent.
        if (!$api_key || trim($api_key) == "") {
            $this->response($this->_result_NG("API key harus dikirim.", 20), 200);
        }

        if (!$location_id || $location_id == "") {
            $this->response($this->_result_NG("ID Lokasi harus dikirim.", 20), 200);
        }

        if (!$result_image || $result_image == "") {
            $this->response($this->_result_NG("image file harus dikirim.", 20), 200);
        }

        if (!$name || $name == "") {
            $this->response($this->_result_NG("Nama visualizer harus dikirim.", 20), 200);
        }

        if ($is_published === "" || !is_numeric($is_published)) {
            $this->response($this->_result_NG("Published harus dikirim dan harus angka.", 20), 200);
        }
        
        if (!$layers || $layers == "") {
            $this->response($this->_result_NG("Layers harus dikirim.", 20), 200);
        }

        //check api key if valid
        $member = $this->check_api_key("post");

        //upload image
        $file_error = false;

        if (isset($result_image['size']) && $result_image['size'] > 0) {
            $result_upload_file = $this->upload_file("result_image", '', false, 'upload/visualizer_result/');

            //if image not uploaded.
            if (!isset($result_upload_file['uploaded_path'])) {
                $file_error = true;
                $data = "Failed to move file.";
            }
        }

        if (isset($result_image_large['size']) && $result_image_large['size'] > 0) {
            $result_upload_file_large = $this->upload_file("result_image_large", '', false, 'upload/visualizer_result/large/');

            //if image not uploaded.
            if (!isset($result_upload_file_large['uploaded_path'])) {
                $file_error = true;
                $data = "Failed to move file.";
            }
        }

        if ($file_error == true) {
            $this->response($this->_result_NG(ERROR_CODE_19, 19), 200);
        }

        //insert to visual result
        $data_insert = array(
            "location_id" => $location_id,
            "member_id" => $member['member_id'],
            "name" => $name,
            "publish_date" => date('Y-m-d H:i:s'),
            "is_published" => $is_published,
        );

        if (isset($result_upload_file['uploaded_path'])) {
            $data_insert["result_image_url"] = $result_upload_file['uploaded_path'];
        }

        if (isset($result_upload_file_large['uploaded_path'])) {
            $data_insert["result_image_large"] = $result_upload_file_large['uploaded_path'];
        }

        //trans begin
        $this->db->trans_begin();

        $result_id = $this->_dm->set_model("dtb_visual_result", "dvr", "id")->insert($data_insert, array("is_version" => true));

        //insert to visual result detail
        $models = array();

        $layers_data = json_decode($layers);

        foreach ($layers_data as $det) {
            $field = array(
                "visual_result_id" => $result_id,
                "color_id" => $det->color_id,
                "pallete_id" => $det->pallete_id,
                "visual_location_detail_id" => $det->layer_id,
                "product_id" => ((isset($det->product_id)) ? $det->product_id : 0),
            );

            array_push($models, $field);
        }

        //insert to visual result detail
        $visual_detail = $this->_dm->set_model("dtb_visual_result_detail", "dvrd", "id")->insert($models, array("is_batch" => true, "is_version" => true, "is_direct" => true));

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
        } else {
            $this->db->trans_commit();

            $this->response($this->_result_OK(array(
                "id"    => $result_id,
            )), 200);
        }
    }

    /**
     * API - 021
     * delete visual result
     * format, api_key , visualizer_result_id
     */
    public function index_delete()
    {
        $parameter_invalid = $this->_result_NG(ERROR_CODE_12, 12);

        //$api_key = (isset($this->_delete_args["api_key"])) ? $this->_delete_args["api_key"] : "";
        $api_key = $this->delete('api_key');
        $visualizer_result_id = $this->delete('visualizer_result_id');

        //check if all param had been sent
        if (!$api_key || trim($api_key) == "" || !$visualizer_result_id || $visualizer_result_id == 0) {
            $this->response($parameter_invalid, 200);
        }

        //check api key if valid
        $member = $this->check_api_key("delete");

        //check if data is exist
        $v_result = $this->_dm->set_model("dtb_visual_result", "dvr", "id")->get_all_data(array(
            "conditions" => array(
                "id" => $visualizer_result_id,
                "member_id" => $member['member_id'],
            ),
            "row_array" => true,
        ))['datas'];

        if (!$v_result) {
            $this->response($this->_result_NG(ERROR_CODE_18, 18), 200);
        }

        //trans begin
        $this->db->trans_begin();

        //delete visual result
        $this->_dm->set_model("dtb_visual_result", "dvr", "id")->delete(array('id' => $visualizer_result_id), array("is_version" => true));

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $this->response($this->_result_NG(ERROR_CODE_20, 20), 200);
        } else {
            $this->db->trans_commit();

            $this->response($this->_result_OK(array(
                "id"    => $visualizer_result_id,
            )), 200);
        }
    }
}
