<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Changeprofile extends Baseapi_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    /**
     * API - 004
     * update password or name
     */

    function index_put() {
        $parameter_invalid = $this->_result_NG (ERROR_CODE_12,12);

        $api_key = $this->put("api_key") ? $this->put("api_key") : "";
        $name = $this->put("new_name") ? $this->put("new_name") : "";
        $password = $this->put("new_password") ? $this->put("new_password") : "";
        $old_password = $this->put("old_password") ? $this->put("old_password") : "";

        //check if all param had been sent
        $member = $this->check_api_key("put");

        //check if new password send, old password is required
        if ((!$old_password && $password) || (trim($password) != "" && trim($old_password) == "")) {
            $this->response($parameter_invalid, REST_Controller::HTTP_OK);
        }

        $this->load->model("member/Member_model");

        if ($old_password || $old_password != "") {
            $password_ok = $this->Member_model->check_password ($member['password'], $old_password);
            if (!$password_ok) {
                $this->response($this->_result_NG(ERROR_CODE_17, 17), REST_Controller::HTTP_OK);
            }
        }

        $datas = array();

        if ($name && trim($name) != "") {
            $datas['name'] = $name;
        }

        //update member
        if (isset($password) && $password != '') {
            $datas['password'] = $password;
        }

        $this->Member_model->update ($datas,array(
            'id' => $member['member_id']
        ));

        //return ok
        $this->response($this->_result_OK (), REST_Controller::HTTP_OK);

    }

}
