<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Products extends Baseapi_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
        // $this->load->library('Mail');
    }

    /**
     * API - 007
     * get all product list and detail
     * format , product_version , category_version
     */
    public function index_get () {

        $product_version = $this->get("product_version") ? $this->get("product_version") : 0;
        $category_version = $this->get("category_version") ? $this->get("category_version") : 0;
        $pslider_version = $this->get("product_slider_version") ? $this->get("product_slider_version") : 0;

        $this->load->model("product/Product_model");

        //get all product list and detail
        $product = $this->Product_model->getProductListAndDetail($product_version);

		//get product_slider
        $slider = $this->_dm->set_model("dtb_product_slider", "dps", "id")->get_all_data(array(
            "select" => array('id','title','image_url','image_big','ordering','is_show','category_id','version'),
            "conditions" => array(
                "version > " => $pslider_version,
            ),
        ))['datas'];

        //get categories product
        $categories = $this->_dm->set_model("dtb_product_category", "dpc", "id")->get_all_data(array(
            "select" => "id as category_id, name as category_name, description as category_description, is_show, version, show_in_store_filter, ordering",
            "conditions" => array(
                "version > " => $category_version,
            ),
            "order_by" => array("ordering" , "asc")
        ))['datas'];

        $this->response($this->_result_OK (array(
            "products" => $product,
            "categories" => $categories,
            "product_slider" => $slider,
        )), 200);

    }

}
