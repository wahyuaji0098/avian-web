<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

/**
 * COLUMNS in ADMIN TABLE
 * array('user_id','name','username','email','password','unique_code','end_forgotpass_time','IsActive','user_type','last_login_time','created_by', 'last_updated_by','deleted_by','created_date','updated_date','deleted_date')
 */
class User_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'mst_user';
        $this->_table_alias = 'us';
        $this->_pk_field    = 'user_id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    public function insert($datas, $extra_param = array())
    {
        if (isset($datas['password'])) {
			$options = [
				'cost' => 8,
			];
			$datas['password'] = password_hash($datas['password'], PASSWORD_BCRYPT, $options);
		}

        //add create date and update date
        $this->db->insert($this->_table, $datas);

        return $this->db->insert_id();
    }

    public function update($datas, $condition, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $options = [
                'cost' => 8,
            ];
            $datas['password'] = password_hash($datas['password'], PASSWORD_BCRYPT, $options);
        }

        return $this->db->update($this->_table, $datas, $condition);
    }

    //generate a unique code and save the code to user's "unique_code".
    private function _generate_forgot_code($id) {

        //create a unique code, and make sure
        //that there is no same unique code in the table.
        do {
            $generated_link = uniqid();
        } while ($this->checkCode($generated_link));

        //insert to user's "unique_code".
        $this->update(array(
            "unique_code" => $generated_link,
            "end_forgotpass_time" => strtotime("+1 day"),
        ),array(
            "user_id" => $id,
        ));

        return $generated_link;
    }


    //find if the generated code is exists in the table or not.
    public function checkCode($link) {
        $this->db->where("unique_code", $link);
        return $this->db->get($this->_table)->row_array();
    }


    //method to generate an url with a unique code.
    public function send_forgot_pass($datas) {

        //create unique code.
        $code = $this->_generate_forgot_code($datas['user_id']);

        //encode the unique code.
        $link = base_url()."reset-password/".urlencode(base64_encode($code));

        //return as valid URL link.
        return $link;
    }


    //function to reset user's password to something generated.
    public function reset_password($datas) {

        //generate a new pass.
        $new_pass = substr(uniqid(),0,10);

        //change password and null the unique_code.
        $this->update(array(
            "password" => $password,
            "unique_code" => null,
            "end_forgotpass_time" => null,
        ),array(
            "user_id" => $datas['user_id'],
        ));

        return $new_pass;
    }
}
