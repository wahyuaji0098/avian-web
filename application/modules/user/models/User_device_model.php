<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class User_device_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'mst_user_device';
        $this->_table_alias = 'usr';
        $this->_pk_field    = 'user_device_id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    /**
     * check username and password (for login).
     */
	public function check_login($username, $password) {

		//get data by username.
        $this->db->where("username" , $username);

		$user = $this->db->get($this->_table)->row_array();

		//if no user found, it's wrong then.
		if (!$user) {
			return false;
		}

		//check password.
		if (password_verify($password, $user['password'])) {
			return $user;
		}

		return false;
	}

    /*==============================================================================
     * API FUNCTION
     *==============================================================================*/
    //check api key exists or not.
    private function _key_exists($key) {
        return $this->db->where("api_key", $key)
                        ->count_all_results($this->_table) > 0;
    }

    /**
 	 * generate api key for login device.
 	 */
    public function generate_api_key() {
        do {
            // Generate unique_id
            $new_key = uniqid(PREFIX_API_KEY, true);
        }
        while ($this->_key_exists($new_key));
        // Already in the DB? Fail. Try again

        return $new_key;
    }
}
