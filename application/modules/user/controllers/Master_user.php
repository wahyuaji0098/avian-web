<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Master_customer Controller.
 * data yang sudah diproses dari RAW.
 */
class Master_user extends Baseadmin_Controller  {

    private $_title       = "User";
    private $_title_page  = '<i class="fa-fw fa fa-users"></i> User';
    private $_breadcrumb  = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "user";
    private $_back        = "/user/master-user";
    private $_js_path     = "/js/pages/user/";
    private $_view_folder = "user/";

    /**
     * constructor.
     */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List User
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List User</span>',
            "active_page"   => 'master-user-list',
            "breadcrumb"    => $this->_breadcrumb . '<li>User</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an admin
     */
    public function create () {
        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span> Create User</span>',
            "active_page"   => "master-user-create",
            "breadcrumb"    => $this->_breadcrumb . '<li><a href="/user/master-user">User</a></li><li>Create User</li>',
        );
        
        $this->_breadcrumb .= '<li><a href="/user/master-user">User</a></li>';

        $footer = array(
            "script" => $this->_js_path . "create.js", 
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an user
     */
    public function edit ($user_id = null) {
        $this->_breadcrumb .= '<li><a href="/user/master-user">User</a></li>';
        //
        $data['item'] = null;

        //validate ID and check for data.
        if ( $user_id === null || !is_numeric($user_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("user_id" => $user_id));
        //get the data.
        $data['item'] = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span> > Edit User</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit User</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => $this->_js_path . "create.js",
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required|min_length[3]|max_length[100]");

        //special validations for when editing.
        $this->form_validation->set_rules('username', 'Username', "trim|required|callback_check_username[$id]");
        $this->form_validation->set_rules('email', 'Email', "trim|required|callback_check_email[$id]");
        $this->form_validation->set_rules('use_carto', 'Use Carto', "trim|is_natural");

        //when insert only, check password.
        if (!$id) {
            $this->form_validation->set_rules('password', 'Password', "trim|required|min_length[6]|max_length[20]");
            $this->form_validation->set_rules('conf_password', 'Confirmation Password', "trim|required|min_length[6]|max_length[20]|matches[password]");
        } else {
            $this->form_validation->set_rules('new_password', 'New Password', "trim|min_length[6]|max_length[20]");
            if($this->input->post('new_password') != "") $this->form_validation->set_rules('conf_new_password', 'Confirmation New Password', "trim|required|min_length[6]|max_length[20]|matches[new_password]");
        }
    }

    /**
     * This is a custom form validation rule to check that username is must unique.
     */
    public function check_username($str, $id) {
        //sanitize input
        $str = sanitize_str_input($str);
        $id = sanitize_str_input($id);

        if (!$this->_secure) {
            show_404();
        }

        //flag.
        $isValid = true;
        $params = array("row_array" => true);

        if ($id == "") {
            //from create
            $params['conditions'] = array("lower(user_name)" => strtolower($str));
        } else {
            $params['conditions'] = array("lower(user_name)" => strtolower($str), "user_id !=" => $id);
        }

        $datas = $this->_dm->set_model('mst_user','mu','user_id')->get_all_data($params)['datas'];

        if ($datas) {
            $isValid = false;
            $this->form_validation->set_message('check_username', '{field} is already taken.');
        }

        return $isValid;
    }

     /**
     * This is a custom form validation rule to check that username is must unique.
     */
    // public function check_use_carto($str, $id) {
    //     //sanitize input
    //     $str = sanitize_str_input($str);
    //     $id = sanitize_str_input($id);

    //     if (!$this->_secure) {
    //         show_404();
    //     }

    //     //flag.
    //     $isValid = true;
    //     $params = array("row_array" => true);

    //     if ($id == "") {
    //         //from create
    //         $params['conditions'] = array("lower(use_carto)" => strtolower($str));
    //     } else {
    //         $params['conditions'] = array("lower(use_carto)" => strtolower($str), "user_id !=" => $id);
    //     }

    //     $datas = $this->_dm->set_model('mst_user','mu','user_id')->get_all_data($params)['datas'];

    //     if ($datas) {
    //         $isValid = false;
    //         $this->form_validation->set_message('check_use_carto', '{field} is already taken.');
    //     }

    //     return $isValid;
    // }

     /**
     * This is a custom form validation rule to check that email is must unique.
     */
    public function check_email($str, $id) {
        //sanitize input
        $str = sanitize_str_input($str);
        $id = sanitize_str_input($id);

        if (!$this->_secure) {
            show_404();
        }

        //flag.
        $isValid = true;
        $params = array("row_array" => true);

        if ($id == "") {
            //from create
            $params['conditions'] = array("lower(email)" => strtolower($str));
        } else {
            $params['conditions'] = array("lower(email)" => strtolower($str), "user_id !=" => $id);
        }

        $datas = $this->_dm->set_model('mst_user','mu','user_id')->get_all_data($params)['datas'];
        if ($datas) {
            $isValid = false;
            $this->form_validation->set_message('check_email', '{field} is already taken.');
        }

        return $isValid;
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('user_id', 'name','user_name','email','is_active','use_carto');

        $column_sort = preg_replace('/ as .*/', '', $select[$sort_col]);

        //initialize.
        $data_filters = array();
        $conditions   = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'user_name':
                        if ($value != "") {
                            $data_filters['lower(user_name)'] = $value;
                        }
                        break;

                    case 'email':
                        if ($value != "") {
                            $data_filters['lower(email)'] = $value;
                        }
                        break;

                    case 'status':
                        if ($value != "") {
                            $status = ($value == "active") ? STATUS_ACTIVE : STATUS_DELETE;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data(array(
            'select'          => $select,
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            'status'          => $status,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;
        
        //load models
        $this->load->model('User_model');

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id = sanitize_str_input($this->input->post('id'), "numeric");
        $name = sanitize_str_input($this->input->post('name'));
        $username = sanitize_str_input($this->input->post('username'));
        $email = sanitize_str_input($this->input->post('email'));
        $password = sanitize_str_input($this->input->post('password'));
        $new_password = sanitize_str_input($this->input->post('new_password'));
        $use_carto    = $this->input->post('use_carto');
        
        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            //begin transaction.
            $this->db->trans_begin();
            
            //validation success, prepare array to DB.
            $arrayToDB = array('name'       => $name,
                               'user_name'  => $username,
                               'password'   => $password,
                               'email'      => $email,
                           );

            //insert or update?
            if ($id == "") {

                if ($use_carto == 1) {
                    //check penggunaan carto maks 3 org.
                    $select = "count(use_carto) as carto";
                    $params = array(
                        'select' => $select,
                        'conditions' => array('use_carto' => 1),
                    );
                    $datas = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data($params)['datas'];

                    if ($datas[0]['carto'] >= 3) {
                        $message['error_msg'] = 'tidak bisa menggunakan carto lagi karena sudah ada 3 user yang menggunakan carto (max 3 user).';
                        echo json_encode($message);
                        exit();
                    } else {
                        $arrayToDB['use_carto'] = 1;
                    }
                }

                $arrayToDB['password'] = $password;

                //insert to DB.
                $result = $this->User_model->insert($arrayToDB);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New User has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/user/master-user";
                }

            } else {

                //update.
                if ($new_password != "") {
                    $arrayToDB['password'] = $new_password;
                }

                //check penggunaan carto maks 3 org.
                if ($use_carto == 1) {
                    //check penggunaan carto maks 3 org.
                    $select = "count(use_carto) as carto";
                    $params = array(
                        'select' => $select,
                        'conditions' => array('use_carto' => 1, "user_id !=" => $id),
                    );
                    $datas = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data($params)['datas'];

                    if ($datas[0]['carto'] >= 3) {
                        $message['error_msg'] = 'tidak bisa menggunakan carto lagi karena sudah ada 3 user yang menggunakan carto (max 3 user).';
                        echo json_encode($message);
                        exit();
                    } else {
                        $arrayToDB['use_carto'] = 1;
                    }
                } else {
                    $arrayToDB['use_carto'] = 0;
                }

                //condition for update.
                $condition = array(
                    "user_id" => $id,
                );
                $result = $this->User_model->update($arrayToDB, $condition);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    //check if admin id equals to current user login
                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "User has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/user/master-user";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Deactivate User.
     */
    public function deactivate() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        //check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data(array(
                "conditions" => array("user_id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (!$data) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // deactivate the user
                $condition = array("user_id" => $id);
                $delete = $this->_dm->set_model("mst_user", "mu", "user_id")->delete($condition);

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "User has been De-actived.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Reactivate an admin.
     */
    public function reactivate() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        // sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        // check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model("mst_user", "mu", "user_id")->get_all_data(array(
                "conditions" => array("user_id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            // no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // reactivate the user
                $condition = array("user_id" => $id);
                $delete = $this->_dm->set_model("mst_user", "mu", "user_id")->update(array("is_active" => STATUS_ACTIVE),$condition, array('is_direct' => TRUE));

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "User has been re-activated.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
