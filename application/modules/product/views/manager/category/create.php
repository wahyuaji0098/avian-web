<?php
    $id         = isset($item["id"]) ? $item["id"] : "";
    $title      = isset($item["name"]) ? $item["name"] : "";
    $url        = isset($item["pretty_url"]) ? $item["pretty_url"] : "";
    $description    = isset($item["description"]) ? $item["description"] : "";
    $image_url      = isset($item["image_url"]) ? $item["image_url"] : "";
    $image_url_pro  = isset($item["image_url_large"]) ? $item["image_url_large"] : "";
    $ordering       = isset($item["ordering"]) ? $item["ordering"] : "";
    $is_show        = isset($item["is_show"]) ? $item["is_show"] : false;
    $show_in        = isset($item["show_in"]) ? $item["show_in"] : false;
    $file_pretty_name   = isset($item["image_pretty_name"]) ? $item["image_pretty_name"] : "";
    $show_filter        = isset($item["show_in_store_filter"]) ? $item["show_in_store_filter"] : "";
    $meta_keys= isset($item["meta_keys"]) ? $item["meta_keys"] : "";
    $meta_desc= isset($item["meta_desc"]) ? $item["meta_desc"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Product Category</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/product/category/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Show In Strore Filter <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <?= select_yes_no('show_in_store_filter', $show_filter); ?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?= $title; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Pretty Url</label>
                                        <label class="input">
                                            <input name="pretty_url" id="pretty_url" type="text"  class="form-control" placeholder="Pretty URL" value="<?= $url; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Description</label>
                                        <label class="textarea">
                                            <textarea name="description" type="text"  class="form-control" placeholder="Description" /><?= $description; ?></textarea>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Image Small<sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-url">
                                                <?php if($image_url): ?>
                                                <img src="<?= $image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimageurl" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Image Large<sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-large">
                                                <?php if($image_url_pro): ?>
                                                <img src="<?= $image_url_pro ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimagelarge" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Image prettyname</label>
                                        <label class="input">
                                            <input name="file_pretty_name" type="text"  class="form-control" placeholder="Image Filename" value="<?= $file_pretty_name; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                                <fieldset>
                                    <section>
                                        <label class="label">Meta description </label>
                                        <label class="textarea">
                                            <textarea name="meta_desc" id="meta_desc" class="form-control" placeholder="untuk SEO, deskripsi dari Tips / Article, sebaiknya 70 karakter minimum."><?= $meta_desc ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Meta keywords </label>
                                        <label class="textarea">
                                            <textarea name="meta_keys" id="meta_keys" class="form-control" placeholder="Kata kunci untuk SEO dari Tips / Article"><?= $meta_keys ?></textarea>
                                        </label>
                                    </section>
                                </fieldset>
                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>
                <!-- end widget div -->
                </article>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
