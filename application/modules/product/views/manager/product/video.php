<?php 
     $id= isset($item["id"]) ? $item["id"] : "";
    $product_category_id= isset($item["product_category_id"]) ? $item["product_category_id"] : "";
    $pretty_url= isset($item["pretty_url"]) ? $item["pretty_url"] : "";
    $code= isset($item["code"]) ? $item["code"] : "";
    $name= isset($item["name"]) ? $item["name"] : "";
    $description= isset($item["description"]) ? $item["description"] : "";
    $image_url= isset($item["image_url"]) ? $item["image_url"] : "";
    $image_pretty_name= isset($item["image_pretty_name"]) ? $item["image_pretty_name"] : "";
    $image_url_hot= isset($item["image_url_hot"]) ? $item["image_url_hot"] : "";
    $image_pretty_name_hot= isset($item["image_pretty_name_hot"]) ? $item["image_pretty_name_hot"] : "";
    $file_url= isset($item["file_url"]) ? $item["file_url"] : "";
    $filename= isset($item["filename"]) ? $item["filename"] : "";
    $file_url_msds= isset($item["file_url_msds"]) ? $item["file_url_msds"] : "";
    $filename_msds= isset($item["filename_msds"]) ? $item["filename_msds"] : "";
    $usability_feature= isset($item["usability_feature"]) ? $item["usability_feature"] : "";
    $technical_data= isset($item["technical_data"]) ? $item["technical_data"] : "";
    $surface_prep= isset($item["surface_prep"]) ? $item["surface_prep"] : "";
    $how_to_use= isset($item["how_to_use"]) ? $item["how_to_use"] : "";
    $cleaning_tools= isset($item["cleaning_tools"]) ? $item["cleaning_tools"] : "";
    $how_to_store= isset($item["how_to_store"]) ? $item["how_to_store"] : "";
    $safety_info= isset($item["safety_info"]) ? $item["safety_info"] : "";
    $packaging= isset($item["packaging"]) ? $item["packaging"] : "";
    $additional_information= isset($item["additional_information"]) ? $item["additional_information"] : "";
    $status= isset($item["status"]) ? $item["status"] : "";
    $is_show= isset($item["is_show"]) ? $item["is_show"] : "";
    $is_hot_item= isset($item["is_hot_item"]) ? $item["is_hot_item"] : "";
    $created_date= isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date= isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date= isset($item["deleted_date"]) ? $item["deleted_date"] : "";
    $meta_keys= isset($item["meta_keys"]) ? $item["meta_keys"] : "";
    $meta_desc= isset($item["meta_desc"]) ? $item["meta_desc"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $success_message_custom = ($id == 0) ? "Successfully Added!" : "Successfully Updated!!";
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="video-forms" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

     <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-10">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Product Video</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="video-forms" action="/manager/product/products/process-form-video" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                            <fieldset>
                                 <section>
                                    <label class="label">Video</label>
                                    <label class="input" style="display: table-cell;">
                                            <button type="button" id="btn-add-video" class="btn btn-primary">Add Video</button>
                                    </label>
                                    <div class="row row-formated">
                                        <div class="col-md-12" id="add-video">
                                        </div>
                                        <?php if(isset($video)): ?>
                                        <div class="col-md-12" style="margin-top:20px;">
                                            <?php foreach ($video as $model): ?>
                                              <div class="col-md-12 bordered" id="video-<?= $model['id'] ?>">
                                                <div class="col-md-5"><?= $model['url'] ?></div>
                                                <div class="col-md-5 col-md-offset-1 text-center"><iframe width="200" height="150" src="<?= $model['url'] ?>" frameborder="0" allowfullscreen></iframe></div>
                                                <div class="col-md-1"><button type="button" name="deleteimage" class="deleteimage btn btn-warning btn-xs" data-id="<?= $model['id'] ?>">Delete</button></div>
                                              </div>
                                            <?php endforeach; ?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </section>  
                            </fieldset>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </section>
</div>