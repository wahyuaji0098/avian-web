<?php
    $id                     = isset($item["id"]) ? $item["id"] : "";
    $product_category_id    = isset($item['product_category_id']) ? $item['product_category_id'] : "";
    $is_new_item            = isset($item['is_new_item']) ? $item['is_new_item'] : "";
    $is_hot_item            = isset($item['is_hot_item']) ? $item['is_hot_item'] : "";
    $url                    = isset($item["pretty_url"]) ? $item["pretty_url"] : "";
    $code                   = isset($item['code']) ? $item['code'] : "";
    $name                   = isset($item["name"]) ? $item["name"] : "";
    $description            = isset($item["description"]) ? $item["description"] : "";
    $image_url              = isset($item["image_url"]) ? $item["image_url"] : "";
    $file_pretty_name       = isset($item["image_pretty_name"]) ? $item["image_pretty_name"] : "";
    $file_url               = isset($item['file_url']) ? $item['file_url'] : "";
    $filename               = isset($item['filename']) ? $item['filename'] : "";
    $file_msds              = isset($item['file_url_msds']) ? $item['file_url_msds'] : "";
    $file_name_msds         = isset($item['filname_msds']) ? $item['filename_msds'] : "";
    $usability_feature      = isset($item['usability_feature']) ? $item['usability_feature'] : "";
    $technical_data         = isset($item['technical_data']) ? $item['technical_data'] : "";
    $surface_prep           = isset($item['surface_prep']) ? $item['surface_prep'] : "";
    $how_to_use             = isset($item['how_to_use']) ? $item['how_to_use'] : "";
    $cleaning_tools         = isset($item['cleaning_tools']) ? $item['cleaning_tools'] : "";
    $how_to_store           = isset($item['how_to_store']) ? $item['how_to_store'] : "";
    $safety_info            = isset($item['safety_info']) ? $item['safety_info'] : "";
    $packaging              = isset($item['packaging']) ? $item['packaging'] : "";
    $satuan                 = isset($item['satuan']) ? $item['satuan'] : "";
    $additional_information = isset($item['additional_information']) ? $item['additional_information'] : "";
    $ordering               = isset($item["ordering"]) ? $item["ordering"] : "";
    $is_show                = isset($item["is_show"]) ? $item["is_show"] : false;
    $show_in                = isset($item["show_in"]) ? $item["show_in"] : false;
    $show_filter            = isset($item["show_in_filter"]) ? $item["show_in_filter"] : "";
    $meta_keys              = isset($item["meta_keys"]) ? $item["meta_keys"] : "";
    $meta_desc              = isset($item["meta_desc"]) ? $item["meta_desc"] : "";
    $created_date           = isset($item['created_date']) ? $item['created_date'] : "";
    $updated_date           = isset($item['updated_date']) ? $item['updated_date'] : "";
    $deleted_date           = isset($item['deleted_date']) ? $item['deleted_date'] : "";
    $kualitas_rating        = isset($item["kualitas_rating"]) ? $item["kualitas_rating"] : "";
    $harga_rating           = isset($item["harga_rating"]) ? $item["harga_rating"] : "";
    $deskripsi_singkat      = isset($item["deskripsi_singkat"]) ? $item["deskripsi_singkat"] : "";
    $paling_cocok_untuk     = isset($item["paling_cocok_untuk"]) ? $item["paling_cocok_untuk"] : "";
    $kelebihan              = isset($item["kelebihan"]) ? $item["kelebihan"] : "";
    $spread_rate            = isset($item['spread_rate']) ? $item['spread_rate'] : "";
    $min_spread_rate        = isset($item['min_spread_rate']) ? $item['min_spread_rate'] : "";
    $bisa_tinting           = isset($item['bisa_tinting']) ? $item['bisa_tinting'] : "";
    $selectedtags           = isset($item['tags']) ? $item['tags'] : "";
    $hasil_akhir            = isset($item['hasil_akhir']) ? $item['hasil_akhir'] : "";
    $time_kering            = isset($item['time_kering']) ? $item['time_kering'] : "";
    $lapisan_cat            = isset($item['lapisan_cat']) ? $item['lapisan_cat'] : "";
    $ytb_vid                = isset($item['ytb_vid']) ? $item['ytb_vid'] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
    // pr($item);exit;
    /*
        $('input').val('a');
        $('textarea').text('a');
    */
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Product Category</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/product/products/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label"> Product Category <sup class="color-red">*</sup></label>
                                        <label class="select">
                                            <?php echo select_product_category('product_category_id', $product_category_id , 'id="product_category_id" class="form-control"'); ?>
                                            <i></i>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show In Store Filter <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <?= select_yes_no('show_in_filter', $show_filter); ?>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Is Hot <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <div class='inline-group'>
                                                <label class="radio">
                                                    <input type="radio" <?= ($is_hot_item == 1) ? "checked" : "" ?> name="is_hot_item" value="1"><i></i>Yes
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" <?= ($is_hot_item == 0 || $is_hot_item == "" ) ? "checked" : "" ?> name="is_hot_item" value="0" ><i></i>No
                                                </label>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Is New <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <div class='inline-group'>
                                                <label class="radio">
                                                    <input type="radio" <?= ($is_new_item == 1) ? "checked" : "" ; ?> name="is_new_item" value="1"><i></i>Yes
                                                </label>
                                                <label class="radio">
                                                    <input type="radio" <?= ($is_new_item == 0 || $is_new_item == "") ? "checked" : "" ; ?> name="is_new_item" value="0" ><i></i>No
                                                </label>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Product Code</label>
                                        <label class="input">
                                                <input name="code" type="text"  class="form-control" placeholder="Product Code" value="<?= $code; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?= $name; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Pretty Url <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="pretty_url" id="pretty_url" type="text"  class="form-control" placeholder="Pretty URL" value="<?= $url; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image Small(325x400)<sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image">
                                                <?php if($image_url): ?>
                                                <img src="<?= $image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>

                                    <section>
                                        <label class="label">Image pretty name</label>
                                        <label class="input">
                                            <input name="image_pretty_name" type="text"  class="form-control" placeholder="Image Pretty Name" value="<?= $file_pretty_name; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">File Technical</label>
                                        <label class="input">
                                            <input name="file_technical" type="file"  class="form-control" placeholder="File Technical" value="" /> <br>
                                            <?php if($file_url): ?>
                                            <div class="file_tech_div">
                                                <a href="<?= $file_url ?>" target="_blank"><?= $file_url ?></a>
                                                <button type="button" class="btn btn-danger btn-xs" id="delete_tech" data-id="<?= $id?>" data-name="<?= $filename ?>"> Delete File</button>
                                            </div>
                                            <?php  endif; ?>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Filename Technical</label>
                                        <label class="input">
                                            <input name="filename" type="text"  class="form-control" placeholder="Filename Technical" value="<?= $filename; ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">File MSDS</label>
                                        <label class="input">
                                            <input name="file_msds" type="file"  class="form-control" placeholder="File MSDS" value="" /> <br>
                                            <?php if($file_msds): ?>
                                            <div class="file_tech_div">
                                               <a href="<?php echo $file_msds; ?>" target="_blank"><?php echo $file_msds; ?></a>
                                                <button type="button" class="btn btn-danger btn-xs" id="delete_msds" data-id="<?= $id ?>">Delete File MSDS</button>
                                            </div>
                                            <?php  endif; ?>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Filename MSDS</label>
                                        <label class="input">
                                            <input name="filename_msds" type="text"  class="form-control" placeholder="Filename Technical" value="<?= $file_name_msds ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Description <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="description" type="text"  class="form-control" placeholder="Description" ><?= $description; ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Fitur Dan Kegunaan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="usability_feature" id="full_content" class="form-control tinymce" rows="10"><?= $usability_feature?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Data Teknis <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="technical_data" id="full_content" class="form-control tinymce" rows="10"><?= $technical_data ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Persiapan Permukaan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="surface_prep" id="full_content" class="form-control tinymce" rows="10"><?= $surface_prep ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Cara Penggunaan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="how_to_use" id="full_content" class="form-control tinymce" rows="10"><?= $how_to_use ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Pembersihan Peralatan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="cleaning_tools" id="full_content" class="form-control tinymce" rows="10"><?= $cleaning_tools ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Cara Simpan Dan Penanganannya <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="how_to_store" id="full_content" class="form-control tinymce" rows="10"><?= $how_to_store ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Info Keselamatan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="safety_info" id="full_content" class="form-control tinymce" rows="10"><?= $safety_info ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Kemasan Tersedia <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input type="text" name="packaging" class="form-control" placeholder="Packaging" value="<?= $packaging ?>">
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Satuan Ukuran Kemasan <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="satuan" type="text"  class="form-control" placeholder="Satuan kemasan, misal: liter, kg" value="<?= $satuan ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Informasi Tambahan <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="additional_information" id="full_content" class="form-control tinymce" rows="10"><?= $additional_information ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Kualitas (<span class="reset-rating" id="reset-kualitas">Reset</span>)</label>
                                        <label>
                                            <div class="rating">
                                                <input type="radio" name="kualitas_rating" class="kualitas" id="kualitas-5" value="5" <?= checked_rating(5, $kualitas_rating); ?>>
                                                <label for="kualitas-5"><i class="fa fa-star"></i></label>
                                                <input type="radio" name="kualitas_rating" class="kualitas" id="kualitas-4" value="4" <?= checked_rating(4, $kualitas_rating); ?>>
                                                <label for="kualitas-4"><i class="fa fa-star"></i></label>
                                                <input type="radio" name="kualitas_rating" class="kualitas" id="kualitas-3" value="3" <?= checked_rating(3, $kualitas_rating); ?>>
                                                <label for="kualitas-3"><i class="fa fa-star"></i></label>
                                                <input type="radio" name="kualitas_rating" class="kualitas" id="kualitas-2" value="2" <?= checked_rating(2, $kualitas_rating); ?>>
                                                <label for="kualitas-2"><i class="fa fa-star"></i></label>
                                                <input type="radio" name="kualitas_rating" class="kualitas" id="kualitas-1" value="1" <?= checked_rating(1, $kualitas_rating); ?>>
                                                <label for="kualitas-1"><i class="fa fa-star"></i></label>
                                            </div>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Tingkat Harga (<span class="reset-rating" id="reset-harga">Reset</span>)</label>
                                        <label>
                                            <div class="rating">
                                                <input type="radio" name="harga_rating" class="harga" id="harga-5" value="5" <?= checked_rating(5, $harga_rating); ?>>
                                                <label for="harga-5"><i class="fa fa-money"></i></label>
                                                <input type="radio" name="harga_rating" class="harga" id="harga-4" value="4" <?= checked_rating(4, $harga_rating); ?>>
                                                <label for="harga-4"><i class="fa fa-money"></i></label>
                                                <input type="radio" name="harga_rating" class="harga" id="harga-3" value="3" <?= checked_rating(3, $harga_rating); ?>>
                                                <label for="harga-3"><i class="fa fa-money"></i></label>
                                                <input type="radio" name="harga_rating" class="harga" id="harga-2" value="2" <?= checked_rating(2, $harga_rating); ?>>
                                                <label for="harga-2"><i class="fa fa-money"></i></label>
                                                <input type="radio" name="harga_rating" class="harga" id="harga-1" value="1" <?= checked_rating(1, $harga_rating); ?>>
                                                <label for="harga-1"><i class="fa fa-money"></i></label>
                                            </div>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Deskripsi Singkat </label>
                                        <label class="textarea">
                                            <textarea name="deskripsi_singkat" placeholder="Deskripsi singkat"><?= $deskripsi_singkat ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Paling Cocok Untuk</label>
                                        <label class="input">
                                            <input name="paling_cocok_untuk" type="text" placeholder="Paling cocok untuk" value="<?= $paling_cocok_untuk ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Kelebihan </label>
                                        <label class="textarea">
                                            <textarea name="kelebihan" placeholder="Kelebihan"><?= $kelebihan ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Hasil Akhir</label>
                                        <label class="input">
                                            <input name="hasil_akhir" type="text"  class="form-control" placeholder="Hasil Akhir (Semi Gloss, Gloss, dsb.)" value="<?= $hasil_akhir ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Waktu Pengeringan</label>
                                        <label class="input">
                                            <input name="time_kering" type="text"  class="form-control" placeholder="Waktu Pengeringan" value="<?= $time_kering ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Jumlah Lapisan Cat</label>
                                        <label class="input">
                                            <input name="lapisan_cat" type="text"  class="form-control" placeholder="2 Lapis , 3Lapis dsb." value="<?= $lapisan_cat ?>" />
                                        </label>
                                    </section>
                                    

                                    <section>
                                        <label class="label">Minimum Spread Rate</label>
                                        <label class="input">
                                            <input name="min_spread_rate" type="text"  class="form-control" placeholder="Spread rate cat, dengan angka di belakang koma" value="<?= $min_spread_rate ?>" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Spread Rate</label>
                                        <label class="input">
                                            <input name="spread_rate" type="text"  class="form-control" placeholder="Spread rate cat, dengan angka di belakang koma" value="<?= $spread_rate ?>" />
                                        </label>
                                    </section>


                                    <section>
                                        <label class="label">Bisa Tinting?</label>
                                        <label class="select">
                                            <select name="bisa_tinting">
                                                <option value="0" <?= ($bisa_tinting=='0') ? 'selected="selected"' : '' ?>>Tidak Bisa</option>
                                                <option value="1" <?= ($bisa_tinting=='1') ? 'selected="selected"' : '' ?>>Ready Mix Only</option>
                                                <option value="2" <?= ($bisa_tinting=='2') ? 'selected="selected"' : '' ?>>Tinting Only</option>
                                                <option value="3" <?= ($bisa_tinting=='3') ? 'selected="selected"' : '' ?>>Bisa Ready Mix dan Tinting</option>
                                            </select>
                                            <i></i>
                                        </label>
                                    </section>
                                    
                                    <section>
                                        <label class="label">Youtube Video / Link Video </label>
                                        <label class="input">
                                            <input name="ytb_vid" type="text"  class="form-control" placeholder="youtube.com/qweqweqw=" value="<?= ytb_vid ?>" />
                                        </label>
                                    </section>
                                    
                                    <section>
                                        <label class="label">Tags</label>
                                        <label class="select">
                                            <select name="select_tags" class="select2_tags" multiple="multiple">
                                                <?php
                                                $selectedtags_arr = explode(", ", $selectedtags);
                                                foreach($tags as $key => $value){ ?>
                                                <option value="<?= $value ?>" <?= (in_array($value, $selectedtags_arr) !== false) ? 'selected="selected"' : '' ?>><?= $value ?></option>
                                                <?php } ?>
                                            </select>
                                            <i></i>
                                        </label>
                                        <input id="tags" type="hidden" name="tags" value="<?= $selectedtags ?>" />
                                    </section>
                                </fieldset>

                                <fieldset>
                                    <section>
                                        <label class="label">Meta description </label>
                                        <label class="input">
                                            <textarea name="meta_desc" id="meta_desc" class="form-control" placeholder="untuk SEO, deskripsi dari Tips / Article, sebaiknya 70 karakter minimum."><?= $meta_desc ?></textarea>
                                    </label>
                                    </section>

                                    <section>
                                        <label class="label">Meta keywords </label>
                                        <label class="input">
                                            <textarea name="meta_keys" id="meta_keys" class="form-control" placeholder="Kata kunci untuk SEO dari Tips / Article"><?= $meta_keys ?></textarea>
                                        </label>
                                    </section>
                                </fieldset>

                                <fieldset>
                                    <?php if ($created_date != ""): ?>
                                    <section>
                                        <label class="label">Created date </label>
                                        <label class="input"> 
                                            <?php echo $created_date; ?>
                                        </label>
                                    </section> 
                                    <?php endif; ?>
                                    
                                    <?php if ($updated_date != ""): ?>
                                    <section>
                                        <label class="label">Update date </label>
                                        <label class="input"> 
                                            <?php echo $updated_date; ?>
                                        </label>
                                    </section> 
                                    <?php endif; ?>
                                    
                                    <?php if ($deleted_date != ""): ?>
                                    <section>
                                        <label class="label">Deleted date </label>
                                        <label class="input"> 
                                            <?php echo $deleted_date; ?>
                                        </label>
                                    </section> 
                                    <?php endif; ?>
                                </fieldset>
                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>
                <!-- end widget div -->
                </article>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
