<?php
    $id    = isset($item["id"]) ? $item["id"] : "";
    $title  = isset($item["title"]) ? $item["title"] : "";
    $image_url = isset($item["image_url"]) ? $item["image_url"] : "";
    $image_big = isset($item["image_big"]) ? $item["image_big"] : "";
    $file_pretty_name = isset($item["file_pretty_name"]) ? $item["file_pretty_name"] : "";
    $ordering = isset($item["ordering"]) ? $item["ordering"] : "";
    $is_show = isset($item["is_show"]) ? $item["is_show"] : "";
    $category_id = isset($item["category_id"]) ? $item["category_id"] : "";
    $created_date = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date = isset($item["updated_date"]) ? $item["updated_date"] : "";
    
    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>


    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Product Slider</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/product/slider/process-form" method="post" enctype="multipart/form-data">
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                            <fieldset>
                                <section>
                                    <label class="label">Title</label>
                                    <label class="input">
                                        <input name="title" type="text"  class="form-control" placeholder="Title" value="<?= $title; ?>" />
                                    </label>
                                </section>
                                
                                <section>
                                    <label class="label"> Category Product <sup class="color-red">*</sup></label>
                                    <label class="select">
                                        <?php echo select_product_category('category_id', $category_id , 'id="category_id" class="form-control"'); ?>
                                        <i></i>
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Image Tabel Comparison <sup class="color-red">*</sup></label>
                                    <label class="input">
                                        <input type="file" name="attachment" id="file" class="form-control">
                                        <?php if ($image_big): ?>
                                        <img src="<?php echo $image_big; ?>" width="300px" height="200px"/>
                                        <?php endif; ?>
                                    </label>            
                                </section> 
                                
                                <section>
                                    <label class="label">Image Small(1588x347)<sup class="color-red">*</sup></label>
                                    <div class="input">
                                        <div class="add-image-preview" id="preview-image">
                                            <?php if($image_url): ?>
                                            <img src="<?= $image_url ?>" height="100px"/>
                                            <?php endif; ?>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                    </div>
                                </section>

                                <section>
                                    <label class="label">Image pretty name</label>
                                    <label class="input">
                                        <input name="file_pretty_name" type="text"  class="form-control" placeholder="Image Pretty Name" value="<?= $file_pretty_name; ?>" />
                                    </label>
                                </section>

                                <?php if ($created_date != ""): ?>
                                <section>
                                    <label class="label">Created date </label>
                                    <label class="input"> 
                                        <?php echo $created_date; ?>
                                    </label>
                                </section> 
                                <?php endif; ?>
                                
                                <?php if ($updated_date != ""): ?>
                                <section>
                                    <label class="label">Update date </label>
                                    <label class="input"> 
                                        <?php echo $updated_date; ?>
                                    </label>
                                </section> 
                                <?php endif; ?>  
                            </fieldset>
                        </form>

                    </div>
                        <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </article>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->