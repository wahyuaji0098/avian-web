<?php
    $produk_id          = isset($model["id"]) ? $model["id"] : "";
    $rating             = isset($rating["rating"]) ? $rating["rating"] : "";
    $kualitas_rating    = isset($model["kualitas_rating"]) ? $model["kualitas_rating"] : "";
    $harga_rating       = isset($model["harga_rating"]) ? $model["harga_rating"] : "";
    $reviews            = isset($model["total_reviews"]) ? $model["total_reviews"] : "";
    $deskripsi_singkat  = isset($model["deskripsi_singkat"]) ? $model["deskripsi_singkat"] : "";
    $paling_cocok_untuk = isset($model["paling_cocok_untuk"]) ? $model["paling_cocok_untuk"] : "";
    $kelebihan          = isset($model["kelebihan"]) ? $model["kelebihan"] : "";

    // view_product_rating
    $rating_avg         = isset($vpr["rating_avg"]) ? $vpr["rating_avg"] : "";
    $total_discussions  = isset($vpr["total_discussions"]) ? $vpr["total_discussions"] : "";
    $count_5_star       = isset($vpr["count_5_star"]) ? $vpr["count_5_star"] : "";
    $count_4_star       = isset($vpr["count_4_star"]) ? $vpr["count_4_star"] : "";
    $count_3_star       = isset($vpr["count_3_star"]) ? $vpr["count_3_star"] : "";
    $count_2_star       = isset($vpr["count_2_star"]) ? $vpr["count_2_star"] : "";
    $count_1_star       = isset($vpr["count_1_star"]) ? $vpr["count_1_star"] : "";
    $count_0_star       = isset($vpr["count_0_star"]) ? $vpr["count_0_star"] : "";
?>

<div class="container container_full" itemscope itemtype="http://schema.org/Product">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/product">KATEGORI PRODUK</a>
                <a href="/product/lists/<?= $model['category_pretty_url'] ?>"><?= strtoupper($model['category_name']) ?></a>
                <span><?= strtoupper($model['name']) ?></span>
            </div>
        </div>
    </div>
    <div class="section section-product-quikinfo">
        <div class="section_content">
            <div class="section-product-quikinfo-r">
                <div class="product-image">
                    <img src="<?= $model['image_url'] ?>" alt="<?= $model['name'] ?>" itemprop=”image”/>
                </div>
                <div class="product-cat"><?= strtolower($model['category_name']) ?></div>
            </div>
            <div class="section-product-quikinfo-l">
                <h1 class="product-name"><?= $model['name'] ?></h1>
                <div class="product-infos">
                    <div class="product-info">
                        <h4 class="product-info-name">Ukuran:</h4>
                        <div class="product-info-content"><?= str_replace(";"," ".$model['satuan'].", ",$model['packaging'])." ".$model['satuan'] ?></div>
                    </div>
                    <div class="product-info">
                        <h4 class="product-info-name">TDS &amp; MSDS:</h4>
                        <div class="product-info-content">Lihat <a href="<?= $model['file_url'] ?>">TDS</a><?php if ($model['file_url_msds']): ?> dan <a href="<?= $model['file_url_msds'] ?>">MSDS</a><?php endif; ?> di sini.</div>
                    </div>
                    <div class="product-info">
                        <h4 class="product-info-name">Pilihan warna:</h4>
                        <div class="product-info-content">
                            <?php if($color == 1) : ?>
                            <?= number_format($total_color) ?> warna<?= Dynamic_model::$tinting_text_product_detail[$model['bisa_tinting']] ?><br/>
                            <a href="/palette/lists/<?= $model['pretty_url'] ?>">(klik di sini untuk melihat daftar warna)</a>
                            <?php else: ?>
                                Tidak ada Warna.
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-product-buttons">
        <div class="section_content">
            <?php if (count($awards) > 0): ?>
            <div class="btn btn_invicon btn_awards trigger triggerofst triggerscrollbar" targid="awardspop">Lihat Penghargaan</div>
            <?php endif; ?>
            <?php if (count($video) > 0): ?>
            <div class="btn btn_invicon btn_videos trigger triggerofst triggerscrollbar" targid="videopop">Lihat Video</div>
            <?php endif; ?>
        </div>
    </div>
    <div class="section section-product-widget">
        <div class="section_content">
            <div class="product-widget">
                <div class="product-widget-info rates">
                    <div class="rate">
                        <div class="ratename">Kualitas:</div>
                        <div class="ratecount ratecount-<?= $kualitas_rating ?> ratecount-staryellow"></div>
                    </div>
                    <div class="rate">
                        <div class="ratename">Tingkat harga:</div>
                        <div class="ratecount ratecount-<?= $harga_rating ?> ratecount-coin"></div>
                    </div>
                    <?php /*
                    <div class="rate">
                        <div class="ratename">Nilai pengguna:</div>
                        <div class="ratecount ratecount-<?= round($rating_avg) ?> ratecount-stargreen"></div>
                    </div>
                    */ ?>
                </div>
                <div class="product-widget-btns">
                    <a href="/store/find/<?= $model['name'] ?>" class="widget-btn widget-btn-nav"><i class="fa fa-location-arrow" aria-hidden="true"></i>Lokasi</a>
                    <div class="widget-btn widget-btn-share toggler_popup toggler_popup_trigger toggler-sharepop" data-toggler-popup="sharepop"><i class="fa fa-share-alt" aria-hidden="true"></i>Bagikan
                        <!-- share with plugin -->
                        <div class="addthis_inline_share_toolbox sharepopup toggler_popup toggler-sharepop" id="share"></div>
                    </div>
                    <?php if( !empty($model['spread_rate']) ): ?>
                    <a href="#" class="widget-btn widget-btn-calc" id="calcpopupbtn"><i class="fa fa-calculator" aria-hidden="true"></i>Kalkulator</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-product-quikinfocont">
        <div class="section_content">
            <div class="product-infos row">
                <?php if(!empty($deskripsi_singkat)) : ?>
                    <div class="product-info col col-sm-12 col-md-4">
                        <h4 class="product-info-name">Deskripsi singkat:</h4>
                        <div class="product-info-content"><?= $deskripsi_singkat ?></div>
                    </div>
                <?php else : ?>
                    <div></div>
                <?php endif; ?>
                <?php if(!empty($paling_cocok_untuk)) : ?>
                    <div class="product-info col col-sm-12 col-md-4">
                        <h4 class="product-info-name">Paling cocok untuk:</h4>
                        <div class="product-info-content"><?= $paling_cocok_untuk ?></div>
                    </div>
                <?php else : ?>
                    <div></div>
                <?php endif; ?>
                <?php if(!empty($kelebihan)) : ?>
                    <div class="product-info col col-sm-12 col-md-4">
                        <h4 class="product-info-name">Kelebihan:</h4>
                        <div class="product-info-content"><?= $kelebihan ?></div>
                    </div>
                <?php else : ?>
                    <div></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="section section-product-detail product-infos">
        <div class="section_content product-info">
            <h4 class="product-info-name">Detail produk:</h4>
            <div class="product-info-content"><?= $model['description'] ?></div>
        </div>
    </div>
    <div class="section section-product-accordions">
        <div class="section_content accordions">

           <?php if ($model['usability_feature']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-1">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-1" data-toggler-group="a" data-toggler-id="1">Kegunaan dan Keistimewaan</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['usability_feature'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['technical_data']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-2">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-2" data-toggler-group="a" data-toggler-id="2">Data Teknis</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['technical_data'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['surface_prep']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-3">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-3" data-toggler-group="a" data-toggler-id="3">Persiapan Permukaan</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['surface_prep'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['how_to_use']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-4">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-4" data-toggler-group="a" data-toggler-id="4">Cara Penggunaan</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['how_to_use'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['cleaning_tools']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-5">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-5" data-toggler-group="a" data-toggler-id="5">Pembersihan Peralatan</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['cleaning_tools'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['how_to_store']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-6">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-6" data-toggler-group="a" data-toggler-id="6">Cara Simpan dan Penanganannya</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['how_to_store'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if ($model['additional_information']): ?>
            <div class="accordion toggler_grouped toggler-group-a toggler-a-7">
                <div class="accordion_name toggler_grouped_trigger toggler-group-a toggler-a-7" data-toggler-group="a" data-toggler-id="7">Info Lainnya</div>
                <div class="accordion_contain">
                    <div class="accordion_content">
                        <?= $model['additional_information'] ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="section section-product-ratings">
        <div class="section_content">
            <div class="product-ratings row">
                <div class=" product-rating col col-md-6 col-sm-12">
                    <h4 class="product-rating-name">Rating kualitas dari Avian Brands:</h4>
                    <div class="product-rating-content">
                        <div class="product-rating-star star-yellow rate-<?= $kualitas_rating ?>"><?= isset($model["kualitas_rating"]) ? $kualitas_rating.".0" : "-" ?></div>
                        <div class="product-rating-info">Info</div>
                        <p>
                            Penilaian yang diberikan dimaksudkan untuk memberikan gambaran kualitas dan fitur produk bila dibandingkan dengan produk lain.
                        </p>
                    </div>
                </div>
                <div class=" product-rating col col-md-6 col-sm-12">
                    <h4 class="product-rating-name">Rating harga dari Avian Brands:</h4>
                    <div class="product-rating-content">
                        <div class="product-rating-star coin-yellow rate-<?= $harga_rating ?>"><?= isset($model["harga_rating"]) ? $harga_rating.".0" : "-" ?></div>
                        <div class="product-rating-info">Info</div>
                        <p>
                            Tingkat harga yang diberikan dimaksudkan untuk memberikan gambaran jangkauan harga produk bila dibandingkan dengan harga produk lain.
                        </p>
                    </div>
                </div>
                <?php /*
                <div class=" product-rating col col-md-6 col-sm-12">
                    <h4 class="product-rating-name">Rating rata-rata dari pengguna:</h4>
                    <div class="product-rating-content">
                        <div class="product-rating-star user-green rate-<?= $rating_avg ?>"> <?= isset($vpr['rating_avg']) ? round($rating_avg, 1) : "-" ?></div>
                        <div class="ratingcounts">
                            <div class="ratingcount ratingcount-5"><?= $count_5_star+0 ?></div>
                            <div class="ratingcount ratingcount-4"><?= $count_4_star+0 ?></div>
                            <div class="ratingcount ratingcount-3"><?= $count_3_star+0 ?></div>
                            <div class="ratingcount ratingcount-2"><?= $count_2_star+0 ?></div>
                            <div class="ratingcount ratingcount-1"><?= $count_1_star+0 ?></div>
                            <div class="ratingcounttotals"><span><?= $count_5_star+$count_4_star+$count_3_star+$count_2_star+$count_1_star ?></span> Penilaian &nbsp;&nbsp;<span><?= $total_discussions+0 ?></span> Diskusi</div>
                        </div>
                    </div>
                </div>
                */ ?>
            </div>
        </div>
    </div>
    <?php /*
    <div class="section section-product-threads">
        <div class="section_content">
            <div class="product-threads">
                <div class="product-threads-tabs-contain">
                    <div class="product-threads-tabs">
                        <div class="product-threads-tab togglertab_grouped_trigger togglertab-group-b togglertab-b-1 active" data-togglertab-group="b" data-togglertab-id="1">Review</div>
                        <div class="product-threads-tab togglertab_grouped_trigger togglertab-group-b togglertab-b-2" data-togglertab-group="b" data-togglertab-id="2">Diskusi</div>
                    </div>
                </div>
                <div class="product-threads-contents">
                    <div class="product-threads-content togglertab-group-b togglertab-b-1 active">
                        <?php foreach($all_review as $key => $value) : ?>
                        <div class="thread-item rates">
                            <div class="thread-title"><?= $value['review_title'] ?></div>
                            <div class="thread-rating rate">
                                <div class="ratecount ratecount-<?= $value['rating_score'] ?> ratecount-stargreen"></div>
                                <div class="thread-author">oleh <span><?= $value['name'] ?></span></div>
                            </div>
                            <div class="thread-datetime"><?= dateformatonly_indonesia($value['review_datetime']) ?></div>
                            <div class="thread-content"><?= $value['review_content'] ?></div>
                        </div>
                        <?php endforeach; ?>
                        <?= empty($all_review) ? '<div class="thread-empty">Belum ada ulasan</div>' : '' ?>
                        <div class="loadingcell"><span>Memuat lainnya ...</span></div>
                    </div>
                    <div class="product-threads-content togglertab-group-b togglertab-b-2">
                        <?php foreach($all_comment as $key => $value) : ?>
                        <div class="thread-item">
                            <div class="thread-content"><?= $value['discussion_content'] ?></div>
                            <div class="mc-author">By <?= $value['name'] ?> on <?= dateformatonly_indonesia($value['discuss_datetime']) ?></div>

                            <!-- LIST REPLY -->
                            <?php foreach($value['reply'] as $key2 => $value2) : ?>
                            <div class="mc-reply">
                                <div class="thread-content"><?= $value2['discussion_content']; ?></div>
                                <div class="mc-author">By <?= $value2['name'] ?> on <?= dateformatonly_indonesia($value2['discuss_datetime']) ?></div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endforeach; ?>
                        <?= empty($all_comment) ? '<div class="thread-empty">Belum ada diskusi</div>' : '' ?>
                        <div class="loadingcell"><span>Memuat lainnya ...</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    */ ?>
</div>

<div class="modal-contain targetvideopop">
    <div class="modal-shade"></div>
    <div class="modal-window modal-free modalofst">
        <div class="modal-content">
            <div class="modal-close trigger" targid="videopop"></div>
            <div class="modal-title">
                VIDEOS (<?= $model['name'] ?>)
            </div>
            <div class="media-slider">
                <div class="medias">
                    <?php if(count($video)>0): $iv=1; foreach($video as $vid): ?>
                    <div class="media_item targroupmedia targetmedia<?= $iv ?> <?= ($iv==1) ? "active" : "" ?>"><iframe width="600" height="340" src="<?= $vid['url'] ?>" frameborder="0" allowfullscreen></iframe></div>
                    <?php $iv++; endforeach; endif; ?>
                </div>
                <div class="media_thumbs horizontal-scroller scrollbarcust avian-scroll">
                    <div class="horizontal-scroller-content">
                        <?php if(count($video)>0): $jv=1; foreach($video as $vid): ?>
                        <div class="tab tabgroup media_thumb horizontal-scroller-item triggergroup" targid="media<?= $jv ?>" targroup="media"><img src="<?= $vid['image_url'] ?>" /></div>
                        <?php $jv++; endforeach; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-contain targetawardspop">
    <div class="modal-shade"></div>
    <div class="modal-window modal-free modalofst">
        <div class="modal-content">
            <div class="modal-close trigger" targid="awardspop"></div>
            <div class="modal-title">
                AWARDS (<?= $model['name'] ?>)
            </div>
            <div class="media-slider">
                <div class="medias">
                    <?php if(count($awards)>0): $i=1; foreach($awards as $awd): ?>
                    <div class="media_item targroupawd targetawd<?= $i ?> <?= ($i==1) ? "active" : "" ?>"><img src="<?= $awd['image_url'] ?>" alt="<?= $awd['image_alt_name'] ?>" /></div>
                    <?php $i++; endforeach; endif; ?>
                </div>
                <div class="media_thumbs horizontal-scroller scrollbarcust avian-scroll">
                    <div class="horizontal-scroller-content">
                        <?php if(count($awards)>0): $j=1; foreach($awards as $awd): ?>
                        <div class="tab tabgroup media_thumb horizontal-scroller-item triggergroup" targid="awd<?= $j ?>" targroup="awd"><img src="<?= $awd['image_url'] ?>" alt="<?= $awd['image_alt_name'] ?>" /></div>
                        <?php $j++; endforeach; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-contain calcpopup" data-sprate="<?= $model['spread_rate'] ?>" data-pack="<?= $model['packaging'] ?>" data-unit="<?= $model['satuan'] ?>">
    <div class="modal-shade"></div>
    <div class="modal-window modal-free modalofst">
        <div class="modal-content">
            <div class="modal-close closecalcpopup" targid="calcpopup"></div>
            <div class="modal-title">
                Kalkulator Estimasi Kebutuhan untuk Produk <?= $model['name'] ?>
            </div>
            <div>
                <div class="popcalc">
                    <div class="error"></div>
                    <div class="segment-title">Ukuran</div>
                    <div class="numbers">
                        <div class="number-title">Panjang Bidang (Meter)</div>
                        <input class="number-val" type="number" id="val-hei" min="0" />
                    </div>
                    <div class="numbers">
                        <div class="number-title">Lebar Bidang (Meter)</div>
                        <input class="number-val" type="number" id="val-wid" min="0" />
                    </div>
                    <div class="numbers">
                        <div class="number-title">Jumlah Bidang</div>
                        <input class="number-val" type="number" id="val-num" min="0" />
                    </div>
                    <hr />
                    <div class="segment-title">Dikurangi</div>
                    <div class="numbers">
                        <div class="number-title">Panjang Bidang (Meter)</div>
                        <input class="number-val" type="number" id="val-hei-min" min="0" />
                    </div>
                    <div class="numbers">
                        <div class="number-title">Lebar Bidang (Meter)</div>
                        <input class="number-val" type="number" id="val-wid-min" min="0" />
                    </div>
                    <div class="numbers">
                        <div class="number-title">Jumlah Bidang</div>
                        <input class="number-val" type="number" id="val-num-min" min="0" />
                    </div>
                    <div class="btncont">
                        <button class="btn tpaintcalc">Kalkulasi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-contain hasilcalc">
    <div class="modal-shade"></div>
    <div class="modal-window modal-free modalofst">
        <div class="modal-content">
            <div class="modal-close closehasilcalc" targid="hasilcalc"></div>
            <div class="modal-title">
                Hasil Perhitungan
            </div>
            <div>
                <div  class="desc_bidang">
                    <p>
                        Berikut ini adalah perkiraan jumlah kebutuhan cat untuk bidang seluas:
                    </p>
                    <p id="total_bidang">

                    </p>
                </div>
                <hr />
                <div class="divmiddle">
                    <p class="judul">
                        Untuk Produk:
                    </p>
                    <div class="prod">
                        <div class="left_prod">
                            <img src="<?= $model['image_url'] ?>" alt="<?= $model['name'] ?>" itemprop="image"/>
                        </div>
                        <div class="judul right_prod">
                            <p>
                                <?= $model['name'] ?>
                            </p>
                        </div>
                    </div>
                </div>
                <hr />
                <div>
                    <p class="judul">
                        Membutuhkan:
                    </p>
                    <div id="hasilprodcalc">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
