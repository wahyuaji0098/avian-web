<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/product">KATEGORI PRODUK</a>
                <span><?= strtoupper($pcategory['name']) ?></span>
            </div>
            <?php
                if (count($slider) > 0):
            ?>
            <div class="main-slider hotslider flexgrid" id="hotslider">
                <?php
                    foreach ($slider as $slide) :
                ?>
                <div class='mainslide mainslidewithcaption'>
                    <a href="<?= $slide['image_big'] ?>" class="mainslide-cont" data-lightbox="image" data-title="<?= $slide['title'] ?>">
                        <img src="<?= $slide['image_url'] ?>" alt="<?= $slide['title'] ?>"/>
                    </a>
                </div>
                <?php
                    endforeach;
                ?>
            </div>
            <?php
                endif;
            ?>
        </div>
    </div>
    <div class="section">
        <div class="section_content">
            <div class="content_sbar">
                <div class="sidebar sidebar-folded targetfold1">
                    <div class="sidebar-title trigger" targid="fold1">PRODUK</div>
                    <div class="sidebar-accordion">
                        <?php
                            if(count($allcat) > 0):
                                $i = 1;
                                foreach ($allcat as $cate):
                                    if (count($cate['product']) > 0):
                        ?>
                        <div class="accgroup <?= ($cate['id'] == $pcategory['id']) ? "active" : "" ?> target<?= $cate['id'] ?> targroup1">
                            <div class="accgroupname triggergroup" targid="<?= $cate['id'] ?>" targroup="1" targlink="/product/lists/<?= $cate['pretty_url'] ?>"><?= $cate['name'] ?></div>
                            <div class="accgroupcont">
                                <?php foreach ($cate['product'] as $prd):  ?>
                                <a href="/product/detail/<?= $prd['pretty_url'] ?>" class="accgroupitem"><?= $prd['name'] ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                            <?php   else: ?>
                        <a href="/product/lists/<?= $cate['pretty_url'] ?>" class="accgroupname"><?= $cate['name'] ?></a>
                        <?php       endif;
                                    $i++;
                                endforeach;
                            endif;
                        ?>

                    </div>
                </div>
                <div class="sbarcontent">
                    <div class="sbarcontentsection">
                        <div class="sbarcontent-arti">
                            <div class="arti-section">
                                <div class="arti-section-img">
                                    <img src="<?= $pcategory['image_url_large'] ?>" alt="<?= $pcategory['name'] ?>" />
                                </div>
                                <h1 class="arti-title mtop-15"><?= $pcategory['name'] ?></h1>
                                <div class="arti-text">
                                    <?= $pcategory['description'] ?>
                                </div>
                                <div class="panelthumbs row rowtight">
                                    <?php if(count($products) > 0): foreach($products as $prod): ?>
                                    <div class="col col-sm-12 col-md-6">
                                        <a class="panelthumb" href="/product/detail/<?= $prod['pretty_url'] ?>">
                                            <div class="panelthumb-thumb"><img src="<?= $prod['image_url'] ?>" alt="<?= $prod['name'] ?>"/></div>
                                            <div class="panelthumb-name"><?= $prod['name'] ?></div>
                                            <div class="panelthumb-text">
                                                <?= ($prod['description']) ? oneSentence(strip_tags($prod['description'])) : "" ; ?>
                                            </div>
                                            <?php if(!empty($prod['paling_cocok_untuk'])) : ?>
                                                <div class="panelthumb-sub">Cocok untuk:</div>
                                                <div class="panelthumb-subtext">
                                                    <?= $prod['paling_cocok_untuk'] ?>
                                                </div>
                                            <?php else:?>
                                                <div></div>
                                            <?php endif;?>
                                            <?php if(!empty($prod['kelebihan'])): ?>
                                                <div class="panelthumb-sub">Kelebihan:</div>
                                                <div class="panelthumb-subtext">
                                                    <?= $prod['kelebihan'] ?>
                                                </div>
                                            <?php else: ?>
                                                <div></div>
                                            <?php endif;?>
                                            <div class="rates">
                                                <div class="rate">
                                                    <div class="ratename">Kualitas:</div>
                                                    <div class="ratecount ratecount-<?= $prod['kualitas_rating'] ?> ratecount-staryellow"></div>
                                                </div>
                                                <div class="rate">
                                                    <div class="ratename">Tingkat harga:</div>
                                                    <div class="ratecount ratecount-<?= $prod['harga_rating'] ?> ratecount-coin"></div>
                                                </div>
                                                <?php /*
                                                <div class="rate">
                                                    <div class="ratename">Nilai pengguna:</div>
                                                    <div class="ratecount ratecount-<?=$prod['total_reviews'] ?> ratecount-stargreen"></div>
                                                </div>
                                                */ ?>
                                            </div>
                                        </a>
                                    </div>
                                    <?php endforeach; endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
