<div class="container">
    <?php
        if (count($pcategory) > 0):
    ?>
    <div class="section gridds">
        <div class="section_content">
            <div class="section-title">KATEGORI PRODUK</div>
        </div>
        <div class="section_content col12 griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <?php
                foreach ($pcategory as $model) :
            ?>
            <a class="boxlink2 griddsitem width-3" href="/product/lists/<?= $model['pretty_url'] ?>">
                <div class="boxlinkimg">
                    <img src="<?= $model['image_url'] ?>" alt="<?= $model['name'] ?>"/>
                </div>
                <div class="boxlinktext"><?= $model['name'] ?></div>
            </a>
            <?php
                endforeach;
            ?>
        </div>
    </div>
    <?php
        endif;
    ?>
</div>
