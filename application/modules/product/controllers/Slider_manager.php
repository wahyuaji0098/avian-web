<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Controller.
 */
class Slider_manager extends Baseadmin_Controller  {

    private $_title = "Product Slider";
    private $_title_page = '<i class="fa-fw fa fa-cubes"></i>Product Slider';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "product-slider";
    private $_back = "/manager/product/slider/list_slider";
    private $_js_path = "/js/pages/product/slider/";
    private $_view_folder = "product/manager/slider/";

    private $_table = "dtb_product_slider";
    private $_table_aliases = "dps";
    private $_pk = "dps.id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product Slider
    */
    public function list_slider() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Product Slider </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li> Product Slider</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/lightbox/js/lightbox.min.js",
                $this->_js_path . "list.js",
            ),
            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            )
        );

        // //load models
        $this->load->model('product/Slider_model');
        $data['ordering_data'] = $this->Slider_model->getAllOrdering();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
    * Create an Product Category
    */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/product/slider/list_slider">Product Slider</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Product Slider</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Product Slider</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an product category
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/product/products">Product</a></li>';

        //load the model.
        $this->load->model('Product_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();
        }

        $params = array("row_array" => true,"conditions" => array("id" => $id, "is_show" => SHOW));
        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Product Slider</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Product Slider</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
                "/js/plugins/tinymce/tinymce.min.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation() {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("title", "title", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data product category
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //load model
        $this->load->model('Slider_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("dps.id", "dps.title", "dps.image_url", "dps.image_big","dps.file_pretty_name", "dps.ordering", "dp.name");
        $joined = array("dtb_product_category dp" => array("dp.id" => "dps.category_id"));
        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array(
            "dps.is_show" => SHOW,
        );

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {

                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'title':
                        if ($value != "") {
                            $data_filters['lower(title)'] = $value;
                        }
                        break;

                    case 'category_name':
                        if ($value != "") {
                            $data_filters['lower(dp.name)'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Slider_model->get_all_data(array(
            'select' => $select,
            'from'        => 'dtb_product_slider dps',
            'left_joined' => $joined,
            'order_by' => array($column_sort => $sort_dir),
            'limit' => $limit,
            'start' => $start,
            'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //load the model.
        $this->load->model('Slider_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                 = sanitize_str_input($this->input->post('id'));
        $title              = sanitize_str_input($this->input->post('title'));
        $file_pretty_name   = sanitize_str_input($this->input->post('file_pretty_name'));
        // $is_show = $this->input->post('is_show');
        $category_id        = sanitize_str_input($this->input->post('category_id'));
        $data_image         = $this->input->post('data-image');
        // pr($this->input->post());exit();

        //server side validation.
        $this->_set_rule_validation();

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $image_big = $this->upload_file ("attachment", $file_pretty_name, false, "upload/product-slider", $id);
            //init upload image
            $image_url = $this->upload_image($file_pretty_name, "upload/product-slider", "image-file", $data_image, 1588, 347, $id);

            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'title'             => $title,
                'file_pretty_name'  => $file_pretty_name,
                'category_id'       => $category_id,
            );

            if (isset($image_big['uploaded_path'])) {
                $arrayToDB['image_big'] = $image_big['uploaded_path'];
            }

            if (!empty($image_url)) {
                $arrayToDB['image_url'] = $image_url;
            }
            //insert or update?
            if ($id == "") {
                //insert to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert($arrayToDB, array('is_version' => true, "ordering" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Product Slider has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/product/slider/list_slider";
                }

            } else {
                //get current slider data
                $curr_data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

                //update.
                if (!empty($image_url) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                    unlink( FCPATH . $curr_data['image_url'] );
                }

                 //file is exists
                if(isset($image_big['uploaded_path']) && isset($curr_data['image_big']) && !empty($curr_data['image_big'])) {
                    unlink( FCPATH . $curr_data['image_big'] );
                }

                //condition for update.
                $condition = array("id" => $id);
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update($arrayToDB, $condition, array('is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Products Slider has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/product/slider/list_slider";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an admin.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('product/Slider_model');

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                $models = $this->Slider_model->getAllSliderAfterNumber($data['ordering']);
                $ordering = $data['ordering'];
                foreach ($models as $model) {
                    $this->Slider_model->update(array(
                        'ordering' =>  $ordering,
                    ),array('id' => $model['id']), array("is_version" => true));
                    $ordering++;
                }

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(array("is_show" => HIDE, "ordering" => NULL), $condition, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Product Slider has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
    /**
     * ordering
     */

    public function ordering() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id    = $this->input->post('id');
        $order = $this->input->post('val');

        $this->load->model("product/Slider_model");

        $table = $this->Slider_model;
        $model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        $datas = $table->getAllSlider();

        $min = $table->getFirstOrdering();
        $max = $table->getLastOrdering();

        $newkey = searchForIdInt($order, $datas, 'ordering');
        $oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

        $oldOrdering = $model['ordering'];

        $this->db->trans_begin();

        if ($oldOrdering > $order) {
            //smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
            for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']+1,
                ),array('id' => $datas[$i]['id']), array("is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array("is_version" => true));

        } else if ($oldOrdering < $order) {
            //smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
            for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']-1,
                ),array('id' => $datas[$i]['id']), array("is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array("is_version" => true));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title']   = "Done!";
            $message['notif_message'] = "Product Slider Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
