<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Controller.
 */
class Products_manager extends Baseadmin_Controller  {

    private $_title = "Product";
    private $_title_page = '<i class="fa-fw fa fa-cubes"></i> Product';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "product";
    private $_back = "/manager/product/products/list_product";
    private $_js_path = "/js/pages/product/product/";
    private $_view_folder = "product/manager/product/";

    private $_table = "dtb_product";
    private $_table_aliases = "dp";
    private $_pk = "dp.id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product
    */
    public function list_product() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List  Product </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li> Product</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load models
        $this->load->model('product/Product_model');
        $data['ordering_data'] = $this->Product_model->getAllOrdering();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Product Category
    */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/product/product">Product</a></li>';

        //load the model.
        $this->load->model('Product_model');
        $data['tags'] = null;

        //get the tags data from dtb_product.
        $tags = $this->Product_model->get_all_data(array("select" => "tags", "conditions" => array("tags is not null" => NULL)))['datas'];

        $tags_arr = [];

        //arrange and merge the tags into one array.
        foreach($tags as $key => $value){
            $tags_arr = array_merge($tags_arr, explode(", ", $value['tags']));
        }

        //Tidy up array, remove duplicates and empty spaces, reindex then sort by value.

        $tags_arr = array_unique($tags_arr);
        $tags_arr = array_filter($tags_arr, function($value) { return $value !== ''; });
        $tags_arr = array_values($tags_arr);
        asort($tags_arr);

        $data['tags'] = $tags_arr;

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Product</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Product</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
                "/js/plugins/select2.min.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an product category
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/product/products">Product</a></li>';

        //load the model.
        $this->load->model('Product_model');
        $data['item'] = null;
        $data['tags'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();
        }

        $params = array("row_array" => true,"conditions" => array("id" => $id));
        //get the data.
        $data['item'] = $this->Product_model->get_all_data($params)['datas'];

        //get the tags data from dtb_product.
        $tags = $this->Product_model->get_all_data(array("select" => "tags"))['datas'];

        $tags_arr = [];

        //arrange and merge the tags into one array.
        foreach($tags as $key => $value){
            $tags_arr = array_merge($tags_arr, explode(", ", $value['tags']));
        }

        //Tidy up array, remove duplicates and empty spaces, reindex then sort by value.

        $tags_arr = array_unique($tags_arr);
        $tags_arr = array_filter($tags_arr, function($value) { return $value !== ''; });
        $tags_arr = array_values($tags_arr);
        asort($tags_arr);

        $data['tags'] = $tags_arr;

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Product</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Product</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * video youtube
     */
    public function video($id=0) {
        $this->_breadcrumb .= '<li><a href="/manager/product/product">Product</a></li>';

        //load the models
        $this->load->model(array(
            'Product_model',
            'Dynamic_model'
        ));

        //first chek id
        if(!is_numeric($id)) {
            show_404();
        }
        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create video</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create video</li>',
            "back"          => $this->_back,
        );

        //get data product
        $data['item'] = $this->Product_model->get_all_data(array(
            "conditions"    => array("id" => $id),
            "row_array"     => true,
        ))['datas'];

        //get data video
        $data['video'] = $this->Dynamic_model->set_model("dtb_product_video","dpv","id")->get_all_data(array(
            "conditions" => array("product_id" => $id),
            "count_all_first" => true,
        ))['datas'];

        //check data
        if(empty($data['item'])) {
            show_404();
        }

        $footer = array(
            "script" => array(
                $this->_js_path . "video.js",
                $this->_js_path . "create.js",
                "/js/plugins/tinymce/tinymce.min.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'video', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("pretty_url", "Pretty URL", "trim|required|callback_check_pretty_url[$id]");
        $this->form_validation->set_rules("description", "Description", "trim|required");
        $this->form_validation->set_rules("product_category_id", "Category Product", "trim|required");
        $this->form_validation->set_rules("show_in_filter", "Show In Filter", "trim|required");
    }

    /**
     * check pretty url
     */
    public function check_pretty_url($str, $id) {
        //sanitize input
        $str = sanitize_str_input($str);
        $id = sanitize_str_input($id);

        if (!$this->_secure) {
            show_404();
        }
        //load the model
        $this->load->model('Product_model');

        //flag.
        $isValid = true;
        $params = array("row_array" => true);

        if ($id == "") {
            //from create
            $params['conditions'] = array("lower(pretty_url)" => strtolower($str));
        } else {
            $params['conditions'] = array("lower(pretty_url)" => strtolower($str), "id !=" => $id);
        }

        $datas = $this->Product_model->get_all_data($params)['datas'];

        if ($datas) {
            $isValid = false;
            $this->form_validation->set_message('check_pretty_url', '{field} is already taken.');
        }

        return $isValid;
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data product category
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //load model
        $this->load->model('Product_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("dp.id","dp.name","dpc.name as category","dp.ordering", "is_hot_item", "dp.description", "dp.pretty_url", "dp.is_show", "dp.show_in_filter");
        $joined = array("dtb_product_category dpc" => array("dpc.id" => "dp.product_category_id"));
        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'kategori':
                        if ($value != "") {
                            $data_filters['lower(dpc.name)'] = $value;
                        }
                        break;

                    case 'names':
                        if ($value != "") {
                            $data_filters['lower(dp.name)'] = $value;
                        }
                        break;

                    case 'description':
                        if ($value != "") {
                            $data_filters['lower(description)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['dp.is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'in_filter':
                        if ($value != "") {
                            $data_filters['show_in_filter'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'hot_item':
                        if ($value != "") {
                            $data_filters['is_hot_item'] = ($value == "active") ? 0 : 1;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Product_model->get_all_data(array(
            'select' => $select,
            'left_joined' => $joined,
            'order_by' => array($column_sort => $sort_dir),
            'limit' => $limit,
            'start' => $start,
            'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
            "status" => STATUS_ACTIVE,
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //load the model.
        $this->load->model('Product_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                     = sanitize_str_input($this->input->post('id'));
        $product_category_id    = sanitize_str_input($this->input->post('product_category_id'));
        $code                   = sanitize_str_input($this->input->post('code'));
        $name                   = sanitize_str_input($this->input->post('name'));
        $pretty_url             = sanitize_str_input($this->input->post('pretty_url'));
        $description            = sanitize_str_input($this->input->post('description'));
        $image_pretty_name      = sanitize_str_input($this->input->post('image_pretty_name'));
        $filename               = sanitize_str_input($this->input->post('filename'));
        $filename_msds          = sanitize_str_input($this->input->post('filename_msds'));
        $usability_feature      = $this->input->post('usability_feature');
        $technical_data         = $this->input->post('technical_data');
        $is_hot_item            = $this->input->post('is_hot_item');
        $ytb_vid                = $this->input->post('ytb_vid');
        $is_new_item            = $this->input->post('is_new_item');
        $surface_prep           = $this->input->post('surface_prep');
        $how_to_use             = $this->input->post('how_to_use');
        $cleaning_tools         = $this->input->post('cleaning_tools');
        $how_to_store           = $this->input->post('how_to_store');
        $safety_info            = $this->input->post('safety_info');
        $packaging              = sanitize_str_input_a_semicolon($this->input->post('packaging'));
        $satuan                 = sanitize_str_input($this->input->post('satuan'));
        $additional_information = $this->input->post('additional_information');
        $status                 = sanitize_str_input($this->input->post('status'));
        $is_show                = sanitize_str_input($this->input->post('is_show'));
        $show_in_filter         = sanitize_str_input($this->input->post('show_in_filter'));
        $meta_keys              = sanitize_str_input($this->input->post('meta_keys'));
        $meta_desc              = sanitize_str_input($this->input->post('meta_desc'));
        $data_image_large       = $this->input->post('data-image');
        $kualitas_rating        = sanitize_str_input($this->input->post('kualitas_rating'));
        $harga_rating           = sanitize_str_input($this->input->post('harga_rating'));
        $deskripsi_singkat      = sanitize_str_input($this->input->post('deskripsi_singkat'));
        $paling_cocok_untuk     = sanitize_str_input($this->input->post('paling_cocok_untuk'));
        $kelebihan              = sanitize_str_input($this->input->post('kelebihan'));
        $spread_rate            = sanitize_str_input($this->input->post('spread_rate'));
        $min_spread_rate        = sanitize_str_input($this->input->post('min_spread_rate'));
        $hasil_akhir        = sanitize_str_input($this->input->post('hasil_akhir'));
        $time_kering        = sanitize_str_input($this->input->post('time_kering'));
        $lapisan_cat        = sanitize_str_input($this->input->post('lapisan_cat'));

        $bisa_tinting           = $this->input->post('bisa_tinting');
        $tags                   = $this->input->post('tags');

        $tags_arr = explode(",", $tags);
        $tags = str_replace(",",", ",$tags);

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $this->load->library('Uploader');
            if(isset($_FILES['file_technical'])){
                $upload_file_url  = $this->upload_file("file_technical", $filename  , false,"upload/product/files",$id);
            }if(isset($_FILES['file_msds'])){
                $upload_file_msds = $this->upload_file("file_msds", $filename_msds  , false,"upload/product/files/msds",$id);
            }if(isset($_FILES['image-url'])){
                $image_pro = $this->upload_image($image_pretty_name, "upload/product", "image-url", $data_image_large, 325, 400, $id);
            }

            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'product_category_id'    => $product_category_id,
                'pretty_url'             => $pretty_url,
                'description'            => $description,
                'code'                   => $code,
                'name'                   => $name,
                'description'            => $description,
                'image_pretty_name'      => $image_pretty_name,
                'filename'               => $filename,
                'filename_msds'          => $filename_msds,
                'usability_feature'      => $usability_feature,
                'technical_data'         => $technical_data,
                'surface_prep'           => $surface_prep,
                'how_to_use'             => $how_to_use,
                'cleaning_tools'         => $cleaning_tools,
                'is_new_item'            => $is_new_item,
                'is_hot_item'            => $is_hot_item,
                'how_to_store'           => $how_to_store,
                'safety_info'            => $safety_info,
                'packaging'              => $packaging,
                'satuan'                 => $satuan,
                'additional_information' => $additional_information,
                'show_in_filter'         => $show_in_filter,
                'meta_keys'              => $meta_keys,
                'meta_desc'              => $meta_desc,
                'kualitas_rating'        => $kualitas_rating,
                'harga_rating'           => $harga_rating,
                'paling_cocok_untuk'     => $paling_cocok_untuk,
                'kelebihan'              => $kelebihan,
                'spread_rate'            => $spread_rate,
                'bisa_tinting'           => $bisa_tinting,
                'deskripsi_singkat'      => $deskripsi_singkat,
                'tags'                   => $tags,
                'min_spread_rate'        => $min_spread_rate,
                'hasil_akhir'            => $hasil_akhir,
                'time_kering'            => $time_kering,
                'ytb_vid'                => $ytb_vid,
                'lapisan_cat'            => $lapisan_cat,
            );

            if (!empty($image_pro)) {
                $arrayToDB['image_url'] = $image_pro;
            }

            if(isset($upload_file_url['uploaded_path'])) {
                $arrayToDB['file_url'] = $upload_file_url['uploaded_path'];
            }

            if(isset($upload_file_msds['uploaded_path'])) {
                $arrayToDB['file_url_msds'] = $upload_file_msds['uploaded_path'];
            }

            //insert or update?
            if ($id == "") {

                $ordering = $this->Product_model->getLastOrdering();
                $arrayToDB['ordering'] = $ordering+1;

                //insert to DB.
                $result = $this->Product_model->insert($arrayToDB, array('is_direct' => true, 'is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Product has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/product/products/list_product";
                }

            } else {
                //get current slider data
                $curr_data = $this->Product_model->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

                //update.
                if (!empty($image_url) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                    unlink( FCPATH . $curr_data['image_url'] );
                }

                if (!empty($image_pro) && isset($curr_data['image_url_large']) && !empty($curr_data['image_url_large'])) {
                    unlink( FCPATH . $curr_data['image_url_large'] );
                }

                if (!empty($upload_file_url['uploaded_path']) && isset($curr_data['file_url']) && !empty($curr_data['file_url'])) {
                    unlink( FCPATH . $curr_data['file_url'] );
                }

                if (!empty($upload_file_msds['uploaded_path']) && isset($curr_data['file_msds']) && !empty($curr_data['file_msds'])) {
                    unlink( FCPATH . $curr_data['file_msds'] );
                }

                //condition for update.
                $condition = array("id" => $id);
                $result = $this->Product_model->update($arrayToDB, $condition, array('is_direct' => true, 'is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Product has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/product/products/list_product";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an admin.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Product_model');

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                $models = $this->Product_model->getAllSliderAfterNumber($data['ordering']);
                $ordering = $data['ordering'];
                foreach ($models as $model) {
                    $this->Product_model->update(
                        array(
                            'ordering' =>  $ordering,
                        ),
                        array('id' => $model['id']),
                        array( "is_version" => true )
                    );
                    $ordering++;
                }

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $this->Product_model->update(array(
                        'status' => STATUS_DELETE,
                        "ordering" => NULL,
                    ),
                    $condition,
                    array(
                        "is_version" => true
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Product has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    ///////////////////////// AJAX CALL FROM OTHER //////////////////////////////

    /**
     * get all list of product
     * FOR : SELECT 2
     */
    public function get_list_product()
    {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //get ajax query and page.
        $select_q = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;

        //sanitazion.
        $select_q = sanitize_str_input($select_q);

        //page must numeric.
        $select_page = is_numeric($select_page) ? $select_page : 1;

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //for paging, calculate start.
        $limit = 10;
        $start = ($limit * ($select_page - 1));

        //filters.
        $filters = array();
        if ($select_q != "") {
            $filters["lower(name)"] = strtolower($select_q);
        }

        //conditions.
        $conditions = array( );

        //get data.
        $datas = $model->get_all_data(array(
            "conditions" => $conditions,
            "filter_or" => $filters,
            "count_all_first" => true,
            "limit" => $limit,
            "start" => $start,
            "status" => STATUS_ACTIVE,
        ));

        //prepare returns.
        $message["page"] = $select_page;
        $message["total_data"] = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"] = $datas['datas'];

        echo json_encode($message);
        exit;
    }

    /**
     * delete-file
     */
    public function delete_file() {
        //chek if ajax request
        if(!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        //load model
        $this->load->model('Product_model');

        //initial
        $message['is_error'] = true;

        $id = sanitize_str_input($this->input->post('id'));
        $type = sanitize_str_input($this->input->post('type'));

        if(!empty($id)) {
            $data = $this->Product_model->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array"  => true,
            ))['datas'];

            if(!$data) {
                $message["is_error"] = true;
                $message["message"] = "Entry Not Existing";
            } else {

                if($type == 2) {
                    if (!empty($data['file_url_msds'])) {
                        unlink(FCPATH .$data['file_url_msds']);
                    }

                    $conditions = array("id" => $id);
                    $this->Product_model->update(array('file_url_msds' => null), $conditions, array( "is_version" => true ));
                } else if($type == 1) {
                    if (!empty($data['file_url'])) {
                        unlink(FCPATH . $data['file_url']);
                    }
                    $this->Product_model->update(array('file_url' => null), $conditions, array( "is_version" => true ));
                }

                $message["is_error"] = false;
                $message["message"] = "Entry Removed Successfully";
            }
        } else {
            $message["message"] = "Entry Not Existing";
        }

         // $message["data"] = $data;
        echo json_encode($message);
        exit;
    }

   /**
     * Function process video
     */
    public function process_form_video(){
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //initial
        $message['is_error'] = false;
        $message['is_redirect'] = true;
        $message['error_count'] = 0;
        $message['redirect'] = true;

        $id  = sanitize_str_input($this->input->post('id'));
        $url = $this->input->post('url');

        $table = $this->_dm->set_model("dtb_product_video","dpv","id");

        if (count($url) > 0) {
            $url = array_reverse($url);
            foreach ($url as $value) {
                if ($value != "") {
                    $value = str_replace("/watch?v=","/embed/",$value);
                    if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $value, $match)) {
                        $video_id = $match[1];

                        //i will use the image_thumb_file for image thumbnail from youtube
                        $image_url = "http://img.youtube.com/vi/".$video_id."/0.jpg";
                    }

                    $table->insert(array(
                        'product_id' => $id,
                        'url' => $value,
                        'image_url' => $image_url,
                    ), array('is_direct' => true));
                }
            }
            //if succes page reload
            $message['redirect_to'] = '';
        }
        echo json_encode($message);
        exit;
    }

    /**
     * delete video
     */
    public function delete_video() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        //initial
        $message["is_error"] = true;

        $id = sanitize_str_input($this->input->post("id"));

        //begin transaction
        $this->db->trans_begin();
        //procces
        if (!empty($id)) {
            $data = $this->_dm->set_model("dtb_product_video", "dpv","id")->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array"  => true,
            ))['datas'];

            if (!$data) {
                $message["error_msg"] = "Entry Not Existing";
            }
            else {
                //condition for delete, array is_permanently = true
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model("dtb_product_video", "dpv","id")->delete($condition, array("is_permanently" => true));
                if($this->db->trans_status() == false) {
                    //failed
                    $this->db->trans_rollback();
                } else {
                    //success
                    $this->db->trans_commit();
                    $message["is_error"]       = false;
                    $message['notif_title']   = "Good!";
                    $message['notif_message'] = "Delete Video Successfully";
                    $message['redirect_to']   = '';
                }
            }
        }  else {
            $message['error_msg'] = "Invalid ID";
        }
        //encoding
        echo json_encode($message);
        exit;
    }

    /**
     * ordering
     */
    public function ordering() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id    = $this->input->post('id');
        $order = $this->input->post('val');

        $this->load->model("product/Product_model");

        $table = $this->Product_model;
        $model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        $datas = $table->getAllSlider();

        $min = $table->getFirstOrdering();
        $max = $table->getLastOrdering();

        $newkey = searchForIdInt($order, $datas, 'ordering');
        $oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

        $oldOrdering = $model['ordering'];

        $this->db->trans_begin();

        if ($oldOrdering > $order) {
            //smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
            for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']+1,
                ),array('id' => $datas[$i]['id']), array( "is_version" => true ));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array( "is_version" => true ));

        } else if ($oldOrdering < $order) {
            //smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
            for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']-1,
                ),array('id' => $datas[$i]['id']), array( "is_version" => true ));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array( "is_version" => true ));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title']   = "Done!";
            $message['notif_message'] = "Product Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
