<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Controller.
 */
class Product extends Basepublic_Controller  {

    private $_view_folder = "product/front/";

    private $_table = "dtb_product";
    private $_table_aliases = "dp";
    private $_pk = "dp.id";

    public function index() {
        //get header page
        redirect(base_url('products'));
		// $page = get_page_detail('product');

  //       //get product category
  //       $pcategory = $this->_dm->set_model("dtb_product_category", "dpc", "id")->get_all_data(array(
  //           "conditions" => array(
  //               "is_show" => SHOW,
  //           ),
  //           "order_by" => array("ordering" => "asc"),
  //       ))['datas'];

  //       $header = array(
  //           'header'    => $page,
  //           'pcategory' => $pcategory,
  //       );

  //       //load the views.
  //       $this->load->view(FRONT_HEADER, $header);
  //       $this->load->view($this->_view_folder . 'index');
  //       $this->load->view(FRONT_FOOTER);

	}

    public function lists() {

        $url = $this->uri->segment(3, 0);

        if (empty($url)) {
            show_404('page');
        }

        //load product model
        $this->load->model("Product_model");

        //check url if exist
        $pcategory = $this->_dm->set_model("dtb_product_category", "dpc", "id")->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
                "pretty_url" => $url,
            ),
            "row_array" => true,
        ))['datas'];

        if(empty($pcategory)) {
            show_404('page');
        }

        redirect(base_url('products/detail/'.$url));

        exit;
        //get all product by this category
        $products = $this->Product_model->get_all_data(array(
            "select"      => array("vpr.*", "dp.*"),
            "left_joined" => array("view_product_rating vpr" => array("vpr.product_id" => "dp.id")),
            "conditions" => array(
                "is_show" => SHOW,
                "product_category_id" => $pcategory['id']
            ),
            "order_by" => array("ordering" => "asc"),
            "status" => STATUS_ACTIVE
        ))['datas'];

        // Ambil view_product_rating
        $vpr = $this->_dm->set_model("view_product_rating", "vpr", "product_id")->get_all_data(array(
            "select" => array("vpr.*", "dp.*"),
            "conditions" => array("dp.pretty_url" => $url,),
            "left_joined" => array(
                "dtb_product dp" => array("dp.id" => "vpr.product_id"),
            ),
            "row_array" => true
        ))['datas'];
        // pr($vpr);exit;

        //get slider product
        $slider = $this->_dm->set_model("dtb_product_slider", "dps", "id")->get_all_data (array(
            "conditions" => array(
                "is_show" => SHOW,
                "category_id" => $pcategory['id']
            ),
            "order_by" => array("ordering" => "asc"),
        ))['datas'];

        //get all product category with product
        $allcat = $this->Product_model->getCategoryAndProduct();

		$page = array(
			"title"      => $pcategory['name'],
			"meta_desc"  => $pcategory['meta_desc'],
			"meta_keys"  => $pcategory['meta_keys'],
		);

        $header = array(
            'header'    => $page,
            'products'  => $products,
            'pcategory' => $pcategory,
            'slider'    => $slider,
            'allcat'    => $allcat,
            'vpr'       => $vpr
        );

        $footer = array(
            "script" => array(
                "/js/plugins/lightbox/js/lightbox.min.js",
                "/js/front/product_list.js"
            ),
            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            ),
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'lists');
        $this->load->view(FRONT_FOOTER, $footer);

	}

    public function detail() {

        $url = $this->uri->segment(3, 0);

        //load product model
        $this->load->model("Product_model");
        $this->load->model("awards/Awards_image_model");

        if (empty($url)) {
            show_404('page');
        }


        redirect(base_url('products/items/'.$url));

        exit;

        //check url if exist
        $product = $this->Product_model->get_all_data(array(
            "select" => array("dp.*", "dpc.pretty_url as category_pretty_url" , "dpc.name as category_name", "vpr.*"),
            "conditions" => array(
                "dp.is_show" => SHOW,
                "dp.pretty_url" => $url,
            ),
            "left_joined" => array(
                "dtb_product_category dpc" => array("dpc.id" => "dp.product_category_id"),
                "view_product_rating vpr" => array("vpr.product_id" => "dp.id")
            ),
            "status" => STATUS_ACTIVE,
            "row_array" => true,
        ))['datas'];

        if(empty($product)) {
            show_404('page');
        }

        //get hot item product
        $hot_items = $this->Product_model->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
                "is_hot_item" => HOT_ITEM_YES
            ),
            "status" => STATUS_ACTIVE,
        ))['datas'];

        $product_like = 0;

        //get product likebox
        if (isset($this->data_login_user['id'])) {
            $is_like = find_like_by_member ($this->data_login_user['id'], LIKEBOX_PRODUCT, $product['id']);
            $product_like = ($is_like) ? 1 : 0;
        }

		//get product awards
		$awards = $this->Awards_image_model->getAwardsImageForProduct($product['id']);

		//get product video
		$video = $this->_dm->set_model("dtb_product_video","dpv","id")->get_all_data(array(
            "conditions" => array(
                "product_id" => $product['id']
            ),
        ))['datas'];

		//get if exist avail color for this product
		$p_color = $this->_dm->set_model("mst_palette_product","mpp","mpp.id")->get_all_data(array(
            "conditions" => array(
                "product_id" => $product['id'],
                "dp.is_show" => SHOW,
                "dp.status" => STATUS_ACTIVE,
            ),
            "left_joined" => array(
                "dtb_pallete dp" => array("dp.id" => "mpp.palette_id"),
            ),
            "row_array" => true,
        ))['datas'];

		$color = (isset($p_color['id'])) ? 1 : 0;

        //get jumlah warna untuk product ini
        $total_color = $this->Product_model->get_total_color ($product['id']);

        // Ambil semua review untuk produk ini
        $all_review = $this->_dm->set_model("trs_product_review", "tpr", "id")->get_all_data(array(
            "select" => "tpr.*, dm.name",
            "conditions" => array(
                "dp.pretty_url" => $url,
            ),
            "left_joined" => array(
                "dtb_product dp" => array("dp.id" => "tpr.foreign_key"),
                "dtb_member dm" => array("dm.id" => "tpr.member_id")
            ),
            "order_by" => array("review_datetime" => "desc")
        ))['datas'];

        // Ambil semua comment untuk produk ini
        $all_comment = $this->_dm->set_model("trs_product_discussion", "tpd", "id")->get_all_data(array(
            "select" => "tpd.*, dm.name",
            "conditions" => array(
                "dp.pretty_url" => $url,
                "tpd.type" => 1
            ),
            "left_joined" => array(
                "dtb_product dp" => array("dp.id" => "tpd.foreign_key"),
                "dtb_member dm" => array("dm.id" => "tpd.member_id")
            ),
            "order_by" => array("discuss_datetime" => "desc")
        ))['datas'];

        // Ambil semua reply
        if($all_comment) {
            foreach($all_comment as $key => $value) {
                $all_comment[$key]['reply'] = $this->_dm->set_model("trs_product_discussion", "tpd", "id")->get_all_data(array(
                    "select" => "tpd.*, dm.name",
                    "conditions" => array("discussion_topic_id" => $value['id']),
                    "left_joined" => array(
                        "dtb_member dm" => array("dm.id" => "tpd.member_id")
                    ),
                ))['datas'];
            }
        }

        // Ambil view_product_rating
        $vpr = $this->_dm->set_model("view_product_rating", "vpr", "product_id")->get_all_data(array(
            "select" => array("vpr.*"),
            "conditions" => array("product_id" => $product['id']),
            "row_array" => true
        ))['datas'];

        #pr($all_comment);exit;

		$page = array(
			"title"      => $product['name'],
			"meta_desc"  => $product['meta_desc'],
			"meta_keys"  => $product['meta_keys'],
		);

        $header = array(
            'header'        => $page,
            'hot_items'     => $hot_items,
            'model'         => $product,
            'product_like'  => $product_like,
            'awards'        => $awards,
			'video'         => $video,
			'color'         => $color,
            'all_review'    => $all_review,
            'all_comment'   => $all_comment,
            'vpr'           => $vpr,
            'total_color'   => $total_color
        );

        $footer = array(
            "script" => array(
                "/js/front/product.js",
                "/js/plugins/sweetalert.min.js",
                "/js/plugins/jquery.form.min.js",
                "/js/plugins/jquery.validate.min.js",
                "//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ae74c4dc6f7454b"
            ),
            "css" => array(
                "/css/sweetalert.css",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'detail');
        $this->load->view(FRONT_FOOTER, $footer);

	}

    public function save_rating () {
        // Must AJAX and POST
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // Declare variable here
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        // Populate form fields
        $rate = $this->input->post('rate');
        $produk_id = $this->input->post('produk_id');
        $user_id = "";

        // Kalo belum login, ga bs kasih rating

        if (isset($this->data_login_user['id'])) {
            $user_id = $this->data_login_user['id'];
        }
        else {
            $message['error_msg']   = "Untuk memberikan rating, silahkan login terlebih dahulu.";
            echo json_encode($message);
            exit;
        }

        // Gagal jika ga dapet produk id
        if(!$produk_id) {
            $message['error_msg']   = "Invalid produk id.";
            echo json_encode($message);
            exit;
        }

        $this->db->trans_begin();

        $arrayToDb = array(
            "rating" => $rate,
            "user_id" => $user_id,
            "produk_id" => $produk_id
        );

        $extra_params = array("is_direct" => true);

        // Cek apakah user ini sudah pernah kasih rating di produk ini apa belum
        // Kalo sudah ada : update
        // Belum ada : insert
        $check = $this->_dm->set_model('trs_rating_product')->get_all_data(array(
            "conditions" => array("produk_id" => $produk_id, "user_id" => $user_id)
        ))['datas'];

        if($check) {
            // Update
            $conditions = array("produk_id" => $produk_id, "user_id" => $user_id);
            $this->_dm->set_model('trs_rating_product')->update($arrayToDb, $conditions, $extra_params);
        }
        else {
            // Insert
            $this->_dm->set_model('trs_rating_product')->insert($arrayToDb, $extra_params);
        }

        // Check transaction status
        if ($this->db->trans_status() === FALSE) {
            // Failed, rollback
            $this->db->trans_rollback();
            $message['error_msg'] = 'Failed to rate! Please try again.';
        }
        else {
            // Success
            $this->db->trans_commit();
            $message['is_error']        = false;
            $message['notif_title']     = "Success!";
            $message['notif_message']   = "Thanks for your rating.";
        }

        echo json_encode($message);
        exit;
    }

    public function save_comment () {
        // Must AJAX and POST
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // Declare variable here
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        // Populate form fields
        $comment    = $this->input->post('comment');
        $produk_id  = $this->input->post('produk_id');
        $now        = date("Y-m-d H:i:s");
        $user_id    = "";

        // Kalo belum login, ga bs kasih rating
        if (isset($this->data_login_user['id'])) {
            $user_id = $this->data_login_user['id'];
        }
        else {
            $message['error_msg']   = "Untuk memberikan komentar, silahkan login terlebih dahulu.";
            echo json_encode($message);
            exit;
        }

        // Gagal jika ga dapet produk id
        if(!$produk_id) {
            $message['error_msg']   = "Invalid produk id.";
            echo json_encode($message);
            exit;
        }

        $this->db->trans_begin();

        $arrayToDb = array(
            "foreign_key" => $produk_id,
            "discuss_datetime" => $now,
            "member_id" => $user_id,
            "discussion_content" => $comment
        );

        // Insert
        $params = array("is_direct" => true);
        $this->_dm->set_model('trs_product_discussion')->insert($arrayToDb, $params);

        // Check transaction status
        if ($this->db->trans_status() === FALSE) {
            // Failed, rollback
            $this->db->trans_rollback();
            $message['error_msg'] = 'Failed to comment! Please try again.';
        }
        else {
            // Success
            $this->db->trans_commit();
            $message['is_error']        = false;
            $message['notif_title']     = "Success!";
            $message['notif_message']   = "Thanks for your comment.";
        }

        echo json_encode($message);
        exit;
    }

    public function save_reply () {
        // Must AJAX and POST
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // Declare variable here
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        // Populate form fields
        $reply_content = $this->input->post('reply_content');
        $discussion_id = $this->input->post('discussion_id');
        $produk_id = $this->input->post('produk_id');
        $now        = date("Y-m-d H:i:s");
        $user_id = "";

        // Kalo belum login, ga bs kasih reply
        if (isset($this->data_login_user['id'])) {
            $user_id = $this->data_login_user['id'];
        }
        else {
            $message['error_msg']   = "Untuk memberikan balasan, silahkan login terlebih dahulu.";
            echo json_encode($message);
            exit;
        }

        // Gagal jika ga dapet produk id
        if(!$produk_id) {
            $message['error_msg']   = "Invalid produk id.";
            echo json_encode($message);
            exit;
        }

        $this->db->trans_begin();

        $arrayToDb = array(
            "foreign_key"           => $produk_id,
            "discuss_datetime"      => $now,
            "member_id"             => $user_id,
            "type"                  => 2,
            "discussion_topic_id"   => $discussion_id,
            "discussion_content"    => $reply_content
        );

        // Insert
        $params = array("is_direct" => true);
        $this->_dm->set_model('trs_product_discussion')->insert($arrayToDb, $params);

        // Check transaction status
        if ($this->db->trans_status() === FALSE) {
            // Failed, rollback
            $this->db->trans_rollback();
            $message['error_msg'] = 'Failed to reply! Please try again.';
        }
        else {
            // Success
            $this->db->trans_commit();
            $message['is_error']        = false;
            $message['notif_title']     = "Success!";
            $message['notif_message']   = "Replied for this comment is successful.";
        }

        echo json_encode($message);
        exit;
    }

    public function save_review () {
        // Must AJAX and POST
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // Declare variable here
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        // Populate form fields
        $rating         = $this->input->post('rating');
        $review_title   = $this->input->post('review_title');
        $review_content = $this->input->post('review_content');
        $produk_id      = $this->input->post('produk_id');
        $now            = date("Y-m-d H:i:s");
        $user_id        = "";

        #pr($this->input->post());exit;

        // Kalo rating nya null, jadiin 0
        if(!$rating) {
            $rating = 0;
        }

        // Kalo belum login, ga bs kasih rating
        if (isset($this->data_login_user['id'])) {
            $user_id = $this->data_login_user['id'];
        }
        else {
            $message['error_msg']   = "Untuk memberikan rating, silahkan login terlebih dahulu.";
            echo json_encode($message);
            exit;
        }

        // Gagal jika ga dapet produk id
        if(!$produk_id) {
            $message['error_msg'] = "Invalid produk id.";
            echo json_encode($message);
            exit;
        }

        $this->db->trans_begin();

        $arrayToDb = array(
            "foreign_key"       => $produk_id,
            "review_datetime"   => $now,
            "member_id"         => $user_id,
            "rating_score"      => $rating,
            "review_title"      => $review_title,
            "review_content"    => $review_content,
        );

        $extra_params = array("is_direct" => true);

        // Cek apakah user ini sudah pernah kasih review di produk ini apa belum
        // Kalo sudah ada : update
        // Belum ada : insert
        $check = $this->_dm->set_model('trs_product_review')->get_all_data(array(
            "conditions" => array("foreign_key" => $produk_id, "member_id" => $user_id),
            "row_array" => true
        ))['datas'];

        if($check) {
            // Update
            $arrayToDb['status'] = 3;
            $conditions = array("id" => $check['id']);
            $this->_dm->set_model('trs_product_review')->update($arrayToDb, $conditions, $extra_params);
        }
        else {
            // Insert
            $arrayToDb['status'] = 0;
            $this->_dm->set_model('trs_product_review')->insert($arrayToDb, $extra_params);
        }

        // Check transaction status
        if ($this->db->trans_status() === FALSE) {
            // Failed, rollback
            $this->db->trans_rollback();
            $message['error_msg'] = 'Failed to review! Please try again.';
        }
        else {
            // Success
            $this->db->trans_commit();
            $message['is_error']        = false;
            $message['notif_title']     = "Success!";
            $message['notif_message']   = "Thanks for your review.";
        }

        echo json_encode($message);
        exit;
    }
}
