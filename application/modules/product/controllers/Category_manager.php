<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Category Controller.
 */
class Category_manager extends Baseadmin_Controller  {

    private $_title         = "Product Category";
    private $_title_page    = '<i class="fa-fw fa fa-cubes"></i> Product Category';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "product-category";
    private $_back          = "/manager/product/category/list_category";
    private $_js_path       = "/js/pages/product/category/";
    private $_view_folder   = "product/manager/category/";

    private $_table         = "dtb_product_category";
    private $_table_aliases = "dpc";
    private $_pk            = "dpc.id";

    /**
     * constructor.
     */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product Category
    */
    public function list_category() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Product Category </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Product Category</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load models
        $this->load->model('Category_model');
        $data['ordering_data'] = $this->Category_model->getAllOrdering();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Product Category
    */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/product/category">Product Category</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Product Category</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Product Category</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an product category
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/product/category">Product Category</a></li>';

        //load the model.
        $this->load->model('Category_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();
        }

        $params = array("row_array" => true,"conditions" => array("id" => $id));
        //get the data.
        $data['item'] = $this->Category_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Product Category</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Product Category</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation() {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("pretty_url", "Pretty URL", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data product category
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //load model
        $this->load->model('Category_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("id", "name", "description", "ordering", "is_show", "show_in_store_filter");

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'description':
                        if ($value != "") {
                            $data_filters['lower(description)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['lower(is_show)'] = $value;
                        }
                        break;
                    case 'show_in_filter':
                        if ($value != "") {
                            $data_filters['lower(show_in_store_filter)'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Category_model->get_all_data(array(
            'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
            'limit' => $limit,
            'start' => $start,
            'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //load the model.
        $this->load->model('Category_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                   = sanitize_str_input($this->input->post('id'));
        $name                 = sanitize_str_input($this->input->post('name'));
        $description          = sanitize_str_input($this->input->post('description'));
        $pretty_url           = sanitize_str_input($this->input->post('pretty_url'));
        $is_show              = sanitize_str_input($this->input->post('is_show'));
        $file_pretty_name     = sanitize_str_input($this->input->post('file_pretty_name'));
        $show_in_store_filter = sanitize_str_input($this->input->post('show_in_store_filter'));
        $meta_keys            = sanitize_str_input($this->input->post('meta_keys'));
        $meta_desc            = sanitize_str_input($this->input->post('meta_desc'));
        $data_image           = $this->input->post('data-image');
        $data_image_large     = $this->input->post('data-image-url');
        // pr($this->input->post());exit();

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $image_large = $this->upload_image($file_pretty_name, "upload/product-category/large", "image-file-large", $data_image_large, 850, 300, $id);
            $image_url = $this->upload_image($file_pretty_name, "upload/product-category", "image-file", $data_image, 274, 274, $id);
            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'name'                  => $name,
                'description'           => $description,
                'pretty_url'            => $pretty_url,
                'image_pretty_name'     => $file_pretty_name,
                'is_show'               => $is_show,
                'show_in_store_filter'  => $show_in_store_filter,
                'meta_keys'             => $meta_keys,
                'meta_desc'             => $meta_desc
            );

            if (!empty($image_url)) {
                $arrayToDB['image_url'] = $image_url;
            }

           if (!empty($image_large)) {
                $arrayToDB['image_url_large'] = $image_large;
            }

            //insert or update?
            if ($id == "") {
                $ordering = $this->Category_model->getLastOrdering();
                $arrayToDB['ordering'] = $ordering+1;

                //insert to DB.
                $result = $this->Category_model->insert($arrayToDB, array('is_direct' => true, 'is_version' => true, "ordering" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Category has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/product/category/list_category";
                }

            } else {
                //get current slider data
                $curr_data = $this->Category_model->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

                //update.
                if (!empty($image_url) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                    unlink( FCPATH . $curr_data['image_url'] );
                }

                if (!empty($image_large) && isset($curr_data['image_url_large']) && !empty($curr_data['image_url_large'])) {
                    unlink( FCPATH . $curr_data['image_url_large'] );
                }

                //condition for update.
                $condition = array("id" => $id);
                $result = $this->Category_model->update($arrayToDB, $condition, array('is_direct' => true, 'is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Category has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/product/category/list_category";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * ordering
     */

    public function ordering() {
        //check if ajax request
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }

        $id    = $this->input->post('id');
        $order = $this->input->post('val');

        $this->load->model("product/Category_model");

        $table = $this->Category_model;
        $model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

        $datas = $table->getAllSlider();

        $min = $table->getFirstOrdering();
        $max = $table->getLastOrdering();

        $newkey = searchForIdInt($order, $datas, 'ordering');
        $oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

        $oldOrdering = $model['ordering'];

        $this->db->trans_begin();

        if ($oldOrdering > $order) {
            //smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
            for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']+1,
                ),array('id' => $datas[$i]['id']), array('is_direct' => true, "is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array('is_direct' => true, "is_version" => true));

        } else if ($oldOrdering < $order) {
            //smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
            for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
                $table->update(array(
                    'ordering' =>  $datas[$i]['ordering']-1,
                ),array('id' => $datas[$i]['id']), array('is_direct' => true, "is_version" => true));
            }

            $table->update(array(
                'ordering' =>  $order,
            ),array('id' => $datas[$oldkey]['id']), array('is_direct' => true, "is_version" => true));
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title']   = "Done!";
            $message['notif_message'] = "Catregory Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }
}
