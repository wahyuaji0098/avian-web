<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">HOME</a>
                <span>KALKULATOR PERHITUNGAN CAT</span>
            </div>
            <div class="paintcalc">
                <div class="paintcalc-seg seg-mat">
                    <div class="seg-title">Pilih Tipe:</div>
                    <div class="mat mat-wall">
                        <input class="painttype" name="painttype" id="pty1" type="radio"
                        value="1"
                        checked="checked"
                        />
                        <label class="matlab" for="pty1">Tembok</label>
                    </div>
                    <div class="mat mat-wood">
                        <input class="painttype" name="painttype" id="pty2" type="radio"
                        value="2"
                        />
                        <label class="matlab" for="pty2">Kayu</label>
                    </div>
                    <div class="mat mat-metl">
                        <input class="painttype" name="painttype" id="pty3" type="radio"
                        value="3"
                        />
                        <label class="matlab" for="pty3">Besi</label>
                    </div>
                    <div class="mat mat-wprf">
                        <input class="painttype" name="painttype" id="pty4" type="radio"
                        value="4"
                        />
                        <label class="matlab" for="pty4">Anti Bocor</label>
                    </div>
                </div>
                <div class="paintcalc-seg seg-calc">
                    <div class="number-err"></div>
                    <div class="seg-title">Ukuran</div>
                    <div class="numbers">
                        <div class="number-title">Panjang (M)</div>
                        <input class="number-val" type="text" id="val-hei"/>
                    </div>
                    <div class="numbers">
                        <div class="number-title">Lebar (M)</div>
                        <input class="number-val" type="text" id="val-wid"/>
                    </div>
                    <div class="numbers">
                        <div class="number-title">Jumlah</div>
                        <input class="number-val" type="text" id="val-num"/>
                    </div>
                    <div class="seg-title tembok">Ukuran Jendela</div>
                    <div class="numbers tembok">
                        <div class="number-title">Panjang (M)</div>
                        <input class="number-val" type="text" id="val-hei-window"/>
                    </div>
                    <div class="numbers tembok">
                        <div class="number-title">Lebar (M)</div>
                        <input class="number-val" type="text" id="val-wid-window"/>
                    </div>
                    <div class="numbers tembok">
                        <div class="number-title">Jumlah</div>
                        <input class="number-val" type="text" id="val-num-window"/>
                    </div>
                    <div class="seg-title tembok">Ukuran Pintu</div>
                    <div class="numbers tembok">
                        <div class="number-title">Panjang (M)</div>
                        <input class="number-val" type="text" id="val-hei-door"/>
                    </div>
                    <div class="numbers tembok">
                        <div class="number-title">Lebar (M)</div>
                        <input class="number-val" type="text" id="val-wid-door"/>
                    </div>
                    <div class="numbers tembok">
                        <div class="number-title">Jumlah</div>
                        <input class="number-val" type="text" id="val-num-door"/>
                    </div>

                </div>
                <div class="paintcalc-seg seg-rslt">
                    <div class="seg-title">Hasil</div>
                    <div class="result">
                        <div class="result-title">Total Luas Permukaan</div>
                        <div class="result-field" id="surfa">10,000 m&sup2;</div>
                    </div>
                    <div class="result">
                        <div class="result-title">Cat yang dibutuhkan</div>
                        <div class="paintcosts">
                        </div>
                    </div>
                    <button class="btn tpaintcalc">Kalkulasi</button>
                    <div class="lead"></div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
