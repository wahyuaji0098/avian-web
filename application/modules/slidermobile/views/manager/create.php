<?php
    $id         = isset($item["id"]) ? $item["id"] : "";
    $title      = isset($item["title"]) ? $item["title"] : "";
    $url        = isset($item["url"]) ? $item["url"] : "";
    $description    = isset($item["description"]) ? $item["description"] : "";
    $image_url      = isset($item["image_url"]) ? $item["image_url"] : "";
    $image_url_pro  = isset($item["image_url_pro"]) ? $item["image_url_pro"] : "";
    $image_for_device      = isset($item["image_url_device"]) ? $item["image_url_device"] : "";
    $popup_image_url      = isset($item["popup_image_url"]) ? $item["popup_image_url"] : "";
    $ordering       = isset($item["ordering"]) ? $item["ordering"] : "";
    $is_show        = isset($item["is_show"]) ? $item["is_show"] : false;
    $show_in        = isset($item["show_in"]) ? $item["show_in"] : false;
    $show_as_popup        = isset($item["is_show_as_popup"]) ? $item["is_show_as_popup"] : false;
    $file_pretty_name   = isset($item["file_pretty_name"]) ? $item["file_pretty_name"] : "";
    $created_date       = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date       = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Slider Mobile</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/slidermobile/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Title</label>
                                        <label class="input">
                                                <input name="title" type="text"  class="form-control" placeholder="Title" value="<?= $title; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Description</label>
                                        <label class="textarea">
                                            <textarea name="description" type="text"  class="form-control" placeholder="Description" /><?= $description; ?></textarea>
                                        </label>
                                    </section>
									<section>
                                        <label class="label">Link Url</label>
                                        <label class="input">
                                            <input name="url" type="text"  class="form-control" placeholder="Link Url ( ex: http://google.com )" value="<?= $url; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Image For Device (563x281 px)</label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-device">
                                                <?php if($image_for_device): ?>
                                                <img src="<?= $image_for_device ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimagedevice" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Popup Image For Device (563x563 px)</label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-popup">
                                                <?php if($popup_image_url): ?>
                                                <img src="<?= $popup_image_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimagepopup" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Image filename</label>
                                        <label class="input">
                                            <input name="file_pretty_name" type="text"  class="form-control" placeholder="Image Filename" value="<?= $file_pretty_name; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show In </label>
                                        <label class="input">
                                            <?= select_show_in('show_in', $show_in); ?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show as Popup </label>
                                        <label class="input">
                                            <?= select_show_as_popup('show_as_popup', $show_as_popup); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
