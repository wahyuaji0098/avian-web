<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Slider Controller.
 */
class Slidermobile_manager extends Baseadmin_Controller  {

    private $_title = "Slider Mobile";
    private $_title_page = '<i class="fa-fw fa fa-image"></i> Slider Mobile ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "slider";
    private $_back = "/manager/slidermobile";
    private $_js_path = "/js/pages/slidermobile/manager/";
    private $_view_folder = "slidermobile/manager/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Slider
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Slider Mobile</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Slider Mobile</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        $this->load->model('Slidermobile_model');

        //get all ordering slidermobile
        $data['ordering_data'] = $this->Slidermobile_model->getAllOrdering();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create an slider
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/slidermobile">Slider Mobile</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Slider Mobile</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Slider Mobile</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            )
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an slider
     */
    public function edit ($slider_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/slidermobile">Slider Mobile</a></li>';

        //load the model.
		$this->load->model('Slidermobile_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ( $slider_id === null || !is_numeric($slider_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $slider_id));
        //get the data.
        $data['item'] = $this->Slidermobile_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Slider Mobile</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Slider Mobile</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            )
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("is_show", "Show In", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

		//load model
        $this->load->model('Slidermobile_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','title', 'description','url','image_url','ordering');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'title':
                        if ($value != "") {
                            $data_filters['lower(title)'] = $value;
                        }
                        break;

                    case 'description':
                        if ($value != "") {
                            $data_filters['lower(description)'] = $value;
                        }
                        break;

                    case 'url':
                        if ($value != "") {
                            $data_filters['lower(url)'] = $value;
                        }
                        break;

                    case 'create_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(created_date as date) <="] = $date['end'];
                            $conditions["cast(created_date as date) >="] = $date['start'];

                        }
                        break;
                    case 'update_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(updated_date as date) <="] = $date['end'];
                            $conditions["cast(updated_date as date) >="] = $date['start'];

                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Slidermobile_model->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //load the model.
        $this->load->model('Slidermobile_model');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id = sanitize_str_input($this->input->post('id'), "numeric");
        $title = sanitize_str_input($this->input->post('title'));
        $description = sanitize_str_input($this->input->post('description'));
        $is_show = sanitize_str_input($this->input->post('is_show'), "numeric");
        $show_in = sanitize_str_input($this->input->post('show_in'), "numeric");
        $url = sanitize_str_input($this->input->post('url'));
        $file_pretty_name = sanitize_str_input($this->input->post('file_pretty_name'));
        // $data_image = $this->input->post('data-image');
        // $data_image_pro = $this->input->post('data-image-pro');
        $data_image_device = $this->input->post('data-image-device');
        $data_image_popup = $this->input->post('data-image-popup');
        $show_as_popup = sanitize_str_input($this->input->post('show_as_popup'), "numeric");

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            // $image_user = $this->upload_image($file_pretty_name, "upload/slidermobile/user", "image-file", $data_image, 1126, 539, $id);
            // $image_pro = $this->upload_image($file_pretty_name, "upload/slidermobile/pro", "image-file-pro", $data_image_pro, 784, 460, $id);
            $image_device = $this->upload_image($file_pretty_name, "upload/slidermobile/device", "image-file-device", $data_image_device, 563, 281, $id);
            $image_popup = $this->upload_image($file_pretty_name, "upload/slidermobile/popup", "image-file-popup", $data_image_popup, 563, 563, $id);

            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'title'         => $title,
                'description'   => $description,
                'is_show'       => $is_show,
                'show_in'       => $show_in,
                'url'           => $url,
                'is_show_as_popup' => $show_as_popup
            );

            // if (!empty($image_user)) {
            //     $arrayToDB['image_url'] = $image_user;
            // }
            //
            // if (!empty($image_pro)) {
            //     $arrayToDB['image_url_pro'] = $image_pro;
            // }

            if (!empty($image_device)) {
                $arrayToDB['image_url_device'] = $image_device;
            }
            if (!empty($image_popup)) {
                $arrayToDB['popup_image_url'] = $image_popup;
            }

            //insert or update?
            if ($id == "") {
                $ordering = $this->Slidermobile_model->getLastOrdering();
                $arrayToDB['ordering'] = $ordering+1;

                //insert to DB.
                $result = $this->Slidermobile_model->insert($arrayToDB);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Slider Mobile has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/slidermobile";
                }

            } else {
                //get current slider data
                $curr_data = $this->Slidermobile_model->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

                //update.
                // if (!empty($image_user) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                //     unlink( FCPATH . $curr_data['image_url'] );
                // }
                // if (!empty($image_user) && isset($curr_data['image_url_pro']) && !empty($curr_data['image_url_pro'])) {
                //     unlink( FCPATH . $curr_data['image_url_pro'] );
                // }

                if (!empty($image_user) && isset($curr_data['image_url_device']) && !empty($curr_data['image_url_device'])) {
                    unlink( FCPATH . $curr_data['image_url_device'] );
                }

                if (!empty($image_user) && isset($curr_data['popup_image_url']) && !empty($curr_data['popup_image_url'])) {
                    unlink( FCPATH . $curr_data['popup_image_url'] );
                }

                //condition for update.
                $condition = array("id" => $id);
                $result = $this->Slidermobile_model->update($arrayToDB, $condition);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Slider Mobile has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/slidermobile";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an admin.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Slidermobile_model');

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->Slidermobile_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                $models = $this->Slidermobile_model->getAllSliderAfterNumber($data['ordering']);
                $ordering = $data['ordering'];
                foreach ($models as $model) {
                    $this->Slidermobile_model->update(array(
                        'ordering' =>  $ordering,
                    ),array('id' => $model['id']));
                    $ordering++;
                }

                if (isset($data['image_url']) && !empty($data['image_url'])) {
                    unlink (FCPATH.$data['image_url']);
                }

                if (isset($data['image_url_pro']) && !empty($data['image_url_pro'])) {
                    unlink (FCPATH.$data['image_url_pro']);
                }

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $this->Slidermobile_model->delete($condition, array("is_permanently" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Slider Mobile has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function ordering() {
    	//check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $id = $this->input->post('id');
    	$order = $this->input->post('val');

        $this->load->model("Slidermobile_model");

		$table = $this->Slidermobile_model;
		$model = $table->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
        ))['datas'];

		$datas = $table->getAllSlider();

		$min = $table->getFirstOrdering();
		$max = $table->getLastOrdering();

		$newkey = searchForIdInt($order, $datas, 'ordering');
		$oldkey = searchForIdInt($model['ordering'], $datas, 'ordering');

		$oldOrdering = $model['ordering'];

        $this->db->trans_begin();

		if ($oldOrdering > $order) {
			//smua image yg id nya mulai dari == $order ~sd~ $oldOrdering - 1 ==> harus ditambah 1.
			for ($i = $oldkey - 1 ; $i >= $newkey; $i--) {
                $table->update(array(
					'ordering' =>  $datas[$i]['ordering']+1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));

		} else if ($oldOrdering < $order) {
			//smua image yg id nya mulai dari == $oldOrdering + 1 ~sd~ $order ==> harus dikurang 1.
			for ($i = $oldkey+ 1 ; $i <= $newkey ; $i++) {
				$table->update(array(
					'ordering' =>  $datas[$i]['ordering']-1,
				),array('id' => $datas[$i]['id']));
			}

			$table->update(array(
				'ordering' =>  $order,
			),array('id' => $datas[$oldkey]['id']));
		}

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();

            $message["error_message"] = $this->db->error();
        } else {
            $this->db->trans_commit();

            $message["is_error"] = false;

            //growler.
            $message['notif_title'] = "Done!";
            $message['notif_message'] = "Slider Mobile Ordered Successfully.";
        }

        echo json_encode($message);
        exit;
    }

}
