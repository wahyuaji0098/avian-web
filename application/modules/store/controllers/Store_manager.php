<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Store Controller.
 */
class Store_manager extends Baseadmin_Controller
{
    private $_title = "Store";
    private $_title_page = '<i class="fa-fw fa fa-building"></i> Store ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "store";
    private $_back = "/manager/store";
    private $_js_path = "/js/pages/store/manager/";
    private $_view_folder = "store/manager/";

    /**
     * constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Store
     */
    public function index()
    {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Store</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Store</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an store
     */
    public function edit($kode_customer = null)
    {
        $this->_breadcrumb .= '<li><a href="/manager/store">Store</a></li>';

        //load the model.
        $this->load->model('Store_addon_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ($kode_customer === null) {
            show_404();
        }

        $params = array(
            "select" => array(
                'msa.*',
                "IF(msa.nama_customer IS NULL or msa.nama_customer ='', dst.nama_customer, msa.nama_customer) as nama_customer",
                "IF(msa.alamat IS NULL or msa.alamat ='', dst.alamat, msa.alamat) as alamat",
                "IF(msa.latitude IS NULL or msa.latitude ='', dst.latitude, msa.latitude) as latitude",
                "IF(msa.longitude IS NULL or msa.longitude ='', dst.longitude, msa.longitude) as longitude",
                "IF(msa.foto_toko_url IS NULL or msa.foto_toko_url ='', dst.foto_toko_url, msa.foto_toko_url) as foto_toko_url",
                "IF(msa.punya_mesin_tinting IS NULL or msa.punya_mesin_tinting ='', dst.punya_mesin_tinting, msa.punya_mesin_tinting) as punya_mesin_tinting",
            ),
            "row_array" => true,
            "conditions" => array("msa.kode_customer" => $kode_customer),
            "joined" => array(
                "dtb_store_v2 dst" => array("dst.kode_customer" => "msa.kode_customer")
            )
        );
        //get the data.
        $data['item'] = $this->Store_addon_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Slider</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Slider</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    public function view($kode_customer = null)
    {
        $this->_breadcrumb .= '<li><a href="/manager/store">Store</a></li>';

        //load the model.
        $this->load->model('Store_addon_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ($kode_customer === null) {
            show_404();
        }

        //get the data.
        $data['item'] = $this->_dm->set_model('dtb_store_v2', 'ds', 'kode_customer')->get_all_data(array(
            "select"    => array("ds.*", "msa.phone", "msa.fax", "msa.email", "msa.website"),
            "row_array" => true,
            "conditions" => array("ds.kode_customer" => $kode_customer),
            "joined" => array(
                "mst_store_addon msa" => array("ds.kode_customer" => "msa.kode_customer")
            ),
            "left_joined" => array(
                "view_store_rating vsr" => array("vsr.foreign_key" => "ds.kode_customer")
            )
        ))['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> View Store</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>View Store</li>',
            "back"          => $this->_back,
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'view', $data);
        $this->load->view(MANAGER_FOOTER);
    }

    /**
     * Import View
     */
    public function import()
    {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Import Store</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Store</li>',
            "back"          => $this->_back,
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                $this->_js_path . "import.js",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'import');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id)
    {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("is_show", "Show In", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data()
    {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //load model
        $this->load->model('Store_addon_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array(
            'msa.kode_customer',
            "IF(msa.nama_customer IS NULL or msa.nama_customer ='', dst.nama_customer, msa.nama_customer) as nama_customer",
            "IF(msa.alamat IS NULL or msa.alamat ='', dst.alamat, msa.alamat) as alamat",
            'rating_avianbrands',
            "IF(msa.email IS NULL or msa.email ='', dst.email, msa.email) as email",
            "IF(msa.website IS NULL or msa.website ='', dst.website, msa.website) as website",
            "IF(msa.punya_mesin_tinting IS NULL or msa.punya_mesin_tinting ='', dst.punya_mesin_tinting, msa.punya_mesin_tinting) as punya_mesin_tinting"
        );

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'kode':
                        if ($value != "") {
                            $data_filters['lower(msa.kode_customer)'] = $value;
                        }
                        break;

                    case 'nama':
                        if ($value != "") {
                            $data_filters["lower(IF(msa.nama_customer IS NULL or msa.nama_customer = '', dst.nama_customer, msa.nama_customer))"] = $value;
                        }
                        break;

                    case 'alamat':
                        if ($value != "") {
                            $data_filters["lower(IF(msa.alamat IS NULL or msa.alamat ='', dst.alamat, msa.alamat))"] = $value;
                        }
                        break;

                    case 'rating':
                        if ($value != "") {
                            $data_filters['lower(rating_avianbrands)'] = $value;
                        }
                        break;

                    case 'email':
                        if ($value != "") {
                            $data_filters["lower(IF(msa.email IS NULL or msa.email ='', dst.email, msa.email)"] = $value;
                        }
                        break;

                    case 'website':
                        if ($value != "") {
                            $data_filters["lower(IF(msa.website IS NULL or msa.website ='', dst.website, msa.website))"] = $value;
                        }
                        break;

                    case 'create_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(created_date as date) <="] = $date['end'];
                            $conditions["cast(created_date as date) >="] = $date['start'];
                        }
                        break;
                    case 'update_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(updated_date as date) <="] = $date['end'];
                            $conditions["cast(updated_date as date) >="] = $date['start'];
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Store_addon_model->get_all_data(array(
            'select' => $select,
            "joined" => array(
                "dtb_store_v2 dst" => array("dst.kode_customer" => "msa.kode_customer")
            ),
            'order_by' => array($column_sort => $sort_dir),
            'limit' => $limit,
            'start' => $start,
            'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
        ));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process import via ajax post.
     */
    public function process_import()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";
        $validate = "";
        $error_validate_flag = false;

        //check validation
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > 0) {
            $filename = $_FILES['file']['tmp_name'];
            //check max file upload
            if ($_FILES['file']['size'] > MAX_IMPORT_FILE_SIZE) {
                $message['error_msg'] = "Maximum file size is ".MAX_IMPORT_FILE_SIZE_IN_KB;
            } elseif (mime_content_type($filename) != "application/vnd.ms-excel"
                        && mime_content_type($filename) != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                //check extension if xls dan xlsx
                $message['error_msg'] = "File must .xls or .xlsx";
            } else {
                //load and init library
                $this->load->library("PHPExcel_reader");
                $excel = new PHPExcel_reader();

                //read the file and give back the result of the file in array
                $result = $excel->read_from_excel($filename);

                if ($result['is_error'] == true) {
                    $message['redirect_to'] = "";
                    $message['error_msg']   = $result['error_msg'];
                } else {

                    //begin transaction
                    $this->db->trans_begin();

                    $total_data = count($result['datas']);

                    $extra_param = array(
                        "is_direct"  => true,
                        "is_batch"   => true,
                        "is_version" => true
                    );

                    if ($total_data > 0) {
                        //loop the result and insert to db
                        $line = 2;

                        foreach ($result['datas'] as $model) {
                            if (count($model) < 8) {
                                $validate = "Invalid Template.";
                                $error_validate_flag = true;
                                break;
                            } else {
                                // check untuk semua kolom apakah null
                                if (empty($model[0]) && empty($model[1]) &&  empty($model[2]) &&  empty($model[3]) && empty($model[4]) && empty($model[5]) &&  empty($model[6]) &&  empty($model[7])) {
                                    // klo kosong semua isinya, skip
                                    $line++;
                                } else {
                                    // cek color code if exist
                                    $store_exist = $this->_dm->set_model("mst_store_addon", "msa", "kode_customer")->get_all_data(array(
                                        "conditions" => array(
                                            "kode_customer" => strtolower($model[0]),
                                        ),
                                        "row_array" => true,
                                    ))['datas'];

                                    // Begin checker for empty coloumn phone
                                    // First Phase
                                    if (!empty($model[2]) && empty($model[3]) && empty($model[4])) {
                                        $str_phone = trim($model[2]);
                                    }
                                    if (empty($model[2]) && !empty($model[3]) && empty($model[4])) {
                                        $str_phone = trim($model[3]);
                                    }
                                    if (empty($model[2]) && empty($model[3]) && !empty($model[4])) {
                                        $str_phone = trim($model[4]);
                                    }

                                    // Second Phase
                                    if (!empty($model[2]) && !empty($model[3]) && empty($model[4])) {
                                        $str_phone = trim($model[2].','.$model[3]);
                                    }
                                    if (empty($model[2]) && !empty($model[3]) && !empty($model[4])) {
                                        $str_phone = trim($model[3].','.$model[4]);
                                    }
                                    if (!empty($model[2]) && empty($model[3]) && !empty($model[4])) {
                                        $str_phone = trim($model[2].','.$model[4]);
                                    }

                                    // Third Phase
                                    if (!empty($model[2]) && !empty($model[3]) && !empty($model[4])) {
                                        $str_phone = trim($model[2].','.$model[3].','.$model[4]);
                                    }
                                    if (empty($model[2]) && empty($model[3]) && empty($model[4])) {
                                        $str_phone = "-";
                                    }
                                    // End checker for empty coloumn phone

                                    // concat phone
                                    $str_phone = $str_phone;

                                    // prepare data before update
                                    $data_store = array(
                                        "phone"   => trim($str_phone),
                                        "fax"     => $model[5],
                                        "website" => $model[6],
                                        "email"   => $model[7]
                                    );

                                    if (!empty($store_exist)) {
                                        // update store
                                        $store_id = $store_exist["kode_customer"];
                                        $this->_dm->set_model("mst_store_addon", "msa", "kode_customer")->update(
                                            $data_store,
                                            array("kode_customer" => $store_id),
                                            $extra_param
                                        );
                                    }

                                    $line++;
                                }
                            }
                        }
                    }

                    if ($this->db->trans_status() === false || $error_validate_flag == true) {
                        $this->db->trans_rollback();
                        $message['redirect_to'] = "";

                        if ($error_validate_flag == true) {
                            $message['error_msg'] = $validate;
                        } else {
                            $message['error_msg'] = 'database operation failed.';
                        }
                    } else {
                        $this->db->trans_commit();

                        $message['is_error'] = false;
                        // success.
                        $message['notif_title']   = "Good!";
                        $message['notif_message'] = "Data Store has been imported.";

                        // not redirected.
                        $message['redirect_to'] = "";
                    }
                }
            }
        } else {
            $message['error_msg'] = 'File is empty (xls or xlsx).';
        }

        echo json_encode($message);
        exit;
    }
}
