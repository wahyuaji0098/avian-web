<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Review Controller.
 */
class Review_manager extends Baseadmin_Controller  {

    private $_title         = "Store Review";
    private $_title_page    = '<i class="fa-fw fa fa-thumbs-up"></i> Store Review';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "store-review";
    private $_back          = "/manager/store/review/list-review";
    private $_js_path       = "/js/pages/store/review/";
    private $_view_folder   = "store/manager/review/";

    /**
     * constructor.
     */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Review
    */
    public function list_review() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Review </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Review</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
    * Edit Review
    */
    public function edit($review_id = "") {
        if(!$review_id) {
            show_404();
        }

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Review </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Product</li><li><a href="/manager/store/review/list-review">Review</a></li><li>Edit</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                $this->_js_path . "edit.js",
            ),
        );

        $data['item'] = $this->_dm->set_model("trs_store_review", "tpr", "tpr.id")->get_all_data(array(
            'select'            => array("tpr.id", "dp.name as store_name", "dm.name as member_name", "tpr.rating_score", "tpr.review_title", "tpr.review_content", "tpr.review_datetime", "tpr.status as status_review", "tpr.reject_reason"),
            'left_joined'       => array(
                "dtb_store dp" => array("dp.id" => "tpr.foreign_key"),
                "dtb_member dm" => array("dm.id" => "tpr.member_id"),
            ),
            'conditions' => array("tpr.id" => $review_id),
            'row_array' => true
        ))['datas'];

        #pr($datas);exit;

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'edit', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("tpr.id", "dp.name as store_name", "dm.name as member_name", "tpr.rating_score", "tpr.review_title", "tpr.review_content", "tpr.review_datetime", "tpr.status");

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(tpr.id)'] = $value;
                        }
                        break;

                    case 'store_name':
                        if ($value != "") {
                            $data_filters['lower(dp.name)'] = $value;
                        }
                        break;

                    case 'member_name':
                        if ($value != "") {
                            $data_filters['lower(dm.name)'] = $value;
                        }
                        break;

                    case 'rating_score':
                        if ($value != "") {
                            $data_filters['lower(rating_score)'] = $value;
                        }
                        break;

                    case 'review_title':
                        if ($value != "") {
                            $data_filters['lower(review_title)'] = $value;
                        }
                        break;

                    case 'review_content':
                        if ($value != "") {
                            $data_filters['lower(review_content)'] = $value;
                        }
                        break;

                    case 'review_datetime':
                        if ($value != "") {
                            $data_filters['lower(review_datetime)'] = $value;
                        }
                        break;

                    case 'status':
                        if ($value != "") {
                            $data_filters['lower(tpr.status)'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model("trs_store_review", "tpr", "tpr.id")->get_all_data(array(
            'select'            => $select,
            'left_joined' 		=> array(
            	"dtb_store dp" => array("dp.id" => "tpr.foreign_key"),
            	"dtb_member dm" => array("dm.id" => "tpr.member_id"),
            ),
            'order_by'          => array($column_sort => $sort_dir),
            'limit'             => $limit,
            'start'             => $start,
            'conditions'        => $conditions,
            'filter'            => $data_filters,
            "count_all_first"   => true,
        ));

        foreach($datas['datas'] as $key => $value) {
        	$datas['datas'][$key]['review_datetime'] = dateformatampm($value['review_datetime']);
        }

        #pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post - not ready.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => true,
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $review_status       = sanitize_str_input($this->input->post('review_status'), "numeric");
        $reject_reason       = sanitize_str_input($this->input->post('reject_reason'));

        $arrayToDB = array(
            "status"            => $review_status,
            "reject_reason"     => $reject_reason,
        );

        // Begin transaction.
        $this->db->trans_begin();

        $conditions = array("id" => $id);
        $this->_dm->set_model("trs_store_review")->update($arrayToDB, $conditions, $extra_param);

        // End transaction.
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            // growler.
            $message['notif_title']   = "Good!";
            $message['notif_message'] = "Review has been updated.";
            $message['redirect_to'] = "/manager/store/review/list-review";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function approve() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => true,
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        // Get data
        $get_data = $this->_dm->set_model("trs_store_review","rev","rev.id")->get_all_data(array(
            "select" => array("rev.status", "rev.member_id"),
            "joined" => array("dtb_store store" => array("store.id" => "rev.foreign_key")),
            "conditions" => array(
                                "rev.id" => $id
                            ),
            "row_array" => TRUE
        ))["datas"];
        
        $arrayToDB = array(
            "status" => 1,
        );

        // Begin transaction.
        $this->db->trans_begin();

        $conditions = array("id" => $id);
        $this->_dm->set_model("trs_store_review")->update($arrayToDB, $conditions, $extra_param);

        // Check review point, already get point 
        if ($get_data["status"] == 0) {
            // Add point after approve a review (can only get one point per store)
            $add_point = $this->Dynamic_model->set_model("trs_member_point","point","id")->insert(
                array(
                    "member_id"      => $get_data["member_id"],
                    "point_mutation" => POINT_STORE_REVIEW,
                    "activity_code"  => CODE_STORE_REVIEW,
                    "description"    => DESC_STORE_REVIEW
                )
            );
        }

        // End transaction.
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            // growler.
            $message['notif_title']   = "Good!";
            $message['notif_message'] = "Review has been updated.";
            $message['redirect_to'] = "/manager/store/review/list-review";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}