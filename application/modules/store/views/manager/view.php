<?php
	# var_builder($item);

	$customer_id = isset($item["customer_id"]) ? $item["customer_id"] : "";
	$kode_customer = isset($item["kode_customer"]) ? $item["kode_customer"] : "";
	$nama_customer = isset($item["nama_customer"]) ? $item["nama_customer"] : "";
	$alamat = isset($item["alamat"]) ? $item["alamat"] : "";
	$latitude = isset($item["latitude"]) ? $item["latitude"] : "";
	$longitude = isset($item["longitude"]) ? $item["longitude"] : "";
	$phone       = isset($item["phone"]) ? $item["phone"] : "";
    $fax       = isset($item["fax"]) ? $item["fax"] : "";
    $email       = isset($item["email"]) ? $item["email"] : "";
    $website       = isset($item["website"]) ? $item["website"] : "";
	$foto_toko_url = isset($item["foto_toko_url"]) ? $item["foto_toko_url"] : "";
	$punya_mesin_tinting = isset($item["punya_mesin_tinting"]) ? $item["punya_mesin_tinting"] : "";
	$version = isset($item["version"]) ? $item["version"] : "";
	$is_active = isset($item["is_active"]) ? $item["is_active"] : "";
	$jual_pipa_power = isset($item["jual_pipa_power"]) ? $item["jual_pipa_power"] : "";
	$jual_pipa_power_max = isset($item["jual_pipa_power_max"]) ? $item["jual_pipa_power_max"] : "";
	$jual_fitting_power = isset($item["jual_fitting_power"]) ? $item["jual_fitting_power"] : "";
	$jual_talang_power = isset($item["jual_talang_power"]) ? $item["jual_talang_power"] : "";
	$jual_fitting_talang_power = isset($item["jual_fitting_talang_power"]) ? $item["jual_fitting_talang_power"] : "";
	$jual_no_odor = isset($item["jual_no_odor"]) ? $item["jual_no_odor"] : "";
	$jual_fres = isset($item["jual_fres"]) ? $item["jual_fres"] : "";
	$jual_avitex_exterior = isset($item["jual_avitex_exterior"]) ? $item["jual_avitex_exterior"] : "";
	$jual_boyo_plamir = isset($item["jual_boyo_plamir"]) ? $item["jual_boyo_plamir"] : "";
	$jual_no_drop = isset($item["jual_no_drop"]) ? $item["jual_no_drop"] : "";
	$jual_avitex = isset($item["jual_avitex"]) ? $item["jual_avitex"] : "";
	$jual_aries_gold = isset($item["jual_aries_gold"]) ? $item["jual_aries_gold"] : "";
	$jual_aries = isset($item["jual_aries"]) ? $item["jual_aries"] : "";
	$jual_sunguard = isset($item["jual_sunguard"]) ? $item["jual_sunguard"] : "";
	$jual_supersilk = isset($item["jual_supersilk"]) ? $item["jual_supersilk"] : "";
	$jual_everglo = isset($item["jual_everglo"]) ? $item["jual_everglo"] : "";
	$jual_aquamatt = isset($item["jual_aquamatt"]) ? $item["jual_aquamatt"] : "";
	$jual_avitex_alkali_resisting_primer = isset($item["jual_avitex_alkali_resisting_primer"]) ? $item["jual_avitex_alkali_resisting_primer"] : "";
	$jual_no_drop_107 = isset($item["jual_no_drop_107"]) ? $item["jual_no_drop_107"] : "";
	$jual_no_drop_100 = isset($item["jual_no_drop_100"]) ? $item["jual_no_drop_100"] : "";
	$jual_no_drop_bitumen_black = isset($item["jual_no_drop_bitumen_black"]) ? $item["jual_no_drop_bitumen_black"] : "";
	$jual_absolute = isset($item["jual_absolute"]) ? $item["jual_absolute"] : "";
	$jual_avitex_roof = isset($item["jual_avitex_roof"]) ? $item["jual_avitex_roof"] : "";
	$jual_belmas_roof = isset($item["jual_belmas_roof"]) ? $item["jual_belmas_roof"] : "";
	$jual_yoko_roof = isset($item["jual_yoko_roof"]) ? $item["jual_yoko_roof"] : "";
	$jual_lem_putih_vip_pvac = isset($item["jual_lem_putih_vip_pvac"]) ? $item["jual_lem_putih_vip_pvac"] : "";
	$jual_lem_putih_max_pvac = isset($item["jual_lem_putih_max_pvac"]) ? $item["jual_lem_putih_max_pvac"] : "";
	$jual_avian_road_line_paint = isset($item["jual_avian_road_line_paint"]) ? $item["jual_avian_road_line_paint"] : "";
	$jual_wood_eco_woodstain = isset($item["jual_wood_eco_woodstain"]) ? $item["jual_wood_eco_woodstain"] : "";
	$jual_boyo_politur_vernis = isset($item["jual_boyo_politur_vernis"]) ? $item["jual_boyo_politur_vernis"] : "";
	$jual_tan_politur = isset($item["jual_tan_politur"]) ? $item["jual_tan_politur"] : "";
	$jual_avian_hammertone = isset($item["jual_avian_hammertone"]) ? $item["jual_avian_hammertone"] : "";
	$jual_suzuka_lacquer = isset($item["jual_suzuka_lacquer"]) ? $item["jual_suzuka_lacquer"] : "";
	$jual_viplas = isset($item["jual_viplas"]) ? $item["jual_viplas"] : "";
	$jual_vip_paint_remover = isset($item["jual_vip_paint_remover"]) ? $item["jual_vip_paint_remover"] : "";
	$jual_avian_anti_fouling = isset($item["jual_avian_anti_fouling"]) ? $item["jual_avian_anti_fouling"] : "";
	$jual_avian_lem_epoxy = isset($item["jual_avian_lem_epoxy"]) ? $item["jual_avian_lem_epoxy"] : "";
	$jual_avian_non_sag_epoxy = isset($item["jual_avian_non_sag_epoxy"]) ? $item["jual_avian_non_sag_epoxy"] : "";
	$jual_thinner_a_avia = isset($item["jual_thinner_a_avia"]) ? $item["jual_thinner_a_avia"] : "";
	$jual_lenkote_colorants = isset($item["jual_lenkote_colorants"]) ? $item["jual_lenkote_colorants"] : "";
	$jual_giant_mortar_220 = isset($item["jual_giant_mortar_220"]) ? $item["jual_giant_mortar_220"] : "";
	$jual_giant_mortar_260 = isset($item["jual_giant_mortar_260"]) ? $item["jual_giant_mortar_260"] : "";
	$jual_giant_mortar_270 = isset($item["jual_giant_mortar_270"]) ? $item["jual_giant_mortar_270"] : "";
	$jual_giant_mortar_380 = isset($item["jual_giant_mortar_380"]) ? $item["jual_giant_mortar_380"] : "";
	$jual_giant_mortar_480 = isset($item["jual_giant_mortar_480"]) ? $item["jual_giant_mortar_480"] : "";
	$jual_platinum = isset($item["jual_platinum"]) ? $item["jual_platinum"] : "";
	$jual_avian = isset($item["jual_avian"]) ? $item["jual_avian"] : "";
	$jual_yoko = isset($item["jual_yoko"]) ? $item["jual_yoko"] : "";
	$jual_belmas_zinchromate = isset($item["jual_belmas_zinchromate"]) ? $item["jual_belmas_zinchromate"] : "";
	$jual_avian_zinchromate = isset($item["jual_avian_zinchromate"]) ? $item["jual_avian_zinchromate"] : "";
	$jual_yoko_loodmeni = isset($item["jual_yoko_loodmeni"]) ? $item["jual_yoko_loodmeni"] : "";
	$jual_thinner_a_special_avia = isset($item["jual_thinner_a_special_avia"]) ? $item["jual_thinner_a_special_avia"] : "";
	$jual_yoko_yzermenie = isset($item["jual_yoko_yzermenie"]) ? $item["jual_yoko_yzermenie"] : "";
	$jual_glovin = isset($item["jual_glovin"]) ? $item["jual_glovin"] : "";
	$jual_no_odor_wall_putty = isset($item["jual_no_odor_wall_putty"]) ? $item["jual_no_odor_wall_putty"] : "";
	$jual_lenkote_alkali_resisting_primer = isset($item["jual_lenkote_alkali_resisting_primer"]) ? $item["jual_lenkote_alkali_resisting_primer"] : "";
	$jual_no_lumut_solvent_based = isset($item["jual_no_lumut_solvent_based"]) ? $item["jual_no_lumut_solvent_based"] : "";
	$jual_no_drop_tinting = isset($item["jual_no_drop_tinting"]) ? $item["jual_no_drop_tinting"] : "";
	$rating_avg = isset($item["rating_avg"]) ? $item["rating_avg"] : "";
	$total_reviews = isset($item["total_reviews"]) ? $item["total_reviews"] : "";
	$count_5_star = isset($item["count_5_star"]) ? $item["count_5_star"] : "";
	$count_4_star = isset($item["count_4_star"]) ? $item["count_4_star"] : "";
	$count_3_star = isset($item["count_3_star"]) ? $item["count_3_star"] : "";
	$count_2_star = isset($item["count_2_star"]) ? $item["count_2_star"] : "";
	$count_1_star = isset($item["count_1_star"]) ? $item["count_1_star"] : "";
	$count_0_star = isset($item["count_0_star"]) ? $item["count_0_star"] : "";
	$total_discussions = isset($item["total_discussions"]) ? $item["total_discussions"] : "";
?>

<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2>View Store</h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="" method="post">
                                
                                <fieldset>
                                	<div class="row">
                                		<section class="col col-6">
	                                        <label class="label">Kode Customer</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $kode_customer; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Nama Customer</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $nama_customer; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Latitude</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $latitude; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Longitude</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $longitude; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Phone</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $phone; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Fax</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $fax; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Email</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $email; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Website</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $website; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Foto Toko</label>
	                                        <label class="input">
	                                            <input readonly value="<?= $foto_toko_url; ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-6">
	                                        <label class="label">Punya Mesin Tinting</label>
	                                        <label class="input">
	                                            <input readonly value="<?= convert_yesno($punya_mesin_tinting); ?>" />
	                                        </label>
	                                    </section>
	                                    <section class="col col-12">
	                                        <label class="label">Alamat</label>
	                                        <label class="textarea">
	                                            <textarea readonly rows="3"><?= $alamat ?></textarea>
	                                        </label>
	                                    </section>
                                	</div>       
                                </fieldset>

                                <fieldset>
                                	<div class="row">
                                        <section class="col col-4">
                                            <label class="label">Jual Pipa Power</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_pipa_power); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Pipa Power Max</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_pipa_power_max); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Fitting Power</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_fitting_power); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Talang Power</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_talang_power); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Fitting Talang Power</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_fitting_talang_power); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Odor</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_odor); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Fres</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_fres); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avitex Exterior</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avitex_exterior); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Boyo Plamir</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_boyo_plamir); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Drop</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_drop); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avitex</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avitex); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Aries Gold</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_aries_gold); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Aries</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_aries); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Sunguard</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_sunguard); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Supersilk</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_supersilk); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Everglo</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_everglo); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Aquamatt</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_aquamatt); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avitex Alkali Resisting Primer</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avitex_alkali_resisting_primer); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Drop 107</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_drop_107); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Drop 100</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_drop_100); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Drop Bitumen Black</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_drop_bitumen_black); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Absolute</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_absolute); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avitex Roof</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avitex_roof); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Belmas Roof</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_belmas_roof); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Yoko Roof</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_yoko_roof); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Lem Putih Vip Pvac</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_lem_putih_vip_pvac); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Lem Putih Max Pvac</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_lem_putih_max_pvac); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Road Line Paint</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_road_line_paint); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Wood Eco Woodstain</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_wood_eco_woodstain); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Boyo Politur Vernis</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_boyo_politur_vernis); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Tan Politur</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_tan_politur); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Hammertone</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_hammertone); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Suzuka Lacquer</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_suzuka_lacquer); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Viplas</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_viplas); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Vip Paint Remover</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_vip_paint_remover); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Anti Fouling</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_anti_fouling); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Lem Epoxy</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_lem_epoxy); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Non Sag Epoxy</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_non_sag_epoxy); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Thinner A Avia</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_thinner_a_avia); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Lenkote Colorants</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_lenkote_colorants); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Giant Mortar 220</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_giant_mortar_220); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Giant Mortar 260</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_giant_mortar_260); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Giant Mortar 270</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_giant_mortar_270); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Giant Mortar 380</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_giant_mortar_380); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Giant Mortar 480</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_giant_mortar_480); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Platinum</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_platinum); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Yoko</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_yoko); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Belmas Zinchromate</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_belmas_zinchromate); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Avian Zinchromate</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_avian_zinchromate); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Yoko Loodmeni</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_yoko_loodmeni); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Thinner A Special Avia</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_thinner_a_special_avia); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Yoko Yzermenie</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_yoko_yzermenie); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Glovin</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_glovin); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Odor Wall Putty</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_odor_wall_putty); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual Lenkote Alkali Resisting Primer</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_lenkote_alkali_resisting_primer); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Lumut Solvent Based</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_lumut_solvent_based); ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Jual No Drop Tinting</label>
                                            <label class="input">
                                                <input readonly value="<?= convert_yesno($jual_no_drop_tinting); ?>" />
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>

                                <fieldset>
                                	<div class="row">
                                		<section class="col col-6">
										    <label class="label">Rating Average</label>
										    <label class="input">
										        <input readonly value="<?= ($rating_avg)  ;?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Reviews</label>
										    <label class="input">
										        <input readonly value="<?= $total_reviews; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Rating 5</label>
										    <label class="input">
										        <input readonly value="<?= $count_5_star; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Rating 4</label>
										    <label class="input">
										        <input readonly value="<?= $count_4_star; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Rating 3</label>
										    <label class="input">
										        <input readonly value="<?= $count_3_star; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Rating 2</label>
										    <label class="input">
										        <input readonly value="<?= $count_2_star; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Rating 1</label>
										    <label class="input">
										        <input readonly value="<?= $count_1_star; ?>" />
										    </label>
										</section>
										<section class="col col-6">
										    <label class="label">Total Diskusi</label>
										    <label class="input">
										        <input readonly value="<?= $total_discussions; ?>" />
										    </label>
										</section>
                                	</div>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

         

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->