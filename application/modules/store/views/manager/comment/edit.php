<?php
    $id               = isset($item["id"]) ? $item["id"] : "";
    $store_name            = isset($item["store_name"]) ? $item["store_name"] : "";
    $member_name        = isset($item["member_name"]) ? $item["member_name"] : "";
    $discussion_content          = isset($item["discussion_content"]) ? $item["discussion_content"] : "";
    $discuss_datetime      = isset($item["discuss_datetime"]) ? $item["discuss_datetime"] : "";
    $reject_reason      = isset($item["reject_reason"]) ? $item["reject_reason"] : "";
    $status       = isset($item["status_review"]) ? $item["status_review"] : "";
    $status_review_name        = isset($item["status_review_name"]) ? $item["status_review_name"] : "";
?>

<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/store/review/list-review';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2>Edit Comment</h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/store/comment/process-form" method="POST">
                            <header>Comment Form</header>
                        	<input type="hidden" name="id" value="<?= $id ?>" />

                            <fieldset>
                                <section>
                                    <label class="label">Store Name</label>
                                    <label class="input">
                                        <input name="store_name" type="text" readonly value="<?php echo $store_name; ?>" />
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Comment By</label>
                                    <label class="input">
                                        <input name="member_name" type="text" readonly value="<?php echo $member_name; ?>" />
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Comment Content</label>
                                    <label class="textarea">
                                        <textarea rows="5" name="discussion_content" readonly><?php echo $discussion_content; ?></textarea>
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Comment Time</label>
                                    <label class="input">
                                        <input name="discuss_datetime" type="text" readonly value="<?php echo dateformatampm($discuss_datetime); ?>" />
                                    </label>
                                </section>
                            </fieldset>

                            <fieldset>
                                <section>
                                    <label class="label">Comment Status</label>
                                    <label class="select">
                                        <select name="review_status">
                                        	<option value="0" <?= selected(0, $status) ?>>Unapproved</option>
                                        	<option value="1" <?= selected(1, $status) ?>>Approved</option>
                                        	<option value="2" <?= selected(2, $status) ?>>Rejected</option>
                                        </select>
                                        <i></i>
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Reject Reason</label>
                                    <label class="textarea">
                                        <textarea rows="5" name="reject_reason"><?= $reject_reason; ?></textarea>
                                    </label>
                                </section>
                            </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
