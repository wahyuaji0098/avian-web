<?php
    $id         = isset($item["id"]) ? $item["id"] : "";
    $kode_customer      = isset($item["kode_customer"]) ? $item["kode_customer"] : "";
    $nama_customer        = isset($item["nama_customer"]) ? $item["nama_customer"] : "";
    $alamat    = isset($item["alamat"]) ? $item["alamat"] : "";
    $latitude      = isset($item["latitude"]) ? $item["latitude"] : "";
    $longitude  = isset($item["longitude"]) ? $item["longitude"] : "";
    $foto_toko_url       = isset($item["foto_toko_url"]) ? $item["foto_toko_url"] : "";
    $punya_mesin_tinting       = isset($item["punya_mesin_tinting"]) ? $item["punya_mesin_tinting"] : "";

    $rating_avian_brands       = isset($item["rating_avian_brands"]) ? $item["rating_avian_brands"] : "";
    $pretty_url       = isset($item["pretty_url"]) ? $item["pretty_url"] : "";
    $opening_hour       = isset($item["opening_hour"]) ? $item["opening_hour"] : "";
    $phone       = isset($item["phone"]) ? $item["phone"] : "";
    $fax       = isset($item["fax"]) ? $item["fax"] : "";
    $email       = isset($item["email"]) ? $item["email"] : "";
    $website       = isset($item["website"]) ? $item["website"] : "";
    $is_show        = isset($item["is_show"]) ? $item["is_show"] : false;
    $meta_keys        = isset($item["meta_keys"]) ? $item["meta_keys"] : false;
    $meta_desc        = isset($item["meta_desc"]) ? $item["meta_desc"] : false;
    $created_date       = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date       = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Store</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/store/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Pretty URL</label>
                                        <label class="input">
                                            <input name="pretty_url" type="text"  class="form-control" placeholder="Pretty URL" value="<?= $pretty_url; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Kode Customer</label>
                                        <label class="input">
                                            <input name="kode_customer" type="text"  class="form-control" placeholder="Kode Customer" value="<?= $kode_customer; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Nama Customer</label>
                                        <label class="input">
                                            <input name="nama_customer" type="text"  class="form-control" placeholder="Nama Customer" value="<?= $nama_customer; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Punya Mesin Tinting </label>
                                        <label class="input">
                                            <?= select_yes_no('punya_mesin_tinting', $punya_mesin_tinting); ?>
                                        </label>
                                    </section>
									<section>
                                        <label class="label">Alamat</label>
                                        <label class="textarea">
                                            <textarea name="alamat" type="text"  class="form-control" placeholder="Alamat" ><?= $alamat; ?></textarea>
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Latitude</label>
                                            <label class="input">
                                                <input name="latitude" type="text"  class="form-control" placeholder="Latitude" value="<?= $latitude; ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Longitude</label>
                                            <label class="input">
                                                <input name="longitude" type="text"  class="form-control" placeholder="Longitude" value="<?= $longitude; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Opening Hour</label>
                                        <label class="input">
                                            <input name="opening_hour" type="text"  class="form-control" placeholder="Opening Hour" value="<?= $opening_hour; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Phone</label>
                                        <label class="input">
                                            <input name="phone" type="text"  class="form-control" placeholder="Phone" value="<?= $phone; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Fax</label>
                                        <label class="input">
                                            <input name="fax" type="text"  class="form-control" placeholder="Fax" value="<?= $fax; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Email</label>
                                        <label class="input">
                                            <input name="email" type="text"  class="form-control" placeholder="Email" value="<?= $email; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Website</label>
                                        <label class="input">
                                            <input name="website" type="text"  class="form-control" placeholder="Website" value="<?= $website; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Image Toko ()</label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image">
                                                <?php if($foto_toko_url): ?>
                                                <img src="<?= $foto_toko_url ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($foto_toko_url != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Meta Keys</label>
                                        <label class="textarea">
                                            <textarea name="meta_keys" type="text"  class="form-control" placeholder="Meta Keys" ><?= $meta_keys; ?></textarea>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Meta Desc</label>
                                        <label class="textarea">
                                            <textarea name="meta_desc" type="text"  class="form-control" placeholder="Meta Desc" ><?= $meta_desc; ?></textarea>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-6">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-6">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
