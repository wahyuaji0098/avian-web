<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>CARI LOKASI TOKO</span>
            </div>
            <div class="content_sbar store_sbar">
                <div class="sidebar">
                    <div class="sidebar-title">PILIHAN</div>
                    <div class="sidebar-group">
                        <div class="option-options row">
                            <?php if(count($pcategory) > 0): foreach($pcategory as $cat): ?>
                            <div class="col col-lg-12 col-md-6 col-sm-12">
                                <div class="option option-checkbox option-checkbox-spc">
                                    <input class="p_category_<?= $cat['id'] ?>" type="checkbox" name="parent_category[]" value="<?= $cat['id'] ?>" id="<?= str_replace(" ","",$cat['name']) ?>" />
                                    <label for="<?= str_replace(" ","",$cat['name']) ?>">
                                        <div class="cicon"></div>
                                        <span><?= $cat['name'] ?></span>
                                    </label>
                                </div>
                                <?php if(count($cat['product']) > 0): ?>
                                <div class="option-sub">
                                    <?php foreach($cat['product'] as $prd): ?>
                                    <div class="option option-checkbox">
                                        <input type="checkbox" class="category_<?= $cat['id'] ?>" name="category[]" value="<?= $prd['id'] ?>" id="<?= str_replace(" ","",$prd['name']) ?>" <?= (is_array($ss_search['category']) && array_search($prd['id'], $ss_search['category']) !== false) ? "checked" : "" ?>/>
                                        <label for="<?= str_replace(" ","",$prd['name']) ?>">
                                            <div class="cicon"></div>
                                            <span><?= $prd['name'] ?></span>
                                        </label>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; endif; ?>
                        </div>
                    </div>
                    <div class="lead"></div>
                </div>
                <div class="sbarcontent">
                    <div class="sbarcontentsection">
                        <div class="storesearchcontain">
                            <div class="pinbutton" id="pin-location"></div>
                            <div class="storesearchbox">
                                <input type="text" id="search-box" placeholder="Masukan Nama Lokasi" value="<?= ($ss_search['search']) ? $ss_search['search'] : "" ?>"/>
                            </div>
                            <div class="option option-select">
                                <div class="select-cont">
                                    <select id="search-by">
                                        <option value="1" <?= ($ss_search['search_by'] == "1") ? "selected = 'selected'" : "" ?>>berdasarkan Lokasi</option>
                                        <option value="2" <?= ($ss_search['search_by'] == "2") ? "selected = 'selected'" : "" ?>>berdasarkan Nama Toko</option>
                                        <option value="3" <?= ($ss_search['search_by'] == "3") ? "selected = 'selected'" : "" ?>>berdasarkan Nama Produk</option>
                                    </select>
                                </div>
                            </div>
                            <input class="btn btn-sml" type="button" value="Cari" id="search-store"/>
                            <input class="btn" type="button" value="Hapus" id="clear-btn"/>
                            <input type="hidden" id="f_lat" value="<?= ($ss_search['lat']) ? $ss_search['lat'] : 0 ?>" />
                            <input type="hidden" id="f_long" value="<?= ($ss_search['long']) ? $ss_search['long'] : 0 ?>" />
                            <input type="hidden" id="f_user_lat" value="<?= ($ss_search['lat_user']) ? $ss_search['lat_user'] : 0 ?>" />
                            <input type="hidden" id="f_user_long" value="<?= ($ss_search['long_user']) ? $ss_search['long_user'] : 0 ?>" />
                            <input type="hidden" id="f_search_by" value="<?= ($ss_search['search_by']) ? $ss_search['search_by'] : 1 ?>" />
                            <input type="hidden" id="f_zoom" value="<?= ($ss_search['zoom']) ? $ss_search['zoom'] : 15 ?>" />
                        </div>
                    </div>
                    <div class="gmaparea">
                        <div class="gmap"><!-- Google Map -->
                            <div class="gmap_object" id="gmap_object"></div>
                            <div class="contloading"></div>
                        </div>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
