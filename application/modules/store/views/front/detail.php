<?php
    $store_id = isset($model["id"]) ? $model["id"] : "";
    $rating = isset($rating["rating"]) ? $rating["rating"] : "";
    $kualitas_rating = isset($model["rating_avianbrands"]) ? intval($model["rating_avianbrands"]) : "";
    $harga_rating = isset($model["harga_rating"]) ? $model["harga_rating"] : "";
    $deskripsi_singkat = isset($model["deskripsi_singkat"]) ? $model["deskripsi_singkat"] : "";
    $paling_cocok_untuk = isset($model["paling_cocok_untuk"]) ? $model["paling_cocok_untuk"] : "";
    $kelebihan = isset($model["kelebihan"]) ? $model["kelebihan"] : "";

    // view_store_rating
    $rating_avg = isset($vsr["rating_avg"]) ? round($vsr["rating_avg"], 1) : "";
    $total_reviews = isset($vsr["total_reviews"]) ? $vsr["total_reviews"] : "";
    $count_5_star = isset($vsr["count_5_star"]) ? $vsr["count_5_star"] : "";
    $count_4_star = isset($vsr["count_4_star"]) ? $vsr["count_4_star"] : "";
    $count_3_star = isset($vsr["count_3_star"]) ? $vsr["count_3_star"] : "";
    $count_2_star = isset($vsr["count_2_star"]) ? $vsr["count_2_star"] : "";
    $count_1_star = isset($vsr["count_1_star"]) ? $vsr["count_1_star"] : "";
    $count_0_star = isset($vsr["count_0_star"]) ? $vsr["count_0_star"] : "";
?>

<div class="container container_full" itemscope itemtype="http://schema.org/Product">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/store">CARI LOKASI TOKO</a>
                <span><?= strtoupper($model['nama_customer']) ?></span>
            </div>
        </div>
    </div>
    <div class="section section-product-quikinfo">
        <div class="section_content">
            <div class="section-product-quikinfo-r">
                <div class="product-image">
                    <img src="<?= (!empty($model['foto_toko_url']) && $model['foto_toko_url'] != '-') ? $model['foto_toko_url'] : "/img/ui/store-img-placeholder-large.png" ?>"/>
                </div>
            </div>
            <div class="section-product-quikinfo-l">
                <h1 class="product-name"><?= $model['nama_customer'] ?></h1>
                <div class="product-infos row">
                    <div class="product-info col col-sm-12 col-md-12">
                        <h4 class="product-info-name">Alamat:</h4>
                        <div class="product-info-content"><?= $model['alamat'] ?></div>
                    </div>
                </div>
                <div class="product-infos row">
                    <div class="product-info col col-sm-12 col-md-6">
                        <h4 class="product-info-name">Nomor telepon:</h4>
                        <div class="product-info-content"><?= ($model['phone']) ? "<a href='tel:".$model['phone']."'>".$model['phone']."</a>" : "Belum ada nomor telepon" ?></div>
                    </div>
                    <div class="product-info col col-sm-12 col-md-6">
                        <h4 class="product-info-name">Nomor fax:</h4>
                        <div class="product-info-content"><?= ($model['fax']) ? "<a href='tel:".$model['fax']."'>".$model['fax']."</a>" : "Belum ada nomor HP" ?></div>
                    </div>
                </div>
                <div class="product-infos row">
                    <div class="product-info col col-sm-12 col-md-6">
                        <h4 class="product-info-name">Website:</h4>
                        <div class="product-info-content"><?= ($model['website']) ? "<a href='".$model['website']."'>".$model['website']."</a>" : "Belum ada website" ?></div>
                    </div>
                    <div class="product-info col col-sm-12 col-md-6">
                        <h4 class="product-info-name">Email:</h4>
                        <div class="product-info-content"><?= ($model['email']) ? "<a href='mailto:".$model['email']."'>".$model['email']."</a>" : "Belum ada email" ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-store-widget">
        <div class="section_content">
            <div class="store-widget">
                <div class="store-widget-nav">
                    <a href="https://www.google.com/maps/dir/Current+Location/<?= $model['latitude'] ?>,<?= $model['longitude'] ?>" target="_blank" class="btn-store-nav"><i class="fa fa-location-arrow" aria-hidden="true"></i>Navigasi<span class="hideinmobile"> ke toko</span></a>
                    <div class="store-widget-distance">Jarak<span class="hideinmobile"> ke toko</span>:<span class="store-widget-distanceval"><?= number_format($distance_store,2) ?></span><span class="store-widget-distanceunit">KM</span></div>
                </div>
                <div class="store-widget-btns">
                    <div class="widget-btn widget-btn-share toggler_popup toggler_popup_trigger toggler-sharepop" data-toggler-popup="sharepop"><i class="fa fa-share-alt" aria-hidden="true"></i>Bagikan
                        <!-- share with plugin -->
                        <div class="addthis_inline_share_toolbox sharepopup toggler_popup toggler-sharepop" id="share"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section section-product-ratings">
        <div class="section_content">
            <div class="product-ratings row">
                <div class=" product-rating col col-md-6 col-sm-12">
                    <h4 class="product-rating-name">Rating kelengkapan dari Avian Brands:</h4>
                    <div class="product-rating-content">
                        <div class="product-rating-star star-yellow rate-<?= $kualitas_rating ?>"><?= isset($model["rating_avianbrands"]) ? $kualitas_rating.".0" : "-" ?></div>
                        <div class="product-rating-info">Kriteria penilaian:</div>
                        <ol>
                            <li>Menjual Avian, Avitex, No Drop, Sunguard.</li>
                            <li>Memiliki mesin campur warna.</li>
                            <li>Kelengkapan warna selalu di atas 50%.</li>
                        </ol>
                    </div>
                </div>
                <?php /*
                <div class=" product-rating col col-md-6 col-sm-12">
                    <h4 class="product-rating-name">Rating rata-rata dari pengguna:</h4>
                    <div class="product-rating-content">
                        <div class="product-rating-star user-green rate-<?= $rating_avg ?>"><?= isset($vsr["rating_avg"]) ? round($rating_avg, 1) : "-" ?></div>
                        <div class="ratingcounts">
                            <div class="ratingcount ratingcount-5"><?= $count_5_star+0 ?></div>
                            <div class="ratingcount ratingcount-4"><?= $count_4_star+0 ?></div>
                            <div class="ratingcount ratingcount-3"><?= $count_3_star+0 ?></div>
                            <div class="ratingcount ratingcount-2"><?= $count_2_star+0 ?></div>
                            <div class="ratingcount ratingcount-1"><?= $count_1_star+0 ?></div>
                            <div class="ratingcounttotals"><span><?= $count_5_star+$count_4_star+$count_3_star+$count_2_star+$count_1_star ?></span> Penilaian &nbsp;&nbsp;<span><?= $total_reviews+0 ?></span> Ulasan</div>
                        </div>
                    </div>
                </div>
                */ ?>
            </div>
        </div>
    </div>
    <?php /*
    <div class="section section-product-threads">
        <div class="section_content">
            <div class="product-threads">
                <div class="product-threads-tabs-contain">
                    <div class="product-threads-tabs">
                        <div class="product-threads-tab togglertab_grouped_trigger togglertab-group-a togglertab-a-1 active" data-togglertab-group="a" data-togglertab-id="1">Review</div>
                        <div class="product-threads-tab togglertab_grouped_trigger togglertab-group-a togglertab-a-2" data-togglertab-group="a" data-togglertab-id="2">Diskusi</div>
                    </div>
                </div>
                <div class="product-threads-contents">
                    <div class="product-threads-content togglertab-group-a togglertab-a-1 active">
                        <?php foreach($all_review as $key => $value) : ?>
                        <div class="thread-item rates">
                            <div class="thread-title"><?= $value['review_title'] ?></div>
                            <div class="thread-rating rate">
                                <div class="ratecount ratecount-<?= $value['rating_score'] ?> ratecount-stargreen"></div>
                                <div class="thread-author">oleh <span><?= $value['nama_customer'] ?></span></div>
                            </div>
                            <div class="thread-datetime"><?= dateformatonly_indonesia($value['review_datetime']) ?></div>
                            <div class="thread-content"><?= $value['review_content'] ?></div>
                        </div>
                        <?php endforeach; ?>
                        <?= empty($all_review) ? '<div class="thread-empty">Belum ada ulasan</div>' : '' ?>
                        <div class="loadingcell"><span>Memuat lainnya ...</span></div>
                    </div>
                    <div class="product-threads-content togglertab-group-a togglertab-a-2">
                        <?php foreach($all_comment as $key => $value) : ?>
                        <div class="thread-item">
                            <div class="thread-content"><?= $value['discussion_content'] ?></div>
                            <div class="mc-author">By <?= $value['nama_customer'] ?> on <?= dateformatonly_indonesia($value['discuss_datetime']) ?></div>

                            <!-- LIST REPLY -->
                            <?php foreach($value['reply'] as $key2 => $value2) : ?>
                            <div class="mc-reply">
                                <div><?= $value2['discussion_content']; ?></div>
                                <div class="mc-author">By <?= $value2['nama_customer'] ?> on <?= dateformatonly_indonesia($value2['discuss_datetime']) ?></div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <?php endforeach; ?>
                        <?= empty($all_comment) ? '<div class="thread-empty">Belum ada diskusi</div>' : '' ?>
                        <div class="loadingcell"><span>Memuat lainnya ...</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    */ ?>
</div>
