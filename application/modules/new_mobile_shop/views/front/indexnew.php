 <div id="content" class="mr horizontal-wrapper pemasaran" data-aos="fade">
    <div class="kantor-map map-box not-hscroll">
      <div class="map not-hscroll" id="map"></div>
    </div>

    <div class="map-filter row bg-light-green" id="tokoFilter">
      <div class="col-md-12">
        <h3 class="font-sofia-bold font-md  font-green">Cari Toko</h3>
      </div>
      <div class="col-md-8">
        <div class="form-group">
          <select class="input-form">
            <option>Berdasarkan Lokasi</option>
            <option>Berdasarkan Nama Toko</option>
            <option>Berdasarkan Nama Produk</option>
          </select>
          <input type="text" class="input-form" placeholder="Lokasi Anda">
        </div>
      </div>
      <div class="col-md-2">
        <button class="button button-rounded button-block btn-inline-green font-green" type="submit">Cari</button>
      </div>
      <div class="col-md-2">
        <button class="button button-rounded button-block btn-inline-green font-green btn-mfilter" data-container="body" data-toggle="popover" data-placement="bottom" data-popover-content="#popoverContent">
          + Filter
        </button>

        <div style="display: none;">
          <div id="popoverContent">
            <div class="row">
              <div class="col-sm-6">
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><h4 class="font-sofia-bold">Cat Tembok Premium</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Sunguard All-In-One</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">No Odor Medicar</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Everglo</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Supersilk Anti Noda</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Aquamatt</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">HomeDeco Metallic</p></div>
                    </div>
                  </li>
                </ul>
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><h4 class="font-sofia-bold">Cat Tembok Premium</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Sunguard All-In-One</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">No Odor Medicar</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Everglo</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Supersilk Anti Noda</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Aquamatt</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">HomeDeco Metallic</p></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="col-sm-6">
                <ul class="fgroup">
                  <li class="title-group">
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><h4 class="font-sofia-bold">Cat Tembok Premium</h4></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Sunguard All-In-One</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">No Odor Medicar</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Everglo</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Supersilk Anti Noda</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Aquamatt</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">HomeDeco Metallic</p></div>
                    </div>
                  </li>
                  <li>
                    <div class="row rowcheck">
                      <div class="col-md-2 padcheck">
                        <label class="content-check">
                          <input type="checkbox" >
                          <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-10 textcheck"><p class="font-sofia-light font-sm font-green text">Avitex Dopir dan Kamar Mandi</p></div>
                    </div>
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="map-info left bg-light-green md">
      <div id="marker-detail" class="marker-detail"></div>
    </div>

    <div class="map-info bg-light-green">
      <a href="#" class="fa fa-remove" onclick="$('.map-info').fadeOut()"></a>
      <img src="/avian_new/images/cabang/tirtakencana.png" width="50%">
      <p class="font-sofia-light font-sm">Kami memastikan seluruh produk Avian Brands tersedia secara merata di seluruh pelosok Indonesia melalui jalur distribusi terintegrasi PT Tirtakencana Tatawarna.</p>
    </div>
  </div>

<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
 <script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAdfB-1tzijt8NQRVY6SLNft9_JwxWxu1s&libraries=geometry'
  type="text/javascript"></script>
<script type="text/javascript" src="/avian_new/js/store-location.js"></script>
<script>
    
</script>
<script type="text/javascript" src="/avian_new/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.matchHeight-min.js"></script>