<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>Avian Brands Peduli</span>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-5 col-sm-12">
                    <div class="section-title tabgroup targrouptaba targettaba0 section-title-top" targid="taba0" targroup="taba">
                        <div class="page-logo">
                            <img src="/img/avian_csr_logo.png" alt="Avian Brands Peduli" />
                        </div>
                    </div>
                    <div class="page-tagline">
                        Avian Brands Peduli
                    </div>
                    <div class="csr-text-subtag">
                        <p>
                            Kepedulian kami untuk memajukan dan mengembangkan bangsa Indonesia, hadir lewat CSR Avian Brands Peduli dalam dukungan terhadap  dunia pendidikan, lingkungan hidup serta korban bencana alam.
                        </p>
                    </div>

                    <div class="row csr-controls">
                        <div class="col-lg-12 col-md-12 col-sm-4">
                            <div class="styledselect_contain">
                                <select class="styledselect" id="search-box" data-search="<?= $ss_search ?>">
                                    <option class="defaultoption" value="">Pilih Lokasi</option>
                                    <option class="alloption" value="" data-iconurl="">Semua</option>
                                    <?php foreach ($lokasi as $name): ?>
                                    <option value="<?= strtolower($name['lokasi']) ?>" <?= ($ss_search == strtolower($name['lokasi'])) ? "selected='selected'" : "" ?>><?= $name['lokasi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-4">
                            <div class="styledselect_contain">
                                <select class="styledselect" name="search-type" id="search-type">
                                    <option class="defaultoption" value="" data-iconurl="">Pilih Tipe CSR</option>
                                    <option class="alloption" value="" data-iconurl="">Semua</option>
                                    <option class="csr1" value="1" data-iconurl="/img/ui/pin-csr-education-20.png">Pendidikan</option>
                                    <option class="csr2" value="2" data-iconurl="/img/ui/pin-csr-vacation-20.png">Lingkungan</option>
                                    <option class="csr3" value="3" data-iconurl="/img/ui/pin-csr-disaster-20.png">Bencana Alam</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-7 col-sm-12">
                    <div class="gmaparea">
                        <div class="gmap"><!-- Google Map -->
                            <div class="gmap_object" id="gmap_object"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="csrlist_contain">
                <div class="csrlist row">
                </div>
            </div>
            <div class="lead"></div>
        </div>
    </div>
</div>
