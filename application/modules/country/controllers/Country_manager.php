<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact Controller.
 */
class Country_manager extends Baseadmin_Controller  {

    private $_title = "Country";
    private $_title_page = '<i class="fa-fw fa fa-envelope"></i> Country ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "country";
    private $_back = "/manager/country";
    private $_js_path = "/js/pages/country/";
    private $_view_folder = "country/";

    private $_table = "mtb_country";
    private $_table_aliases = "mc";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Contact
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Country</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Country</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Reply email
     */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/country">Country</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Country </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Country</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
    * Edit 
    */
    public function edit($id = "") {
        if(!$id) {
            show_404();
        }

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Country </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Country</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'conditions' => array("id" => $id),
            'row_array' => true
        ))['datas'];

        #pr($data);exit;

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Import User
     */
    public function import () {
        $this->_breadcrumb .= '<li><a href="/manager/country">Country</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Import Player</span>',
            "active_page"   => "user-list",
            "breadcrumb"    => $this->_breadcrumb . '<li>Import Player</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "import.js",
            ),
            "css" => array(
                "/css/select2.min.css",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'import');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //special validations for when editing.
        $this->form_validation->set_rules('name', 'Country Name', "trim|required|callback_check_name[$id]");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','name', 'is_show');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * This is a custom form validation rule to check that username is must unique.
     */
    public function check_name($str, $id) {
        //sanitize input
        $str = sanitize_str_input($str);
        $id = sanitize_str_input($id);

        if (!$this->_secure) {
            show_404();
        }

        //flag.
        $isValid = true;
        $params = array("row_array" => true);

        if ($id == "") {
            //from create
            $params['conditions'] = array("lower(name)" => strtolower($str));
        } else {
            $params['conditions'] = array("lower(name)" => strtolower($str), "id !=" => $id);
        }

        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];

        if ($datas) {
            $isValid = false;
            $this->form_validation->set_message('check_name', '{field} is already taken.');
        }

        return $isValid;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;
        
        //
        $this->load->library('form_validation');
        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id       = sanitize_str_input($this->input->post('id'), "numeric");
        $name     = sanitize_str_input($this->input->post('name'));
        $is_show  = sanitize_str_input($this->input->post('is_show'));
        
        $this->_set_rule_validation($id);

        if ($this->form_validation->run($this) == FALSE) {
            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            $this->db->trans_begin();
            //prepare insert
            $arrayToDb = array(
                "name"    => $name,
                "is_show" => $is_show,
            );

            if($id == "") {
              
                
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert($arrayToDb, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Country has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/country";
                }
            } else {
                //update
                //conditions for update
                $conditions = array("id" => $id);

                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update($arrayToDb, $conditions, array('is_version' => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Products has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/country";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * delete.
     */
    public function delete() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        //check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (!$data) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // deactivate the user
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(array("is_show" => 0),$condition, array('is_version' => true));

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Country has been Deleted.";
                    $message['redirect_to']   = "";
                }
            }

        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Reactivate an admin.
     */
    public function reactivate() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        // sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        // check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            // no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // reactivate the user
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(array("is_show" => STATUS_ACTIVE),$condition, array('is_version' => TRUE));

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Country has been re-activated.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * process export 
     */
    public function process_export_country()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        //load model and library php excel
        $this->load->library("PHPExcel_reader");

        $header = array();
        $header[] = array(
            'Name',
            'Show',
        );

        $select = "name, is_show";

        $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => $select,
            "conditions"      => array('is_show' => STATUS_ACTIVE),
            "count_all_first" => false,
            "debug"           => false,
        ))['datas'];

        $result = array_map(array($this, 'export_country_mapping'), $result);

        //merge array
        if (count($result) > 0) {
            $all_data = $header;
            $all_data = array_merge($header,$result);
            $exlc = new PHPExcel_reader();
            $data = $exlc->exportToExcelSingleSheets($all_data,"Export Country ". strtotime(date('Y-m-d H:i:s')), null, null,'A', 'B', 2, count($all_data));

            $message['is_error']  = false;
            $message['filename']  = "Export Country ". strtotime(date('Y-m-d H:i:s'));
            $message['file_data'] = "data:application/vnd.ms-excel;base64,".base64_encode($data);

            $message['notif_title']   = "Good!";
            $message['notif_message'] = "Download success";

        } else {
            $message['is_error']    = true;
            $message['error_msg']   = "Tidak ada Laporan Country.";
            $message['redirect_to'] = "";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
    
    //call back array map
    public function export_country_mapping($source)
    {
        return array(
            'name'         => $source['name'],
            'is_show_name' => $source['is_show_name'],
        );
    }

    /**
     * validating import section
     */
    public function validate_import($data, $line)
    {

        $this->load->model('Dynamic_model');
        $error = "";

        if (check_null_space($data['name'])) {
            $error .= "Row " . $line . ": Name cannot be empty.";
        }

        //check if username is exist
        $name = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "conditions" => array("name" => $data['name']),
            "row_array"  => true,
        ))['datas'];


        if (!empty($name)) {
            $error .= "Row " . $line . ": Country Name is Exists.";
        }


        if ($error != "") {
            return $error;
        } else {
            return true;
        }
    }
    
   /**
     * Process Import
     */
    public function process_import()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Dynamic_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $validate = "";
        $error_validate_flag = false;

        //check validation
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > 0) {
            $filename = $_FILES['file']['tmp_name'];
            //check max file upload
            if ($_FILES['file']['size'] > MAX_UPLOAD_FILE_SIZE) {
                $message['error_msg'] = "Maximum file size is ".WORDS_MAX_UPLOAD_FILE_SIZE;
            } elseif (mime_content_type($filename) != "application/vnd.ms-excel"
                        && mime_content_type($filename) != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                //check extension if xls dan xlsx
                $message['error_msg'] = "File must .xls or .xlsx";
            } else {
                //load and init library
                $this->load->library("PHPExcel_reader");
                $excel = new PHPExcel_reader();

                //read the file and give back the result of the file in array
                $result = $excel->read_from_excel($filename);

                if ($result['is_error'] == true) {
                    $message['redirect_to'] = "";
                    $message['error_msg']   = $result['error_msg'];
                } else {

                    //begin transaction
                    $this->db->trans_begin();

                    if (count($result['datas']) > 0) {
                        //loop the result and insert to db
                        $line = 2;
                        foreach ($result['datas'] as $model) {
                            if (count($model) < 2) {
                                $validate = "Invalid Template.";
                                $error_validate_flag = true;
                                break;
                            } else {
                                //check untuk semua kolom apakah null
                                if (empty($model[0]) && empty($model[1]) ) {
                                    // klo kosong semua isinya, skip
                                    $line++;
                                } else {

                                    // persiapan data
                                    $data = array(
                                        "name"     => trim($model[0]),
                                    );

                                    $validate = $this->validate_import($data, $line);

                                    if ($validate !== true) {
                                        //if validate false, return error
                                        $error_validate_flag = true;
                                        break;
                                    } else {
                                        // 
                                        $insert = array(
                                            "name"     => trim($model[0]),
                                            "is_show"  => 1
                                        );

                                        $line++;
                                    }
                                }
                            }
                        }

                        // insert_batch data player
                        $insert_data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                            $insert,
                            array(
                                "is_version" => true,
                            )
                        );

                    }

                    if ($this->db->trans_status() === false || $error_validate_flag == true) {
                        $this->db->trans_rollback();
                        $message['redirect_to'] = "";

                        if ($error_validate_flag == true) {
                            $message['error_msg'] = $validate;
                        } else {
                            $message['error_msg'] = 'database operation failed.';
                        }
                    } else {
                        $this->db->trans_commit();

                        $message['is_error'] = false;

                        // success.
                        $message['notif_title']   = "Good!";
                        $message['notif_message'] = "Country has been imported.";

                        // on insert, not redirected.
                        $message['redirect_to'] = "";
                    }
                }
            }
        } else {
            $message['error_msg'] = 'File is empty (xls or xlsx).';
        }

        echo json_encode($message);
        exit;
    }
}

