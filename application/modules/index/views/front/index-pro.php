<div class="container">
    <div class="section section-nocrumb section-thirds gridds">
        <div class="section_content griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <div class="twothird main-slider griddsitem shadowedhov flexgrid width-7 height-6" id="sliderpro">
                <?php
                    if ($sliders) :
                        foreach ($sliders as $mslide) :
                ?>
                <div class='mainslide <?= ($mslide['title'] != "" || $mslide['description'] != "") ? "mainslidewithcaption" : "" ?>'>
                    <?php if($mslide['url']): ?>
                    <a href="<?= $mslide['url'] ?>" target="_blank">
                    <?php endif; ?>
                    <div class="mainslide-cont">
                        <img src="<?= $mslide['image_url_pro'] ?>" />
                        <?php if($mslide['title'] != "" || $mslide['description'] != ""): ?>
                        <div class="mainslidecaption">
                            <h2 class='mainslidecaptitle'><?= $mslide['title'] ?></h2>
                            <p class='mainslidecaptext'><?= $mslide['description'] ?></p>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if($mslide['url']): ?>
                    </a>
                    <?php endif; ?>
                </div>
                <?php
                        endforeach;
                    endif;
                ?>

            </div>
            <h3 class="athird-vizcol griddsitem shadowedhov width-3 height-3 autopalette" colhex="<?= $menu[1]['color'] ?>"><a href="/visualize">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[1]['name'] ?></span>
            </a></h3>
            <h3 class="athird-colmod griddsitem shadowedhov width-3 height-3 autopalette" colhex="<?= $menu[2]['color'] ?>"><a href="/colours">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[2]['name'] ?></span>
            </a></h3>
            <a class="twoup article griddsitem shadowedhov width-2 height-3 autopalette" colhex="<?= $menu[3]['color'] ?>" href="/article">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[3]['name'] ?></span>
            </a>
            <a class="twoup stores griddsitem shadowedhov width-2 height-3 autopalette" colhex="#<?= $menu[4]['color'] ?>" href="/store">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[4]['name'] ?></span>
            </a>
            <div class="topdownitem itemsmallico newsarchive griddsitem shadowedhov top-7 left-5 width-3 height-3 autopalette" colhex="<?= $menu[5]['color'] ?>">
                <div class="smallico"></div>
                <div class="smallicotitle"><?= $menu[5]['name'] ?></div>
                <div class="itemcontent">
                <?php if(count($article) > 0): ?>
                    <ul class="simpletree">
                    <?php foreach ($article as $model): ?>
                        <li><a href="/article/detail/<?= $model['pretty_url'] ?>"><?= trimstr($model['title'],40, 'WORDS', '...') ?></a></li>
                    <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                </div>
            </div>
            <div class="griddsitem shadowedhov top-7 left-8 width-3 height-3 autopalette" colhex="<?= $menu[6]['color'] ?>">
                <a href="/paint-calculator">
                    <span class="large-icon-cust"><img src="/img/ui/large-icon-calculator.png"/></span>
                    <span class="large-text"><?= $menu[6]['name'] ?></span>
                </a>
            </div>
            <a class="onedown learnavian griddsitem shadowedhov top-10 left-1 width-4 height-3" href="/about-us">
            </a>
            <a href="/palette/detail/<?= $color['pallete']['id'] ?>" class="colplate colplate_shad griddsitem shadowedhov top-10 left-5 width-3 height-3 autopalette" colhex="<?= $color['hex_code'] ?>">
                <div class="colname"><?= $color['name'] ?></div>
                <div class="colcode"><?= $color['code'] ?></div>
                <div class="colgroup"><?= $color['pallete']['name'] ?></div>
                <div class="colfootnot">tekan saya untuk melihat warna lainnya dari pallete ini</div>
            </a>
            <a href="/download" class="downapp griddsitem shadowedhov top-10 left-8 width-3 height-3" >
                <span class="downappcont autopalette" colhex="<?= $menu[8]['color'] ?>">
                    <span class="itemtitle"><?= $menu[8]['name'] ?></span>
                    <span class="btn">DOWNLOAD</span>
                </span>
            </a>
        </div>
    </div>
</div>
