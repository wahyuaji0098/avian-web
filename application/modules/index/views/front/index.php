<div class="container">
    <div class="section section-nocrumb gridds">
        <div class="section_content col12 griddscontent">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <div class="main-slider griddsitem shadowedhov width-12 height-7 flexgrid " id="sliderA">

                <?php
                    if ($sliders) :
                        foreach ($sliders as $mslide) :

                            /**
                            <div class='mainslide <?= ($mslide['title'] != "" || $mslide['description'] != "") ? "mainslidewithcaption" : "" ?> '>
                                <?php if($mslide['url']): ?>
                                <a href="<?= $mslide['url'] ?>" target="_blank">
                                <?php endif; ?>
                                <div class="mainslide-cont">
                                    <img src="<?= $mslide['image_url'] ?>" alt="<?= $mslide['title'] ?>"/>
                                    <?php if($mslide['title'] != "" || $mslide['description'] != ""): ?>
                                    <div class="mainslidecaption">
                                        <h2 class='mainslidecaptitle'><?= $mslide['title'] ?></h2>
                                        <p class='mainslidecaptext'><?= $mslide['description'] ?></p>
                                    </div>
                                    <?php endif; ?>
                                </div>
                                <?php if($mslide['url']): ?>
                                </a>
                                <?php endif; ?>
                            </div>
                            **/
                ?>
                    <div>
                        <?php if($mslide['url']): ?>
                        <a href="<?= $mslide['url'] ?>" target="_blank">
                        <?php endif; ?>
                            <img src="<?= $mslide['image_url'] ?>" alt="<?= $mslide['title'] ?>"/>
                        <?php if($mslide['url']): ?>
                        </a>
                        <?php endif; ?>
                    </div>
                <?php
                        endforeach;
                    endif;
                ?>
            </div>
            <h3 class="griddsitem shadowedhov athird-vizcol width-4 height-3 autopalette" colhex="<?= $menu[1]['color'] ?>" ><a href="/visualize">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[1]['name'] ?></span>
            </a></h3>
            <h3 class="griddsitem shadowedhov athird-colmod width-4 height-3 autopalette" colhex="<?= $menu[2]['color'] ?>" ><a href="/colours">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[2]['name'] ?></span>
            </a></h3>
            <h3 class="griddsitem shadowedhov athird-stoloc width-4 height-3 autopalette" colhex="<?= $menu[3]['color'] ?>" ><a href="/store">
                <span class="large-icon"></span>
                <span class="large-text"><?= $menu[3]['name'] ?></span>
            </a></h3>
            <h3 class="griddsitem shadowedhov width-4 height-3 autopalette" colhex="<?= $menu[4]['color'] ?>" ><a href="/article">
                <span class="large-icon-cust"><img src="/img/ui/tips-and-article-home-icon.png"/></span>
                <span class="large-text"><?= $menu[4]['name'] ?></span>
            </a></h3>
            <h3 class="griddsitem shadowedhov width-4 height-3 autopalette" colhex="<?= $menu[5]['color'] ?>" ><a href="/about-us">
                <span class="large-icon-cust"><img src="/img/ui/large-icon-about.png"/></span>
                <span class="large-text"><?= $menu[5]['name'] ?></span>
            </a></h3>
            <h3 class="griddsitem shadowedhov width-4 height-3 autopalette" colhex="<?= $menu[6]['color'] ?>" ><a href="/download">
                <span class="large-icon-cust"><img src="/img/ui/large-icon-app-1.png"/></span>
                <span class="large-text"><?= $menu[6]['name'] ?></span>
            </a></h3>
        </div>
    </div>
    <div class="section section-thirds">
        <div class="section_content">
            <div class="section-title">TIPS &amp; ARTIKEL</div>
            <div class="thirds thirds-flex home" id="article-container">
                <?php if(count($article) > 0): foreach ($article as $model): ?>
                <div class="athird athirdpictxt" ><a href="/article/detail/<?= $model['pretty_url'] ?>">
                    <div class="athirdpic"><img src="<?= $model['image_thumb'] ?>" alt="<?= $model['title'] ?>" /></div>
                    <span class="athirdtitle"><?= $model['title'] ?></span>
                    <span class="athirdtxt"><?= ($model['short_content']) ? trimstr(strip_tags($model['short_content']), 150, 'WORDS', '...') : trimstr(strip_tags($model['full_content']), 200, 'WORDS', '...'); ?></span>
                </a></div>
                <?php endforeach; endif; ?>
                <div class="lead"></div>
                <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
            </div>
        </div>
    </div>
</div>
