<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Index Controller.
 */
class Index extends Basepublic_Controller  {

    private $_view_folder = "new_index/front/";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }


    public function index() {
        //ambil title, seo meta dari db untuk page home
		$page = get_page_detail('home');

        //get slider data
        $sliders = array();
        $template;
        $page_active;

        $template = "index";

        if ( $this->user_choice == 2 ) {
           
            $page_active = 'index-pro';
            $limit = 2;

            $sliders = $this->_dm->set_model("dtb_slider", "ds", "id")->get_all_data(array(
                "conditions_or" => array("show_in" => SHOW_IN_PRO , "show_in" => SHOW_IN_ALL),
                "conditions" => array("is_show" => SHOW),
                "order_by" => array("ordering" => "asc"),
            ))['datas'];
        } else {
			$this->user_choice = 1;

          
            $page_active = 'index';
            $limit = 3;

            $sliders = $this->_dm->set_model("dtb_slider", "ds", "id")->get_all_data(array(
                "conditions_or" => array("show_in" => SHOW_IN_USER , "show_in" => SHOW_IN_ALL),
                "conditions" => array("is_show" => SHOW),
                "order_by" => array("ordering" => "asc"),
            ))['datas'];

        }     

        $header = array(
            'header'        => $page,
            'page'          => $page_active,
            'sliders'       => $sliders,
        );

        $footer = array(
            "script" => array(
                "/js/front/article.js",
                "/js/plugins/slick/slick.min.js",
                "/js/front/home.js",
            ),
            "css" => array(
                "/js/plugins/slick/slick.css",
                "/js/plugins/slick/slick-theme.css",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER_2, $header);
        $this->load->view($this->_view_folder . $template);
        $this->load->view(FRONT_FOOTER_2, $footer);
    }

	/**
	 * Logout function.
	 */
	public function logout() {
        //unset sessions and back to login.
        $this->session->unset_userdata(USER_SESSION);
		redirect('/');
	}


}
