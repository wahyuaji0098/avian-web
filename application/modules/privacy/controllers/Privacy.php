<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Privacy Controller.
 */
class Privacy extends Basepublic_Controller  {
    private $_view_folder = "privacy/front/";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //get header page
		$page = get_page_detail('privacy');

        $header = array(
            'header'    => $page,
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER);
    }
}
