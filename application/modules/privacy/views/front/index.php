<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>KEBIJAKAN PRIVASI</span>
            </div>
            <h2 class="section-title section-title-top">KEBIJAKAN PRIVASI</h2>
            <div class="ps">
                <div>
                   Ini adalah pernyataan Privasi dalam situs web www.avianbrands.com ("Situs Web"). Situs ini dioperasikan dan diasuh oleh entitas Avian Brands yang disebutkan pada bagian "Pemilik Situs Web" dalam situs web ini (selanjutnya disebut "Avian Brands” atau "kami"). PT. Avia Avian merupakan pengawas pemrosesan semua data pribadi yang dikumpulkan melalui situs web ini.
                </div>
                <div>
                    Kami ingin mempersembahkan pengalaman terbaik Avian Brands kepada Anda. Kami menggunakan informasi yang Anda berikan kepada kami untuk menjadikan layanan kami dan pengalaman Anda lebih baik lagi. Pernyataan Privasi ini bertujuan untuk memberikan gambaran yang jelas perihal bagaimana kami menggunakan data pribadi Anda, dedikasi kami untuk melindungi data Anda, serta opsi yang Anda miliki untuk mengontrol data pribadi dan melindungi privasi Anda.
                </div>
                <div>
                    Pernyataan Privasi ini dapat diubah sewaktu-waktu. Pengguna Situs Web disarankan untuk membaca Pernyataan Privasi secara rutin untuk melihat jika ada perubahan. Pernyataan Privasi ini terakhir diubah pada tanggal 1 Mei 2016.
                </div>
                <div>
                    Pernyataan Privasi ini dapat disimpan atau dicetak dengan menggunakan tombol-tombol di bagian atas halaman ini.
                </div>

                <div>
                    <span>Kapan Pernyataan Privasi ini berlaku ?</span>
                    Pernyataan Privasi ini berlaku untuk pemrosesan data pribadi pengguna melalui Situs Web ini.
                </div>

                <div>
                    <span>Untuk apa kami memproses data pribadi Anda ?</span>
                    Kami menggunakan data pribadi Anda beberapa tujuan. Anda dapat melihat gambaran jenis data pribadi beserta tujuan penggunaannya.
                    <ul>
                        <li>
                            Untuk mengirimkan Situs Web dan Aplikasi kepada Anda dan untuk manajemen teknis dan fungsional Situs Web ketika Anda menggunakan Situs Web atau Aplikasi kami.
                        </li>
                        <div>
                            <p>Kami memproses data teknis seperti alamat IP, browser internet yang Anda gunakan, halaman web yang Anda kunjungi, situs web yang Anda kunjungi sebelumnya/selanjutnya, dan durasi kunjungan dalam rangka memberikan fungsionalitas Situs Web sehingga Anda dapat menemukan informasi dengan cepat dan mudah. Data ini juga digunakan untuk mengatasi permasalahan teknis atau untuk meningkatkan kemudahan akses ke bagian situs web tertentu. Untuk tujuan ini kami juga menggunakan cookie.</p>
                        </div>
                        <li>
                            Untuk mengelola akun yang dimaksud dan untuk menjamin kerahasiaan dan keamanan Anda ketika Anda membuka akun Avian Brands dengan kami
                        </li>
                        <div>
                            <span>Apa yang diperlukan untuk mencapai tujuan ini?</span>
                            <p>Ketika Anda mendaftar pada kami, Anda perlu memberikan data pribadi seperti nama, alamat email yang valid, serta informasi lain yang diminta, sehingga kami dapat membuat akun pribadi untuk Anda. Saat membuat akun, kami akan mengirimkan informasi login pribadi Anda. Dengan data pribadi ini kami dapat mengelola akun Anda dan memastikan kerahasiaan serta menjaga keamanan data Anda. Kami juga memproses data seperti nama tampilan, peran, dan data login Anda. Kami, misalnya, dapat mengubah password untuk Anda. Akan tetapi, kami tidak dapat melihat password Anda.</p>
                        </div>
                        <li>
                            Untuk memproses permintaan anda untuk mengetahui toko-toko Avian Brands yang terdekat dengan Anda dan untuk manajemen hubungan dan layanan pelanggan ketika Anda ingin mencari produk kami melalui Lokasi Toko kami
                        </li>
                        <div>
                            <span>Apa yang diperlukan untuk mencapai tujuan ini?</span>
                            <p>Ketika Anda menggunakan layanan Lokasi Toko di web kami, kami menggunakan data pribadi Anda setidaknya nama dan alamat geografis Anda, data kontak seperti nomor telepon atau alamat email, persetujuan Anda terhadap syarat dan ketentuan kami yang berlaku, dan kesediaan Anda menerima newsletter. Ketika Anda menghubungi kami melalui formulir kontak di Situs Web, email, atau cara lainnya, kami akan menggunakan informasi kontak Anda, korespondensi Anda dengan kami mengenai pertanyaan Anda, dan data pribadi lainnya yang Anda berikan untuk menjawab pertanyaan Anda.</p>
                        </div>
                        <li>
                            Untuk pengembangan dan penyempurnaan produk dan layanan
                        </li>
                        <div>
                            <span>Apa yang diperlukan untuk mencapai tujuan ini?</span>
                            <p>Kami menggunakan koleksi data pribadi untuk menganalisis perilaku pelanggan dan menyesuaikan produk dan layanan kami dengan sedemikian rupa, untuk memastikan relevansinya bagi pelanggan kami. Artinya kami akan menganalisis seberapa sering Anda membaca newsletter kami, seberapa sering Anda mengunjungi Situs Web kami, halaman mana yang Anda klik. Berdasarkan informasi ini, kami dapat menyesuaikan penawaran, buletin, atau Situs Web kami. Kami juga dapat menerapkan promosi berdasarkan analisis kami. Kami juga dapat melakukan riset terhadap tren pasar melalui analisis statistik untuk mengevaluasi dan menyesuaikan produk dan pemasaran kami dengan perkembangan baru, tetapi hasil riset hanya dilaporkan secara kolektif. Kami dapat membeli data tambahan dari sumber-sumber publik untuk melengkapi basis data kami untuk tujuan di atas.</p>
                        </div>
                    </ul>
                </div>

                <div>
                    <span>Pilihan Anda</span>
                    <ul>
                        <li>
                            Untuk mengelola partisipasi Anda jika Anda mengikuti kontes berhadiah, kegiatan, kompetisi, promosi, dan/atau permainan.
                        </li>
                        <div>
                            <span>Apa yang diperlukan untuk mencapai tujuan ini?</span>
                            <p>Atas persetujuan Anda, kami dapat mengirim email berisi promosi dan undangan untuk berpartisipasi dalam kontes berhadiah, kegiatan, kompetisi, atau permainan. Jika Anda memilih untuk berpartisipasi dalam salah satu aktivitas ini, kami memerlukan data pribadi Anda agar dapat mengumumkan dan mengelolanya. Di samping itu, jika Anda berpartisipasi dalam salah satu aktivitas ini, kami memerlukan data pribadi Anda untuk mengumumkan jika Anda pemenangnya, untuk mendistribusikan hadiah, dan untuk mengukur respons terhadap kontes berhadiah, kegiatan, kompetisi, promosi, dan atau permainan. Untuk itu kami akan memproses nama, alamat, alamat email, dan entri Anda dalam aktivitas tersebut.</p>
                        </div>
                        <li>
                            Untuk mengirimkan informasi personal mengenai Avian Brands, tetapi hanya jika Anda telah menyetujuinya saat pendaftaran akun
                        </li>
                        <div>
                            <span>Apa yang diperlukan untuk mencapai tujuan ini?</span>
                            <p>Anda dapat memilih untuk menerima pesan komersial secara personal, termasuk newsletter yang dipersonalisasi, dari Avian Brands yang disesuaikan dengan preferensi pribadi Anda. Untuk itu kami akan membuat profil dan menganalisis interaksi Anda dengan kami. Kami akan mengumpulkan data pribadi Anda seperti:</p>
                            <ul>
                                <li>nama, jenis kelamin, tanggal lahir, alamat email, dan alamat surat-menyurat Anda;</li>
                                <li>preferensi dan minat Anda, seperti yang Anda berikan kepada kami secara aktif, dan juga yang diperoleh melalui interaksi terdaftar Anda melalui situs web dan aplikasi Avian Brands (yang mungkin mengharuskan kami menggunakan cookie); dan</li>
                                <li>tautan yang Anda klik dalam email kami. Dengan cara ini, kami dapat mempelajari minat Anda dan memastikan bahwa kami memberikan informasi dan promosi yang paling sesuai dengan minat Anda tersebut. Kami juga dapat menanyakan pendapat Anda mengenai produk dan layanan kami. Jika Anda tidak bersedia menerima pesan apa pun atau jika Anda memilih untuk tidak menerima atau melihat bentuk komunikasi tertentu dari kami (misalnya SMS, media sosial, email, atau surat biasa) cukup ikuti langkah-langkah dalam komunikasi yang dimaksud.</li>
                            </ul>
                        </div>
                    </ul>
                </div>

                <div>
                    <span>Siapa yang dapat mengakses data Anda?</span>
                    Kami memperlakukan data Anda dengan prinsip kehati-hatian dan kerahasiaan serta tidak membagikan data Anda kepada pihak ketiga selain yang secara tegas dinyatakan dalam Pernyataan Privasi.
                </div>

                <div>
                    <span>*Dengan entitas lain dalam grup Avian Brands</span>
                    Kami dapat membagikan informasi dengan identitas yang disamarkan atau anonim, atau informasi kolektif mengenai pelanggan kami kepada anggota Grup Avian Brands lainnya, misalnya untuk digunakan dalam analisis tren.
                    <ul>
                        <li>
                            Dengan penyedia layanan
                            <div>
                                Kami menggunakan berbagai perusahaan yang memproses data atas nama kami, seperti perusahaan hosting, layanan pengiriman. Perusahaan ini menggunakan informasi Anda atas nama kami dan tunduk terhadap peraturan ketat yang harus mereka patuhi dalam memproses informasi Anda. Kami tidak mengizinkan perusahaan tersebut untuk menggunakan data dengan cara lain yang tidak sesuai dengan petunjuk kami.
                            </div>
                        </li>
                        <li>
                            Pihak ke-3 lainnya
                            <div>
                                Kami mungkin perlu mengungkapkan informasi pribadi atas permintaan badan pemerintah atau penegak hukum. Kami hanya akan membagikan informasi Anda dengan perusahaan lain atau perorangan jika kami mendapatkan persetujuan dari Anda untuk melakukannya.
                            </div>
                        </li>
                    </ul>
                </div>
                <div>
                    <span>Bagaimana memperlakukan tautan ke situs web lain dan media sosial ?</span>
                </div>

                <div>
                    <span>Situs web eksternal</span>
                    Di Situs Web, Anda akan menemukan sejumlah tautan ke situs web lainnya. Meskipun situs web ini dipilih secara hati-hati, akan tetapi kami tidak bertanggung jawab terhadap pemrosesan data pribadi Anda melalui situs web ini. Dengan demikian, Syarat dan Ketentuan kami https://www.avianbrands.com/Kebijakan Privasi tidak berlaku terhadap penggunaan situs web yang demikian itu.
                </div>

                <div>
                    <span>Media sosial</span>
                    <p>Situs Web dan Aplikasi kami mungkin menyediakan plugin sosial dari berbagai jejaring sosial. Jika Anda memilih untuk berinteraksi dengan suatu jejaring sosial (misalnya dengan mendaftarkan akun), aktivitas Anda di Situs Web kami atau melalui Aplikasi juga akan disediakan untuk jejaring sosial seperti Facebook, YouTube, Instagram dan LinkedIn.</p>
                    <p>Jika Anda masuk ke dalam salah satu jejaring sosial ini selama berkunjung ke salah satu Situs Web atau Aplikasi kami, jejaring sosial tersebut mungkin akan menambahkan informasi ini ke profil Anda pada jaringan ini. Jika Anda berinteraksi dengan salah satu plugin sosial, maka informasi ini akan dipindahkan ke jejaring sosial tersebut. Jika Anda tidak menginginkan pemindahan data tersebut, silakan keluar dari jejaring sosial Anda sebelum masuk ke salah satu Situs Web atau Aplikasi kami.</p>
                    <p>Kami juga menawarkan kesempatan untuk membuat akun pada kami melalui akun jejaring sosial Anda (seperti Facebook). Ini akan menghemat waktu Anda dalam membuat akun baru, sebab kredensial masuk akan diimpor dari akun jejaring sosial Anda. Harap diperhatikan bahwa saat Anda menggunakan opsi "social sign-on" (mendaftar dari jejaring sosial) ini, kami mungkin akan menerima informasi tertentu melalui jejaring sosial, seperti nama Anda, usia, lokasi, preferensi, pekerjaan, dan informasi lainnya melalui profil publik Anda, dan mungkin juga mengakses gambar atau daftar teman Anda di jejaring sosial. Informasi ini tidak diminta oleh kami, melainkan disediakan oleh jejaring sosial melalui penggunaan opsi social sign-on. Ketika Anda menggunakan opsi social sign-on, kami hanya akan mengimpor informasi yang diperlukan untuk pembuatan akun Anda pada kami dan setelah informasi lain diterima kami akan membuang informasi yang diterima melalui jejaring sosial. Setelah membuat akun, Anda dapat melengkapi informasi apa pun yang ingin Anda berikan kepada kami.</p>
                    <p>Platform media sosial Anda akan memperlihatkan iklan Avian Brands melalui halaman media sosialnya jika Anda mengizinkannya melalui pengaturan privasi penyedia media sosial Anda</p>
                    <p>Silakan baca kebijakan privasi jejaring sosial tersebut untuk informasi lengkap mengenai pengumpulan dan pemindahan data pribadi, hak-hak apa yang Anda miliki, dan bagaimana Anda dapat membuat pengaturan privasi yang memuaskan.</p>
                </div>
                <div>
                    <span>Dapatkah orang yang berusia di bawah 16</span>
                    Orang yang berusia di bawah 16 tahun hanya dapat memberikan data pribadi kepada Avian Brands jika mereka memperoleh izin tertulis dari salah satu orang tua mereka atau wali yang sah yang telah membaca Pernyataan Privasi.
                </div>

                <div>
                    <span>Bagaimanakah kami mengamankan data?</span>
                    Data pribadi Anda diperlakukan dengan sangat rahasia dan kami telah mengambil langkah-langkah keamanan organisatoris dan teknis yang tepat terhadap kehilangan atau pemrosesan yang tidak sah atas data ini. Untuk tujuan ini kami menggunakan beberapa teknik pengamanan termasuk server yang aman, firewall, dan enkripsi, serta perlindungan fisik di lokasi tempat data disimpan.
                </div>
                <div>
                    <span>Penyimpanan data</span>
                    Kami tidak akan menyimpan data Anda melebihi periode yang diperlukan untuk mencapai tujuan pengumpulan data tersebut, atau sejauh yang diizinkan oleh hukum yang berlaku.
                </div>
                <div>
                    <span>Kepada siapa pertanyaan dan permintaan akses, penghapusan, dll. harus diajukan</span>
                    Anda dapat meminta akses data pribadi yang kami proses mengenai Anda kapan pun, dan meminta data tersebut dikoreksi atau dihapus dengan mengirim permohonan kepada:
                </div>

                <div>
                    <span>PT. Avia Avian</span>
                    <br>
                    Jl. Raya Surabaya, Sidoarjo KM 19, Desa Wadungasih,
                    <br>
                    Buduran, Sidoarjo, Jawa Timur.
                    <br>
                    PO BOX 126 Sidoarjo 61200
                    <br>
                    Telp: <a href="tel:0318968000">031-8968000</a> atau <a href="tel:0318969000">031-8969000</a>
                    <br>
                    Fax: <a href="tel:0318964118">031-8964118</a> atau <a href="tel:031892175">031-892175</a>
                    <br>
                    Email: <a href="mailto:marketing@avianbrands.com">marketing@avianbrands.com</a>
                </div>
            </div>
        </div>
    </div>
</div>
