<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends Basepublic_Controller {

    private $_view_folder = "forgot_password/front/";

    public function __construct() {
        parent::__construct();

        if($this->session->has_userdata(USER_SESSION)) {
            //redirect to dashboard
            redirect('/');
        }
    }

    /**
	 * Register controller and for register form processing.
	 */
	public function index() {
        redirect(base_url());
        // $page = get_page_detail('forgot-password');

        // $header = array(
        //     'header'        => $page,
        // );

        // $footer = array(
        //     "script" => array(
        //         "/js/plugins/jquery.validate.min.js",
        //         "/js/plugins/jquery.form.min.js",
        //         '/js/front/member.js',
        //     )
        // );

        // //load the views
        // $this->load->view(FRONT_HEADER ,$header);
        // $this->load->view($this->_view_folder . 'index');
        // $this->load->view(FRONT_FOOTER ,$footer);
	}

    private function setRuleValidation () {
        $this->form_validation->set_rules("email", "Email", "required|valid_email|callback_check_email");
    }

    public function check_email ($email) {

        $this->form_validation->set_message('check_email', 'Email tidak terdaftar.');

        if (!$this->_secure) {
            return false;
        }

		//check page title if exist
        $d_email = $this->Member_model->get_all_data(array(
            "conditions" => array("email" => $email),
            "status" => STATUS_ACTIVE,
            "row_array" => true
        ))['datas'];

        if (!$d_email) {
            $this->form_validation->set_message('check_email', 'Email tidak terdaftar.');
			return FALSE;
        }

        return TRUE;
	}

    public function send() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        //load model
        $this->load->library('form_validation');
        $this->load->model("member/Member_model");

        $this->_secure = true;

		$message['is_error'] = true;
		$message['error_count'] = 1;
        $message['is_redirect'] = false;
		$data = array();

		$this->setRuleValidation();

		if ($this->form_validation->run($this) == FALSE) {
            $data = validation_errors();
            $count = count($this->form_validation->error_array());
            $message['error_count'] = $count;
        } else {
			$email 		= sanitize_str_input($this->input->post('email'));

            $model_member = new Member_model ();

            $data_member = $model_member->get_all_data (array(
                "conditions" => array("email" => $email),
                "row_array" => true
            ))['datas'];

            if ($data_member) {
                $message['is_error'] = false;
                $message['is_redirect'] = true;
                $message['error_count'] = 0;

                $model_member->sendResetPassword($data_member,$data_member['id']);
            } else {
                $data = "database operation failed.";
            }
        }

        $message['data'] = $data;
        $this->output->set_content_type('application/json');
        echo json_encode($message);

        exit;
	}

    public function resets($code) {

        $page = get_page_detail('reset-password');

        //check code
		if (empty($code)) {
			show_404("page");
		}

        //load model
        $this->load->model("member/Member_model");

        $code_decoded = base64_decode(urldecode($code));

        $check_code = $this->Member_model->checkCode ($code_decoded);

        if (!$check_code) {
			show_404("page");
		}

        $header = array(
            'header'        => $page,
            'code'          => $code,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/jquery.validate.min.js",
                "/js/plugins/jquery.form.min.js",
                '/js/front/member.js',
            )
        );

        //load the views
        $this->load->view(FRONT_HEADER ,$header);
        $this->load->view($this->_view_folder . 'resets');
        $this->load->view(FRONT_FOOTER ,$footer);
	}

    public function reset_pass() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$message["is_error"] = true;
		$code = $this->input->post('code');
		$code_decoded = base64_decode(urldecode($code));

        //load model
        $this->load->model("member/Member_model");

		$check_code = $this->Member_model->checkCode ($code_decoded);

		if (!$check_code) {
			$message["message"] = "Entry Not Existing";
		} else {
			$message["is_error"] = false;
			$message["message"] = "Send successfully";
			//change password
			$this->Member_model->resetedPassword($check_code);
		}

        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;

	}

}
