<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>RESET PASSWORD</span>
            </div>
            <h2 class="section-title section-title-top">RESET PASSWORD</h2>
            <form id="frpass-fm" action="/forgot-password/reset_pass" method="POST">
            <div class="bigform">
                <?php echo "<input type='hidden' value='".$code."' name='code' id='code' />"; ?>
                <div class="bigformsmallnote">Klik RESET PASSWORD untuk meng-reset password Anda.</div>
                <div class="bigform-error" id="smessage"></div>
                <button type="submit" class="btn bigform-btn">RESET PASSWORD</button>
                <div class="bigformnote">KEMBALI KE <a href="/">BERANDA</a></div>
            </div>
            </form>
            <div class="bigform contact-done">
                <div class="bigformsmallnote">Silakan periksa email Anda untuk melihat password yang baru. <br/> Thank you.</div>
                <div class="bigformnote">KEMBALI KE <a href="/">BERANDA</a></div>
            </div>
        </div>
    </div>
</div>
