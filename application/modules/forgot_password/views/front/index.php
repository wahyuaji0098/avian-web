<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>LUPA PASSWORD</span>
            </div>
            <h2 class="section-title section-title-top">LUPA PASSWORD</h2>
            <form id="fpass-fm" action="/forgot-password/send" method="POST">
            <div class="bigform">
                <div class="bigformsmallnote">Jika Anda lupa password Anda, Anda dapat mencoba untuk mereset password Anda dengan memasukkan email Anda.</div>
                <div class="bigform-input">
                    <input type="text" placeholder="Email" id="email" name="email"/>
                </div>
                <div class="bigform-error" id="smessage"></div>
                <button type="submit" class="btn bigform-btn">LUPA PASSWORD</button>
                <div class="bigformnote">KEMBALI KE <a href="/">BERANDA</a></div>
            </div>
            </form>
            <div class="bigform contact-done">
                <div class="bigformsmallnote">Silakan periksa email Anda dan ikuti langkah-langkah untuk mereset password Anda. <br/> Terima Kasih. </div>
                <div class="bigformnote">KEMBALI KE <a href="/">BERANDA</a></div>
            </div>
        </div>
    </div>
</div>
