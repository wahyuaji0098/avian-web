<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>HASIL PENCARIAN</span>
            </div>
            <div class="search_results">
                <div class="search-main search-results-searchbox">
                    <form method="POST" action="/search">
                        <input type="text" placeholder="Cari Produk, Tips & Artikel, Warna..."  name="search" id="search">
                        <input type="submit">
                    </form>
                </div>
                <?php if(count($models_article) == 0 && count($models_product) == 0 && count($models_color) == 0): ?>
                    <p class="search-nothing">Maaf, Pencarian "<span><?= $search_word ?></span>" tidak di temukan.</p>
                <?php else: ?>
                <?php if(count($models_article) > 0): ?>
                <div class="search_result_contain">
                    <div class="search_result_catname srslt_cat_arti">TIPS & ARTIKEL</div>
                    <div class="thirds">
                        <?php foreach($models_article as $article): ?>
                        <div class="athird athirdpictxt" ><a href="/article/detail/<?= $article['pretty_url'] ?>">
                            <div class="athirdpic"><img src="<?= $article['image_thumb'] ?>" alt="<?= $article['title'] ?>" /></div>
                            <span class="athirdtitle"><?= $article['title'] ?></span>
                            <span class="athirdtxt"><?= ($article['short_content']) ? trimstr(strip_tags($article['short_content']), 150, 'WORDS', '...') : trimstr(strip_tags($article['full_content']), 200, 'WORDS', '...'); ?></span>
                        </a></div>
                        <?php endforeach;?>
                    </div>
                </div>
                <div class="lead"></div>
                <?php endif; ?>
                <?php if(count($models_product) > 0): ?>
                <div class="search_result_contain">
                    <div class="search_result_catname srslt_cat_prod">PRODUK</div>
                    <div class="thumblist search_thumblist">
                        <?php foreach($models_product as $product): ?>
                        <a href="/product/detail/<?= $product['pretty_url'] ?>" class="thumbitem">
                            <div class="thumbpic"><img src="<?= $product['image_url'] ?>" alt="<?= $product['name'] ?>"/></div>
                            <div class="thumbname"><?= $product['name'] ?></div>
                        </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="lead"></div>
                <?php endif; ?>
                <?php if(count($models_color) > 0): ?>
                <div class="search_result_contain">
                    <div class="search_result_catname srslt_cat_col">WARNA</div>
                        <div class="thumblist search_thumblist">
                            <?php foreach($models_color as $color): ?>
                            <div class="thumbitem trigger coltrigger" targid="colpop" colcode="<?= $color['code'] ?>">
                                <div class="colbox autopalette" colhex="<?= $color['hex_code'] ?>"></div>
                                <div class="thumbcode"><?= $color['code'] ?></div>
                                <div class="thumbname colourboxname"><?= $color['name'] ?></div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                </div>
                <div class="lead"></div>
                <?php endif; ?>
                <!-- <div class="superbig-btn">LOAD MORE</div> -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
