<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Basepublic_Controller {

    private $_view_folder = "search/front/";

    function __construct() {
        parent::__construct();

    }

    public function index() {
        //get header page
		$page = get_page_detail('search');
        redirect(base_url());
		//get search post data
		$search = $this->input->post('search');

		$this->session->set_userdata('fr_search', $search);

		if ($search == "") {
			$d_search_article = array();
			$d_search_product = array();
			$d_search_color = array();
			$total_page_article = 1;
			$total_page_product = 1;
			$total_page_color = 1;
		} else {
			//get article
            $limit = 3;
			$data_article = $this->_dm->set_model("dtb_article", "da", "id")->get_all_data(array(
                "conditions" => array(
                    "is_show" => SHOW,
                ),
                "order_by" => array("date" => "asc"),
                "limit" => $limit,
                "filter_or" => array(
                    "title" => $search,
                    "short_content" => $search
                ),
                "count_all_first" => true,
            ));
            $d_search_article = $data_article['datas'];
			$total_article = $data_article['total'];
			$total_page_article = ceil($total_article/$limit);

			//get product
			$limit = 14;

            $data_product = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
                "conditions" => array(
                    "is_show" => SHOW,
                ),
                "order_by" => array("name" => "asc"),
                "limit" => $limit,
                "filter_or" => array(
                    "name" => $search,
                    "description" => $search
                ),
                "count_all_first" => true,
            ));

            $d_search_product   = $data_product['datas'];
            $total_product      = $data_product['total'];
            $total_page_product = ceil($total_product/$limit);

			//get color
			$limit = 14;

            $data_color = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data(array(
                "conditions" => array(
                    "is_show" => SHOW,
                    "id not in (select color_id from mst_palette_color mp, dtb_pallete dp where dp.id = mp.palette_id and dp.status = '".STATUS_DELETE."')"
                ),
                "order_by" => array("name" => "asc"),
                "limit" => $limit,
                "filter_or" => array(
                    "name" => $search,
                    "code" => $search
                ),
                "count_all_first" => true,
            ));

			$d_search_color = $data_color['datas'];
			$total_color = $data_color['total'];
			$total_page_color = ceil($total_color/$limit);
		}

        $header = array(
            'page'      => 'search',
			'header'    => $page,
			'search_word'    => $search,
			'models_article'    => $d_search_article,
			'models_product'    => $d_search_product,
			'models_color'    => $d_search_color,
			'total_page_article'    => $total_page_article,
			'total_page_product'    => $total_page_product,
			'total_page_color'    => $total_page_color,
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER);


	}
}
