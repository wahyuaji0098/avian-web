<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>HUBUNGI KAMI</span>
            </div>
            <h2 class="section-title section-title-top">HUBUNGI KAMI</h2>
            <form id="contact-fm" action="/contact-us/send" method="POST">
            <div class="bigform">
                <div class="bigformsmallnote">Jika Anda memiliki pertanyaan atau saran jangan ragu untuk menghubungi kami</div>
                <div class="bigform-input">
                    <input type="text" placeholder="Nama" name="name" />
                </div>
                <div class="bigform-input">
                    <input type="text" placeholder="Email" name="email"  />
                </div>
                <div class="bigform-input">
                    <textarea name="message" placeholder="Pesan"></textarea>
                </div>
                <button type="submit" class="btn bigform-btn">KIRIM</button>
            </div>
            </form>
            <div class="bigform contact-done">
                <div class="bigformsmallnote">Terima kasih untuk pesan Anda. Kami akan membalas email Anda segera.</div>
                <div class="bigformnote">KEMBALI KE <a href="/">BERANDA</a></div>
            </div>
        </div>
    </div>
</div>
