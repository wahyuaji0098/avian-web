<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact Controller.
 */
class Contact_us extends Basepublic_Controller  {

    private $_view_folder = "contact_us/front/";

    private $_table = "dtb_contact_us";
    private $_table_aliases = "dcu";
    private $_pk = "id";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect( base_url('contact_us') );
        //get header page
        // $page = get_page_detail('contact');

        // $header = array(
        //     'header'            => $page,
        // );

        // $footer = array(
        //     "script" => array(
        //         "/js/plugins/jquery.validate.min.js",
        //         "/js/plugins/jquery.form.min.js",
        //         "/js/front/contact.js"
        //     )
        // );

        // //load the views.
        // $this->load->view(FRONT_HEADER, $header);
        // $this->load->view($this->_view_folder . 'index');
        // $this->load->view(FRONT_FOOTER, $footer);
    }


    /////////////////////////////RULES//////////////////////////

    public function setRuleValidation () {

        $this->form_validation->set_rules("name", "Name", "required");
        $this->form_validation->set_rules("email", "Email", "required");
        $this->form_validation->set_rules("message", "Message", "required");
    }

    /////////////////////////////AJAX//////////////////////////

    public function send () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        //load form validation lib.
        $this->load->library('form_validation');

		$message = array();
		$message['is_error'] = true;
        $message['is_redirect'] = false;
		$message['error_count'] = 1;
		$data = array();

		$this->setRuleValidation();

		if ($this->form_validation->run($this) == FALSE) {
            $data = validation_errors();
            $count = count($this->form_validation->error_array());
            $message['error_count'] = $count;
        } else {
			$name 	= $this->input->post('name');
			$email 	= $this->input->post('email');
			$content= $this->input->post('message');

			$datas = array (
				"name" 			=> $name,
				"email_address" => $email,
				"message" 		=> $content,
			);

			$model_contact = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

            $this->db->trans_begin();

			$insert = $model_contact->insert($datas);

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data = "database operation failed.";

            } else {
                $this->db->trans_commit();

                $message['is_error'] = false;
                $message['is_redirect'] = true;
                $message['error_count'] = 0;

                //get template email contact
                $content_email = $this->load->view('layout/email/front/template_contact', '', true);

                //notif the team about the contact.
                sendmail (array(
    				'subject'	=> SUBJECT_CONTACT_US_ADMIN,
    				'message'	=>  'Date : '. date('Y-m-d H:i:s') .'<br /><br />'.
                                    'Name : '. $name . '<br /><br />'.
                                    'Email Address : '. $email . '<br /><br />'.
                                    'Message : '. $content,
    				'to'		=> array("marketing@avianbrands.com", OUR_EMAIL_ADDRESS),
    			), "html");

                $content_email = str_replace('%NAME%',$name,$content_email);
                $content_email = str_replace('%LOGO%',base_url().'img/ui/logo-head.png',$content_email);

                //send to user
                sendmail (array(
    				'subject'	=> SUBJECT_CONTACT_US_USER,
    				'message'	=> $content_email,
    				'to'		=> array($email),
    			), "html");

            }
        }

        $message['data'] = $data;
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
	}

}
