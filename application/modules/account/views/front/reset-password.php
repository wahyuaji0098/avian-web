<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/account/likebox">AKUN</a>
                <span>UBAH PASSWORD</span>
            </div>
            <div class="content_sbar">
                <div class="sidebar">
                    <div class="sidebar-title"><?= $login_user['name'] ?></div>
                    <div class="sidebar_links">
                        <span class="sidebar_link active">UBAH PASSWORD</span>
                        <a href="/logout" class="sidebar_link">KELUAR</a>
                    </div>
                    <div class="lead"></div>
                </div>
                <div class="sbarcontent">
                    <form id="rpass-fm" action="/account/reset-password-send" method="POST">
                    <div class="sbarcontentsection">
                        <div class="sbarcontent_title">UBAH PASSWORD</div>
                        <div class="bigform bigform_left">
                            <div class="bigform-input">
                                <input type="password" placeholder="Password Lama" id="password" name="password" autocomplete="off"/>
                            </div>
                            <div class="bigform-input">
                                <input type="password" placeholder="Password Baru" id="new_password" name="new_password" autocomplete="off"/>
                            </div>
                            <div class="bigform-input">
                                <input type="password" placeholder="Konfirm Password Baru" id="confirm_password" name="confirm_password" autocomplete="off"/>
                            </div>
                            <div class="bigform-error" id="smessage"></div>
                            <button type="submit" class="btn bigform-btn">SIMPAN</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
