<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/account/likebox">AKUN</a>
                <span>KOTAK FAVORIT</span>
            </div>
            <div class="content_sbar">
                <div class="sidebar">
                    <div class="sidebar-title"><?= $login_user['name'] ?></div>
                    <div class="sidebar_links">
                        <a href="/account/reset-password" class="sidebar_link">UBAH PASSWORD</a>
                        <a href="/logout" class="sidebar_link">KELUAR</a>
                    </div>
                    <div class="lead"></div>
                </div>
                <div class="sbarcontent">
                    <div class="sbarcontentsection">
                        <div class="sbarcontent_title">KOTAK FAVORIT SAYA</div>
                        <div class="tabbed_content">
                            <div class="tabs">
                                <div class="tab active tabgroup targrouptaba targettaba1" targid="taba1" targroup="taba">PRODUK</div>
                                <div class="tab tabgroup targrouptaba targettaba2" targid="taba2" targroup="taba">TOKO</div>
                                <div class="tab tabgroup targrouptaba targettaba3" targid="taba3" targroup="taba">WARNA</div>
                                <div class="tab tabgroup targrouptaba targettaba4" targid="taba4" targroup="taba">PERHITUNGAN</div>
                                <div class="tab tabgroup targrouptaba targettaba5" targid="taba5" targroup="taba">VISUALISASI</div>
                            </div>
                            <div class="tabcontents">
                                <div class="tabcontent scrollbarcust avian-scroll avian-scroll-yonly targrouptaba targettaba1 active">
                                    <div class="thumblist">
                                        <?php if (count($likebox['product']) > 0): foreach ($likebox['product'] as $product) : ?>
                                        <a href="/product/detail/<?= $product['pretty_url'] ?>" class="thumbitem">
                                            <div class="thumbpic"><img src="<?= $product['image'] ?>"/></div>
                                            <div class="thumbname"><?= $product['name'] ?></div>
                                        </a>
                                        <?php endforeach; endif; ?>
                                        <div class="lead"></div>
                                    </div>
                                </div>
                                <div class="tabcontent scrollbarcust avian-scroll avian-scroll-yonly targrouptaba targettaba2">
                                    <div class="thumblist">
                                        <?php if (count($likebox['store']) > 0): foreach ($likebox['store'] as $store) : ?>
                                        <a href="/store/detail/<?= $store['pretty_url'] ?>" class="thumbitem">
                                            <div class="thumbpic"><img src="<?= ($store['image']) ? $store['image']  : "/rsc/img/ui/store-img-placeholder.png" ?>"/></div>
                                            <div class="thumbname"><?= $store['name'] ?></div>
                                        </a>
                                        <?php endforeach; endif; ?>
                                        <div class="lead"></div>
                                    </div>
                                </div>
                                <div class="tabcontent scrollbarcust avian-scroll avian-scroll-yonly targrouptaba targettaba3">
                                    <div class="thumblist">
                                        <?php if (count($likebox['color']) > 0): foreach ($likebox['color'] as $color) : ?>
                                        <div class="thumbitem trigger coltrigger" targid="colpop" colcode="<?= $color['code'] ?>" coltype="<?= $color['type'] ?>" colid=<?= ($color['type'] == 6) ? $color['custom_color_id'] : $color['color_id'] ?>>
                                            <div class="colbox autopalette" colhex="<?= $color['hex_code'] ?>"></div>
                                            <div class="colourboxcode"><?= $color['code'] ?></div>
                                            <div class="thumbname colourboxname"><?= $color['name'] ?></div>
                                        </div>
                                        <?php endforeach; endif; ?>
                                        <div class="lead"></div>
                                    </div>
                                </div>
                                <div class="tabcontent scrollbarcust avian-scroll avian-scroll-yonly targrouptaba targettaba4">
                                    <div class="itemlist">
                                        <?php if (count($likebox['paint']) > 0): foreach ($likebox['paint'] as $paint) : ?>
                                        <div class="item trigger paintcalc" targid="calcpop" data-id="<?= $paint['paint_calc_result_id'] ?>" data-t="<?= $paint['top'] ?>" data-b="<?= $paint['bottom'] ?>">Perhitungan <?= date('Y/m/d H:i',strtotime($paint['date'])) ?></div>
                                        <?php endforeach; endif; ?>
                                        <div class="lead"></div>
                                    </div>
                                </div>
                                <div class="tabcontent scrollbarcust avian-scroll avian-scroll-yonly targrouptaba targettaba5">
                                    <div class="themeboxlist likeboxviz">
                                        <?php if (count($likebox['visual']) > 0): foreach ($likebox['visual'] as $visual) : ?>
                                        <a href="/visualize/detail/<?= url_title($visual['name']."-".$visual['visualizer_result_id']) ?>" class="themeboxitem">
                                            <div class="themebox">
                                                <div class="themepic"><img src="<?= ($visual['result_image_url']) ? $visual['result_image_url'] : $visual['cover_image'] ?>"></div>
                                                <div class="themecollist">
                                                    <?php if (count($visual['colors']) > 0): foreach($visual['colors'] as $col): ?>
                                                    <div class="themecol autopalette" colhex="<?= $col ?>"></div>
                                                    <?php endforeach; endif; ?>
                                                </div>
                                                <div class="themename"><?= $visual['name'] ?></div>
                                                <div class="themeauth">Oleh: <?= $visual['member_name'] ?></div>
                                            </div>
                                        </a>
                                        <?php endforeach; endif; ?>
                                        <div class="lead"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="lead"></div>
                        </div>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
<div class="modal-contain targetcolpop">
    <div class="modal-shade modal-shade-white trigger" targid="colpop"></div>
    <div class="modal-window modal-col colhextarg">
        <div class="modal-content">
            <div class="modal-close trigger" targid="colpop"></div>
            <div class="modal-colinfo">
                <div class="modal-colname colnametarg">
                    Nama Warna
                </div>
                <div class="modal-colcode colcodetarg">
                    0000000
                </div>
                <div class="btn-colsave active removecolor" data-id="" data-type=""><div></div></div>
            </div>
        </div>
    </div>
</div>
<div class="modal-contain targetcalcpop">
    <div class="modal-shade"></div>
    <div class="modal-window modal-free">
        <div class="modal-content">
            <div class="modal-close trigger" targid="calcpop"></div>
            <div class="modal-title">
                HASIL PERHITUNGAN
            </div>
            <div class="modal-text-framed">
                <p id="ex_top">
                </p>
                <p id="ex_bot">
                </p>
            </div>
            <div class="btn-colsave active removelikebox" data-id=""><div></div></div>
        </div>
    </div>
</div>
