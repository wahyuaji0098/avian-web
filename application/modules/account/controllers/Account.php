<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Basepublic_Controller {

    private $_view_folder = "account/front/";

    function __construct() {
        parent::__construct();

    }

    // public function likebox() {
    //     //load model
    //     $this->load->model("likebox/Likebox_model");
    //
    //     //get header page
	// 	$page = get_page_detail('likebox');
    //
	// 	//get data likebox
	// 	$member_id = $this->data_login_user['id'];
	// 	$likebox = $this->Likebox_model->get_likebox_and_detail($member_id);
    //
    //
    //     $header = array(
    //         'header'        => $page,
    //         'likebox'      => $likebox,
    //     );
    //
    //     $footer = array(
    //         "script" => array(
    //             "/js/front/likebox.js",
    //         ),
    //     );
    //
    //     //load the views.
    //     $this->load->view(FRONT_HEADER, $header);
    //     $this->load->view($this->_view_folder . 'likebox');
    //     $this->load->view(FRONT_FOOTER, $footer);
    //
    //
	// }

	public function reset_password() {
		//get header page
		$page = get_page_detail('change-password');

        $header = array(
            'header'        => $page,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/jquery.validate.min.js",
                "/js/plugins/jquery.form.min.js",
                '/js/front/member.js',
            ),
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'reset-password');
        $this->load->view(FRONT_FOOTER, $footer);

	}

	public function setRuleValidation () {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("password", "password", "required|min_length[6]|max_length[12]|callback_password_check");
        $this->form_validation->set_rules("new_password", "new password", "required|min_length[6]|max_length[12]|matches[confirm_password]");
        $this->form_validation->set_rules("confirm_password", "confirm password", "required|min_length[6]|max_length[12]");
    }

    public function password_check ($old_pass) {
        $pass = $this->data_login_user['password'];

        if (sha1(PASSWORD_FRONT . $this->db->escape_str($old_pass)) == $pass) {
            return TRUE;
        } else {
            $this->form_validation->set_message('password_check', 'Password lama tidak sesuai.');
			return FALSE;
        }
    }

	public function reset_password_send(){

		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $this->load->model("member/Member_model");

		$message['is_error'] = true;
		$message['error_count'] = 0;
		$data = array();

        $this->setRuleValidation();

        if ($this->form_validation->run($this) == FALSE) {
            $message['is_redirect'] = false;
            $data = validation_errors();
            $count = count($this->form_validation->error_array());
            $message['error_count'] = $count;
        } else {
            $this->db->trans_begin();

            $id = $this->data_login_user['id'];

            $password = $this->input->post('confirm_password');

            $data_insert_array = array(
                'password' => $password,
            );

            if(isset($id) && !empty($id)) {
                $condition = array("id" => $id);
                $insert = $this->Member_model->update($data_insert_array,$condition);

				//re-set login user
				$member = $this->Member_model->get_all_data (array(
                    "find_by_pk" => array($id),
                    "row_array" => true,
                ))['datas'];

				$this->session->set_userdata('login_user', $member);

                $data = "Password berhasil di ubah.";
                $message['is_redirect'] = true;
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['is_error'] = true;
                $message['is_redirect'] = false;
                $message['error_count'] = 1;
                $data = "Something Went Wrong..";
            } else{
                $this->db->trans_commit();
                $message['is_error'] = false;
                $message['is_redirect'] = true;

            }
        }

        $message['data'] = $data;
        echo json_encode($message);

        exit;
    }
}
