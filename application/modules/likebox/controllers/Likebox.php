<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Likebox extends Basepublic_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("Likebox_model");

    }

	public function send () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$from_arr = array(
			'product' => array("type" => 1, "field" => "product_id"),
			'color' => array("type" => 2, "field" => "color_id"),
			'store' => array("type" => 3, "field" => "store_id"),
			'paint_calc' => array("type" => 4, "field" => "paint_calc_result_id"),
			'visualizer' => array("type" => 5, "field" => "visualizer_result_id"),
			'custom_color' => array("type" => 6, "field" => "custom_color_id"),
		);

		$message = array();
		$message['is_error'] = false;
		$message['error_count'] = 0;
		$data = array();

		//check if user already login
		if (!isset($this->data_login_user['id'])) {
			$message['is_error'] = true;
			$message['error_count'] = 1;
			$data = "Silahkan masuk terlebih dahulu untuk menambahkan ke Kotak Favorit Anda.";
		} else {
			$from 	= $this->input->post('_from');
			$id 	= $this->input->post('_id');

			$type = $from_arr[$from]['type'];
			$field = $from_arr[$from]['field'];

            $this->db->trans_begin();

			$data_insert = array(
				"type" => $type,
				"member_id" => $this->data_login_user['id'],
				$field => $id,
			);

			//insert to likebox
			$insert = $this->Likebox_model->insert($data_insert, array("is_version" => true));

			if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['is_error'] = true;
				$message['error_count'] = 1;
				$data = "Something Went Wrong..";
			} else {
                $this->db->trans_commit();
				$message['is_error'] = false;
			}
		}


		$message['data'] = $data;
        echo json_encode($message);

        exit;
	}

	public function remove () {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

		$from_arr = array(
			'product' => array("type" => 1, "field" => "product_id"),
			'color' => array("type" => 2, "field" => "color_id"),
			'store' => array("type" => 3, "field" => "store_id"),
			'paint_calc' => array("type" => 4, "field" => "paint_calc_result_id"),
			'visualizer' => array("type" => 5, "field" => "visualizer_result_id"),
			'custom_color' => array("type" => 6, "field" => "custom_color_id"),
		);

		$message = array();
		$message['is_error'] = false;
		$message['error_count'] = 0;
		$data = array();

		//check if user already login
		if (!isset($this->data_login_user['id'])) {
			$message['is_error'] = true;
			$message['error_count'] = 1;
			$data = "Silahkan masuk terlebih dahulu untuk mengurangi Kotak Favorit Anda.";
		} else {
			$from 	= $this->input->post('_from');
			$id 	= $this->input->post('_id');

			$type = $from_arr[$from]['type'];
			$field = $from_arr[$from]['field'];

			$condition = array(
				"type" => $type,
				"member_id" => $this->data_login_user['id'],
				$field => $id,
			);

            $this->db->trans_begin();

			//insert to likebox
			$delete = $this->Likebox_model->delete($condition, array("is_version" => true));

            //if from custom_color , delete custom_color
            if ($from == "custom_color") {
                $this->_dm->set_model("dtb_custom_color", "dcc", "id")->delete(array("id" => $id), array("is_version" => true));
            }

			//if from paint_calc , delete custom_color
            if ($from == "paint_calc") {
                $this->_dm->set_model("dtb_paint_calc", "dcc", "id")->delete(array("id" => $id), array("is_version" => true));
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
				$message['is_error'] = true;
				$message['error_count'] = 1;
				$data = "Something Went Wrong..";
			} else {
                $this->db->trans_commit();
                $message['is_error'] = false;
			}
		}


		$message['data'] = $data;
        echo json_encode($message);

        exit;
	}
}
