<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Likebox_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = 'dtb_likebox';
        $this->_table_alias = 'dl';
        $this->_pk_field = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    /**
	 * get likebox and detail
	 */
	public function get_likebox_and_detail($member_id) {
		$type_arr = array(
			'1' => array("field" => "product_id"),
			'2' => array("field" => "color_id"),
			'3' => array("field" => "store_id"),
			'4' => array("field" => "paint_calc_result_id"),
			'5' => array("field" => "visualizer_result_id"),
			'6' => array("field" => "custom_color_id"),
		);

		$this->db->where("member_id",$member_id);
		$this->db->where("status",STATUS_ACTIVE);
		$result = $this->db->get($this->_table)->result_array();

		$product = array();
		$color = array();
		$store = array();
		$paint = array();
		$vizual = array();

        $this->load->model("visualize/Result_model");
		$m_vis = new Result_model;

		if (count($result) > 0) {
			foreach ($result as $model) {
				if ($model['type'] == 1) {
					if ($model[$type_arr[1]['field']]) {
						$data = $this->Dynamic_model->set_model("dtb_product", "dp", "id")->get_all_data(array(
                            "find_by_pk" => array($model[$type_arr[1]['field']]),
                            "row_array" => true,
                        ))['datas'];

						if ($data) {
							$model['name'] = $data['name'];
							$model['image'] = $data['image_url'];
							$model['pretty_url'] = $data['pretty_url'];

							array_push ($product,$model);
						}
					}
				} else if ($model['type'] == 2) {
					if ($model[$type_arr[2]['field']]) {
                        $data = $this->Dynamic_model->set_model("dtb_color", "dc", "id")->get_all_data(array(
                            "find_by_pk" => array($model[$type_arr[2]['field']]),
                            "row_array" => true,
                        ))['datas'];

						if ($data) {
							$model['name'] = $data['name'];
							$model['code'] = $data['code'];
							$model['hex_code'] = $data['hex_code'];
							array_push ($color,$model);
						}
					}
				} else if ($model['type'] == 3) {
					if ($model[$type_arr[3]['field']]) {
                        $data = $this->Dynamic_model->set_model("dtb_store", "ds", "id")->get_all_data(array(
                            "find_by_pk" => array($model[$type_arr[3]['field']]),
                            "row_array" => true,
                        ))['datas'];

						if ($data) {
							$model['name'] = $data['name'];
							$model['image'] = $data['store_photo'];
							$model['pretty_url'] = $data['pretty_url'];
							array_push ($store,$model);
						}
					}
				} else if ($model['type'] == 4) {
					if ($model[$type_arr[4]['field']]) {
                        $data = $this->Dynamic_model->set_model("dtb_paint_calc", "dpc", "id")->get_all_data(array(
                            "find_by_pk" => array($model[$type_arr[4]['field']]),
                            "row_array" => true,
                        ))['datas'];

						if ($data) {
							$model['top'] = nl2br($data['top']);
							$model['bottom'] = nl2br($data['bottom']);
							$model['date'] = $data['created_date'];
							array_push ($paint,$model);
						}
					}
				} else if ($model['type'] == 5) {
					if ($model[$type_arr[5]['field']]) {
						$data = $m_vis->findByPkForLikeBox($model[$type_arr[5]['field']]);
						if ($data) {
							$model['colors'] = $data['colors'];
							$model['cover_image'] = $data['cover_image'];
							$model['member_name'] = $data['member_name'];
							$model['result_image_url'] = $data['result_image_url'];
							$model['name'] = $data['name'];

							array_push ($vizual,$model);
						}
					}
				} else if ($model['type'] == 6) {
					if ($model[$type_arr[6]['field']]) {
                        $data = $this->Dynamic_model->set_model("dtb_custom_color", "dcc", "id")->get_all_data(array(
                            "find_by_pk" => array($model[$type_arr[6]['field']]),
                            "row_array" => true,
                        ))['datas'];

						if ($data) {
							$model['name'] = $data['name'];
							$model['code'] = "";
							$model['hex_code'] = $data['hex_code'];
							array_push ($color,$model);
						}
					}
				}
			}
		}

		return array(
			"product" => $product,
			"color" => $color,
			"store" => $store,
			"paint" => $paint,
			"visual" => $vizual,
		);
	}

    /*==============================================================================
     * API FUNCTION
     *==============================================================================*/
    function getLikeBoxByID ($id){
        $col = array('id as id_web','type', 'COALESCE(product_id,color_id,store_id,paint_calc_result_id,visualizer_result_id,custom_color_id) as id','version','status');

        $this->db->select($col);
        $this->db->from($this->_table);

        $this->db->where($this->_pk_field,$id);
        $this->db->limit(1);
        $query = $this->db->get()->row_array();

        return $result;
    }

}
