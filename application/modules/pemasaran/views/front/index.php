<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>PEMASARAN</span>
            </div>
            <div class="gmaparea office_area">
                <div class="gmap"><!-- Google Map -->
                    <div class="gmap_object office_map" id="gmap_object"></div>
                </div>
            </div>
            <div class="officesearchcontain">
                <div class="pinbutton"></div>
                <div class="option option-select option-select-white">
                    <div class="select-cont">
                        <select id="search-box" data-search="<?= $ss_search ?>">
                            <option value="">Pilih Lokasi</option>
                            <?php foreach ($bname as $name): ?>
                            <option value="<?= strtolower($name['province']) ?>" <?= ($ss_search == strtolower($name['province'])) ? "selected='selected'" : "" ?>><?= $name['province'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="storelist">
                <?php if($ss_search != ""): if(count($models) > 0): foreach($models as $office): ?>
                <div class="store" itemscope itemtype="http://schema.org/LocalBusiness">
                    <div class="storelink">
                        <span class="storepic <?= ($office['type'] == OFFICE_AGENT) ? "store-agent" : "" ?>" itemprop="photo">
                            <img src="/rsc/img/ui/<?= ($office['type'] == OFFICE_AGENT) ? "agent" : "office" ?>-img-placeholder.png" />
                        </span>
                        <span class="storeinfo">
                            <span class="storename" itemprop="name"><?= $office['name'] ?></span>
                            <span class="storecont store-add" itemprop="address"><?= nl2br($office['address']) ?></span>
                            <span class="storecont store-phone" itemprop="telephone">T : <?= $office['telephone'] ?></span>
                            <span class="storecont store-handphone" itemprop="handphone">H : <?= ($office['handphone']) ? $office['handphone'] : "-" ?></span>
                            <span class="storecont store-fax" itemprop="fax">F : <?= ($office['fax']) ? $office['fax'] : "-" ?></span>
                            <?php if(!empty($office['email'])): ?><span class="store-email" itemprop="email"><a href="mailto:<?= $office['email'] ?>"><?= $office['email'] ?></a></span><?php endif; ?>
                        </span>
                    </div>
                </div>
                <?php endforeach; endif; endif; ?>
            </div>
            <div class="lead"></div>
        </div>
    </div>
</div>
