<?php
    $id= isset($item["id"]) ? $item["id"] : "";
    $name= isset($item["name"]) ? $item["name"] : "";
    $province= isset($item["province"]) ? $item["province"] : "";
    $address= isset($item["address"]) ? $item["address"] : "";
    $map_address= isset($item["map_address"]) ? $item["map_address"] : "";
    $postal_code= isset($item["postal_code"]) ? $item["postal_code"] : "";
    $telephone= isset($item["telephone"]) ? $item["telephone"] : "";
    $handphone= isset($item["handphone"]) ? $item["handphone"] : "";
    $fax= isset($item["fax"]) ? $item["fax"] : "";
    $email= isset($item["email"]) ? $item["email"] : "";
    $website= isset($item["website"]) ? $item["website"] : "";
    $latitude= isset($item["latitude"]) ? $item["latitude"] : "0";
    $longitude= isset($item["longitude"]) ? $item["longitude"] : "0";
    $status= isset($item["status"]) ? $item["status"] : "";
    $is_show= isset($item["is_show"]) ? $item["is_show"] : "";
    $created_date= isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date= isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date= isset($item["deleted_date"]) ? $item["deleted_date"] : "";
    $version= isset($item["version"]) ? $item["version"] : "";
    $type= isset($item["type"]) ? $item["type"] : "";
    $status_name= isset($item["status_name"]) ? $item["status_name"] : "";
    $is_show_name= isset($item["is_show_name"]) ? $item["is_show_name"] : "";
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
                <button class="btn btn-primary back-button" onclick="go('/manager/pemasaran/edit/<?= $id ?>');" title="Edit" rel="tooltip" data-placement="top">
					<i class="fa fa-pencil fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Detail Office</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="admin-form" method="post">
                            <fieldset>
                                <section>
                                    <label class="label">Type</label>
                                    <label class="select">
                                        <?= branch_type_select('type', $type, 'disabled'); ?>
                                        <i></i>
                                    </label>
                                </section>
								<section>
									<label class="label">Name</label>
									<label class="input">
										<input name="name" id="name" type="text"  class="form-control" readonly value="<?= $name; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Province</label>
									<label class="input">
										<input name="province" id="province" type="text"  class="form-control" readonly value="<?= $province; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Address</label>
									<label class="textarea">
										<textarea name="address" id="address"  class="form-control" rows="7" readonly/><?= $address; ?></textarea>
									</label>
								</section>
								<section>
									<label class="label">Map Address</label>
									<label class="input">
										<input name="map_address" id="map_address" type="text"  class="form-control" readonly value="<?= $map_address; ?>" readonly="readonly"/>
									</label>
								</section>
								<div class="row">
                                    <section class="col col-6">
                                        <label class="label">Latitude</label>
                                        <label class="input">
                                            <input name="latitude" id="latitude" type="text"  class="form-control" readonly value="<?= $latitude; ?>"/>
                                        </label>
                                    </section>
                                    <section  class="col col-6">
                                        <label class="label">Longitude</label>
                                        <label class="input">
                                            <input name="longitude" id="longitude" type="text"  class="form-control" readonly value="<?= $longitude; ?>"/>
                                        </label>
                                    </section>
                                </div>
								<section>
									<label class="label">Postal code </label>
									<label class="input">
										<input name="postal_code" id="postal_code" type="text"  class="form-control" readonly value="<?= $postal_code; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Telephone </label>
									<label class="input">
										<input name="telephone" id="telephone" type="text"  class="form-control" readonly value="<?= $telephone; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Handphone </label>
									<label class="input">
										<input name="handphone" type="text"  class="form-control" readonly value="<?= $handphone; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Fax </label>
									<label class="input">
										<input name="fax" type="text"  class="form-control" readonly value="<?= $fax; ?>" />
									</label>
								</section>
								<section>
									<label class="label">Email </label>
									<label class="input">
										<input name="email" type="text"  class="form-control" readonly value="<?= $email; ?>" />
									</label>
								</section>
                                <section>
									<label class="label">Website </label>
									<label class="input">
										<input name="website" type="text"  class="form-control" readonly value="<?= $website; ?>" />
									</label>
								</section>
								<section>
                                    <label class="label">Show / Hide</label>
                                    <label class="input">
                                        <input name="is_show" type="text"  class="form-control" readonly value="<?= $is_show_name; ?>" />
                                    </label>
                                </section>

                            </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <section>
                                    <label class="label">Active</label>
                                    <label class="input">
                                        <input type="text"  class="form-control" readonly value="<?= $status_name; ?>" />
                                    </label>
                                </section>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="input">
                                            <input type="text"  class="form-control" readonly value="<?= dateformatforview($created_date, "d F Y H:i:s") ?>" />
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="input">
                                            <input type="text"  class="form-control" readonly value="<?= dateformatforview($updated_date, "d F Y H:i:s") ?>" />
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Deleted Date</label>
                                        <label class="input">
                                            <input type="text"  class="form-control" readonly value="<?= dateformatforview($deleted_date, "d F Y H:i:s") ?>" />
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
