<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Office Controller.
 */
class Pemasaran_manager extends Baseadmin_Controller  {

    private $_title = "Pemasaran";
    private $_title_page = '<i class="fa-fw fa fa-building"></i> Pemasaran ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "pemasaran";
    private $_back = "/manager/pemasaran";
    private $_js_path = "/js/pages/pemasaran/manager/";
    private $_view_folder = "pemasaran/manager/";

    private $_table = "dtb_branch";
    private $_table_aliases = "dbr";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Faq
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Pemasaran</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Pemasaran</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/pemasaran">Pemasaran</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Office</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Office</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => $this->_js_path . "create.js",
            "script_http" => array(
                "https://maps.googleapis.com/maps/api/js?key=". GOOGLE_API_KEY ."&libraries=geometry",
            )
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    public function edit ($pemasaran_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/pemasaran">Pemasaran</a></li>';

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $data['item'] = null;

        //validate ID and check for data.
        if ( $pemasaran_id === null || !is_numeric($pemasaran_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $pemasaran_id));
        //get the data.
        $data['item'] = $model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Office</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Office</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
            "script_http" => array(
                "https://maps.googleapis.com/maps/api/js?key=". GOOGLE_API_KEY ."&libraries=geometry",
            )
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * view an offices
     */
    public function view ($pemasaran_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/pemasaran">Pemasaran</a></li>';

        //load the model.
		$model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $data['item'] = null;

        //validate ID and check for data.
        if ( $pemasaran_id === null || !is_numeric($pemasaran_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $pemasaran_id));
        //get the data.
        $data['item'] = $model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> View Office</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>View Office</li>',
            "back"          => $this->_back,
        );

        $footer = array();

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'view', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("type", "Type", "trim|required");
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("address", "Address", "trim|required");
        $this->form_validation->set_rules("latitude", "Latitude", "trim|required");
        $this->form_validation->set_rules("longitude", "Longitude", "trim|required");
        $this->form_validation->set_rules("is_show", "Show / Hide", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','name','province', 'address', 'telephone', 'type', 'is_show');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'province':
                        if ($value != "") {
                            $data_filters['lower(province)'] = $value;
                        }
                        break;

                    case 'address':
                        if ($value != "") {
                            $data_filters['lower(address)'] = $value;
                        }
                        break;

                    case 'telephone':
                        if ($value != "") {
                            $data_filters['lower(telephone)'] = $value;
                        }
                        break;

                    case 'type':
                        if ($value != "") {
                            $conditions['type'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $conditions['is_show'] = ($value == "show") ? STATUS_SHOW : STATUS_HIDE;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
            'status' => STATUS_ACTIVE,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //load model
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id          = sanitize_str_input($this->input->post('id'), "numeric");
        $name        = sanitize_str_input($this->input->post('name'));
        $province    = sanitize_str_input($this->input->post('province'));
        $address     = sanitize_str_input($this->input->post('address'));
        $map_address = sanitize_str_input($this->input->post('map_address'));
        $postal_code = sanitize_str_input($this->input->post('postal_code'));
        $telephone   = sanitize_str_input($this->input->post('telephone'));
        $handphone   = sanitize_str_input($this->input->post('handphone'));
        $fax         = sanitize_str_input($this->input->post('fax'));
        $is_show     = sanitize_str_input($this->input->post('is_show'), "numeric");
        $latitude    = sanitize_str_input($this->input->post('latitude'));
        $longitude   = sanitize_str_input($this->input->post('longitude'));
        $type        = sanitize_str_input($this->input->post('type'), "numeric");
        $email       = sanitize_str_input($this->input->post('email'));
        $website     = sanitize_str_input($this->input->post('website'));
        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            //begin transaction.
            $this->db->trans_begin();
            //validation success, prepare array to DB.
            $arrayToDB = array(
                'name'          => $name,
                'province'      => ucfirst($province),
                'address'       => $address,
                'map_address'   => $map_address,
                'postal_code'   => $postal_code,
                'telephone'     => $telephone,
                'handphone'     => $handphone,
                'fax'           => (($fax != "") ? $fax : null) ,
                'is_show'       => $is_show,
                'latitude'      => $latitude,
                'longitude'     => $longitude,
                'type'          => $type,
                'email'         => $email,
                'website'       => $website,
            );

            //insert or update?
            if ($id == "") {
                //insert to DB.
                $result = $model->insert($arrayToDB, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Office has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/pemasaran";
                }

            } else {
                //condition for update.
                $condition = array("id" => $id);
                $result = $model->update($arrayToDB, $condition, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Office has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/pemasaran";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data member
            $data = $model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $model->delete($condition,  array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Office has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
