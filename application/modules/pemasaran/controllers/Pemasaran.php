<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Office Controller.
 */
class Pemasaran extends Basepublic_Controller  {

    private $_view_folder = "pemasaran/front/";

    private $_table = "dtb_branch";
    private $_table_aliases = "dbr";
    private $_pk = "id";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        redirect(base_url('branch'));
        //get header page
       /* $page = get_page_detail('pemasaran');

        //set model
        $model_branch = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

		//get session search
        $ss_search = $this->session->userdata('uv_office_search');

		//get all data branch
        $branches  = $model_branch->get_all_data(array(
            "conditions" => array(
                "is_show" => SHOW,
            ),
            "status" => STATUS_ACTIVE,
        ))['datas'];

		//get all province name
		$bname = $model_branch->get_all_data(array(
            "select" => array("province"),
            "distinct" => true,
            "conditions" => array(
                "is_show" => SHOW,
            ),
            "status" => STATUS_ACTIVE,
            "order_by" => array("province" => "asc"),
        ))['datas'];

        $header = array(
            'header'            => $page,
            'models'            => $branches,
            'ss_search'         => $ss_search,
            'bname'             => $bname,
        );

        $footer = array(
            "script" => array(
                "/js/front/plugins/infobox.min.js",
                "/js/front/office.js",
            ),
            "script_http" => array(
                "https://maps.googleapis.com/maps/api/js?key=". GOOGLE_API_KEY ."&libraries=geometry",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');*/
        
	}

	public function get_data () {
        if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $search = sanitize_str_input($this->input->post('search'));

        $conditions = array(
            "is_show" => SHOW,
        );

        if (!empty($search)) {
            $conditions['lower(province)'] = strtolower($search);
        }

        //set model
        $model_branch = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $datas = $model_branch->get_all_data (array(
            "status" => STATUS_ACTIVE,
            "conditions" => $conditions,
            "order_by" => array("type" => "asc")
        ))['datas'];


        $message['datas'] = $datas;

        //set store session
        $this->session->set_userdata('uv_office_search', $search);

        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
