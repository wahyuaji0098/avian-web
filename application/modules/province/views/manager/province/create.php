<?php
    $id = isset($item["id"]) ? $item["id"] : "";
    $country_id = isset($item["country_id"]) ? $item["country_id"] : "";
    
    $is_show = isset($item["is_show"]) ? $item["is_show"] : "";
    $country = isset($item["country_name"]) ? $item["country_name"] : "";
    $province = isset($item["name"]) ? $item["name"] : "";
    $created_date = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/province/lists';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Save" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/province/process-form" method="POST">
                            <header>Province form</header>
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Country <sup class="color-red">*</sup></label>
                                            <select name="country" class="select2Country" style="width:100%">
                                                <option selected value="<?= $country_id ?>"><?= $country ?></option>
                                            </select>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Province <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <input name="province" id="province" type="text"  class="form-control" placeholder="Province" value="<?php echo $province; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6 pull-right">
                                            <label class="label">Show / Hide </label>
                                            <label class="input"> 
                                                <?php echo select_is_show('is_show', $is_show); ?>
                                            </label>
                                        </section>
                                    </div>

                                    <?php if ($created_date != ""): ?>
                                    <section>
                                        <div class="row">
                                            <label class="label">Created date </label>
                                            <label class="input"> 
                                                <?php echo $created_date; ?>
                                            </label>
                                        </div>
                                    </section> 
                                    <?php endif; ?>
                                    
                                    <?php if ($updated_date != ""): ?>
                                    <section>
                                        <div class="row">
                                            <label class="label">Update date </label>
                                            <label class="input"> 
                                                <?php echo $updated_date; ?>
                                            </label>
                                        </div>
                                    </section> 
                                    <?php endif; ?>    
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
