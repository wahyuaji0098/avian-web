<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Province Controller.
 */
class Province_manager extends Baseadmin_Controller  {

    private $_title         = "Province";
    private $_title_page    = '<i class="fa-fw fa fa-flag"></i> Province ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "province";
    private $_back          = "/manager/province/lists";
    private $_js_path       = "/js/pages/province/manager/province/";
    private $_view_folder   = "province/manager/province/";
    private $_table         = "mtb_province";
    private $_table_aliases = "pro";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Province
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List of Province</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Province</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create Province
     */
    public function create() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Province</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Province</li>',
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "css" => array(
                "/css/select2.min.css"
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Edit an Province
     */
    public function edit ($id = null) {
        if ($id == 0) show_error('Page is not existing', 404);
        $this->_breadcrumb .= '<li><a href="/manager/province/lists">Province</a></li>';

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, 'pro.id')->get_all_data(array(
            "select"     => array('pro.id','pro.country_id','country.name AS country_name ','pro.name','pro.is_show','pro.version'),
            "conditions" => array("pro.is_show" => 1),
            "joined"     => array("mtb_country country" => array("country.id" => "pro.country_id")),
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Province</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Province</li>',
            "back"          => "/manager/province/lists",
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "css" => array(
                "/css/select2.min.css"
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Import an Province
     */
    public function import () {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Import Province</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Province</li>',
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "import.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'import');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation($id) {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("country", "Country", "trim|required");
        $this->form_validation->set_rules("province", "Province", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list Province joining Country
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('pro.id','country.name AS country_name ','pro.name','pro.is_show','pro.version');

        $column_sort = $select[$sort_col];
        // $column_sort = preg_replace('/ AS .*/', '', $select[$sort_col]);

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['pro.id'] = $value;
                        }
                        break;

                    case 'country':
                        if ($value != "") {
                            $data_filters['country.name'] = $value;
                        }
                        break;

                    case 'province':
                        if ($value != "") {
                            $data_filters['pro.name'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['pro.is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'version':
                        if ($value != "") {
                            $data_filters['pro.version'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => $select,
            "joined"          => array("mtb_country country" => array("country.id" => "pro.country_id")),
            "order_by"        => array($column_sort => $sort_dir),
            "limit"           => $limit,
            "start"           => $start,
            "conditions"      => $conditions,
            "filter"          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => FALSE,
            "is_version" => TRUE,
            "ordering"   => FALSE
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $country  = $this->input->post('country');
        $province = $this->input->post('province');

        $arrayToDB = array(
            "name" => ucwords($province),
            "country_id" => $country,
        );

        // server side validation. not implement yet.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            // Check Country with Province exist or not.
            $check = $this->_dm->set_model($this->_table, $this->_table_aliases, 'pro.id')->get_all_data(array(
                "select" => array("pro.name"),
                "conditions" => array(
                    "pro.name" => $province,
                    "pro.country_id" => $country
                ),
                "row_array"  => TRUE
            ))['datas'];

            // Begin transaction.
            $this->db->trans_begin();

            // Insert or update?
            if ($id == "") {

                // conditions before insert
                if (empty($check)) {
                     // Insert to DB.
                    $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                        $arrayToDB,
                        $extra_param
                    );
                } else {
                    $message['error_msg'] = 'Data province already exist.';
                    echo json_encode($message);
                    exit;
                }

            } else {

                // conditions before update
                if (empty($check)) {
                    // Condition for update.
                    $condition = array("id" => $id);
                    // Update to DB.
                    $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                        $arrayToDB,
                        $condition,
                        $extra_param
                    );
                } else {
                    $message['error_msg'] = 'Data province already exist.';
                    echo json_encode($message);
                    exit;
                }

            }

            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if (empty($id)) {
                    $message['notif_message'] = "Province has been added.";
                } else {
                    $message['notif_message'] = "Province has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/province/lists";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an Province.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data admin
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();
                //delete the data (deactivate)
                $condition = array($this->_pk => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "is_show" => 0 ),
                    $condition,
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => TRUE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Province has been hide.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Show an Province.
     */
    public function show() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data admin
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();
                //delete the data (deactivate)
                $condition = array($this->_pk => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "is_show" => 1 ),
                    $condition,
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => TRUE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Province has been show.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Get list country from mtb_country
     */
    public function select_country()
    {

        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //get ajax query and page.
        $select_q    = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;

        //sanitazion.
        $select_q = $select_q;

        //page must numeric.
        $select_page = is_numeric($select_page) ? $select_page : 1;

        //for paging, calculate start.
        $limit = 20;
        $start = ($limit * ($select_page - 1));

        //filters.
        $filters = array();
        if ($select_q != "") {
            $filters["name"] = $select_q;
        }

        //conditions.
        $conditions = array();

        //get data.
        $datas = $this->_dm->set_model('mtb_country')->get_all_data(array(
            "select"          => array("name", "id"),
            "distinct"        => true,
            "conditions"      => $conditions,
            "filter_or"       => $filters,
            "count_all_first" => true,
            "limit"           => $limit,
            "start"           => $start,
        ));

        //prepare returns.
        $message["page"]        = $select_page;
        $message["total_data"]  = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"]       = $datas['datas'];

        echo json_encode($message);
        exit;
    }

    /**
     * process export
     */
    public function process_export()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        //load model and library php excel
        $this->load->library("PHPExcel_reader");

        $header = array();
        $header[] = array(
            'Province Name',
            'Country Name',
            'Show',
        );

        $select = "pro.name as prov_name, con.name as con_name, pro.is_show";

        $result = $this->_dm->set_model($this->_table, $this->_table_aliases, "pro.id")->get_all_data(array(
            "select"     => $select,
            "conditions" => array('pro.is_show' => STATUS_ACTIVE),
            "joined"     => array("mtb_country con" => array("con.id" => "pro.country_id")),
        ))['datas'];

        $result = array_map(array($this, 'export_mapping'), $result);

        //merge array
        if (count($result) > 0) {
            $all_data = $header;
            $all_data = array_merge($header,$result);
            $exlc = new PHPExcel_reader();
            $data = $exlc->exportToExcelSingleSheets($all_data,"Export Province ". strtotime(date('Y-m-d H:i:s')), null, null,'A', 'C', 2, count($all_data));

            $message['is_error']  = false;
            $message['filename']  = "Export Province ". strtotime(date('Y-m-d H:i:s'));
            $message['file_data'] = "data:application/vnd.ms-excel;base64,".base64_encode($data);

            $message['notif_title']   = "Good!";
            $message['notif_message'] = "Download success";

        } else {
            $message['is_error']    = true;
            $message['error_msg']   = "Tidak ada Laporan Province.";
            $message['redirect_to'] = "";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    //call back array map
    public function export_mapping($source)
    {
        return array(
            'prov_name'    => $source['prov_name'],
            'con_name'     => $source['con_name'],
            'is_show_name' => $source['is_show_name'],
        );
    }

    /**
     * validating import section
     */
    public function validate_import($data, $line)
    {

        $this->load->model('Dynamic_model');
        $error = "";

        if (check_null_space($data['name'])) {
            $error .= "Row " . $line . ": Province Name cannot be empty.";
        }

        if (check_null_space($data['country_id'])) {
            $error .= "Row " . $line . ": Country Name cannot be empty.";
        }

        //check is exist
        $check = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "conditions" => array(
                    "name" => $data['name'],
                    "country_id" => $data['country_id'],
                    "is_show" => STATUS_ACTIVE
                ),
                "row_array"  => TRUE
            ))['datas'];

        if (!empty($check)) {
            $error .= "Row " . $line . ": Province " . $data['name'] . " already Exists.";
        }

        if ($error != "") {
            return $error;
        } else {
            return true;
        }
    }

    /**
     * Process Import
     * format import :
     *     (0) PROVINCE NAME , (1) COUTNRY NAME
     */
    public function process_import()
    {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Dynamic_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $validate = "";
        $error_validate_flag = false;

        //check validation
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > 0) {
            $filename = $_FILES['file']['tmp_name'];
            //check max file upload
            if ($_FILES['file']['size'] > MAX_UPLOAD_FILE_SIZE) {
                $message['error_msg'] = "Maximum file size is ".WORDS_MAX_UPLOAD_FILE_SIZE;
            } elseif (mime_content_type($filename) != "application/vnd.ms-excel"
                        && mime_content_type($filename) != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                //check extension if xls dan xlsx
                $message['error_msg'] = "File must .xls or .xlsx";
            } else {
                //load and init library
                $this->load->library("PHPExcel_reader");
                $excel = new PHPExcel_reader();

                //read the file and give back the result of the file in array
                $result = $excel->read_from_excel($filename);

                if ($result['is_error'] == true) {
                    $message['redirect_to'] = "";
                    $message['error_msg']   = $result['error_msg'];
                } else {

                    //begin transaction
                    $this->db->trans_begin();

                    if (count($result['datas']) > 0) {
                        //loop the result and insert to db
                        $line = 2;
                        foreach ($result['datas'] as $model) {
                            if (count($model) < 1) {
                                $validate = "Invalid Template.";
                                $error_validate_flag = true;
                                break;
                            } else {

                                // Get country_id from country_name
                                $data_con = $this->Dynamic_model->set_model('mtb_country','con','id')->get_all_data(array(
                                        "select" => "id",
                                        "conditions" => array(
                                            "name" => trim($model[1]),
                                        ),
                                        "row_array" => TRUE
                                    ))["datas"];

                                // Check if empty for country will be 0
                                if (empty($data_con)) {
                                    $data_con["id"] = 0;
                                }

                                //check untuk semua kolom apakah null
                                if (empty($model[0]) && empty($model[1])) {
                                    // klo kosong semua isinya, skip
                                    $line++;
                                } else {

                                    // persiapan data
                                    $data = array(
                                        "name"       => trim($model[0]),
                                        "country_id" => $data_con["id"]
                                    );

                                    // validasi di jadikan satu di fungsi validate_import.
                                    $validate = $this->validate_import($data, $line);

                                    if ($validate !== true) {
                                        //if validate false, return error
                                        $error_validate_flag = true;
                                        break;
                                    } else {
                                        // Prepare 2 dimensions data array for insert_batch
                                        $array_player[] = array(
                                            "name"       => trim($model[0]),
                                            "country_id" => $data_con["id"]
                                        );

                                        $line++;
                                    }
                                }
                            }
                        }

                        // insert_batch
                        $insert_data = $this->Dynamic_model->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                            $array_player,
                            array(
                                "is_direct" => true,
                                "is_batch" => true
                            )
                        );

                    }

                    if ($this->db->trans_status() === false || $error_validate_flag == true) {
                        $this->db->trans_rollback();
                        $message['redirect_to'] = "";

                        if ($error_validate_flag == true) {
                            $message['error_msg'] = $validate;
                        } else {
                            $message['error_msg'] = 'database operation failed.';
                        }
                    } else {
                        $this->db->trans_commit();

                        $message['is_error'] = false;

                        // success.
                        $message['notif_title']   = "Good!";
                        $message['notif_message'] = "Province has been imported.";

                        // on insert, not redirected.
                        $message['redirect_to'] = "";
                    }
                }
            }
        } else {
            $message['error_msg'] = 'File is empty (xls or xlsx).';
        }

        echo json_encode($message);
        exit;
    }

}
