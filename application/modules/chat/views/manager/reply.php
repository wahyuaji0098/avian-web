<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/chat/lists';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2>Reply Chat <?= $chat['member_name'] ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <div id="chat-body" class="chat-body custom-scroll">
                            <ul>
                                <?php
                                    if (is_array($chat_detail) && count($chat_detail) > 0):
                                        $elem_unread = 0;
                                        foreach ($chat_detail as $det):
                                ?>
                                <?php if ($det['is_read'] == 1 && $elem_unread == 0) : $elem_unread = 1; ?>
                                    <li id="unreadpoint" class="text-center"> <i>unread message</i> </li>
                                <?php endif; ?>
                                <?php
                                    if (empty($det['admin_id'])) :
                                ?>
                                <li class="message">
                                    <img src="<?= empty($det['profile_picture_url']) ? "/img/avatar.png" : $det['profile_picture_url'] ?>" class="online" alt="" height="50px">
                                    <div class="message-text">
                                        <time>
                                            <?= $det['tanggal'] ?>
                                        </time>
                                        <a href="javascript:void(0);" class="username"><?= $det['member_name'] ?></a>
                                        <?= $det['message'] ?>
                                    </div>
                                </li>
                                <?php else: ?>
                                <li class="message myself">
                                    <div class="message-text">
                                        <time>
                                            <?= $det['tanggal'] ?>
                                        </time>
                                        <a href="javascript:void(0);" class="username">Admin</a>
                                        <?= $det['message'] ?>
                                    </div>
                                </li>
                                <?php endif; ?>
                                <?php
                                        endforeach;
                                    endif;
                                ?>
                            </ul>
                        </div>

                        <div class="chat-footer">

                                <!-- CHAT TEXTAREA -->
                                <div class="textarea-div">

                                    <div class="typearea">
                                        <form class="smart-form" id="create-form" action="/manager/chat/process-reply" method="POST">
                                            <input type="hidden" value="<?= $chat['id'] ?>" name="room_id" id="room_id" />
                                            <textarea placeholder="Write a reply..." id="textarea-expand" class="custom-scroll" name="message"></textarea>
                                        </form>
                                    </div>

                                </div>

                                <!-- CHAT REPLY/SEND -->
                                <span class="textarea-controls">
                                    <button class="btn btn-sm btn-primary pull-right submit-form" data-form-target="create-form" >
                                        Reply
                                    </button>
                                </span>

                        </div>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
