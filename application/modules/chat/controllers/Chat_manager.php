<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Chat Controller.
 */
class Chat_manager extends Baseadmin_Controller  {

    private $_title         = "Chat";
    private $_title_page    = '<i class="fa-fw fa fa-comments"></i> Chat ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "job_offers";
    private $_back          = "/manager/chat/lists";
    private $_js_path       = "/js/pages/chat/manager/";
    private $_view_folder   = "chat/manager/";
    private $_table         = "trs_chat_room";
    private $_table_aliases = "tcr";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List job_offers
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Chat</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Chat</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    public function reply ($room_id) {

        if (empty($room_id)) {
            show_404();
        }

        //cek apakah idnya ada
        $data['chat'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select" => "tcr.*, dm.name as member_name",
            "conditions" => array(
                "tcr.id" => $room_id
            ),
            "joined" => array(
                "dtb_member dm" => array("dm.id" => "tcr.member_id")
            ),
            "row_array" => true,
        ))['datas'];

        if (empty($data['chat'])) {
            show_404();
        }

        //get chat detail
        //cek apakah idnya ada
        $data['chat_detail'] = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->get_all_data(array(
            "select" => "tcd.*, dm.name as member_name, dm.profile_picture_url",
            "conditions" => array(
                "chat_room_id" => $room_id
            ),
            "left_joined" => array(
                "dtb_member dm" => array("dm.id" => "tcd.member_id")
            ),
            "order_by" => array(
                "tanggal" => "asc"
            )
        ))['datas'];

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Reply Chat</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Chat</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "reply.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'reply', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation() {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("message", "Message", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get job_offers
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('tcr.id','dm.name','tcr.updated_date','is_read','member_id');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['tcr.id'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['dm.name'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'          => $select,
            'joined'          => array(
                "dtb_member dm" => array("dm.id" => "tcr.member_id")
            ),
            'order_by'        => array("is_read" => "desc", $column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            'status'          => STATUS_ALL,
            "count_all_first" => TRUE,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_reply() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        $message = array();

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";
        $message['notif_message'] = "";

        $extra_param = array(
            "is_direct"  => TRUE,
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $room_id = $this->input->post('room_id');

        $pesan               = $this->input->post('message');

        $arrayToDB = array(
            "message"           => $pesan,
            "chat_room_id"      => $room_id,
            "admin_id"          => $this->_currentUser['id'],
            "tanggal"           => date("Y-m-d H:i:s"),
            "is_read"           => 0,
        );

        // server side validation. not implement yet.
        $this->_set_rule_validation();

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            // Begin transaction.
            $this->db->trans_begin();

            //get last version

            $last_version_chat_detail = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->get_all_data(array(
                "select" => "IFNULL(MAX(version),0) as last_ver",
                "conditions" => array(
                    "chat_room_id" => $room_id
                ),
                "row_array" => true,
            ))['datas']['last_ver'];

            $arrayToDB['version'] = $last_version_chat_detail+1;

            // Insert to DB.
            $result = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->insert(
                $arrayToDB,
                $extra_param
            );

            $result = $this->_dm->set_model("trs_chat_room", "tcr", "id")->update(
                array(
                    "updated_date" => $arrayToDB['tanggal']
                ),
                array(
                    "id" => $room_id
                ),
                array(
                    "is_direct"  => TRUE
                )
            );

            //ambil data token user ini..
            $room_data = $this->_dm->set_model("trs_chat_room", "tcr", "id")->get_all_data(array(
                "conditions" => array(
                    "id" => $room_id
                ),
                "row_array" => true,
            ))['datas'];

            //get token user
            $tokens = $this->_dm->set_model("dtb_push_device", "dpd", "id")->get_all_data(array(
                "conditions" => array(
                    "member_id" => $room_data['member_id']
                ),
            ))['datas'];

            $this->load->library("FirebaseFcm");
            $firebase = new FirebaseFcm();

            $config = array(
                "mode"          => "single",
                "target"        => "ios",
                "priority"      => "PRIORITY_HIGH",
                "sound"         => "default",
                "badge"         => 1,
                "notif_title"   => "Chat dari Avian Brands",
                "notif_content" => $pesan,
            );

            if (is_array($tokens) && count($tokens) > 0) {
                foreach ($tokens as $token) {
                    if (!empty($token['device_token']) && $token['push_setting'] == 1 ) {
                        $config['target'] = $token['device_token'];

                        if ($token['type'] == 1) {
                            $firebase->firepush_ios($config);
                        }
                        else {
                            $firebase->firepush_android($config);
                        }

                    }
                }
            }

            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                $message['notif_message'] = "";
                // redirected.
                $message['redirect_to'] = "";

                $message['data'] = $arrayToDB;
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function set_read() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";
        $message['notif_message'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $room_id    = $this->input->post('room_id');

        // Begin transaction.
        $this->db->trans_begin();

        $result = $this->_dm->set_model("trs_chat_room", "tcr", "id")->update(
            array(
                "is_read" => 0,
            ),
            array(
                "id" => $room_id,
            ),
            array(
                "is_direct"  => TRUE,
            )
        );

        // Insert to DB.
        $result = $this->_dm->set_model("trs_chat_detail", "tcd", "id")->update(
            array(
                "is_read" => 0,
            ),
            array(
                "chat_room_id" => $room_id,
            ),
            array(
                "is_direct"  => TRUE,
            )
        );

        // End transaction.
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            $message['notif_message'] = "";
            // redirected.
            $message['redirect_to'] = "";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
