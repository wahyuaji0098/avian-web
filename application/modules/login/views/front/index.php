<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>MASUK</span>
            </div>
            <h2 class="section-title section-title-top">MASUK</h2>
            <form id="login-fm" action="/login/send" method="POST">
            <div class="bigform">
                <a href="#" onclick="fb_login();" class="btn bigform-btn fb-btn"><span></span>MASUK DENGAN FACEBOOK</a>
                <div class="bigform-sep"><span>OR</span></div>
                <div class="bigform-input">
                    <input type="text" placeholder="Username" id="username" name="username" autocomplete="off"/>
                </div>
                <div class="bigform-input">
                    <input type="password" placeholder="Password" id="password" name="password" autocomplete="off"/>
                </div>
                <div class="bigform-error" id="smessage"></div>
                <button type="submit" class="btn bigform-btn">MASUK</button>
                <a href="/forgot-password" class="linkbtn">Lupa Password?</a>
                <div class="bigformnote">TIDAK PUNYA AKUN? <a href="/register">DAFTAR</a></div>
            </div>
            </form>
        </div>
    </div>
</div>
