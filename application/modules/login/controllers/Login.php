<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Basepublic_Controller {

    private $_view_folder = "login/front/";

    public function __construct() {
        parent::__construct();

        if($this->session->has_userdata(USER_SESSION)) {
            //redirect to dashboard
            redirect('/');
        }
    }

    /**
	 * login controller and for login form processing.
	 */
	public function index() {
        
        redirect(base_url());
        // $page = get_page_detail('login');

        // $header = array(
        //     'header'        => $page,
        // );

        // $footer = array(
        //     "script" => array(
        //         "/js/plugins/jquery.validate.min.js",
        //         "/js/plugins/jquery.form.min.js",
        //         '/js/front/facebook.js',
        //         '/js/front/member.js',
        //     )
        // );

        // //load the views
        // $this->load->view(FRONT_HEADER ,$header);
        // $this->load->view($this->_view_folder . 'index');
        // $this->load->view(FRONT_FOOTER ,$footer);
	}

    private function setRuleValidation () {
        $this->form_validation->set_rules("username", "Username", "required|min_length[6]|max_length[15]");
        $this->form_validation->set_rules("password", "Password", "required|min_length[6]|max_length[12]");
    }

    public function send() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        //load model
        $this->load->library('form_validation');
        $this->load->model("member/Member_model");

		$message['is_error'] = true;
		$message['error_count'] = 1;
        $message['is_redirect'] = false;
		$data = array();

		$this->setRuleValidation();

		if ($this->form_validation->run($this) == FALSE) {
            $data = validation_errors();
            $count = count($this->form_validation->error_array());
            $message['error_count'] = $count;
        } else {
			$username	= sanitize_str_input($this->input->post('username'));
			$password	= sanitize_str_input($this->input->post('password'));

			$model_member = new Member_model ();

			$exist = $model_member->check_login ($username, $password);

			if($exist) {
                $message['is_error'] = false;
                $message['is_redirect'] = true;
                $message['error_count'] = 0;

                $model_member->update(array(
                    "lastlogin_date" => date("Y-m-d H:i:s"),
                    "unique_code" => null,
                ),array("id" => $exist['id']));

                $this->session->set_userdata(USER_SESSION, $exist);

            } else{
                $data = "Username atau Password salah.";
            }
        }

        $message['data'] = $data;
        $this->output->set_content_type('application/json');
        echo json_encode($message);

        exit;
	}

    public function fb_login() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $this->load->model("member/Member_model");

        $message['is_error'] = true;
		$message['error_count'] = 1;
        $message['is_redirect'] = false;
		$data = array();

		$response = sanitize_str_input($this->input->post('data'));

		$fb_id    = $response['id'];
		$name     = $response['name'];

		$datas = array (
			"name" 		=> $name,
			"fb_id" 	=> $fb_id,
		);

		$model_member = new Member_model ();

		$exist = $model_member->get_all_data (array(
            "conditions" => array(
                "fb_id" => $fb_id,
            ),
            "row_array" => true,
        ))['datas'];

		if ($exist) {
            $model_member->update(array(
                "lastlogin_date" => date("Y-m-d H:i:s"),
                "unique_code" => null,
            ),array("id" => $exist['id']));

			$this->session->set_userdata(USER_SESSION, $exist);

            $message['is_error'] = false;
            $message['is_redirect'] = true;
            $message['error_count'] = 0;
		} else {
			$insert = $model_member->insert($datas);
			if($insert) {
                $message['is_error'] = false;
                $message['is_redirect'] = true;
                $message['error_count'] = 0;

				$data_member = $model_member->get_all_data (array(
                    "find_by_pk" => array($insert),
                    "row_array" => true,
                ))['datas'];

                $model_member->update(array(
                    "lastlogin_date" => date("Y-m-d H:i:s"),
                    "unique_code" => null,
                ),array("id" => $data_member['id']));

				$this->session->set_userdata(USER_SESSION, $data_member);

			} else {
				$data = "Something Went Wrong..";
			}
		}

        $message['data'] = $data;
        $this->output->set_content_type('application/json');
        echo json_encode($message);

        exit;
	}

}
