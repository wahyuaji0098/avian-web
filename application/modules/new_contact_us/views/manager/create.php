<?php
    $id= isset($item["id"]) ? $item["id"] : "";
    $email_address= isset($item["email_address"]) ? $item["email_address"] : "";
    $subject= isset($item["subject"]) ? $item["subject"] : "";
    $message= isset($item["message"]) ? $item["message"] : "";
    $name= isset($item["name"]) ? $item["name"] : "";
    $type= isset($item["type"]) ? $item["type"] : "";
    $status= isset($item["status"]) ? $item["status"] : "";
    $created_date= isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date= isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date= isset($item["deleted_date"]) ? $item["deleted_date"] : "";

    $btn_msg = ($id == 0) ? "Create" : " Reply";
    $title_msg = ($id == 0) ? "Create" : " Reply";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Send" rel="tooltip" data-placement="top" >
					<i class="fa fa-paper-plane-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2>Message</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form">
                                <fieldset>
                                    <section>
										<label class="label">From</label>
										<label class="input">
											<input type="text"  class="form-control" readonly value="<?= $email_address; ?>" />
										</label>
									</section>
									<section>
										<label class="label">Name</label>
										<label class="input">
											<input type="text" readonly class="form-control" value="<?= $name; ?>" />
										</label>
									</section>
                                    <section>
										<label class="label">Message</label>
										<label class="textarea">
											<textarea readonly class="form-control" rows="15" ><?= $message; ?></textarea>
										</label>
									</section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Contact</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/contact-us/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">To <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="to" id="to" type="text"  class="form-control" placeholder="Kepada (alamat email)" value="<?= $email_address; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">From <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="email_from" id="email_from" type="text"  class="form-control" placeholder="Dari (alamat email)" value="<?= DEFAULT_EMAIL_FROM ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">From Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="name_from" id="name_from" type="text"  class="form-control" placeholder="Dari Nama" value="<?= DEFAULT_EMAIL_FROM_NAME ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">BCC </label>
                                        <label class="input">
                                            <input name="bcc" id="bcc" type="text"  class="form-control" placeholder="BCC tidak boleh sama dengan [kepada (alamat email)], dipakai untuk mengirimkan kopian pesan ke email" value="" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Subject <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="subject" id="send_subject" type="text"  class="form-control" placeholder="Judul Subjek" value="" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Message <sup class="color-red">*</sup></label>
                                        <label class="textarea">
                                            <textarea name="content" id="content" type="text"  class="form-control" placeholder="Pesan" rows="15"/></textarea>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
