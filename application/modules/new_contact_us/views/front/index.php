<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<div id="content" class="mr horizontal-wrapper karir" data-aos="fade">
     <div class="h-col fw bg-light-green">
        <form id="form_send" method="POST">
            <div class="col-md-10 col-xs-12" style="padding-top: 20px;">
                <p class="font-sofia-bold font-green font-md">Hubungi Kami</p>
                <p class="font-sofia-light font-black font-xs" style="margin-bottom: 20px">Sampaikan pesan atau pertanyaan Anda dengan mengisi formulir di bawah ini.</p>
                <input type="text" class="input-form font-sm" name="name" placeholder="Nama Lengkap">
                <input type="email" class="input-form font-sm" name="email" placeholder="Email">
                <textarea class="input-form font-sm" name="message" placeholder="Pesan" rows="4" style="border-radius: 20px;"></textarea>
                <button id="send_btn" class="button button-rounded button-block btn-inline-green font-green" type="button">Kirim</button>
                <div class="row" style="padding: 10px; margin-top: 20px !important;">
                <div class="col-md-6">
                    <p class="font-green font-sofia-bold font-sm">KANTOR PUSAT</p>
                    <p class="font-sofia-light">
                        Gedung Avian Brands Lt. 9-10<br>
                        Jl. Ahmad Yani 317<br>
                        Surabaya 60234 - Indonesia
                    </p>
                    <p class="font-sofia-light">
                        <b class="font-green">Tel:</b> +6231 9985 0050 | 9985 0600 (Hunting)<br>
                        <b class="font-green">Fax:</b> +6231 998 50505
                    </p>
                </div>
                <div class="col-md-6">
                    <p class="font-green font-sofia-bold font-sm">AVIAN INNOVATION CENTER</p>
                    <p class="font-sofia-light">
                        Jl. Raya Surabaya-Sidoarjo KM 19<br>
                        Desa Wadungasih, Kec. Buduran<br>
                        Sidoarjo 61252 - Indonesia
                    </p>
                    <p class="font-sofia-light">
                        <b class="font-green">Tel:</b> +6231 896 8000 | 896 9000 (Hunting)<br>
                        <b class="font-green">Fax:</b> +6231 892 1734 | 896 4118
                    </p>
                </div>
            </div>
            </div>
          </form>
        </div>
    </div>
    <script type="text/javascript">
        
        $("#send_btn").click(function() {
            /* Act on the event */
            $.ajax({
                url: '<?= base_url() ?>new_contact_us/Contact_us/send/',
                type: 'POST',
                dataType: 'json',
                data: $("#form_send").serialize(),
                success:function(resp){

                    if (resp.status == 200) {

                        Swal.fire({
                          title: 'Berhasil',
                          text: 'Pesan Terkirim',
                          icon: 'success',
                          confirmButtonText: 'OK'
                        });

                        window.location.reload();
                    } 

                }
            });
            
        
        });

    </script>