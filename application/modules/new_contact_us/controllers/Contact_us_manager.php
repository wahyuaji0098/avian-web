<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Contact Controller.
 */
class Contact_us_manager extends Baseadmin_Controller  {

    private $_title = "Contact";
    private $_title_page = '<i class="fa-fw fa fa-envelope"></i> Contact ';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "contact_us";
    private $_back = "/manager/contact_us";
    private $_js_path = "/js/pages/contact_us/manager/";
    private $_view_folder = "contact_us/manager/";

    private $_table = "dtb_contact_us";
    private $_table_aliases = "dcu";
    private $_pk = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Contact
     */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Contact</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Contact</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Reply email
     */
    public function reply ($menu_id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/contact-us">Contact</a></li>';

        //load the model.
        $data['item'] = null;

        //validate ID and check for data.
        if ( $menu_id === null || !is_numeric($menu_id) ) {
            show_404();

        }

        $params = array("row_array" => true,"conditions" => array("id" => $menu_id));
        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Contact</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Contact</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                $this->_js_path . "create.js",
            ),
        );

		//load the view.
		$this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
		$this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("to", "To", "trim|required");
        $this->form_validation->set_rules("email_from", "From", "trim|required");
        $this->form_validation->set_rules("name_from", "From Name", "trim|required");
        $this->form_validation->set_rules("subject", "Subject", "trim|required");
        $this->form_validation->set_rules("content", "Pesan", "trim|required");

    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list_all_data admin
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
		$sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
		$limit = sanitize_str_input($this->input->get("length"), "numeric");
		$start = sanitize_str_input($this->input->get("start"), "numeric");
		$search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

		$select = array('id','name', 'email_address', 'message');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(id)'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'email':
                        if ($value != "") {
                            $data_filters['lower(email)'] = $value;
                        }
                        break;

                    case 'message':
                        if ($value != "") {
                            $data_filters['lower(message)'] = $value;
                        }
                        break;


                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
			'select' => $select,
            'order_by' => array($column_sort => $sort_dir),
			'limit' => $limit,
			'start' => $start,
			'conditions' => $conditions,
            'filter' => $data_filters,
            "count_all_first" => true,
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
			"draw" => intval($this->input->get("draw")),
			"recordsTotal" => $total_rows,
			"recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
			exit('No direct script access allowed');
		}

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error'] = true;
		$message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id = sanitize_str_input($this->input->post('id'), "numeric");
        $to = sanitize_str_input($this->input->post('to'));
        $email_from = sanitize_str_input($this->input->post('email_from'));
        $name_from = sanitize_str_input($this->input->post('name_from'));
        $subject = sanitize_str_input($this->input->post('subject'));
        $content = sanitize_str_input($this->input->post('content'));
        $bcc = sanitize_str_input($this->input->post('bcc'));

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            $bcc = explode(",",$bcc);

            //send email
            $mail = sendmail (array(
                'subject'	=> $subject,
                'message'	=> $content,
                'to'		=> array($to),
                'bcc'		=> $bcc,
                'from'		=> array("email" => $email_from, "name" => $name_from),
            ), "text");

            $message['is_error'] = false;

            //success.
            //growler.
            $message['notif_title'] = "Excellent!";
            $message['notif_message'] = "Email has been sent.";

            //on update, redirect.
            $message['redirect_to'] = "/manager/contact";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
