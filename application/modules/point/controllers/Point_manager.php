<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Category Controller.
 */
class Point_manager extends Baseadmin_Controller  {

    private $_title         = "Point";
    private $_title_page    = '<i class="fa-fw fa fa-cc-paypal"></i> Point';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "point";
    private $_back          = "/manager/point/";
    private $_js_path       = "/js/pages/point/";
    private $_view_folder   = "point/";

    private $_table         = "mst_point_configuration";
    private $_table_aliases = "mpc";
    private $_pk            = "activity_code";

    /**
     * constructor.
     */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product Category
    */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Point </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Point</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/lightbox/js/lightbox.min.js",
                $this->_js_path . "list.js",
            ),
            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Point
    */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/point">Point</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Point</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Point</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an Promo
     */
    public function edit ($point = null) {
        $this->_breadcrumb .= '<li><a href="/manager/point">Point</a></li>';

        $data['item'] = null;

        //validate ID and check for data.
        if ($point === null ) {
            show_404();
        }

        $params = array(
            "row_array"  => true,
            "conditions" => array("point" => $point),
        );

        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];
        // pr($data['item']);exit;

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Promo</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Promo</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("activity_code", "description", "point");

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'activity_code':
                        if ($value != "") {
                            $data_filters['lower(activity_code)'] = $value;
                        }
                        break;

                    case 'point':
                        if ($value != "") {
                            $data_filters['lower(point)'] = $value;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'            => $select,
            'order_by'          => array($column_sort => $sort_dir),
            'limit'             => $limit,
            'start'             => $start,
            'conditions'        => $conditions,
            'filter'            => $data_filters,
            "count_all_first"   => true,
            "status"            => -1
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //dynamic model
        $point_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $activity_code      = sanitize_str_input($this->input->post('activity_code'));
        $description        = sanitize_str_input($this->input->post('description'));
        $point              = sanitize_str_input($this->input->post('point'));
        //convert space to _ .
        $code = str_replace(" ", "_", $activity_code);

        //begin transaction.
        $this->db->trans_begin();

        //validation success, prepare array to DB.
        $arrayToDB = array(
            "activity_code" => $code,
            "description"   => $description,
            "point"         => $point
        );
        //cheked is exist data
        $check = $point_model->get_all_data(array(
            "select"        => "activity_code",
            "row_array"     => true,
            "conditions"    => array("activity_code" => $activity_code)
        ))['datas'];
        
        //if check null , insert
        if (!$check) {

            //insert to DB.
            $result = $point_model->insert($arrayToDB, array("is_direct" => true));

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';

            } else {
                $this->db->trans_commit();

                $message['is_error'] = false;

                //success.
                //growler.
                $message['notif_title'] = "Good!";
                $message['notif_message'] = "New Point has been added.";

                //on insert, not redirected.
                $message['redirect_to'] = "/manager/point";
            }
        } else {

            //condition for update.
            $condition = array("activity_code" => $activity_code);

            $result = $point_model->update($arrayToDB, $condition, array("is_direct" => true));

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';

            } else {
                $this->db->trans_commit();

                $message['is_error'] = false;

                //success.
                //growler.
                $message['notif_title']   = "Excellent!";
                $message['notif_message'] = "Point has been updated.";

                //on update, redirect.
                $message['redirect_to']   = "/manager/point";
            }
        }
        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * delete.
     */
    public function delete() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $activity_code = sanitize_str_input($this->input->post('id'));

        //check first.
        if ($activity_code) {
           
            // begin transaction
            $this->db->trans_begin();

            // deactivate the user
            $condition = array("activity_code" => $activity_code);
            $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->delete($condition, array('is_permanently' => true));

            // end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();

                // failed.
                $message['error_msg'] = 'database operation failed';
            } else {
                $this->db->trans_commit();
                // success.
                $message['is_error']  = false;
                $message['error_msg'] = '';

                // growler.
                $message['notif_title']   = "Done!";
                $message['notif_message'] = "Point has been Deleted.";
                $message['redirect_to']   = "";
            }
        } 
        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
/* End of file Point_manager.php */
/* Location: ./application/modules/point/controllers/Point_manager.php */