<style type="text/css">
    .card-warna.warna-produk{
        width: 100%;
        height: 100px;
        margin-bottom: 20px;
        transition-duration: 0.5s;
        -webkit-transition-duration: 0.5s;
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
    }
    .card-warna.warna-produk:hover{
        transform: scale(1.05);
    }
    .wp-container{
        max-height: 90vh;
        overflow-y: scroll;
        padding-left: 40px !important;
    }

    @media (min-width: 320px) and (max-width: 480px) {
        .wp-container{
            padding-left: 0px !important;
            padding-right: 20px !important;
            max-height: auto !important;
            overflow-y: visible;
        }
        .cd2{
            margin-right: 20px !important;
            padding-top: 10px;
            padding-left: 20px;
            padding-right: 20px;
        }
        .cd2 img{
            padding: 0 30px;
        }
    }
</style>
<div id="content" class="mr produk_detail_produk bg-batik">
 <div class="row box0">
   <div class="row">
     <!-- card detail -->
     <div class="">
       <div class="col-md-3 card-detail cd2 text-left" style="height: auto !important;">
          <h2 class="font-sofia-bold font-green text-center"><?= $product_name ?></h2>
          <img width="100%" src="<?= $model['image_url'] ?>" alt="<?= $model['name'] ?>">
          <center>
          <p class="font-sofia-light"> Warna produk <b><?= $product_name ?></b> </p>
          </center>
        </div>

      </div>
     
      <div class="col-md-8 desc">
 
        <div class="row wp-container">
              

            

              <?php 
                if(count($pallete) > 0){ 
                  foreach ($pallete as $model){
              ?>

              <div class="col-md-12">
                  <h3 class="font-sofia-bold font-green"><?php echo $model['name'] ?></h3>
                  <hr style="border-color: black!important;">
              </div>

              <?php 
                $q = "SELECT `a`.* , `b`.name , `b`.code , `b`.red , `b`.green , `b`.blue , `b`.id as `colid` FROM `mst_palette_color` `a` JOIN `dtb_color` `b` ON `a`.`color_id` = `b`.`id` WHERE `a`.`palette_id` = ".$model['palette_id']." ORDER BY `a`.`id` ASC";
                $data = $this->db->query($q)->result_array();
                foreach ($data as $k) {
               ?>
                <div class="col-md-2 col-sm-3 col-xs-6">
                  <div style="background-color: rgb(<?= $k['red'].','.$k['green'].','.$k['blue'] ?>)" class="card-warna warna-produk font-sofia-light" onclick="window.location.href='/color/detail?color_id=<?= $k['color_id'] ?>'">
                    <span class="circle-c" data-text="" ><?= $k['name'] ?></span>
                  </div>
                </div>
               <?php } ?>

               
              <?php } }else{ ?>
                Belum ada pallete color disini.
              <?php } ?>

              
 
            </div>
 
        </div>
      </div>
    </div>
  </div>
 
