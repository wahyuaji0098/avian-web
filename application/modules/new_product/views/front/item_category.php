<div id="content" class="mr bg-batik produk_detail">
    <div class="row header-produk">
      <div class="col-md-9 col-xs-9">
        <p class="font-sofia-bold font-md font-green"><?= ucwords(str_replace('-',' ' ,$this->uri->segment(3))) ?></p>
      </div>
      <div class="col-md-2 col-xs-3">
        <div class="dropdown-warna">
          <form action="<?= base_url() ?>new_product/Product/search/<?= $url ?>" method="get">
          <button  style="display: none;" type="button" class="btn btn-outline-success"><span class="margin font-sofia-bold font-sofia-md font-green">+ Filter</span></button>
          <div class="dropdown-content-warna">
            <ul>
              <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Posisi</span></a></li>
              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" value="eksterior" name="eksterior">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Eksterior</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="interior" value="interior">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Interior</p></div>
                </div>
              </li>
              <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Sertifikasi</span></a></li>
              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="lihat_semua" value="lihat_semua">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Lihat Semua</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="green_label" value="green_label">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Green Label</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="sni" value="sni">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">SNI</p></div>
                </div>
              </li>

               <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="1x_lapis" value="1x_lapis" >
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">1x Lapis</p></div>
                </div>
              </li>
              <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Tiper Cat</span></a></li>
               <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="all" value="all">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Lihat Semua</p></div>
                </div>
              </li>

               <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="matt" value="matt" >
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Matt</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="mid_sheen" value="mid_sheen">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Mid Sheen</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="gloss" value="gloss">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Gloss</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="special_effect" value="special_effect">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Special Effect</p></div>
                </div>
              </li>
              <li class="headlink"><a href="#" ><span class="font-sofia-bold font-sm font-green">Tiper Ruangan</span></a></li>
               <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="all" value="all">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Lihat Semua</p></div>
                </div>
              </li>

               <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="dapur" value="dapur">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Dapur</p></div>
                </div>
              </li>
              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="kamar_anak_anak"  value="kamar_anak_anak">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Kamar anak-anak</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="kamar_mandi"  value="kamar_mandi">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Kamar Mandi</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="ruang_keluarga" value="ruang_keluarga">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Ruang keluarga</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="ruang_kerja" value="ruang_kerja">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Ruang Kerja</p></div>
                </div>
              </li>

              <li>
                <div class="row rowcheck">
                  <div class="col-md-2 col-xs-2 padcheck">
                    <label class="content-check">
                      <input type="checkbox" name="ruang_makan" value="ruang_makan">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                  <div class="col-md-10 col-xs-9 textcheck"><p class="font-sofia-light font-sm font-green text">Ruang Makan</p></div>
                </div>
              </li>


              <li  class="button-drop">
                <div class="row">
                  <div class="col-md-6 col-xs-6">
                    <button type="submit" class="btn btn-outline-success-nav btn-block font-sm"><span class="font-sofia-bold font-sofia-md font-green">Terapkan</span></button>
                  </div>
                  <div class="col-md-6 col-xs-6">
                    <button type="button" class="btn btn-outline-success-nav btn-block font-sm"><span class="font-sofia-bold font-sofia-md font-green hapus">Hapus</span></button>
                  </div>
                </div>
              </form>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="row content">


      <?php $count = count($products); $i=0;?>
      <?php if($count > 0): foreach($products as $prod): ?>

      <div class="col">
        <div class="panel">
          <div class="box-img" style="background-image: url('<?= $prod['image_url'] ?>')"></div>
          <div class="text-content">
            <div class="prd-title">
              <h3><a href="/products/items/<?= $prod['pretty_url'] ?>"><?= $prod['name'] ?></a>
                <?php if ($prod['is_new_item'] == 1) {  ?>
                  <span class="new">New</span>
                <?php } ?>

                 <?php if ($prod['is_hot_item'] == 1 ) {  ?>
                  <span class="new">Hot</span>
                 <?php } ?>


              </h3>

              <a href="#" class="favorit"><i class="fa fa-heart-o"></i></a>
            </div>
            <div class="desc">
              <p>
                <?php echo $prod['description']; ?>
                <? //= ($prod['description']) ? oneSentence(strip_tags($prod['description'])) : "" ; ?></p>
            </div>
            <a href="/products/items/<?= $prod['pretty_url'] ?>" class="btn-show-prd">Lihat Produk</a>
          </div>
        </div>
      </div>

      <?php endforeach; endif; ?>



    </div>


  </div>

<script type="text/javascript" src="/avian_new/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.matchHeight-min.js"></script>
