<div id="content" class="bg-light-green mr horizontal-wrapper gedung">
        
      <!-- <div class="icon-scroll">
        <i class="fa fa-chevron-right" aria-hidden="true"></i>
      </div> -->
      <!-- ROW -->
      <div class="row">
        <!-- box 1 -->
        <div class="col-md-3 col-xs-12 box-gedung" id="box1">
          <div class="row">
            <div class="col-md-6 col-xs-12 pad-box">
              <p class="font-sofia-bold font-md font-green">Tentang Gedung <br> Avian Brands</p>
              <p class="font-sofia-light font-black font-sm">
                Gedung Avian Brands diresmikan bertepatan dengan hari jadi Avian Brands yang ke-40, yakni 1 November 2018. Sejak saat itu, Gedung Avian Brands menjadi salah satu Gedung ikonik di kota Surabaya. Terkenal dengan sebutan Gedung Mlintir atau dalam Bahasa Indonesia berarti Gedung Berputar, proses pembangunannya menghabiskan waktu 4 tahun. 
                Keunikan fasad Gedung ini menjadikan pemasangan kaca eksteriornya sebagai bagian tersulit selama tahap pembangunan. </p>
            </div>
            <div class="col-md-6 col-xs-12">
                <img src="<?= base_url()?>avian_new/images/gedung/gedung1.jpg" style="height:100vh">
            </div>
          </div>
        </div> <!-- /box 1 -->

        <!-- box 2 -->
        <div class="col-md-3 col-xs-12 box-gedung" id="box2">
          <div class="row pad-box">
            <div class="col-md-6 col-xs-12">
                <img src="<?= base_url()?>avian_new/images/gedung/gedung2.jpg" style="width:100%">
            </div>
            <div class="col-md-6 col-xs-12">
              <div class="box1">
                <p class="font-sofia-light font-sm font-black">
                  Tidak hanya fasad yang unik, bagian interior Gedung Avian Brands juga terlihat menarik. Satu hal yang sangat menarik perhatian adalah area lobby. Kesan luas dan mewah membuat Anda betah untuk duduk berlama-lama di lobby dengan dominasi warna putih ini. 
                  Warna putih yang mendominasi dinding dan plafon secara apik dipadukan dengan sentuhan warna-warni dari Furnitur didalamnya. Pemilihan warna kursi yang kontras menambah keceriaan didalam Gedung. Hal unik lainnya adalah pemilihan bentuk meja di ruang meeting yang asimetris, menjadikan ruangan terlihat artisik dan modern.
                </p>

                <!-- <h2 class="font-sofia-bold font-green">
                  Faucibus et molestie ac <br>
                  feugiat se lectus.
                </h2> -->
              </div>
              <div class="box2">
                  <img src="<?= base_url()?>avian_new/images/gedung/gedung3.jpg" style="width:100%">
              </div>
            </div>
          </div>
        </div> <!-- box 2 -->

        <!-- box 3 -->
        <div class="col-md-3 col-xs-12 box-gedung">
          <img src="<?= base_url()?>avian_new/images/gedung/gedung4.jpg" style="width:100%">
        </div> <!-- box 3 -->

        <!-- box 4 -->
        <div class="col-md-3 col-xs-12 box-gedung" id="box4">
          <div class="row pad-box">
            <div class="col-md-6 col-xs-12">
              <div class="half-vh">
                <p class="font-sofia-light font-sm font-black">
                 Paduan lainnya datang dari unsur kayu yang dipadukan di dinding beberapa ruangan dan memberikan kesan hangat dan elegan. Dipadu dengan juntaian lampu berbentuk asimetris octagon yang memancarkan warna hangat, semakin menjadikan area kerja terasa homey.
                </p>
                <p class="font-sofia-light font-sm font-black">
                 Gedung dengan 23 lantai ini dilengkapi dengan fasilitas kantin dan outdoor garden di lantai 3, ruang meeting di lantai 19, gym di lantai 23, dan puluhan ruang diskusi yang tersebar di setiap lantainya. 
                </p>
              </div>
              <div class="half-vh">
                <img src="<?= base_url()?>avian_new/images/gedung/gedung5.jpg" style="width:100%">
              </div>
            </div>
            <div class="col-md-6 ">
                <img src="<?= base_url()?>avian_new/images/gedung/gedung6.jpg" style="width:100%">
            </div>
          </div>            
        </div> <!-- box 4 -->
      </div> <!-- /row -->  
    </div> <!-- end content -->

    
<script type="text/javascript" src="/avian_new/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.matchHeight-min.js"></script>