<?php
    $id                  = isset($item["id_polling"]) ? $item["id_polling"] : "";
    $question            = isset($item["question"]) ? $item["question"] : "";
    $start_date          = isset($item["start_date"]) ? date("m/d/Y", strtotime($item["start_date"])) : "";
    $end_date            = isset($item["end_date"]) ? date("m/d/Y", strtotime($item["end_date"])) : "";
    $is_show             = isset($item["is_show"]) ? $item["is_show"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/polling/lists';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/polling/process-form" method="POST">
                            <header>Polling form</header>
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Question <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="question" id="question" type="text"  class="form-control" placeholder="Pertanyaan di pollingnya" value="<?php echo $question; ?>" />
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Start Date <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input name="start_date" type="text" id="start_date"  class="form-control" data-dateformat="yy-mm-dd" placeholder="Tanggal mulai polling" value="<?php echo $start_date; ?>" readonly/>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">End Date <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <i class="icon-append fa fa-calendar"></i>
                                                    <input name="end_date" type="text" id="end_date"  class="form-control" data-dateformat="yy-mm-dd" placeholder="Tanggal berakhir polling" value="<?php echo $end_date; ?>" readonly/>
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?php echo select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
