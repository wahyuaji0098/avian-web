<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Polling Controller.
 */
class Polling_manager extends Baseadmin_Controller  {

    private $_title         = "Polling";
    private $_title_page    = '<i class="fa-fw fa fa-briefcase"></i> Polling ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "pol_list";
    private $_back          = "/manager/polling/lists";
    private $_js_path       = "/js/pages/polling/manager/";
    private $_view_folder   = "polling/manager/";
    private $_table         = "dtb_polling";
    private $_table_aliases = "dpol";
    private $_pk            = "id_polling";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();
    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List polling
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Polling</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Polling</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "polling/list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'polling/index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create new polling
     */
    public function create() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create New Polling</span>',
            "active_page"   => 'pol_create',
            "breadcrumb"    => $this->_breadcrumb . '<li>Polling</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "polling/create.js"
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'polling/create');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Edit a polling
     */
    public function edit ($id = null) {
        if ($id == 0) show_error('Page is not existing', 404);
        $this->_breadcrumb .= '<li><a href="/manager/polling/lists/">Polling</a></li>';

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Polling</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Polling</li>',
            "back"          => "/manager/polling/lists",
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "polling/create.js"
            ),
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'polling/create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * List jawaban
     */
    public function answers($id_polling = null) {

        // check.
        if (empty($id_polling)) show_404();

        //set breadcrumb.
        $this->_breadcrumb .= '<li><a href="/manager/polling/lists/">List Polling</a></li>';

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Jawaban</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>List Jawaban</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "answer/list.js",
            ),
        );

        // get polling info.
        $data['polling'] = $this->_dm->set_model("dtb_polling", "dp", "dp.id_polling")->get_all_data(array(
            "conditions" => array("id_polling" => $id_polling),
            "row_array"  => true,
        ))['datas'];

        if (empty($data['polling'])) show_404();

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'answer/index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create new jawaban
     */
    public function create_answer($id_polling = null) {

        // check.
        if (empty($id_polling)) show_404();

        //set breadcrumb.
        $this->_breadcrumb .= '<li><a href="/manager/polling/lists/">List Polling</a></li>';
        $this->_breadcrumb .= '<li><a href="/manager/polling/answers/'.$id_polling.'">List Jawaban</a></li>';

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create New Answer</span>',
            "active_page"   => 'pol_create',
            "breadcrumb"    => $this->_breadcrumb . '<li>Answer</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "answer/create.js"
            ),
        );

        $data['id_polling'] = $id_polling;

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'answer/create', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Edit a jawaban
     */
    public function edit_answer ($id_polling_answer = null) {
        if ($id_polling_answer == 0) show_error('Page is not existing', 404);

        $data['item'] = $this->_dm->set_model("dtb_polling_answer", "dpa", "dpa.id_polling_answer")->get_all_data(array(
            "find_by_pk" => array($id_polling_answer),
            "row_array"  => TRUE
        ))['datas'];

        //set breadcrumb.
        $this->_breadcrumb .= '<li><a href="/manager/polling/lists/">List Polling</a></li>';
        $this->_breadcrumb .= '<li><a href="/manager/polling/answers/'.$data['item']['id_polling'].'">List Jawaban</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Answer</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Answer</li>',
            "back"          => "/manager/polling/Answers/".$data['item']['id_polling'],
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "answer/create.js"
            ),
        );

        $data['id_polling'] = $data['item']['id_polling'];

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'answer/create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation($id) {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("question", "Question", "trim|required");
        $this->form_validation->set_rules("start_date", "Start Date", "trim|required");
        $this->form_validation->set_rules("end_date", "End Date", "trim|required");
    }

    /**
     * Set validation rule for create and edit answer
     */
    private function _set_rule_validation_answer($id) {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("answer", "Answer", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get polling list.
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array(
            "id_polling",
            "question",
            "start_date",
            "end_date",
            "is_show",
        );

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['id_polling'] = $value;
                        }
                        break;

                    case 'question':
                        if ($value != "") {
                            $data_filters['question'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'start':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(start_date as date) <="] = $date['end'];
                            $conditions["cast(start_date as date) >="] = $date['start'];
                        }
                        break;

                    case 'end':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(end_date as date) <="] = $date['end'];
                            $conditions["cast(end_date as date) >="] = $date['start'];
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'          => $select,
            'joined'          => array(),
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Function to get answer list.
     */
    public function list_all_data_answer($poll_id = null) {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET" || empty($poll_id)) {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array(
            "id_polling_answer",
            "answer",
            "score",
        );

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();
        $conditions['id_polling'] = $poll_id;

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['id_polling_answer'] = $value;
                        }
                        break;

                    case 'answer':
                        if ($value != "") {
                            $data_filters['answer'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model("dtb_polling_answer", "dpa", "dpa.id_polling_answer")->get_all_data(array(
            'select'          => $select,
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => TRUE,
            "is_version" => FALSE,
            "ordering"   => FALSE
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $question            = $this->input->post('question');
        $start_date          = validate_date_input($this->input->post('start_date'));
        $end_date            = validate_date_input($this->input->post('end_date'));
        $is_show             = $this->input->post('is_show');

        $arrayToDB = array(
            "question"    => $question,
            "start_date"  => $start_date,
            "end_date"    => $end_date,
            "is_show"     => $is_show,
        );

        // server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            // Begin transaction.
            $this->db->trans_begin();

            // Insert or update?
            if ($id == "") {

                // Insert to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                    $arrayToDB,
                    $extra_param
                );

            } else {

                // Condition for update.
                $condition = array("id_polling" => $id);
                // Update to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    $arrayToDB,
                    $condition,
                    $extra_param
                );

            }

            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "Polling has been added.";
                } else {
                    $message['notif_message'] = "Polling has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/polling/lists";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post for answer.
     */
    public function process_form_answer() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => TRUE,
            "is_version" => FALSE,
            "ordering"   => FALSE
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id         = $this->input->post('id');
        $id_polling = $this->input->post('id_polling');

        $answer = $this->input->post('answer');

        $arrayToDB = array(
            "id_polling" => $id_polling,
            "answer" => $answer,
        );

        // server side validation.
        $this->_set_rule_validation_answer($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            // Begin transaction.
            $this->db->trans_begin();

            // Insert or update?
            if ($id == "") {

                // Insert to DB.
                $result = $this->_dm->set_model("dtb_polling_answer", "dpa", "dpa.id_polling_answer")->insert(
                    $arrayToDB,
                    $extra_param
                );

            } else {

                // Condition for update.
                $condition = array("id_polling_answer" => $id);

                // Update to DB.
                $result = $this->_dm->set_model("dtb_polling_answer", "dpa", "dpa.id_polling_answer")->update(
                    $arrayToDB,
                    $condition,
                    $extra_param
                );

            }

            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title'] = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "Answer has been added.";
                } else {
                    $message['notif_message'] = "Answer has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/polling/answers/" . $id_polling;
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete a jawaban.
     */
    public function delete_answer() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model("dtb_polling_answer", "dpa", "dpa.id_polling_answer");

        //check first.
        if (!empty($id)) {

            //get data admin
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array(
                    "id_polling_answer" => $id
                );
                $delete = $_model->delete($condition, array(
                    "is_permanently" => true,
                ));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Answer has been deleted.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
