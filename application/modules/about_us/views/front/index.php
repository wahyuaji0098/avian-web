<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>TENTANG KAMI</span>
            </div>
            <div class="pictext-centered">
                <img src="/img/ui/avianbrandslogo.png"/>
            </div>
            <div class="about">
                <div class="about-tabs">
                    <div class="about-tab active tabgroup targrouptaba targettaba1" targid="taba1" targroup="taba"><span>Cover</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba2" targid="taba2" targroup="taba"><span>Visi &amp; Misi</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba3" targid="taba3" targroup="taba"><span>Nilai Perusahaan</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba4" targid="taba4" targroup="taba"><span>Logo &amp; Tagline</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba5" targid="taba5" targroup="taba"><span>Sejarah</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba6" targid="taba6" targroup="taba"><span>Tentang Avian</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba7" targid="taba7" targroup="taba"><span>Pengantar Direktur</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba8" targid="taba8" targroup="taba"><span>Teknologi</span></div>
                    <div class="about-tab about-tab-2l tabgroup targrouptaba targettaba9" targid="taba9" targroup="taba"><span>Perjalanan Meraih</br/>Kesuksesan</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba10" targid="taba10" targroup="taba"><span>Jaminan Mutu</span></div>
                    <div class="about-tab tabgroup targrouptaba targettaba11" targid="taba11" targroup="taba"><span>Sistem Terintegrasi</span></div>
                    <div class="about-tab about-tab-2l tabgroup targrouptaba targettaba12" targid="taba12" targroup="taba"><span>Sumber Daya</br/>Manusia</span></div>
                    <div class="lead"></div>
                </div>
                <div class="about-tabconts">
                    <div class="about-tabcont targrouptaba targettaba1 active">
                        <div class="about-contain about-singleimg">
                            <div class="about-image-large"><img src="/img/ui/about-cover.jpg" /></div>
                            <div class="about-image-small"><img src="/img/ui/about-cover-small.jpg" /></div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba2">
                        <div class="about-contain about-standard">
                            <div class="about-num">01</div>
                            <div class="about-title">VISI &amp; MISI</div>
                            <div class="about-corner"><img src="/img/ui/boxy-vm.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-vm.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_sub abtcont_substyled"><span></span>VISI</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Menjadi pemimpin pasar di industri cat yang dicintai semua orang.
                                    </p>
                                </div>
                                <div class="abtcont_sub abtcont_substyled"><span></span>MISI</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Meningkatkan kualitas layanan dan inovasi yang berbasis kepada kepuasan pelanggan.
                                    </p>
                                    <p>
                                        Meningkatkan kompetensi dan daya saing melalui pemberdayaan sumber daya manusia.
                                    </p>
                                    <p>
                                        Memperkuat jaringan distribusi untuk meningkatkan nilai kompetitif.
                                    </p>
                                    <p>
                                        Menjalankan efisiensi di semua lini produksi secara konsisten.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba3">
                        <div class="about-contain about-standard">
                            <div class="about-num">02</div>
                            <div class="about-title">NILAI PERUSAHAAN</div>
                            <div class="about-corner"><img src="/img/ui/boxy-val.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-val.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_sub"><span></span>KEBERSAMAAN</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Mengandalkan rasa kebersamaan diantara semua pihak terkait.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>SELALU ADA</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Selalu berusaha memperluas jangkauan pasar dan juga ragam produk.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>TANGGUH</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Mempunyai kemampuan bersaing yang sangat kuat untuk bersinergi dengan perubahan dunia bisnis.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>TERENCANA</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Semua kegiatan dilandaskan dengan strategi dan perhitungan matang.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>BERINOVASI</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Melakukan inovasi untuk beradaptasi dengan perkembangan tren dan teknologi.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>MEMBERI MANFAAT</div>
                                <div class="abtcont_ps abtcont_ps_short">
                                    <p>
                                        Terus menerus menciptakan dan memberikan nilai lebih bagi semua pihak.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba4">
                        <div class="about-contain about-standard">
                            <div class="about-num">03</div>
                            <div class="about-title">LOGO DAN TAGLINE</div>
                            <div class="about-corner"><img src="/img/ui/boxy-logo.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-logo.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_sub"><span></span>LOGO</div>
                                <div class="abtcont_ps">
                                    <p>
                                        “Introducing Avian Brands”
                                    </p>
                                    <p>
                                        <img src="/img/ui/avianthelogo.png"/>
                                    </p>
                                    <p>
                                        Seiring dengan pertumbuhan yang pesat, pada tahun ini, perusahaan kami merasakan perlunya perubahan terutama dalam membangun sebuah brand korporat baru yang bisa memfasilitasi kebutuhan produk brand yang terus bertambah dan berkembang, sekaligus menjadi payung dari perusahaan secara umum.
                                    </p>
                                    <p>
                                        Pengembangan brand menjadi yang hal esensial bagi perusahaan kami terutama untuk membuka jalan kepada semua segmen pasar yang lebih luas. Hal yang juga penting untuk diperhatikan adalah brand yang baru harus dapat mempertahankan ekuitas kuat yang telah dibangun oleh perusahaan dari tahun-tahun sebelumnya.
                                    </p>
                                    <p>
                                        Oleh karena itu, pengenalan wajah baru AVIAN BRANDS dilakukan untuk memberikan citra baru yang unik dan segar bagi masyarakat luas. Brand baru ini mencerminkan komitmen perusahaan yang diwujudkan dalam brand essence kami: ‘Inheriting the Craftsmanship, Building the Future’ –dan juga merefleksikan 8 karakter brand kami (Bergairah, Ahli, Visioner, Kritis, Sinergis, Unggul, Bersahaja, dan Kemajuan).
                                    </p>
                                    <p>
                                        Sejalan dengan platform brand yang baru, kami juga memperkenalkan identitas brand baru, sebagai bukti dari usaha untuk terus berkembang. Grafis yang diperbaharui di dalam logo menggambarkan bebek yang lebih realistis tapi tetap terlihat ramah, dengan kombinasi tiga elemen garis berwarna hijau dengan yang melambangkan hubungan yang harmonis dari ketiga generasi dalam manajemen PT. Avia Avian.
                                    </p>
                                </div>
                                <div class="abtcont_sub abtcont_subwbadge"><span></span>TAGLINE</div>
                                <div class="abtcont_ps">
                                    <p>
                                        <img src="/img/ui/avianthetagline.png"/>
                                    </p>
                                    <div class="badge"><img src="/img/ui/thewheelofvalues.png" /></div>
                                    <p>
                                        Adalah tagline dari PT.Avia Avian yang memotivasi untuk selalu menciptakan keberagaman produk yang bernilai bagi semua pihak.
                                    </p>
                                    <p>
                                        <span class="apoint">Creating</span>: Selalu berkarya.<br/>
                                        <span class="apoint">Rich</span>: Keberagaman yang luar biasa.<br/>
                                        <span class="apoint">Value</span>: Bernilai, bermanfaat, bermakna.
                                    </p>
                                    <p>
                                        Tagline menggambarkan PT. Avia Avian tidak pernah lelah untuk berkarya dan menciptakan berbagai macam produk – produk yang berguna dan bermanfaat bagi kehidupan orang banyak.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba5">
                        <div class="about-contain about-standard about-standard-timeline">
                            <div class="about-num">04</div>
                            <div class="about-title">SEJARAH</div>
                            <div class="about-corner"><img src="/img/ui/boxy-hist.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-hist.jpg" /></div>
                            <div class="about-content about-content-timeline">
                                <div class="timeline-line"></div>
                                <div class="timeline-first">
                                    <div class="timeline-pic"><img src="/img/ui/history-beginning.png" /></div>
                                    <div class="timeline-date">1978</div>
                                    <div class="timeline-text">
                                        1 November 1978, Tan Tek Swie (Soetikno Tanoko) mendirikan PT. Avia Avian di Sidoarjo untuk memulai kiprahnya di industri cat tembok serta cat kayu dan besi.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">1983</div>
                                    <div class="timeline-text">
                                        Penambahan beberapa produk seperti cat otomotif, cat semprot, cat kayu &amp; cat rotan, serta beberapa cat untuk kebutuhan industri.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">1986</div>
                                    <div class="timeline-text">
                                        Memproduksi alkyd resin &amp; emulsion resin.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">1991</div>
                                    <div class="timeline-text">
                                        Perluasan pabrik diatas lahan baru seluas 60.000 m&sup2;.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-1993.png" /></div>
                                    <div class="timeline-date">1993</div>
                                    <div class="timeline-text">
                                        Mendirikan divisi pembuatan kemasan dari kaleng (tin plate) dan metal printing.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">1995</div>
                                    <div class="timeline-text">
                                        Mendirikan PT. Mitra Mulia Makmur (MMM) untuk pembuatan kemasan plastik.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-1996.png" /></div>
                                    <div class="timeline-date">1996</div>
                                    <div class="timeline-text">
                                        Pabrik PT. Avia Avian ke dua di Serang – Banten mulai beroperasi.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">1997</div>
                                    <div class="timeline-text">
                                        Menambah product line dengan merk Suzuka &amp; Platinum untuk cat otomotif.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-2000.png" /></div>
                                    <div class="timeline-date">2000</div>
                                    <div class="timeline-text">
                                        Mendirikan PT. Panca Kalsiumindo Perkasa di Tuban sebagai penghasil kalsium karbonat.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-2001.png" /></div>
                                    <div class="timeline-date">2001</div>
                                    <div class="timeline-text">
                                        Mendirikan PT. Tirtakencana Tatawarna sebagai distributor bahan bangunan &amp; furniture.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2003</div>
                                    <div class="timeline-text">
                                        Penambahan kapasitas produksi cat dengan mesin semi otomatis.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-2005.png" /></div>
                                    <div class="timeline-date">2005</div>
                                    <div class="timeline-text">
                                        Penambahan 5 (lima) lines mesin kaleng otomatis &amp; silo otomatis di atas lahan 10.000 m&sup2;.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2006</div>
                                    <div class="timeline-text">
                                        Mengakuisisi divisi pigment masterbatch BASF Indonesia dengan nama PT. Kasakata Kimia.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2007</div>
                                    <div class="timeline-text">
                                        Pabrik PT. Avia Avian ke tiga di Medan mulai beroperasi.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pic"><img src="/img/ui/history-2009.png" /></div>
                                    <div class="timeline-date">2009</div>
                                    <div class="timeline-text">
                                        Penambahan kapasitas produksi emulsion &amp; alkyd di PT. Avia Avian – Sidoarjo.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2010</div>
                                    <div class="timeline-text">
                                        Pembelian tanah di wilayah Gresik seluas kurang lebih 300 hektar untuk expansi di masa depan.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2011</div>
                                    <div class="timeline-text">
                                        Membangun pabrik dan mulai memasarkan produk semen instan bermerk Giant Mortar.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2014</div>
                                    <div class="timeline-text">
                                        Membangun pabrik dan mulai memasarkan produk PVC bermerk Power.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2015</div>
                                    <div class="timeline-text">
                                        Penambahan kapasitas produksi Pipa Power.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2016</div>
                                    <div class="timeline-text">
                                        Pembangunan pabrik pipa Power baru di Cirebon.
                                    </div>
                                </div>
                                <div class="timeline-item">
                                    <div class="timeline-line"></div>
                                    <div class="timeline-pin"></div>
                                    <div class="timeline-date">2017</div>
                                    <div class="timeline-text">
                                        Bergabung dengan Selleys (DGL International) membuat PT. Avian Selleys Indonesia untuk menghasilkan beberapa lini produk baru yaitu perekat konstruksi, perekat silikon dan spray pelumas untuk memenuhi pasar Indonesia.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba6">
                        <div class="about-contain about-standard">
                            <div class="about-num">05</div>
                            <div class="about-title">TENTANG AVIAN</div>
                            <div class="about-corner"><img src="/img/ui/boxy-abt.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-avi.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        Bermula pada tahun 1962 Soetikno Tanoko memulai bisnisnya dari sebuah toko cat di Malang. Tidak ada yang menduga jika toko cat kecil tersebut adalah cikal bakal dari PT. Avia Avian. Dengan memanfaatkan kondisi industri cat yang bergantung pada impor saat itu, Soetikno berinovasi mencampur warna – warna cat yang terbatas menjadi beragam warna yang diminati pelanggannya.
                                    </p>
                                    <p>
                                        PT Avia Avian pertama kali mulai beroperasi pada 1978 di Sidoarjo, Jawa Timur. Sebagai produsen cat lokal di dalam pasar cat yang didominasi pemain asing. Merek Avian lahir pada tahun 1978 dari riset ekstensif dan pengembangan para pendirinya.
                                    </p>
                                    <p>
                                        Sejak awal, PT Avia Avian dengan jelas didirikan dengan tujuan utama mampu bersaing dalam pasar yang didominasi produk-produk luar negeri. Seiring waktu, PT Avia Avian menjadi perusahaan yang diakui secara nasional, sukses bersaing dengan memenangkan pertempuran merebut hati dan pikiran konsumen dibanding merek-merek lain yang memimpin pasar, banyak diantaranya merek internasional.
                                    </p>
                                    <p>
                                        Dengan permintaan yang semakin berkembang, PT Avia Avian membuka pabrik kedua di Serang, Jawa Barat pada 1996, diikuti pabrik ketiga di Medan, Sumatera Utara pada 2007. PT Avia Avian kini diakui sebagai produsen cat lokal terbesar yang dimiliki dan dioperasikan di Indonesia.
                                    </p>
                                    <p>
                                        PT. Avia Avian merupakan satu-satunya produsen cat yang menerapkan sistem terintegrasi dalam setiap tahap produksinya dan salah satu produsen cat terkemuka di Indonesia. Kami memproduksi sejumlah merek cat yang dikenal luas, seperti Cat Besi &amp; Kayu Avian, Cat Tembok Avitex, Cat Tembok Eksterior Sunguard, serta Cat Pelapis Anti Bocor No Drop.
                                    </p>
                                    <p>
                                        Visi kami adalah menjadi pemimpin pasar di industri cat yang dicintai semua orang. Visi ini didukung dengan misi kami, yaitu dengan selalu meningkatkan kualitas layanan dan inovasi yang berbasis kepada kepuasan pelanggan. Kami bangga atas reputasi yang kami raih dalam hal kualitas, inovasi, distribusi dan keunggulan. Sampai saat ini PT. Avia Avian telah banyak memperoleh penghargaan, diantaranya Superbrands (periode 2010 - 2013) dan Top Brand (2005 - 2015), Satria Brand (2012-2015).
                                    </p>
                                    <p>
                                        Dengan permintaan yang semakin berkembang, PT Avia Avian membuka pabrik kedua di Serang, Jawa Barat pada 1996, diikuti pabrik ketiga di Medan, Sumatera Utara pada 2007. PT Avia Avian kini diakui sebagai produsen cat lokal terbesar yang dimiliki dan dioperasikan di Indonesia.
                                    </p>
                                    <p>
                                        Untuk meningkatkan penetrasi pasar dan kepuasan pelanggan, pada tahun 2001 PT. Tirtakencana Tatawarna, yang didedikasikan untuk mendistribusikan semua produk cat PT Avia Avian. PT Tirtakencana Tatawarna kini memiliki 62 cabang kantor dan gudang di seluruh Indonesia, didukung lebih dari 3500 tenaga professional dan armada distribusi kendaraan.
                                    </p>
                                    <p>
                                        Pertumbuhan bisnis kini juga didukung infrastruktur teknologi informasi yang canggih, dengan implementasi ERP (Enterprise Resources Planning) atau sistem perencanaan sumber daya perusahaan. Sistem ini meningkatkan pemantauan barang di setiap kantor cabang dan ketepatan waktu distribusi, serta mengurangi waktu respon untuk penanganan pelanggan. Tujuan utama ERP adalah mendukung kemampuan setiap kantor cabang untuk memesan secara online, meningkatkan waktu pengiriman dan kepuasan pelanggan secara keseluruhan.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba7">
                        <div class="about-contain about-standard">
                            <div class="about-num">06</div>
                            <div class="about-title">PENGANTAR DIREKTUR</div>
                            <div class="about-corner"><img src="/img/ui/boxy-pream.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-pream.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_sub"><span></span>PENGANTAR PRESIDEN DIREKTUR</div>
                                <div class="abtcont_ps">
                                    <p>
                                        “PT. Avia Avian, selama lebih dari tiga dekade mewarnai dinamika kehidupan masyarakat”
                                    </p>
                                    <p>
                                        <img src="/img/ui/direction.jpg"/>
                                    </p>
                                    <p>
                                        Sebagai perusahaan cat terkemuka, terus menciptakan produk – produk yang terbaik demi kepuasan pelanggan.
                                    </p>
                                    <p>
                                        Kami menyadari bahwa strategi dan perencanaan yang matang berperan penting dalam meningkatkan nilai tambah dan daya saing perusahaan untuk memenuhi berbagai kebutuhan cat bagi masyarakat secara merata.
                                    </p>
                                    <p>
                                        Dengan proses produksi terintegrasi dan terobosan inovatif serta pengalaman panjang, kami terus berkreasi dalam ragam warna kehidupan menjadikan lebih indah dan bermakna.
                                    </p>
                                    <p>
                                        Kesuksesan ini bisa dicapai dengan dukungan seluruh pihak yang berpartisipasi melalui tenaga dan pikiran demi kelangsungan perusahaan. Untuk itu, kami mengucapkan terima kasih sebesar – besarnya.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba8">
                        <div class="about-contain about-standard">
                            <div class="about-num">07</div>
                            <div class="about-title">TEKNOLOGI</div>
                            <div class="about-corner"><img src="/img/ui/boxy-tech.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-tech.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        PT. Avia Avian menjalin kerjasama dengan perusahaan – perusahaan kelas dunia untuk mendapatkan teknologi terkini dalam industri cat, antara lain :
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>CPS COLOR FINLAND</div>
                                <div class="abtcont_ps">
                                    <p>
                                        Untuk memberikan solusi sistem tinting agar dapat menciptakan jutaan warna cat untuk memenuhi seluruh kebutuhan pelanggan sesuai dengan perkembangan jaman.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>CLARIANT INDONESIA</div>
                                <div class="abtcont_ps">
                                    <p>
                                        Untuk mendukung pendirian laboratorium mikrobiologi sehingga sistem pengontrolan mikrobiologi dan sanitasi produksi dapat tercapai dengan maksimal.
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>TROY CORPORATION ASIA PACIFIC</div>
                                <div class="abtcont_ps">
                                    <p>
                                        Untuk pengembangan cat tembok dengan Teknologi Anti Bakteri.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba9">
                        <div class="about-contain about-standard">
                            <div class="about-num">08</div>
                            <div class="about-title">PERJALANAN MERAIH KESUKSESAN</div>
                            <div class="about-corner"><img src="/img/ui/boxy-road.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-road.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        “PT. Avia Avian berkomitmen terhadap perkembangan dari masa ke masa”
                                    </p>
                                    <p>
                                        Sejak didirikan, PT. Avia Avian bertekad untuk terus melakukan pembangunan sebagai investasi perusahaan. Perbaikan secara berkesinambungan pun menjadi agenda utama untuk mempertahankan kepercayaan pelanggan dan meraih sukses yang berkelanjutan di masa depan.
                                    </p>
                                    <p>
                                        <img src="/img/ui/growthgraph.jpg"/>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba10">
                        <div class="about-contain about-standard">
                            <div class="about-num">09</div>
                            <div class="about-title">JAMINAN MUTU</div>
                            <div class="about-corner"><img src="/img/ui/boxy-qua.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-qua.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        “Komitmen terhadap jaminan mutu menjadi motivasi bagi PT. Avia Avian untuk mempertahankan dan meningkatkan mutu produk dari waktu ke waktu”
                                    </p>
                                    <p>
                                        Untuk mewujudkan produk dengan kualitas terjamin, PT. Avia Avian menerapkan manajemen pengawasan mutu di setiap lini produksi.
                                    </p>
                                    <p>
                                        Tahap pemilihan bahan baku, proses produksi hingga kemasan telah melalui kontrol yang cermat untuk memastikan produk dengan kualitas terbaik sampai di tangan konsumen.
                                    </p>
                                    <p>
                                        Departemen R&amp;D terus berinovasi untuk mengembangkan ragam produk baru yang ramah lingkungan secara berkesinambungan. Program Quality Improvement diterapkan untuk meningkatkan kualitas produk secara berkesinambungan.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba11">
                        <div class="about-contain about-standard">
                            <div class="about-num">10</div>
                            <div class="about-title">SISTEM TERINTEGRASI</div>
                            <div class="about-corner"><img src="/img/ui/boxy-sys.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-sys.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        “Sebagai upaya untuk mewujudkan proses produksi cat yang efisien dan produk yang berkualitas, PT. Avia Avian mengembangkan pabrik resin dan polymer sebagai bahan baku utama cat”
                                    </p>
                                </div>
                                <div class="abtcont_sub"><span></span>RESIN DAN POLIMER</div>
                                <div class="abtcont_ps">
                                    <p>
                                        <span class="bold">Short, Medium, Long Oil Alkyd Resin dan Styrenated Alkyd Resin.</span> <br/>
                                        Produk ini digunakan dalam industri surface coating.
                                    </p>
                                    <p>
                                        <span class="bold">100% Acrylic, Vinyl Acrylic, Styrene Scrylic, PVAc.</span> <br/>
                                        Produk ini digunakan untuk keperluan pembuatan cat tembok dan lem PVAc.
                                    </p>
                                    <p>
                                        <span class="bold">Polyol, Acrylic Thermosetting, Acrylic Thermoplastic.</span> <br/>
                                        Produk ini digunakan untuk keperluan cat otomotif dan cat industri.
                                    </p>
                                    <p>
                                        <span class="bold">Additive Dispersing, Wetting, Coalescent, Defoamer.</span> <br/>
                                        Produk ini digunakan untuk membantu proses pembuatan cat tembok sehingga menghasilkan kualitas yang optimal.
                                    </p>
                                    <p>
                                        <span class="bold">Pigment Masterbatch.</span> <br/>
                                        Pada tahun 2006, PT. Kasakata Kimia mengambil alih divisi usaha pigment masterbatch BASF Indonesia. Perusahaan yang dianugerahi sertifikat ISO 9001 : 2007 ini memiliki pangsa pasar produsen produk – produk seperti otomotif, fiber, perabot, konstruksi, olahraga dan mainan serta kemasan cat. Didukung teknologi mesin dari Jerman, PT. Kasakata Kimia selalu berupaya memproduksi produk berkualitas tinggi yang sesuai dengan spesifikasi pelanggan.
                                    </p>
                                    <p>
                                        <span class="bold">Kalsium Karbonat.</span> <br/>
                                        PT. Panca Kalsiumindo Perkasa memproduksi Kalsium Karbonat (CaCo3) yang berguna sebagai filter / extender pada permukaan cat tembok atau primer. Didirikan tahun 2000 di atas lahan seluas 150.000 m2 di Tuban, perusahaan ini memiliki kapasitas produksi sebesar 50.000 ton pada tahun 2009.
                                    </p>
                                    <p>
                                        <span class="bold">Kontainer Plastik &amp; Printing.</span> <br/>
                                        PT. Mitra Mulia Makmur memproduksi kemasan / container plastik dan printing untuk memenuhi kebutuhan kemasan plastik. Didukung dengan fasilitas mesin cetak 9 warna, desainer handal serta workshop yang lengkap, perusahaan mampu menghasilkan produk dan desain printing yang berkualitas sesuai standar mutu ISO 9001 : 2008.
                                    </p>
                                    <p>
                                        <span class="bold">Metal Printing dan Can Making.</span> <br/>
                                        Untuk menjamin kelancaran dan kualitas bahan kemas, PT. Avia Avian mulai memproduksi metal printing dan pembuatan kaleng pada tahun 1993. Penambahan mesin – mesin kaleng otomatis dilakukan untuk memenuhi kebutuhan kemasan yang semakin meningkat.
                                    </p>
                                    <p>
                                        <span class="bold">Distribusi.</span> <br/>
                                        PT. Tirtakencana Tatawarna didirikan pada tahun 2001 dengan tujuan mendistribusikan seluruh Produk Avian Brands. Saat ini PT. Tirtakencana Tatawarna memiliki 62 kantor pemasaran yang tersebar di seluruh wilayah Indonesia dan melayani lebih dari 40.000 pelanggan toko bahan bangunan. Didukung lebih dari 3500 tenaga professional dan armada pengiriman yang dimiliki lebih dari 200 unit truk serta 190 unit kendaraan lainnya. Untuk sistem teknologi informasi, perusahaan menggunakan sistem ERP (Enterprise Resource Program) Microsoft Dynamic Navision.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-tabcont targrouptaba targettaba12">
                        <div class="about-contain about-standard">
                            <div class="about-num">11</div>
                            <div class="about-title">SUMBER DAYA MANUSIA</div>
                            <div class="about-corner"><img src="/img/ui/boxy-hr.png" /></div>
                            <div class="about-sideimg"><img src="/img/ui/about-hr.jpg" /></div>
                            <div class="about-content">
                                <div class="abtcont_ps">
                                    <p>
                                        “Kunci sukses perusahaan bermula dari kualitas sumber daya manusia yang dimiliki”
                                    </p>
                                    <p>
                                        PT. Avia Avian memahami dan terus berupaya meningkatkan kualitas sumber daya manusia karena mereka adalah salah satu aset terbesar perusahaan.
                                    </p>
                                    <p>
                                        PT. Avia Avian secara berkala menyelenggarakan berbagai pelatihan dan kompetisi internal yang bertujuan memperluas wawasan dan mempererat kerjasama antar sesama karyawan melalui:
                                    </p>
                                    <ul class="colouredsquareslist">
                                        <li>
                                            Penerapan 5R (Rapi, Ringkas, Rawat, Resik, Rajin) dan kegiatan GKM (Gugus Kendali Mutu) internal perusahaan yang diadakan setahun sekali.
                                        </li>
                                        <li>
                                            Pelatihan untuk para karyawan PT. Avia Avian,  antara lain :<br/><br/>
                                            <ol class="alphabetlist">
                                                <li>
                                                    Fundamental ISO 9001 – 2000, Bali 2005.
                                                </li>
                                                <li>
                                                    Implementasi ISO 9001 – 2000, Tretes 2006.
                                                </li>
                                                <li>
                                                    Training Leadership of Personality, 5S, TQC, 7QC tools, Problem Solving, Tretes 2006.
                                                </li>
                                                <li>
                                                    Implementasi OHSAS 18001 – 2007, Surabaya 2007.
                                                </li>
                                                <li>
                                                    Gugus Kendali Mutu, Sidoarjo 2007.
                                                </li>
                                                <li>
                                                    “Coaching The Winner”, Trawas 2008.
                                                </li>
                                                <li>
                                                    Leading Change, Surabaya 2009.
                                                </li>
                                                <li>
                                                    Emotional &amp; Motivation Mindset Training, Trawas 2009.
                                                </li>
                                                <li>
                                                    Lean Manufacturing, Surabaya 2010.
                                                </li>
                                                <li>
                                                    Financial Planning, Surabaya 2010.
                                                </li>
                                                <li>
                                                    Safety Driving, Sidoarjo 2011.
                                                </li>
                                                <li>
                                                    Tertib Lalu Lintas “Think Safety”, Sidoarjo 2012.
                                                </li>
                                                <li>
                                                    Training EMS ISO 14001 – 2004, Sidoarjo 2012.
                                                </li>
                                            </ol>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
