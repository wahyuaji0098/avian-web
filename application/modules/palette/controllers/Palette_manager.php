<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Controller.
 */
class Palette_manager extends Baseadmin_Controller  {

    private $_title = "Palette";
    private $_title_page = '<i class="fa-fw fa fa-paint-brush"></i> Palette';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page = "palette";
    private $_back = "/manager/palette";
    private $_js_path = "/js/pages/palette/manager/";
    private $_view_folder = "palette/manager/";

    private $_table = "dtb_product";
    private $_table_aliases = "dp";
    private $_pk = "dp.id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product
    */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List  Palette </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li> Palette</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Product Category
    */
    public function create () {
        //load_model
        $this->load->model('Palette_model');
        $this->_breadcrumb .= '<li><a href="/manager/pallete">Palette</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Palette</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Palette</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "css"   => array(
                "/css/select2.min.css"
            )
        );

        $color = $this->Palette_model->get_data_color(array(
            "status"  => STATUS_ACTIVE,
        ));

        $data['colors'] = array(
            'data' => $color,
            'total' => count($color),
            'total_each' => ceil (count($color)/3),
            'total_col' => (count($color) > 3) ? 3 : 1,
        );
        // pr($data['colors']);exit;
        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create' , $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an product category
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/pallete">Palette</a></li>';

        //load the model.
        $this->load->model('Palette_model');
        $data['item'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();
        }

        $params = array("row_array" => true,"conditions" => array("id" => $id));
        //get the data.
        $data['item'] = $this->Palette_model->get_all_data($params)['datas'];

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //get data mst_pallete_product
        $data['item_product'] = $this->_dm->set_model("mst_palette_product", "mpp","id")->get_all_data(array(
            // "row_array" => true,
            "select"          => " mpp.*, dp.name",
            "left_joined"          => array("dtb_product dp" => array("dp.id" => "mpp.product_id")),
            // "debug"         => true,
            "conditions"      => array("palette_id" => $id),
        ))['datas'];

        $palette_color = $this->_dm->set_model("mst_palette_color", "mpc","id")->get_all_data(array(
            "conditions"    => array("palette_id" => $data['item']['id']),
            "count_all_first" => true
        ))['datas'];

        //conversion  array 2 dimension to 1 dimension
        $data['item_colors'] = array_column($palette_color, "color_id");

        //get data dtb_color
        $color = $this->Palette_model->get_data_color(array(
            "status"  => STATUS_ACTIVE,
        ));

        $data['colors'] = array(
            'data' => $color,
            'total' => count($color),
            'total_each' => ceil (count($color)/3),
            'total_col' => (count($color) > 3) ? 3 : 1,
        );

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Pallete</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Pallete</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "css"   => array(
                "/css/select2.min.css"
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    public function import () {
        $this->_breadcrumb .= '<li><a href="/manager/pallete">Palette</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Import Palette</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Import Palette</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/validate-extension.js",
                $this->_js_path . "import.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'import');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data product category
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //load model
        $this->load->model('Palette_model');

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("dpl.id", "dpl.name", "group_concat(dp.name) as prod", "dpl.code", "dpl.is_show", "visualizer_type");
        $joined = array(
            "mst_palette_product mpp"          => array("mpp.palette_id" => "dpl.id"),
            "dtb_product dp"          => array("mpp.product_id" => "dp.id")
        );
        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(dpl.id)'] = $value;
                        }
                        break;

                    case 'names':
                        if ($value != "") {
                            $data_filters['lower(dp.name)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $conditions['dpl.is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'in_filter':
                        if ($value != "") {
                            $conditions['show_in_filter'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'hot_item':
                        if ($value != "") {
                            $conditions['is_hot_item'] = ($value == "active") ? 0 : 1;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->Palette_model->get_all_data(array(
            'select'           => $select,
            'left_joined'      => $joined,
            'order_by'         => array($column_sort => $sort_dir),
            'limit'            => $limit,
            'start'            => $start,
            'conditions'       => $conditions,
            'filter'           => $data_filters,
            "group_by"         => array("dpl.id"),
            "count_all_first"  => true,
            "status"           => STATUS_ACTIVE,
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //load the model.
        $this->load->model('Palette_model');

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                 = $this->input->post('id');
        $product_id         = $this->input->post('product_id');
        $color_id           = $this->input->post('color_id');
        $name               = sanitize_str_input($this->input->post('name'));
        $code               = sanitize_str_input($this->input->post('code'));
        $is_show            = sanitize_str_input($this->input->post('is_show'));
        $meta_keys          = sanitize_str_input($this->input->post('meta_keys'));
        $meta_desc          = sanitize_str_input($this->input->post('meta_desc'));
        $pallete_for        = sanitize_str_input($this->input->post('pallete_for'));
        $visualizer_type    = sanitize_str_input($this->input->post('visualizer_type'));
        $data_image         = $this->input->post('data-image');

        $product_id         = (is_array($product_id) && !empty($product_id)) ? $product_id : [];
        $color_id           = (is_array($color_id) && !empty($color_id)) ? $color_id : [];
        // pr($this->input->post());exit();

        //server side validation.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {

            $this->load->library('Uploader');

            $upload_file = $this->upload_file("file_real", "" , false,"upload/pallete",$id);

            //image crop
            $image_url = $this->upload_image(false, "upload/pallete/thumb", "image-url", $data_image, 174, 164, $id);
            //begin transaction.
            $this->db->trans_begin();

            //validation success, prepare array to DB.
            $arrayToDB = array(
                'name'            => $name,
                'code'            => $code,
                'is_show'         => $is_show,
                'visualizer_type' => $visualizer_type,
                'meta_keys'       => $meta_keys,
                'meta_desc'       => $meta_desc
            );

            if (!empty($image_url)) {
                $arrayToDB['image_thumb'] = $image_url;
            }

            if(isset($upload_file['uploaded_path'])) {
                $arrayToDB['image_url'] = $upload_file['uploaded_path'];
            }

            //insert or update?
            if ($id == "") {

                $result = $this->Palette_model->insert($arrayToDB, array('is_version' => true));

                //insert pallete for product
                if(!empty($product_id)) {
                    foreach($product_id as $index => $prod_id) {
                        $insert_product  = array(
                            "product_id" => $prod_id,
                            "palette_id" => $result,
                        );

                        $this->_dm->set_model("mst_palette_product", "mpp", "id")->insert($insert_product, array("is_direct" => true));
                    }
                }


                //insert pallete for color
                if(!empty($color_id)) {
                    foreach($color_id as $key => $value) {
                        $insert_color  = array(
                            "color_id"   => $value,
                            "palette_id" => $result,
                        );

                        $this->_dm->set_model("mst_palette_color", "mpc", "id")->insert($insert_color, array("is_direct" => true));
                    }
                }

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Good!";
                    $message['notif_message'] = "New Palette has been added.";

                    //on insert, not redirected.
                    $message['redirect_to'] = "/manager/palette";
                }
            } else {
                //get current data
                $curr_data = $this->Palette_model->get_all_data(array(
                    "find_by_pk" => array($id),
                    "row_array"  => true,
                ))['datas'];

                //update.
                if (!empty($image_url) && isset($curr_data['image_thumb']) && !empty($curr_data['image_thumb'])) {
                    unlink( FCPATH . $curr_data['image_thumb'] );
                }

                //update.
                if (!empty($upload_file['uploaded_path']) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                    unlink( FCPATH . $curr_data['image_url'] );
                }

                //conditions for update
                $conditions = array("id" => $id);
                $result = $this->Palette_model->update($arrayToDB, $conditions, array('is_version' => true));

                //new insert pallete for product
                if(!empty($product_id)) {
                    //conditions delete
                    $params = array(
                        "palette_id" => $id,
                    );
                    //delete data in mst_palette_product
                    $this->_dm->set_model("mst_palette_product", "mpp", "id")->delete($params, array("is_permanently" => true));
                    //re insert product
                    foreach($product_id as $index => $prod_id) {
                        $insert_product  = array(
                            "product_id" => $prod_id,
                            "palette_id" => $id,
                        );
                        $this->_dm->set_model("mst_palette_product", "mpp", "id")->insert($insert_product, array("is_direct" => true));
                    }
                } else {
                    $this->_dm->set_model("mst_palette_product", "mpp", "id")->delete(array(
                        "palette_id" => $id,
                    ), array("is_permanently" => true));
                }


                //new insert pallete for color
                if(!empty($color_id)) {
                    //conditions delete
                    $params = array(
                        "palette_id" => $id,
                    );
                    //delete data in mst_palette_colors
                    $this->_dm->set_model("mst_palette_color", "mpc", "id")->delete($params, array("is_permanently" => true));
                    //re insert color
                    foreach($color_id as $key => $value) {
                        $insert_color  = array(
                            "color_id"   => $value,
                            "palette_id" => $id,
                        );

                        $this->_dm->set_model("mst_palette_color", "mpc", "id")->insert($insert_color, array("is_direct" => true));
                    }
                }

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $message['error_msg'] = 'database operation failed.';

                } else {
                    $this->db->trans_commit();

                    $message['is_error'] = false;

                    //success.
                    //growler.
                    $message['notif_title'] = "Excellent!";
                    $message['notif_message'] = "Palette has been updated.";

                    //on update, redirect.
                    $message['redirect_to'] = "/manager/palette";
                }
            }
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an palette.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //load the model.
        $this->load->model('Palette_model');

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->Palette_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $this->Palette_model->delete($condition, array("is_version" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Pallete has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function export () {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //load model and library php excel
        $this->load->library("PHPExcel_reader");
        $this->load->model("palette/Palette_model");

        $data_merge = array();
        $data_merge[] = array("palette_id", "palette_name", "products", "colours");


        //get all data to be exported
        $result = $this->Palette_model->get_all_data(array(
            "select" => array("id", "name", "group_concat(pallete_for) as pallete_for", "color_ids"),
            "group_by" => "name",
        ))['datas'];

        //merge array

        if (count($result) > 0) {
            $all_data = array_merge($data_merge,$result);
            $exlc = new PHPExcel_reader();
            $data = $exlc->exportToExcelSingleSheets($all_data,"Export Palette ". strtotime(date('Y-m-d H:i:s')), null, null,'A', 'D', 2, count($all_data));

            $message['is_error'] = false;
            $message['filename'] = "Export Palette ". strtotime(date('Y-m-d H:i:s'));
            $message['file_data'] = "data:application/vnd.ms-excel;base64,".base64_encode($data);

            $message['notif_title'] = "Sukses";
            $message['notif_message'] = "Download Sukses";

        } else {
            $message['is_error'] = true;
            $message['error_msg'] = "Tidak ada Data.";
            $message['redirect_to'] = "";
        }

        //encoding and returning.
        echo json_encode($message);
        exit;
    }

    public function process_import () {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";
        $validate = "";
        $error_validate_flag = false;
        $insert_datas = array();

        //check validation
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > 0) {
            $filename = $_FILES['file']['tmp_name'];
            //check max file upload
            if ($_FILES['file']['size'] > MAX_IMPORT_FILE_SIZE) {
                $message['error_msg'] = "Maximum file size is ".MAX_IMPORT_FILE_SIZE_IN_KB;
            } elseif (mime_content_type($filename) != "application/vnd.ms-excel"
                        && mime_content_type($filename) != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                //check extension if xls dan xlsx
                $message['error_msg'] = "File must .xls or .xlsx";
            } else {
                //load and init library
                $this->load->library("PHPExcel_reader");
                $excel = new PHPExcel_reader();

                //read the file and give back the result of the file in array
                $result = $excel->read_from_excel($filename);

                if ($result['is_error'] == true) {
                    $message['redirect_to'] = "";
                    $message['error_msg']   = $result['error_msg'];
                } else {

                    //begin transaction
                    $this->db->trans_begin();

                    $total_data = count($result['datas']);

                    $extra_param = array(
                        "is_direct" => true,
                        "is_batch" => true,
                    );

                    if ($total_data > 0) {
                        //loop the result and insert to db
                        $line = 2;

                        foreach ($result['datas'] as $model) {
                            if (count($model) < 4) {
                                $validate = "Invalid Template.";
                                $error_validate_flag = true;
                                break;
                            } else {
                                //check untuk semua kolom apakah null
                                if (empty($model[0]) && empty($model[1]) &&  empty($model[2]) &&  empty($model[3])) {
                                    // klo kosong semua isinya, skip
                                    $line++;
                                } else {
                                    // persiapan data
                                    $palette_id = trim($model[0]);
                                    $palette_products = explode(",",trim($model['2']));
                                    $colors = json_decode(trim($model['3']), true);

                                    $data_products = array();
                                    $data_colors = array();

                                    if (count($palette_products) > 0) {
                                        foreach ($palette_products as $val) {
                                            $data_products[] = array(
                                                "product_id" => $val,
                                                "palette_id" => $palette_id,
                                            );
                                        }

                                        //insert to mst_palette_product
                                        //delete first
                                        $this->_dm->set_model("mst_palette_product", "mpp", "id")->delete(array("palette_id" => $palette_id), array("is_permanently" => true));
                                        $this->_dm->set_model("mst_palette_product", "mpp", "id")->insert($data_products,$extra_param);
                                    }

                                    if (count($colors) > 0) {
                                        foreach ($colors as $val) {
                                            $data_colors[] = array(
                                                "color_id" => $val,
                                                "palette_id" => $palette_id,
                                            );
                                        }

                                        //insert to mst_palette_color
                                        $this->_dm->set_model("mst_palette_color", "mpc", "id")->delete(array("palette_id" => $palette_id), array("is_permanently" => true));
                                        $this->_dm->set_model("mst_palette_color", "mpc", "id")->insert($data_colors,$extra_param);
                                    }

                                    $line++;
                                }
                            }
                        }
                    }

                    //update version Palette
                    $this->_dm->set_model("dtb_pallete", "dp", "id")->update(array("id" => $palette_id), array("id" => $palette_id), array("is_version" => true));

                    if ($this->db->trans_status() === false || $error_validate_flag == true) {
                        $this->db->trans_rollback();
                        $message['redirect_to'] = "";

                        if ($error_validate_flag == true) {
                            $message['error_msg'] = $validate;
                        } else {
                            $message['error_msg'] = 'database operation failed.';
                        }
                    } else {
                        $this->db->trans_commit();

                        $message['is_error'] = false;
                        // success.
                        $message['notif_title']   = "Good!";
                        $message['notif_message'] = "Data Palette has been imported.";

                        // on insert, not redirected.
                        $message['redirect_to'] = "";
                    }
                }
            }
        } else {
            $message['error_msg'] = 'File is empty (xls or xlsx).';
        }

        echo json_encode($message);
        exit;

    }

    ///////////////////////// AJAX CALL FROM OTHER //////////////////////////////


    //get list product
    public function list_product()
    {
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //Process
        $select_q    = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;
        $select_q    = $select_q;
        $select_page = is_numeric($select_page) ? $select_page : 1;

        $limit       = 10;
        $start       = ($limit * ($select_page - 1));
        $filters     = array();
        if ($select_q != "") {
            $filters["name"] = $select_q;
        }

        $conditions = array( );
        $datas = $this->_dm->set_model("dtb_product","dp","id")->get_all_data(array(
            "select"          => array("name","id"),
            "conditions"      => $conditions,
            "filter_or"       => $filters,
            "count_all_first" => true,
            "limit"           => $limit,
            "start"           => $start,
            "status"          => 1,
            // "distinct"        => true,
        ));
        // pr($datas);exit;
        //  Prepare OUTPUT
        $message["page"]        = $select_page;
        $message["total_data"]  = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"]       = $datas['datas'];

        //  Send OUTPUT
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
