<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Controller.
 */
class Palimages_manager extends Baseadmin_Controller  {

    private $_title = "Palette Images";
    private $_title_page = '<i class="fa-fw fa fa-paint-brush"></i> Palette Images';
    private $_breadcrumb = "<li><a href='".MANAGER_HOME."'>Home</a></li><li><a href='/manager/palette/'>Palette</a></li>";
    private $_active_page = "pallete";
    private $_back = "/manager/palette/palimages/lists";
    private $_js_path = "/js/pages/palette/manager/palimages/";
    private $_view_folder = "palette/manager/palimages/";

    private $_table = "dtb_product";
    private $_table_aliases = "dp";
    private $_pk = "dp.id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product
    */
    public function lists($id = null) {

        //first get data table dtb_pallete
        $data['pallete'] = $this->_dm->set_model("dtb_pallete", "dp", "id")->get_all_data(array(
            "row_array"  => true,
            "conditions" => array("id" => $id),
        ))['datas'];

        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . "<span>> List  Palette Images (".$data['pallete']['name'].")</span>",
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li> Palette Images</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        // get id
        $data['pallete_id'] = $id;

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index', $data);
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Product Category
    */
    public function create ($id = null) {

        $data['pallete'] = $this->_dm->set_model("dtb_pallete", "dp", "id")->get_all_data(array(
            "row_array"  => true,
            "conditions" => array("id" => $id),
        ))['datas'];

        $this->_breadcrumb .= '<li><a href="/manager/palette/palimages/lists/'. $id .'">Palette Images ('.$data['pallete']['name'].')</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Palette Images ('.$data['pallete']['name'].')</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Palette Images</li>',
            "back"          => $this->_back . "/" . $id,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            ),
            "css"   => array(
                "/css/select2.min.css"
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create' , $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////
    /**
     * Set validation rule for admin create and edit
     */
    private function _set_rule_validation($id) {

        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("file_real", "Image Real", "required");
        $this->form_validation->set_rules("file_small", "Image Small", "required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data product category
     */
    public function list_all_data($id = null) {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array("dpi.id","dp.name as nama_pallete","dpi.image_url", "dpi.image_thumb", "pallete_id");
        $joined = array("dtb_pallete dp" => array("dp.id" => "dpi.pallete_id"));
        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(dpi.id)'] = $value;
                        }
                        break;

                    case 'pallete_id':
                        if ($value != "") {
                            $data_filters['lower(dp.name)'] = $value;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        // pr($palette);exit;
        $conditions = array('dpi.pallete_id' => $id);

        //get data
        $datas = $this->_dm->set_model("dtb_pallete_images","dpi", "id")->get_all_data(array(
            'select'           => $select,
            'joined'           => $joined,
            'order_by'         => array($column_sort => $sort_dir),
            'limit'            => $limit,
            'start'            => $start,
            'conditions'       => $conditions,
            'filter'           => $data_filters,
            "count_all_first"  => true,
            "debug"            => false
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        $palette_id    = $this->input->post('pallete_id');
        $data_image  = $this->input->post('data-image');
        //upload file real
        $upload_file = $this->upload_file("file_real", "" , false,"upload/pallete_image", "");

        //upload image thumb
        $image_url = $this->upload_image(false, "upload/pallete_image/thumb", "file_small", $data_image, 174, 164, "");
        //begin transaction.
        $this->db->trans_begin();

        $arrayToDB = array(
            "pallete_id" => $palette_id,
        );

        // pr($uploaded_file);exit;
        if (!empty($image_url)) {
            $arrayToDB['image_thumb'] = $image_url;
        }

        if(isset($upload_file['uploaded_path'])) {
            $arrayToDB['image_url'] = $upload_file['uploaded_path'];
        }

        //insert to DB.
        $result = $this->_dm->set_model("dtb_pallete_images", "dpi", "id")->insert($arrayToDB, array('is_direct' => true));

        //end transaction.
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';

        } else {
            $this->db->trans_commit();

            $message['is_error'] = false;

            //success.
            //growler.
            $message['notif_title'] = "Excellent!";
            $message['notif_message'] = "Pallete Images has been added.";

            //on update, redirect.
            $message['redirect_to'] = "/manager/palette/palimages/lists/".$palette_id;
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an palette.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->_dm->set_model("dtb_pallete_images", "dpi", "id")->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                if (isset($data['image_thumb']) && !empty($data['image_thumb'])) {
                    unlink (FCPATH.$data['image_thumb']);
                }

                if (isset($data['image_url']) && !empty($data['image_url'])) {
                    unlink (FCPATH.$data['image_url']);
                }

                //delete the data (deactivate)
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model("dtb_pallete_images", "dpi", "id")->delete($condition, array("is_permanently" => true));

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Pallete images has been deleted.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    ///////////////////////// AJAX CALL FROM OTHER //////////////////////////////


    //get list product
    public function list_product()
    {
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //Process
        $select_q    = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;
        $select_q    = $select_q;
        $select_page = is_numeric($select_page) ? $select_page : 1;

        $limit       = 10;
        $start       = ($limit * ($select_page - 1));
        $filters     = array();
        if ($select_q != "") {
            $filters["name"] = $select_q;
        }

        $conditions = array( );
        $datas = $this->_dm->set_model("dtb_product","dp","id")->get_all_data(array(
            "select"          => array("name","id"),
            "conditions"      => $conditions,
            "filter_or"       => $filters,
            "count_all_first" => true,
            "limit"           => $limit,
            "start"           => $start,
            "status"          => 1,
            // "distinct"        => true,
        ));
        // pr($datas);exit;
        //  Prepare OUTPUT
        $message["page"]        = $select_page;
        $message["total_data"]  = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"]       = $datas['datas'];

        //  Send OUTPUT
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
