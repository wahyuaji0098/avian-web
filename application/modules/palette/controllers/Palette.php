<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Palette extends Basepublic_Controller {

    private $_view_folder = "palette/front/";

    function __construct() {
        parent::__construct();

        //list palette
        $this->load->model("Palette_model");

    }

    public function lists() {
  
        redirect(base_url('color'));
  //       $product_url = $this->uri->segment(3, 0);

		// //get header page
  //       $page = get_page_detail('palette-lists');

		// $model_palette = new Palette_model ();
		// $search = $this->session->userdata('pallete_search');
		// $filter = $this->session->userdata('pallete_filter');
  //       $conditions = array(
  //           "is_show" => SHOW,
  //       );

		// if (!empty($product_url)) {
		// 	$prod = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
  //               "conditions" => array(
  //                   "is_show" => SHOW,
  //                   "pretty_url" => $product_url,
  //               ),
  //               "status" => STATUS_ACTIVE,
  //               "row_array" => true,
  //           ))['datas'];
		// 	$filter[] = $prod['id'];
		// }

		// $this->session->set_userdata('pallete_filter',$filter);

  //       if (count($filter) > 0) {
  //           $data_filter = implode("','", $filter);
  //           $conditions["id in ( Select palette_id from mst_palette_product where product_id in ('". $data_filter ."'))"] = NULL;
  //       }

  //       $limit = 5;
  //       $start = 0;

		// $data_palletes = $model_palette->get_all_data(array(
  //           "order_by" => array("name" => "desc"),
  //           "limit" => $limit,
  //           "start" => $start,
  //           "conditions" => $conditions,
  //           "filter_or" => array(
  //               "name" => $search
  //           ),
  //           "status" => STATUS_ACTIVE,
  //           "count_all_first" => true,
  //           "select" => array('id', 'name', 'color_ids', 'image_url', 'image_thumb'),
  //       ));

  //       $palletes = $model_palette->getDetailedProductPallete ($data_palletes['datas']);
		// $total = $data_palletes['total'];
  //       $total_page = ceil($total/$limit);

		// //get data likebox
		// // if (isset($this->data_login_user['id']))
		// // 	$color_likebox = find_like_by_member($this->data_login_user['id'],LIKEBOX_COLOR);
		// // else
		// // 	$color_likebox = array();

		// //get avail product pallete
		// $prod_pallete = $model_palette->getAllProductPallete($filter);

  //       $header = array(
  //           'header'            => $page,
  //           "datas"             => $palletes,
  //           'pallete_page'      => $total_page,
		// 	'palette_search'    => $search,
		// 	// 'color_likebox'     => $color_likebox,
  //           'prod_pallete'      => $prod_pallete,
		// 	'filter'            => $filter,
  //       );

  //       $footer = array(
  //           "script" => array(
  //               "/js/plugins/lightbox/js/lightbox.min.js",
  //               "/js/front/palette-list.js"
  //           ),
  //           "css" => array(
  //               "/js/plugins/lightbox/css/lightbox.min.css",
  //           )
  //       );

  //       //load the views.
  //       $this->load->view(FRONT_HEADER, $header);
  //       $this->load->view($this->_view_folder . 'lists');
  //       $this->load->view(FRONT_FOOTER, $footer);


	}

	public function loadmore_list () {
        //check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $page = $this->input->post("page");
        $search = $this->input->post("search");
		$filter = $this->input->post("filter");
        $limit = 5;
        $start = ($limit * ($page - 1));
		$where = array(
			"is_show" => SHOW,
		);

        $this->session->set_userdata('pallete_search', $search);
		$this->session->set_userdata('pallete_filter', $filter);

        if ($search == "") {
            $this->session->unset_userdata('pallete_search');
        }

        if ($filter == "") {
            $this->session->unset_userdata('pallete_filter');
        }

		$search = $this->session->userdata('pallete_search');
		$filter = $this->session->userdata('pallete_filter');

        $filter_or = array();

        if (!empty($search)) {
            $filter_or = array(
                "name" => $search,
                "code" => $search,
            );
        }

        if (!empty($filter)) {
            $filter = implode("','", $filter);

            $where["id in ( Select palette_id from mst_palette_product where product_id in ('" . $filter . "'))"] = NULL;
        }

        //load model
        $this->load->model("palette/Palette_model");

		$model_palette = new Palette_model ();
        $palletes = $model_palette->get_all_data(array(
            "order_by" => array("name" => "desc"),
            "limit" => $limit,
            "start" => $start,
            "conditions" => $where,
            "filter_or" => $filter_or,
            "status" => STATUS_ACTIVE,
            "count_all_first" => true,
            "select" => array('id', 'name', 'color_ids', 'image_url', 'image_thumb'),
            // "group_by" => array("name"),
        ));

        $lists = $palletes['datas'];
		$total = $palletes['total'];
        $total_page = ceil($total/$limit);

        $models = array();

        if (count($lists) > 0) {
            foreach ($lists as $model) {
				$colours = array();
				$color = json_decode($model['color_ids']);

				$model['colours'] = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data(array(
                    "conditions" => array(
                        "id in (select color_id from mst_palette_color where palette_id = '".$model['id']."')" => NULL,
                    ),
                    "limit" => '4'
                ))['datas'];

				//get pallete images
				$model['pal_images'] = $this->_dm->set_model("dtb_pallete_images" , "dpi", "id")->get_all_data(array(
                    "conditions" => array('pallete_id' => $model['id']),
                ))['datas'];

                array_push ($models, $model);
            }
        }

        echo json_encode(array(
            "result" => "OK",
            "datas" => $models,
            "total_page" => $total_page,
        ));

		exit;
    }

	public function detail ($id) {
		//get id
		if (empty($id)) {
			show_404('page');
		}

        $this->load->model("palette/Palette_model");

		$model_palette = new Palette_model ();
		$palette = $model_palette->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
            "status" => STATUS_ACTIVE,
            "conditions" => array("is_show" => SHOW),
        ))['datas'];

		if (empty($palette)) {
			show_404('page');
		}

		$limit = 20;
		$start = 0;
        $search = $this->session->userdata('palette_detail_search');

        $filter_or = array();

        if (!empty($search)) {
            $filter_or = array(
                "name" => $search,
                "code" => $search,
            );
        }

		$data = $this->_dm->set_model("dtb_color","dc","id")->get_all_data(array(
            "order_by" => array("id" => "asc"),
            "limit" => $limit,
            "start" => $start,
            "conditions" => array(
    			"is_show" => SHOW,
    			"id in ( select color_id from mst_palette_color where palette_id = '".$palette['id']."'  )" => NULL
    		),
            "filter_or" => $filter_or,
            "status" => STATUS_ACTIVE,
            "count_all_first" => true,
        ));
        $lists = $data['datas'];
		$total = $data['total'];
        $total_page = ceil($total/$limit);

		// //get data likebox
		// if (isset($this->data_login_user['id']))
		// 	$color_likebox = find_like_by_member($this->data_login_user['id'],LIKEBOX_COLOR);
		// else
		// 	$color_likebox = array();

        $page = array(
			"title"      => $palette['name'],
			"meta_desc"  => $palette['meta_desc'],
			"meta_keys"  => $palette['meta_keys'],
		);

        $header = array(
            'header'             => $page,
            'models'             => $lists,
			'colours_page'       => $total_page,
			'colours_search'     => $search,
			'palette'            => $palette,
			// 'color_likebox'      => $color_likebox,
        );

        $footer = array(
            "script" => array(
                "/js/front/palette-detail.js"
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'detail');
        $this->load->view(FRONT_FOOTER, $footer);
	}

	public function loadmore_detail () {
        //check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $id = $this->input->post("id");
        $page = $this->input->post("page");
        $search = $this->input->post("search");

        $this->session->set_userdata('palette_detail_search', $search);

        if ($search == "") {
            $this->session->unset_userdata('palette_detail_search');
        }

		$search = $this->session->userdata('palette_detail_search');

        $this->load->model("palette/Palette_model");

		$model_palette = new Palette_model ();
        $palette = $model_palette->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array" => true,
            "status" => STATUS_ACTIVE,
            "conditions" => array("is_show" => SHOW),
        ))['datas'];

		if (empty($palette)) {
			echo json_encode(array(
				"result" => "OK",
				"datas" => array(),
			));

			exit;
		}

        $limit = 20;
        $start = ($limit * ($page - 1));

        $filter_or = array();

        if (!empty($search)) {
            $filter_or = array(
                "name" => $search,
                "code" => $search,
            );
        }

        $colours = $this->_dm->set_model("dtb_color","dc","id")->get_all_data(array(
            "order_by" => array("id" => "asc"),
            "limit" => $limit,
            "start" => $start,
            "conditions" => array(
    			"is_show" => SHOW,
    			"id in ( select color_id from mst_palette_color where palette_id = '".$palette['id']."'  )" => NULL
    		),
            "filter_or" => $filter_or,
            "status" => STATUS_ACTIVE,
            "count_all_first" => true,
        ));

        $lists = $colours['datas'];
		$total = $colours['total'];
        $total_page = ceil($total/$limit);

		// $models = array();
        //
		// if (count($lists) > 0) {
		// 	foreach($lists as $model) {
		// 		//check if this model is in colour list
		// 		$exist = find_like_by_member ($this->data_login_user['id'],LIKEBOX_COLOR,$model['id']);
		// 		$model['is_like'] = ($exist) ? 1  : 0;
        //
		// 		array_push($models,$model);
		// 	}
		// }

        echo json_encode(array(
            "result" => "OK",
            "datas" => $lists,
            "total_page" => $total_page,
        ));

		exit;
    }

}
