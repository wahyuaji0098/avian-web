<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Palette_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = 'dtb_pallete';
        $this->_table_alias = 'dpl';
        $this->_pk_field = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    public function getColorPalleteDeleted () {
		$this->db->select("distinct(color_ids) as color_ids");
		$this->db->where("status", STATUS_DELETE);

		$pallete = $this->db->get($this->_table)->result_array();

		$colors = array();

		if (count($pallete)> 0) {
			foreach ($pallete as $palete) {
				if ($palete['color_ids'] != "") {
					$palete['color_ids'] = json_decode($palete['color_ids']);

					$colors = array_merge($colors,$palete['color_ids']);
				}
			}
		}

		return $colors;
	}

    public function getAllProductPallete($filter = false) {
        $category = $this->db->query("select id, name from dtb_product_category where is_show = ? and id in (select distinct(p.product_category_id) from dtb_product p left join dtb_pallete pal on p.id = pal.pallete_for where p.is_show = ? and p.status = ? and pal.status = ? and pal.is_show = ?) order by ordering asc", array(SHOW, SHOW, STATUS_ACTIVE, STATUS_ACTIVE, SHOW))->result_array();

        $models = array();

        if (count($category) > 0) {
            foreach ($category as $model) {
                //get product by category id
                $model['product'] = $this->db->query("
                    select
                        distinct(m_palette.product_id),
                        t_prod.name
                    from
                        mst_palette_product m_palette
                        left join dtb_product t_prod on m_palette.product_id = t_prod.id
                        left join dtb_pallete tpa on tpa.id = m_palette.palette_id
                    where
                        t_prod.is_show = ?
                        and t_prod.status = ?
                        and tpa.status = ?
                        and t_prod.product_category_id = ?
                    order by
                        t_prod.ordering asc
                ", array(SHOW, STATUS_ACTIVE, STATUS_ACTIVE, $model['id']))->result_array();

                if(is_array($filter)) $model['set_active'] = searchForIdInt($filter[0], $model['product'], 'product_id');
                else $model['set_active'] = null;

                array_push($models, $model);
            }
        }

        return $models;
    }

    public function getDetailedProductPallete ($result) {
        $this->load->model("Dynamic_model");
        $models = array();

        if (count($result) > 0) {
            foreach ($result as $model) {
				$colours = array();
				$color = json_decode($model['color_ids']);

                $model['colours'] = $this->Dynamic_model->set_model("dtb_color", "dc", "id")->get_all_data(array(
                    "conditions" => array(
                        "id in (select color_id from mst_palette_color where palette_id = '".$model['id']."')" => NULL,
                    ),
                    "limit" => '4'
                ))['datas'];

				//get pallete images
				$model['pal_images'] = $this->Dynamic_model->set_model("dtb_pallete_images", "dpi", "id")->get_all_data(array(
                    "conditions" => array(
                        'pallete_id' => $model['id'],
                    )
                ))['datas'];

                array_push ($models, $model);
            }
        }

        return $models;
    }

    public function getPalleteWithColorById ($pall_id) {
		$this->db->where('status', STATUS_ACTIVE);
        $this->db->where('is_show', SHOW);
        $this->db->where('id', $pall_id);
		$this->db->group_by('name');

		$pallete = $this->db->get($this->_table)->row_array();

		$models = array();

		$color_ids = json_decode($pallete['color_ids']);
		$pallete['colors'] = $this->Dynamic_model->set_model("dtb_color", "dc", "id")->get_all_data(array(
            "conditions" => array(
                "id in (select color_id from mst_palette_color where palette_id = '".$pall_id."')" => NULL,
            ),
        ))['datas'];

		return $pallete;
	}
    /**
     * get a data dtb_color
     * @param  boolean $conditions [mixed parameter]
     * @return array
     */
    public function get_data_color($conditions = false)
    {
        if($conditions) {
            $this->db->where($conditions);
        }

        return $this->db->get("dtb_color")->result_array();
    }
}
