<div class="container">
    <div class="section section-crumb mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/colours">WARNA</a>
                <span>KARTU WARNA</span>
            </div>
        </div>
    </div>
    <div class="section section-smargin">
        <div class="section_content">
            <div class="searchcontain">
                <div class="searchbox">
                    <input type="text" placeholder="Cari Nama Kartu Warna" id="search-palette" value="<?= $palette_search ?>"/>
                </div>
                <button class="btn" type="button" id="search-btn"/>Cari</button>
                <button class="btn btn-sml" type="button" id="clear-btn"/>Hapus</button>
            </div>
        </div>
    </div>
     <div class="section">
        <div class="section_content">
            <div class="content_sbar">
                <div class="sidebar sidebar-folded targetfold1">
                    <div class="sidebar-title trigger" targid="fold1">PRODUK</div>
                    <div class="sidebar-accordion">
                        <?php
                            if(count($prod_pallete) > 0):
                                $i = 1;
                                foreach ($prod_pallete as $cate):
                                    if (count($cate['product']) > 0):
                        ?>
                        <div class="accgroup <?= ($cate['set_active'] !== null) ? "active" : "" ?> target<?= $cate['id'] ?> targroup1">
                            <div class="accgroupname triggergroup" targid="<?= $cate['id'] ?>" targroup="1"><?= $cate['name'] ?></div>
                            <div class="accgroupcont">
                                <?php foreach ($cate['product'] as $prd):  ?>
                                <a class="accgroupitem clk_product <?= (is_array($filter) && array_search($prd['product_id'], $filter) !== false) ? "active" : "" ?>" data-id="<?= $prd['product_id'] ?>"><?= $prd['name'] ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php       endif;
                                    $i++;
                                endforeach;
                            endif;
                        ?>

                    </div>
                    <div class="lead"></div>
                </div>
                <div class="sbarcontent gridds">
                    <div class="griddscontent">
                        <div class="grid-sizer"></div>
                        <div class="gutter-sizer"></div>
                        <?php if(count($datas) > 0): foreach($datas as $model): ?>
                        <div class="griddsitem width-10 height-1 gridsep">
                            <h3 class="grid-section-title"><?= $model['name'] ?></h3>
                        </div>
                        <?php if($model['image_thumb']): ?>
                        <div class="griddsitem gridcol width-2 height-3">
                            <a href="<?= $model['image_url'] ?>" class="colourbox trigger" data-lightbox="image-<?= $model['id'] ?>" data-title="<?= $model['name'] ?>">
                                <div class="colourboxcover">
                                    <img src="<?= $model['image_thumb'] ?>" alt="<?= $model['name'] ?>" />
                                </div>
                            </a>
                        </div>
                        <?php endif; ?>
                        <?php if(count($model['pal_images']) > 0): foreach($model['pal_images'] as $pal): ?>
                        <div class="griddsitem gridcol width-2 height-3">
                            <a href="<?= $pal['image_url'] ?>" class="colourbox trigger" data-lightbox="image-<?= $model['id'] ?>" data-title="<?= $model['name'] ?>">
                                <div class="colourboxcover">
                                    <img src="<?= $pal['image_thumb'] ?>" alt="<?= $model['name'] ?>" />
                                </div>
                            </a>
                        </div>
                        <?php endforeach; endif; ?>
                        <?php if(count($model['colours']) > 0): ?>
                        <?php foreach($model['colours'] as $col): ?>
                        <div class="griddsitem gridcol width-2 height-3">
                            <div class="colourbox trigger coltrigger" targid="colpop" colcode="<?= $col['code'] ?>" colid="<?= $col['id'] ?>"
                            colexist="<?php // (count($color_likebox) > 0 && array_search($col['id'],$color_likebox) !== FALSE) ? 1 : 0  ?>">
                                <div class="colourboxtint autopalette" colhex="<?= $col['hex_code'] ?>"></div>
                                <div class="colourboxcode"><?= $col['code'] ?></div>
                                <div class="colourboxname"><?= $col['name'] ?></div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <div class="griddsitem gridcol width-2 height-3">
                            <a href="/palette/detail/<?= $model['id'] ?>" class="colourbox seemore">
                                <div><span>Lihat Semuanya</span></div>
                            </a>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; endif; ?>
                    </div>
                </div>
                <div class="lead"></div>
            </div>
        </div>
    </div>
    <div class="section section-thirds">
        <div class="section_content">
            <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
        </div>
    </div>
    <input type="hidden" id="total_page" value="<?= $pallete_page ?>" />
</div>
<div class="modal-contain targetcolpop">
    <div class="modal-shade modal-shade-white trigger" targid="colpop"></div>
    <div class="modal-window modal-col colhextarg">
        <div class="modal-content">
            <div class="modal-close trigger" targid="colpop"></div>
            <div class="modal-colinfo">
                <div class="modal-colname colnametarg">
                    Colour Name
                </div>
                <div class="modal-colcode colcodetarg">
                    0000000
                </div>
            </div>
        </div>
    </div>
</div>
