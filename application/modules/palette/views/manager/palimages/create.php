<?php
    $id              = isset($pallete['id']) ? $pallete['id'] : "";
    $image_url       = isset($pallete["image_url"]) ? $pallete["image_url"] : "";
    $img_thumb       = isset($pallete["image_thumb"]) ? $pallete["image_thumb"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
    // pr($pallete['id']);exit;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2>Create Palette Image </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/palette/palimages/process-form" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="pallete_id" value="<?= $id ?>">
                                <fieldset>
                                    <section>
                                         <label class="label">Image Real <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="file_real" id="file_real" type="file"  class="form-control" value="" />
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Image Small <sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-url">

                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimageurl" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"> Add</button>
                                        </div>
                                    </section>
                                </fieldset>
                            </form>

                        </div>
                        <!-- end widget content -->
                    </div>
                <!-- end widget div -->
                </article>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
