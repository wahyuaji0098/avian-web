<style>
    .span-color {height: 12px;width: 12px;display: inline-block;margin-right: 5px;}
</style>
<?php
    $id              = isset($item["id"]) ? $item["id"] : "";
    $title           = isset($item["name"]) ? $item["name"] : "";
    $code            = isset($item['code']) ? $item['code'] : "";
    $image_url       = isset($item["image_url"]) ? $item["image_url"] : "";
    $img_thumb       = isset($item["image_thumb"]) ? $item["image_thumb"] : "";
    $is_show         = isset($item["is_show"]) ? $item["is_show"] : false;
    $show_visualizer = isset($item["visualizer_type"]) ? $item["visualizer_type"] : false;
    $meta_keys       = isset($item["meta_keys"]) ? $item["meta_keys"] : "";
    $meta_desc       = isset($item["meta_desc"]) ? $item["meta_desc"] : "";
    $created_date    = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date    = isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date    = isset($item["deleted_date"]) ? $item["deleted_date"] : "";
    $status_name = isset($item["status_name"]) ? $item["status_name"] : "";

    $color_id        = isset($item_color['color_id']) ? $item_color['color_id'] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
    // pr($product_id);exit();
    // pr($product_name[0]['name']);exit;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
            <h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
                    <i class="fa fa-arrow-circle-left fa-lg"></i>
                </button>
                <button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
                    <i class="fa fa-floppy-o fa-lg"></i>
                </button>
            </h1>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Palette </h2>

                    </header>

                    <!-- widget div-->
                    <div>
                        <form class="smart-form" id="create-form" action="/manager/palette/process-form" method="post" enctype="multipart/form-data">
                                <?php if($id != 0): ?>
                                    <input type="hidden" name="id" value="<?= $id ?>" />
                                <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Palette for product </label>
                                        <label class="select">
                                            <?php if(!empty($id)): ?>
                                            <select name="product_id[]" multiple="multiple" class="sel2product" style="width:100%">
                                               <?php
                                                    foreach($item_product as $key => $value) {
                                                        echo '<option value="'.$value['product_id'].'" selected="selected">'.$value['name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                            <?php else:?>
                                            <select name="product_id[]" multiple="multiple" class="sel2product" style="width:100%">
                                            </select>
                                            <?php endif;?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Show in Visualizer </label>
                                        <label class="input">
                                            <?php echo select_visualizer_type('visualizer_type', $show_visualizer); ?>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?= $title ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Code</label>
                                        <label class="input">
                                            <input name="code" type="text"  class="form-control" placeholder="Code" value="<?= $code ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Image real file</label>
                                        <label class="input">
                                            <input name="file_real" type="file" class="form-control"/>
                                        </label>
                                        <?php if($image_url): ?>
                                            <br>
                                            <img src="<?= $image_url ?>" height="100px"/>
                                        <?php endif; ?>
                                    </section>
                                    <section>
                                        <label class="label">Image Small<sup class="color-red">*</sup></label>
                                        <div class="input">
                                            <div class="add-image-preview" id="preview-image-url">
                                                <?php if($img_thumb): ?>
                                                <img src="<?= $img_thumb ?>" height="100px"/>
                                                <?php endif; ?>
                                            </div>
                                            <button type="button" class="btn btn-primary btn-sm" id="addimageurl" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($img_thumb != "") ? "Change" : "Add" ?> Image</button>
                                        </div>
                                    </section>
                                    <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?= select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                                <fieldset>
                                    <section>
                                        <label class="label">Meta description </label>
                                        <label class="textarea">
                                            <textarea name="meta_desc" id="meta_desc" class="form-control" placeholder="untuk SEO, deskripsi dari Tips / Article, sebaiknya 70 karakter minimum."><?= $meta_desc ?></textarea>
                                        </label>
                                    </section>

                                    <section>
                                        <label class="label">Meta keywords </label>
                                        <label class="textarea">
                                            <textarea name="meta_keys" id="meta_keys" class="form-control" placeholder="Kata kunci untuk SEO dari Tips / Article"><?= $meta_keys ?></textarea>
                                        </label>
                                    </section>
                                </fieldset>
                                <?php if(!empty($id)):?>
                                <fieldset>
                                    <section>
                                        <label class="label">Colors</label>
                                        <div class="row">
                                            <?php
                                                $last_number = 0;
                                                for ($i=0; $i < 3; $i++): ?>
                                                <section class="col col-4">
                                                    <?php
                                                        for($j=$last_number; $j< $colors['total_each']+$last_number; $j++):
                                                            if (isset($colors['data'][$j])):
                                                    ?>
                                                    <label class="checkbox">
                                                        <input type="checkbox" class="colors" name="color_id[<?= $colors['data'][$j]['id'] ?>]" value="<?= $colors['data'][$j]['id'] ?>" <?= in_array($colors['data'][$j]['id'], $item_colors) ? "checked" : ""; ?>>
                                                        <i></i><span style="background-color:<?= $colors['data'][$j]['hex_code'] ?>;" class="span-color"></span><?= $colors['data'][$j]['name'] ." (".$colors['data'][$j]['code'].")" ?></label>
                                                    <?php  endif;  endfor;  $last_number = $j;?>
                                                </section>
                                            <?php endfor; ?>
                                        </div>
                                    </section>
                                </fieldset>
                                <?php else: ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Colors</label>
                                        <div class="row">
                                            <?php
                                            // pr($colors);exit;
                                                $last_number = 0;
                                                for ($i=0; $i < 3; $i++): ?>
                                                <section class="col col-4">
                                                    <?php
                                                        for($j=$last_number; $j< $colors['total_each']+$last_number; $j++):
                                                            if (isset($colors['data'][$j])):
                                                                ?>
                                                    <label class="checkbox">
                                                        <input type="checkbox" class="colors" name="color_id[<?= $colors['data'][$j]['id'] ?>]" value="<?= $colors['data'][$j]['id'] ?>" />
                                                        <i></i><span style="background-color:<?= $colors['data'][$j]['hex_code'] ?>;" class="span-color"></span><?= $colors['data'][$j]['name'] ." (".$colors['data'][$j]['code'].")" ?></label>
                                                    <?php endif;  endfor; $last_number = $j;?>
                                                </section>
                                            <?php endfor; ?>
                                        </div>
                                    </section>
                                </fieldset>
                                <?php endif;?>
                            </form>
                        </div>
                        <!-- end widget content -->
                    </div>
                <!-- end widget div -->
                </article>

                <?php if($id != 0): ?>
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <!-- Widget ID (each widget will need unique ID)-->
                    <div class="jarviswidget" id="wid-id-1"
                    data-widget-editbutton="false"
                    data-widget-deletebutton="false">
                        <header>
                            <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                            <h2>Informasi Tambahan</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <form class="smart-form" id="addon-form" method="post">


                                <fieldset>
                                    <section>
                                        <label class="label">Active</label>
                                        <label class="have_data">
                                            <div><?= $status_name ?></div>
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-4">
                                            <label class="label">Created Date</label>
                                            <label class="have_data">
                                                <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Last Updated Date</label>
                                            <label class="have_data">
                                                <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                            </label>
                                        </section>
                                        <section class="col col-4">
                                            <label class="label">Deleted Date</label>
                                            <label class="have_data">
                                                <div><?= dateformatforview($deleted_date, "d F Y H:i:s") ?></div>
                                            </label>
                                        </section>
                                    </div>
                                </fieldset>

                            </form>

                        </div>
                        <!-- end widget content -->


                    </div>
                    <!-- end widget div -->

                </article>
                <?php endif; ?>
            </div>
        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
