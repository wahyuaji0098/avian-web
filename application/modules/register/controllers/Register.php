<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Basepublic_Controller {

    private $_view_folder = "register/front/";

    public function __construct() {
        parent::__construct();

        if($this->session->has_userdata(USER_SESSION)) {
            //redirect to dashboard
            redirect('/');
        }
    }

    /**
	 * Register controller and for register form processing.
	 */
	public function index() {
        $page = get_page_detail('register');
        redirect(base_url());
        exit;
        $header = array(
            'header'        => $page,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/jquery.validate.min.js",
                "/js/plugins/jquery.form.min.js",
                '/js/front/facebook.js',
                '/js/front/member.js',
            )
        );

        //load the views
        $this->load->view(FRONT_HEADER ,$header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER ,$footer);
	}

    private function setRuleValidation () {
        $this->form_validation->set_message('is_unique', '%s is taken.');

        $this->form_validation->set_rules("name", "Name", "required");
        $this->form_validation->set_rules("email", "Email", "required|valid_email|is_unique[dtb_member.email]");
        $this->form_validation->set_rules("username", "Username", "required|is_unique[dtb_member.username]|min_length[6]|max_length[15]");
        $this->form_validation->set_rules("password", "Password", "required|min_length[6]|max_length[12]");
    }

    public function send() {
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        //load model
        $this->load->library('form_validation');
        $this->load->model("member/Member_model");

		$message['is_error'] = true;
		$message['error_count'] = 1;
        $message['is_redirect'] = false;
		$data = array();

		$this->setRuleValidation();

		if ($this->form_validation->run() == FALSE) {
            $data = validation_errors();
            $count = count($this->form_validation->error_array());
            $message['error_count'] = $count;
        } else {
            $name 		= sanitize_str_input($this->input->post('name'));
			$email 		= sanitize_str_input($this->input->post('email'));
			$username	= sanitize_str_input($this->input->post('username'));
			$password	= sanitize_str_input($this->input->post('password'));

            $datas = array (
				"name" 		=> $name,
				"email" 	=> $email,
				"username" 	=> $username,
				"password" 	=> $password,
			);

			$model_member = new Member_model ();

            $this->db->trans_begin();

			$insert = $model_member->insert($datas);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data = "database operation failed.";

            } else {
                $this->db->trans_commit();

                sendmail (array(
                    'subject'   => SUBJECT_USER_REGISTRATION,
                    'message'   =>  'Dear, '. $datas['name'] .'<br /><br />'.
                                    'Thank you for registering with us'.'<br /><br />'.
                                    'Best Regards, <br /><br />'.
                                    'Avian Brands',
                    'to'		=> array( $datas['email'] ),
                ), "html");

                $message['is_error'] = false;
                $message['is_redirect'] = true;
                $message['error_count'] = 0;

                $data_member = $model_member->get_all_data (array(
                    "find_by_pk" => array($insert),
                    "row_array" => true
                ))['datas'];

                $model_member->update(array(
                    "lastlogin_date" => date("Y-m-d H:i:s"),
                    "unique_code" => null,
                ),array("id" => $data_member['id']));

                $this->session->set_userdata(USER_SESSION, $data_member);

            }
        }

        $message['data'] = $data;
        $this->output->set_content_type('application/json');
        echo json_encode($message);

        exit;
	}

}
