<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>DAFTAR</span>
            </div>
            <h2 class="section-title section-title-top">DAFTAR</h2>
            <form id="register-fm" action="/register/send" method="POST">
            <div class="bigform">
                <a href="#" onclick="fb_login();" class="btn bigform-btn fb-btn"><span></span>MASUK DENGAN FACEBOOK</a>
                <div class="bigform-sep"><span>OR</span></div>
                <div class="bigform-input">
                    <input type="text" placeholder="Nama" id="name" name="name" autocomplete="off"/>
                </div>
                <div class="bigform-input">
                    <input type="text" placeholder="Email" id="email" name="email" autocomplete="off"/>
                </div>
                <div class="bigform-input">
                    <input type="text" placeholder="Username" id="username" name="username" autocomplete="off"/>
                </div>
                <div class="bigform-input">
                    <input type="password" placeholder="Password" id="password" name="password" autocomplete="off"/>
                </div>
                <div class="bigform-error" id="smessage"></div>
                <button type="submit" class="btn bigform-btn">DAFTAR</button>
                <div class="bigformsmallnote">Dengan mendaftar, Anda setuju untuk Syarat & Ketentuan, dan Kebijakan Privasi</div>
                <div class="bigformnote">SUDAH MEMILIKI AKUN? <a href="/login">MASUK</a></div>
            </div>
            </form>
        </div>
    </div>
</div>
