<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>APLIKASI</span>
            </div>
            <div class="layoutapppromonew">
                <div class="iphone">
                    <div class="iphonedisp">
                        <div class="appscreens">
                            <div class="appscreen"><img src="/img/ui/appscreen-new-1.jpg" /></div>
                            <div class="appscreen"><img src="/img/ui/appscreen-new-2.jpg" /></div>
                            <div class="appscreen"><img src="/img/ui/appscreen-new-3.jpg" /></div>
                            <div class="appscreen"><img src="/img/ui/appscreen-new-4.jpg" /></div>
                            <div class="appscreen"><img src="/img/ui/appscreen-new-1.jpg" /></div>
                        </div>
                    </div>
                </div>
                <div class="promotitle">Avian di smartphone anda</div>
                <div class="promotext">
                    Cari produk Avian yang tepat untuk Anda dengan Avian App tersedia untuk iPhone dan Android!
                    Download aplikasi gratis kami, semua warna Avian, di genggaman tangan anda!
                </div>
                <div class="promobtns">
                    <a class="promobtn btnapple" href="https://itunes.apple.com/id/app/avian-brands/id1093348644?mt=8" target="_blank"><span class="hidtxt">Download di Appstore</span></a>
                    <a class="promobtn btnandroid" href='https://play.google.com/store/apps/details?id=com.avian.brands' target="_blank"><span class="hidtxt">Temukan di Google Play</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
