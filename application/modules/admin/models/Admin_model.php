<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Admin_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = 'dtb_admin';
        $this->_table_alias = 'adm';
        $this->_pk_field = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    public function insert($datas, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $datas['password'] =  sha1( SHA1TEXT . $this->db->escape_str($datas['password']));
		}

        $datas['created_date'] = date("Y-m-d H:i:s");

        //add create date and update date
        $this->db->insert($this->_table, $datas);

        return $this->db->insert_id();
    }

    public function update($datas, $condition, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $datas['password'] =  sha1( SHA1TEXT . $this->db->escape_str($datas['password']));
        }

        $datas['updated_date'] = date("Y-m-d H:i:s");

        return $this->db->update($this->_table, $datas, $condition);
    }


    /**
     * check username and password (for login).
     */
	public function check_login($username, $password) {

		//get data by username.
        $this->db->where("username" , $username);

		$user = $this->db->get($this->_table)->row_array();

		//if no user found, it's wrong then.
		if (!$user) {
			return false;
		}

		//check password.
		if ($user['password'] == sha1( SHA1TEXT . $this->db->escape_str($password)) ) {
			return $user;
		}

		return false;
	}

    //generate a unique code and save the code to user's "unique_code".
	private function _generate_forgot_code($id) {

        //create a unique code, and make sure
        //that there is no same unique code in the table.
        do {
            $generated_link = uniqid();
        } while ($this->checkCode($generated_link));

        //insert to user's "unique_code".
        $this->update(array(
            "unique_code" => $generated_link,
            "end_forgotpass_time" => strtotime("+1 day"),
        ),array(
            "id" => $id,
        ));

        return $generated_link;
    }


    //find if the generated code is exists in the table or not.
	public function checkCode($link) {
        $this->db->where("unique_code", $link);
        return $this->db->get($this->_table)->row_array();
    }


	//method to generate an url with a unique code.
	public function send_forgot_pass($datas) {

        //create unique code.
		$code = $this->_generate_forgot_code($datas['id']);

        //encode the unique code.
        $link = base_url()."/manager/reset-password/".urlencode(base64_encode($code));

        //return as valid URL link.
        return $link;
	}


	//function to reset user's password to something generated.
	public function reset_password($datas) {

        //generate a new pass.
        $new_pass = substr(uniqid(),0,10);

		//change password and null the unique_code.
		$this->update(array(
            "password" => $new_pass,
            "unique_code" => null,
            "end_forgotpass_time" => null,
        ),array(
            "id" => $datas['id'],
        ));

		return $new_pass;
    }
}
