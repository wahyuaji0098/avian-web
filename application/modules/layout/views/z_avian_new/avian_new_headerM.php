<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<title>Avian Brands | <?= $header['title'] ?></title>
	<meta name="description" content="<?= $header['meta_desc'] ?>" />
    <meta name="keywords" content="<?= $header['meta_keys'] ?>" />
	<meta name="author" content="Avian Brands">
	<link rel="alternate" href="<?= base_url() ?>" hreflang="id-id" />
	<link rel="icon" href="/avian_new/images/favicon.png">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/style-mobile.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/aos.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/jssocials.css">

    <link rel="stylesheet" type="text/css" href="/avian_new/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/avian_new/css/form-select.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<style type="text/css">
		.year-menu{
			max-height: 150px !important;
			overflow-y: scroll !important;
			position: relative !important;
			width: 150px;
		}
	</style>
	<script type="text/javascript" src="/avian_new/js/jquery.js"></script>
</head>
<body class="bg-batik">
<!-- NAVBAR -->

<div>
      <div id="gedung" class="push-right">
        <div class="hov-bg">
        </div> 
      </div> 
      <div id="aic" class="push-right">
        <div class="hov-bg">
        </div>
      </div> 
      <div id="warna" class="push-right">
        <div class="hov-bg">
        </div>
      </div> 
      <div id="pers" class="push-right">
        <div class="hov-bg">
        </div>
      </div> 
      <div id="andalan" class="push-right">
        <div class="hov-bg">
        </div>
      </div> 
      <div id="visimisi" class="push-right">
        <div class="hov-bg">
        </div>
      </div> 
      <div id="finansial" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
      <div id="sejarah" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
      <div id="pemasaran" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
      <div id="sertifikasi" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
      <div id="peduli" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
      <div id="karir" class="push-right">
        <div class="hov-bg">
        </div>
      </div>
    </div>

<!-- NAVBAR-MOBILE -->
