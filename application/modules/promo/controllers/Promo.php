<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Promo Controller.
 */
class Promo extends Basepublic_Controller  {

    private $_view_folder = "promo/front/";

    public function detail ($id = null) {

        $pr_url = $this->uri->segment(3, 0);

        $page = get_page_detail('promo-detail');

        if (empty($pr_url)) {
            show_404('page');
        }

        // Get all csr data
        $promo = $this->_dm->set_model("mst_promo", "mp", "mp.id")->get_all_data(array(
            "select"     => "mp.*, mpv.name as provinsi_name",
            "joined"     => array("mtb_province mpv" => array("mpv.id" => "mp.province_id")),
            "conditions" => array('mp.id' => $id),
            "row_array" => true
        ))['datas'];

        $data['promo'] = $promo;

        $header = array(
            "title"      => NULL,
            "header"     => $page
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'detail' , $data);
        $this->load->view(FRONT_FOOTER);
    }

}
