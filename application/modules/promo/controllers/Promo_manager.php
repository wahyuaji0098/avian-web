<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Product Category Controller.
 */
class Promo_manager extends Baseadmin_Controller  {

    private $_title         = "Promo";
    private $_title_page    = '<i class="fa-fw fa fa-cc-mastercard"></i> Promo';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "promo";
    private $_back          = "/manager/promo/";
    private $_js_path       = "/js/pages/promo/";
    private $_view_folder   = "promo/";

    private $_table         = "mst_promo";
    private $_table_aliases = "mp";
    private $_pk            = "mp.id";

    /**
     * constructor.
     */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////
    /**
    * List Product Category
    */
    public function index() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List Promo </span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Promo</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                "/js/plugins/lightbox/js/lightbox.min.js",
                $this->_js_path . "list.js",
            ),
            "css" => array(
                "/js/plugins/lightbox/css/lightbox.min.css",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// Create //////////////////////////////////////
    /**
    * Create an Promo
    */
    public function create () {
        $this->_breadcrumb .= '<li><a href="/manager/promo">Promo</a></li>';

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Promo</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Create Promo</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    /**
     * Edit an promo
     */
    public function edit ($id = null) {
        $this->_breadcrumb .= '<li><a href="/manager/promo">Promo</a></li>';

        $data['item'] = null;

        //validate ID and check for data.
        if ( $id === null || !is_numeric($id) ) {
            show_404();
        }

        $params = array(
            "select"     => "mp.*, mpv.name",
            "row_array"  => true,
            "left_joined"=> array("mtb_province mpv" => array("mpv.id" =>"mp.province_id")),
            "conditions" => array("mp.id" => $id),
            "debug"      => false
        );

        //get the data.
        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data($params)['datas'];
        // pr($data['item']);exit;

        //if no data found with that ID, throw error.
        if (empty($data['item'])) {
            show_404();
        }

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Edit Promo</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Edit Promo</li>',
            "back"          => $this->_back,
        );

        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                "/js/plugins/select2.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////
    /**
     * Function to get list_all_data
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit = sanitize_str_input($this->input->get("length"), "numeric");
        $start = sanitize_str_input($this->input->get("start"), "numeric");
        $search = sanitize_str_input($this->input->get("search")['value']);
        $filter = $this->input->get("filter");

        $select = array($this->_table_aliases. ".id", "judul", "image_url", "mvp.name","popup_image_url","content", "periode_start", "periode_end", $this->_table_aliases.".is_show");

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['lower(mp.id)'] = $value;
                        }
                        break;

                    case 'judul':
                        if ($value != "") {
                            $data_filters['lower(judul)'] = $value;
                        }
                        break;

                    case 'provinsi':
                        if ($value != "") {
                            $data_filters['lower(mvp.name)'] = $value;
                        }
                        break;

                    case 'periode_start':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_start as date) <="] = $date['end'];
                            $conditions["cast(periode_start as date) >="] = $date['start'];

                        }
                        break;

                    case 'periode_end':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(periode_end as date) <="] = $date['end'];
                            $conditions["cast(periode_end as date) >="] = $date['start'];

                        }
                        break;

                    case 'create_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(created_date as date) <="] = $date['end'];
                            $conditions["cast(created_date as date) >="] = $date['start'];

                        }
                        break;

                    case 'update_date':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(updated_date as date) <="] = $date['end'];
                            $conditions["cast(updated_date as date) >="] = $date['start'];

                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $data_filters['mp.is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'            => $select,
            'left_joined'       => array("mtb_province mvp" => array("mvp.id" => $this->_table_aliases .".province_id")),
            'order_by'          => array($column_sort => $sort_dir),
            'limit'             => $limit,
            'start'             => $start,
            'conditions'        => $conditions,
            'filter'            => $data_filters,
            "count_all_first"   => true,
        ));
        // pr($datas);exit;

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data" => $datas['datas'],
            "draw" => intval($this->input->get("draw")),
            "recordsTotal" => $total_rows,
            "recordsFiltered" => $total_rows,
        );

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
    */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //dynamic model
        $promo_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                 = sanitize_str_input($this->input->post('id'));
        $judul              = sanitize_str_input($this->input->post('judul'));
        $content            = $this->input->post('content');
        $periode_start      = sanitize_str_input($this->input->post('periode_start'));
        $periode_end        = sanitize_str_input($this->input->post('periode_end'));
        $province_id        = sanitize_str_input($this->input->post('province_id'));
        $member_id          = sanitize_str_input($this->input->post('member_id'));
        $is_from_avian      = sanitize_str_input($this->input->post('is_from_avian'));
        $is_show_as_popup   = sanitize_str_input($this->input->post('is_show_as_popup'));
        $is_show            = sanitize_str_input($this->input->post('is_show'));
        $data_image         = $this->input->post('data-image');
        $data_image_large   = $this->input->post('data-image-url');
        //convert &lt; & &gt;
        $convert = html_entity_decode($content);


        //prepare upload image
        $image_url   = $this->upload_image(false, "upload/promo", "image-file", $data_image, 640, 256, $id);
        $image_large = $this->upload_image(false, "upload/promo/popup", "image-file-large", $data_image_large, 640, 640, $id);

        //begin transaction.
        $this->db->trans_begin();

        //validation success, prepare array to DB.
        $arrayToDB = array(
            'judul'             => $judul,
            'content'           => $convert,
            'periode_start'     => $periode_start,
            'periode_end'       => $periode_end,
            'province_id'       => $province_id,
            'is_from_avian'     => $is_from_avian,
            'is_show_as_popup'  => $is_show_as_popup,
            'is_show'           => $is_show,
        );

        if (!empty($image_url)) {
            $arrayToDB['image_url'] = $image_url;
        }

       if (!empty($image_large)) {
            $arrayToDB['popup_image_url'] = $image_large;
        }

        //version +1
        $version = array('is_version' => true);

        //insert or update?
        if ($id == "") {

            //insert to DB.
            $result = $promo_model->insert($arrayToDB, $version);

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';

            } else {
                $this->db->trans_commit();

                $message['is_error'] = false;

                //success.
                //growler.
                $message['notif_title'] = "Good!";
                $message['notif_message'] = "New Promo has been added.";

                //on insert, not redirected.
                $message['redirect_to'] = "/manager/promo";
            }
        } else {
            //get current promo data
            $curr_data = $promo_model->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array" => true,
            ))['datas'];

            // pr($curr_data);exit;

            //update.
            if (!empty($image_url) && isset($curr_data['image_url']) && !empty($curr_data['image_url'])) {
                unlink( FCPATH . $curr_data['image_url'] );
            }

            if (!empty($image_large) && isset($curr_data['popup_image_url']) && !empty($curr_data['popup_image_url'])) {
                unlink( FCPATH . $curr_data['popup_image_url'] );
            }

            //condition for update.
            $condition = array("id" => $id);

            $result = $promo_model->update($arrayToDB, $condition ,$version);

            //end transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';

            } else {
                $this->db->trans_commit();

                $message['is_error'] = false;

                //success.
                //growler.
                $message['notif_title']   = "Excellent!";
                $message['notif_message'] = "Promo has been updated.";

                //on update, redirect.
                $message['redirect_to']   = "/manager/promo";
            }
        }
        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Get list provinsi
     */
    public function get_list_provinsi()
    {

        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
            exit('No direct script access allowed');
        }

        //get ajax query and page.
        $select_q    = ($this->input->get("q") != null) ? trim($this->input->get("q")) : "";
        $select_page = ($this->input->get("page") != null) ? trim($this->input->get("page")) : 1;

        //sanitazion.
        $select_q = $select_q;

        //page must numeric.
        $select_page = is_numeric($select_page) ? $select_page : 1;

        //for paging, calculate start.
        $limit = 10;
        $start = ($limit * ($select_page - 1));

        //filters.
        $filters = array();
        if ($select_q != "") {
            $filters["nama"] = $select_q;
        }

        //conditions.
        $conditions = array();

        //get data.
        $datas = $this->_dm->set_model('mtb_province')->get_all_data(array(
            "select"          => array("name", "id"),
            "distinct"        => true,
            "conditions"      => $conditions,
            "filter_or"       => $filters,
            "count_all_first" => true,
            "limit"           => $limit,
            "start"           => $start,
        ));

        //prepare returns.
        $message["page"]        = $select_page;
        $message["total_data"]  = $datas['total'];
        $message["paging_size"] = $limit;
        $message["datas"]       = $datas['datas'];

        echo json_encode($message);
        exit;
    }

    /**
     * Delete an admin.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['redirect_to'] = "";
        $message['error_msg'] = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'), "numeric");

        //check first.
        if (!empty($id) && is_numeric($id)) {
            //get data slider
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array" => TRUE,
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                //begin transaction
                $this->db->trans_begin();

                //version + 1
                $version =  array('is_version' => true);
                //update is show to 0
                $arrayToDB['is_show'] = 0;
                //conditions for update
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update($arrayToDB, $condition, $version);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error'] = false;
                    $message['error_msg'] = '';

                    //growler.
                    $message['notif_title'] = "Done!";
                    $message['notif_message'] = "Promo has been delete.";
                    $message['redirect_to'] = "";
                }
            }

        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function reactivate() {

        // must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        // initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        // sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        // check first.
        if ($id) {
            //get data user
            $data = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                "conditions" => array("id" => $id),
                "row_array" => TRUE,
            ))['datas'];

            // no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';

            } else {

                // begin transaction
                $this->db->trans_begin();

                // reactivate the user
                $condition = array("id" => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(array("is_show" => STATUS_ACTIVE),$condition, array('is_version' => TRUE));

                // end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();

                    // failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    // success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';

                    // growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Country has been re-activated.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            // id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        // encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }
}
