<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>Promo</span>
            </div>
            <div class="section-title tabgroup targrouptaba targettaba0 section-title-top" targid="taba0" targroup="taba">
                Promo Avian 
            </div>
            <div class="promo_detail">
                <div class="promo_detail_pic">
                    <img src="<?= $promo['image_url'] ?>" alt="Promo">
                </div>
                <div class="promo_detail_content">
                    <div class="promo_detail_title">
                        <?php echo $promo["judul"] ?>
                    </div>
                    <div class="promo_detail_info">
                        <div>Periode Start : <span><?php echo $promo["periode_start"] ?></span></div>
                        <div>Periode End : <span><?php echo $promo["periode_end"] ?></span></div>
                        <div>Provinsi : <span><?php echo $promo["provinsi_name"] ?></span></div>
                    </div>
                    <div class="promo_detail_desc">
                        <?php echo $promo["content"] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>