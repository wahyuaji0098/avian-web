<?php
    $id                 = isset($item["id"]) ? $item["id"] : "";
    $title              = isset($item["judul"]) ? $item["judul"] : "";
    $content            = isset($item["content"]) ? $item["content"] : "";
    $periode_start      = isset($item["periode_start"]) ? date('Y-m-d', strtotime($item["periode_start"])) : "";
    $periode_end        = isset($item["periode_end"]) ? date('Y-m-d', strtotime($item["periode_end"])) : "";
    $is_show            = isset($item["is_show"]) ? $item["is_show"] : "";
    $is_from_avian      = isset($item["is_from_avian"]) ? $item["is_from_avian"] : "";
    $is_show_as_popup   = isset($item["is_show_as_popup"]) ? $item["is_show_as_popup"] : "";
    $image_url          = isset($item["image_url"]) ? $item["image_url"] : "";
    $popup_img          = isset($item["popup_image_url"]) ? $item["popup_image_url"] : "";
    $prov_id            = isset($item['province_id']) ? $item['province_id'] : "";
    $name               = isset($item['name']) ? $item['name'] : "";
    $created_date       = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date       = isset($item["updated_date"]) ? $item["updated_date"] : "";

    $btn_msg    = ($id == 0) ? "Create" : " Update";
    $title_msg  = ($id == 0) ? "Create" : " Update";
    $data_edit  = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?> Promo</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/promo/process-form" method="post" enctype="multipart/form-data">
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                            <fieldset>
                                <section>
                                    <label class="label">Judul </label>
                                    <label class="input">
                                        <input name="judul" type="text"  class="form-control" placeholder="Judul" value="<?= $title; ?>" />
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Periode Start </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input name="periode_start" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Periode Start" value="<?= $periode_start; ?>" />
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Periode End </label>
                                    <label class="input">
                                        <i class="icon-append fa fa-calendar"></i>
                                        <input name="periode_end" type="text"  class="form-control datepicker" data-dateformat="yy-mm-dd" placeholder="Periode End" value="<?= $periode_end; ?>" />
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Provinsi </label>
                                    <select class="sel2form select2Prov" name="province_id" style="width:100%">
                                        <?php if($prov_id != ""): ?>
                                            <option value="<?= $prov_id ?>"><?= $name ?></option>
                                        <?php endif; ?>
                                    </select>
                                </section>

                                <section>
                                    <label class="label">Image URL (640x256)</label>
                                    <label class="input">
                                        <div class="add-image-preview" id="preview-image">
                                            <?php if($image_url): ?>
                                            <img src="<?= $image_url ?>" height="100px"/>
                                            <?php endif; ?>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-sm" id="addimage" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($image_url != "") ? "Change" : "Add" ?> Image</button>

                                    </label>
                                </section>

                                <section>
                                    <label class="label">Popup images URL (640x640)</label>
                                    <label class="input">
                                        <div class="add-image-preview" id="preview-image2">
                                            <?php if($popup_img): ?>
                                            <img src="<?= $popup_img ?>" height="100px"/>
                                            <?php endif; ?>
                                        </div>
                                        <button type="button" class="btn btn-primary btn-sm" id="addimage2" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($popup_img != "") ? "Change" : "Add" ?> Popup image</button>

                                    </label>
                                </section>

                                <section>
                                    <label class="label">Content </label>
                                    <label class="textarea">
                                        <textarea name="content" id="full_content" class="form-control tinymce" rows="10"><?= $content; ?></textarea>
                                    </label>
                                </section>

                                <section>
                                    <label class="label">Show / Hide </label>
                                    <label class="input">
                                        <?= select_is_show('is_show', $is_show); ?>
                                    </label>
                                </section>

                                <section>
                                    <label class="label"> Is From Avian </label>
                                    <label class="input">
                                        <?= select_is_from('is_from_avian', $is_from_avian); ?>
                                    </label>
                                </section>

                                <section>
                                    <label class="label"> Is Show as Popup </label>
                                    <label class="input">
                                        <?= select_is_show_as_popup('is_show_as_popup', $is_show_as_popup); ?>
                                    </label>
                                </section>
                            </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
