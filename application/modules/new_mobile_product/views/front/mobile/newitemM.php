<div id="content" class="mr bg-batik produk_detail">
    <div class="row header-produk">
      <div class="col-md-9 col-xs-9">
        <p class="font-sofia-bold font-md font-green"><?= ucwords(str_replace('-',' ' ,$this->uri->segment(3))) ?></p>
      </div>
      
     
      
<br>
<br>
<br>
    <div class="row content">


      <?php $count = count($products); $i=0;?>
      <?php if($count > 0): foreach($products as $prod): ?>

      <div class="col">
        <div class="panel">
          <div class="box-img" style="background-image: url(<?= $prod['image_url'] ?>)"></div>
          <div class="text-content">
            <div class="prd-title">
              <h3><a href="/products/itemsM/<?= $prod['pretty_url'] ?>"><?= $prod['name'] ?></a>
                <?php 
                  $num = rand(0,99);
                 
                  if ($num % 2 == 0) {
                ?>
                  <span class="new">New</span>
                <?php  
                  } else {
                ?>
                  <span class="new">Hot</span>
                <?php } ?>

                
              </h3>

              <a href="#" class="favorit"><i class="fa fa-heart-o"></i></a>
            </div>
            <div class="desc">
              <p><?= ($prod['description']) ? strip_tags($prod['description']) : "" ; ?></p>
            </div>
            <a href="/products/itemsM/<?= $prod['pretty_url'] ?>" class="btn-show-prd">Lihat Produk</a>
          </div>
        </div>
      </div>
      
      <?php endforeach; endif; ?> 



    </div>

  
  </div>

 <script type="text/javascript" src="/avian_new/js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="/avian_new/js/jquery.matchHeight-min.js"></script>