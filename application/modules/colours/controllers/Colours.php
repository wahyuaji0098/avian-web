<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Colours extends Basepublic_Controller {

    private $_view_folder = "colours/front/";

    function __construct() {
        parent::__construct();

    }

    public function index() {
        
        redirect(base_url('color'));
        //get header page
        /*$page = get_page_detail('colours');

        $header = array(
            'header'            => $page,
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER);*/


	}

	public function lists () {
		//get header page
        $page = get_page_detail('colours-lists');

		$limit = 20;
		$start = 0;
        $search = $this->session->userdata('color_search');
        $q = $this->input->get('q');

        //load model
        $this->load->model("palette/Palette_model");

		//get id colors where pallets is deleted
		$palletes = $this->Palette_model->getColorPalleteDeleted();

		if (!empty($palletes)) {
			$palletes = implode("','",$palletes);
			$where["id not in ('".$palletes."')"] = null;
		}

        $param = array(
            "order_by"   => array("name" => "asc"),
            "limit"      => $limit,
            "start"      => $start,
            "filter_or"  => array("name" => $search,"code" => $search),
            "conditions" => array("is_show" => SHOW ),
            "count_all_first" => true
        );

        if ($q) {
            $param["conditions"]['id'] = $q;
        }

		$data_color = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data($param);

        $lists = $data_color['datas'];
		$total = $data_color['total'];
        $total_page = ceil($total/$limit);

		//get data likebox
		if (isset($this->data_login_user['id']))
			$color_likebox = find_like_by_member ($this->data_login_user['id'],LIKEBOX_COLOR);
		else
			$color_likebox = array();


        $header = array(
            'header'             => $page,
            'models'             => $lists,
			'colours_page'       => $total_page,
			'colours_search'     => $search,
			'color_likebox'      => $color_likebox,
        );

        $footer = array(
            "script" => array(
                "/js/front/colours-list.js",
            )
        );

        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'lists');
        $this->load->view(FRONT_FOOTER, $footer);

	}

	public function loadmore_list () {
        //check if ajax request
		if (!$this->input->is_ajax_request()) {
			exit('No direct script access allowed');
		}

        $page = $this->input->post("page");
        $limit = 20;
        $start = ($limit * ($page - 1));
		$search = $this->input->post("search");

        $this->session->set_userdata('color_search', $search);

        if ($search == "") {
            $this->session->unset_userdata('color_search');
        }

        $filter_or = array();

        if (!empty($search)) {
            $filter_or = array(
                "name" => $search,
                "code" => $search,
            );
        }

        //load model
        $this->load->model("palette/Palette_model");

		//get id colors where pallets is deleted
		$palletes = $this->Palette_model->getColorPalleteDeleted();

		if (!empty($palletes)) {
			$palletes = implode("','",$palletes);
			$where["id not in ('".$palletes."')"] = null;
		}

		$data_color = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data(array(
            "order_by" => array("name" => "asc"),
            "limit" => $limit,
            "start" => $start,
            "filter_or" => $filter_or,
            "conditions" => array(
                "is_show" => SHOW,
            ),
            "count_all_first" => true,
        ));

        $lists = $data_color['datas'];
		$total = $data_color['total'];
        $total_page = ceil($total/$limit);

		$models = array();

		if (count($lists) > 0) {
			foreach($lists as $model) {
				//check if this model is in colour list
				$exist = find_like_by_member ($this->data_login_user['id'],LIKEBOX_COLOR,$model['id']);
				$model['is_like'] = ($exist) ? 1  : 0;

				array_push($models,$model);
			}
		}

        $this->output->set_content_type('application/json');
        echo json_encode(array(
            "result" => "OK",
            "datas" => $models,
            "total_page" => $total_page,
        ));

		exit;
    }
}
