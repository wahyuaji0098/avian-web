<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Colours Controller.
 */
class Colours_manager extends Baseadmin_Controller  {

    private $_title         = "Colours";
    private $_title_page    = '<i class="fa-fw fa fa-paint-brush"></i> Colours ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "colours";
    private $_back          = "/manager/colours/lists";
    private $_js_path       = "/js/pages/colours/manager/";
    private $_view_folder   = "colours/manager/";
    private $_table         = "dtb_color";
    private $_table_aliases = "col";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List colours
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List of Colours</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Colours</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create colours
     */
    public function create() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Colours</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Colours</li>',
            "back"          => $this->_back,
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js",
                $this->_js_path . "create.js",
            ),
            "css" => array(
                "/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css"
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Edit an colours
     */
    public function edit ($id = null) {
        if (empty($id)) {
            show_404();
        }

        $this->_breadcrumb .= '<li><a href="/manager/colours/lists">Colours</a></li>';

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Colours</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Colours</li>',
            "back"          => $this->_back,
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js",
                $this->_js_path . "create.js",
            ),
            "css" => array(
                "/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css"
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    public function import() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Import Colours</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Colours</li>',
            "back"          => $this->_back,
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                $this->_js_path . "import.js",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'import');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    /**
     * Set validation rule for create and edit
     */
    private function _set_rule_validation($id) {
        //prepping to set no delimiters.
        $this->form_validation->set_error_delimiters('', '');

        //validates.
        $this->form_validation->set_rules("name", "Name", "trim|required");
        $this->form_validation->set_rules("code", "Code", "trim|required");
        $this->form_validation->set_rules("red", "Red", "required");
        $this->form_validation->set_rules("green", "Green", "required");
        $this->form_validation->set_rules("blue", "Blue", "required");
        $this->form_validation->set_rules("hex_code", "Hex Code", "required");
    }

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get colours
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('id','name','code','hex_code','is_show','status');

        $column_sort = $select[$sort_col];

        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['id'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['lower(name)'] = $value;
                        }
                        break;

                    case 'code':
                        if ($value != "") {
                            $data_filters['lower(code)'] = $value;
                        }
                        break;

                    case 'show':
                        if ($value != "") {
                            $conditions['is_show'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            'select'          => $select,
            'order_by'        => array($column_sort => $sort_dir),
            'limit'           => $limit,
            'start'           => $start,
            'conditions'      => $conditions,
            'filter'          => $data_filters,
            'status'          => STATUS_ACTIVE,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        $extra_param = array(
            "is_direct"  => FALSE,
            "is_version" => TRUE,
            "ordering"   => FALSE
        );

        //sanitize input (id is primary key, if from edit, it has value).
        $id = $this->input->post('id');

        $name     = $this->input->post('name');
        $code     = $this->input->post('code');
        $red      = $this->input->post('red');
        $green    = $this->input->post('green');
        $blue     = $this->input->post('blue');
        $hex_code = $this->input->post('hex_code');

        $arrayToDB = array(
            "name"     => strtoupper($name),
            "code"     => strtoupper($code),
            "red"      => $red,
            "green"    => $green,
            "blue"     => $blue,
            "hex_code" => $hex_code
        );

        // server side validation. not implement yet.
        $this->_set_rule_validation($id);

        //checking.
        if ($this->form_validation->run($this) == FALSE) {

            //validation failed.
            $message['error_msg'] = validation_errors();

        } else {
            // Begin transaction.
            $this->db->trans_begin();

            // Insert or update?
            if ($id == "") {

                // Insert to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->insert(
                    $arrayToDB,
                    $extra_param
                );

            } else {

                // Condition for update.
                $condition = array("id" => $id);
                // Update to DB.
                $result = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    $arrayToDB,
                    $condition,
                    $extra_param
                );

            }

            // End transaction.
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $message['error_msg'] = 'database operation failed.';
            } else {
                $this->db->trans_commit();
                $message['is_error'] = false;
                // success.
                // growler.
                $message['notif_title']   = "Good!";
                if ($id == "") {
                    $message['notif_message'] = "Colours has been added.";
                } else {
                    $message['notif_message'] = "Colours has been updated.";
                }
                // redirected.
                $message['redirect_to'] = "/manager/colours/lists";
            }

        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Delete an colours.
     */
    public function delete() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);
        $extra_param = array("is_version" => true);

        //check first.
        if (!empty($id)) {

            //get data admin
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "status"     => STATUS_ACTIVE,
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();

                //delete the data (deactivate)
                $condition = array($this->_pk => $id);
                $delete = $_model->delete($condition,$extra_param);

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Colours has been delete.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    public function process_import () {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error'] = true;
        $message['error_msg'] = "";
        $message['redirect_to'] = "";
        $validate = "";
        $error_validate_flag = false;
        $insert_datas = array();

        //check validation
        if (isset($_FILES['file']['size']) && $_FILES['file']['size'] > 0) {
            $filename = $_FILES['file']['tmp_name'];
            //check max file upload
            if ($_FILES['file']['size'] > MAX_IMPORT_FILE_SIZE) {
                $message['error_msg'] = "Maximum file size is ".MAX_IMPORT_FILE_SIZE_IN_KB;
            } elseif (mime_content_type($filename) != "application/vnd.ms-excel"
                        && mime_content_type($filename) != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                //check extension if xls dan xlsx
                $message['error_msg'] = "File must .xls or .xlsx";
            } else {
                //load and init library
                $this->load->library("PHPExcel_reader");
                $excel = new PHPExcel_reader();

                //read the file and give back the result of the file in array
                $result = $excel->read_from_excel($filename);

                if ($result['is_error'] == true) {
                    $message['redirect_to'] = "";
                    $message['error_msg']   = $result['error_msg'];
                } else {

                    //begin transaction
                    $this->db->trans_begin();

                    $total_data = count($result['datas']);

                    $extra_param = array(
                        "is_direct" => true,
                        "is_batch" => true,
                    );

                    if ($total_data > 0) {
                        //loop the result and insert to db
                        $line = 2;

                        $data_palette_color = array();
                        $data_products = array();
                        $prev_products = "";
                        $prev_palette_name = "";
                        $prev_palette_id = "";

                        foreach ($result['datas'] as $model) {
                            if (count($model) < 8) {
                                $validate = "Invalid Template.";
                                $error_validate_flag = true;
                                break;
                            } else {
                                //check untuk semua kolom apakah null
                                if (empty($model[0]) && empty($model[1]) &&  empty($model[2]) &&  empty($model[3]) && empty($model[4]) && empty($model[5]) &&  empty($model[6]) &&  empty($model[7])) {
                                    // klo kosong semua isinya, skip
                                    $line++;
                                } else {
                                    //cek color code if exist
                                    $color_exist = $this->_dm->set_model("dtb_color", "dc", "id")->get_all_data(array(
                                        "conditions" => array(
                                            "lower(code)" => strtolower($model[0]),
                                        ),
                                        "row_array" => true,
                                    ))['datas'];

                                    $rgb = array( $model[4], $model[5], $model[6] );

                                    $data_color = array(
                                        'code'      => strtoupper($model[0]),
                						'name'      => strtoupper($model[1]),
                						'can_size'  => $model[3],
                						'red'       => $model[4],
                						'green'     => $model[5],
                						'blue'      => $model[6],
                						'hex_code'  => rgb2hex($rgb),
                                    );

                                    if (!empty($color_exist)) {
                                        //update color
                                        $color_id = $color_exist['id'];
                                        $this->_dm->set_model("dtb_color", "dc", "id")->update($data_color, array("id" => $color_id), array("is_version" => true));
                                    } else {
                                        //insert color
                                        $color_id = $this->_dm->set_model("dtb_color", "dc", "id")->insert($data_color, array("is_version" => true));
                                    }

                                    //masukin palette
                                    $catalog_name = trim($model[2]);

                                    $palette_exist = $this->_dm->set_model("dtb_pallete", "dp", "id")->get_all_data(array(
                                        "conditions" => array(
                                            "lower(name)" => strtolower($catalog_name),
                                        ),
                                        "row_array" => true,
                                    ))['datas'];

                                    $data_palette = array(
                                        "name" => trim($catalog_name),
                                    );

                                    if (!empty($palette_exist)) {
                                        //update palette
                                        $palette_id = $palette_exist['id'];
                                        $this->_dm->set_model("dtb_pallete", "dp", "id")->update($data_palette, array("id" => $palette_id), array("is_version" => true));
                                    } else {
                                        //insert palette
                                        $palette_id = $this->_dm->set_model("dtb_pallete", "dp", "id")->insert($data_palette, array("is_version" => true));
                                    }


                                    $products = $model[7];

                                    if ($prev_products != $products) {
                                        $prev_products = $products;
                                        $products = explode(",",$products);

                                        //masukin product_palette
                                        if (!empty($products)) {
                                            foreach ($products as $data_prod) {
                                                //get product id
                                                $product_exist = $this->_dm->set_model("dtb_product", "dp", "id")->get_all_data(array(
                                                    "conditions" => array(
                                                        "lower(name)" => trim(strtolower($data_prod)),
                                                    ),
                                                    "row_array" => true,
                                                ))['datas'];

                                                if (!empty($product_exist)) {
                                                    $data_products[] = array(
                                                        "palette_id" => $palette_id,
                                                        "product_id" => $product_exist['id']
                                                    );
                                                }


                                            }
                                        }
                                    }

                                    //masukin color_palette
                                    $data_palette_color[] = array(
                                        "palette_id" => $palette_id,
                                        "color_id" => $color_id,
                                    );

                                    $line++;
                                }
                            }
                        }


                        if (!empty($data_products)) {
                            $this->_dm->set_model("mst_palette_product", "mpp", "id")->delete(array("palette_id" => $palette_id), array("is_permanently" => true));
                            $this->_dm->set_model("mst_palette_product", "mpp", "id")->insert($data_products,$extra_param);
                        }

                        if (!empty($data_palette_color)) {
                            $this->_dm->set_model("mst_palette_color", "mpc", "id")->delete(array("palette_id" => $palette_id), array("is_permanently" => true));
                            $this->_dm->set_model("mst_palette_color", "mpc", "id")->insert($data_palette_color,$extra_param);
                        }

                    }

                    if ($this->db->trans_status() === false || $error_validate_flag == true) {
                        $this->db->trans_rollback();
                        $message['redirect_to'] = "";

                        if ($error_validate_flag == true) {
                            $message['error_msg'] = $validate;
                        } else {
                            $message['error_msg'] = 'database operation failed.';
                        }
                    } else {
                        $this->db->trans_commit();

                        $message['is_error'] = false;
                        // success.
                        $message['notif_title']   = "Good!";
                        $message['notif_message'] = "Data Palette has been imported.";

                        // on insert, not redirected.
                        $message['redirect_to'] = "";
                    }
                }
            }
        } else {
            $message['error_msg'] = 'File is empty (xls or xlsx).';
        }

        echo json_encode($message);
        exit;
    }
}
