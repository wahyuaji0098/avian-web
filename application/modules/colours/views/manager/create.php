<?php
    $id           = isset($item["id"]) ? $item["id"] : "";

    $name         = isset($item["name"]) ? $item["name"] : "";
    $code         = isset($item["code"]) ? $item["code"] : "";
    $red          = isset($item["red"]) ? $item["red"] : "";
    $green        = isset($item["green"]) ? $item["green"] : "";
    $blue         = isset($item["blue"]) ? $item["blue"] : "";
    $hex_code     = isset($item["hex_code"]) ? $item["hex_code"] : "";
    $is_show      = isset($item["is_show"]) ? $item["is_show"] : 1;
    $created_date = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date = isset($item["updated_date"]) ? $item["updated_date"] : "";
    $deleted_date = isset($item["deleted_date"]) ? $item["deleted_date"] : "";
    $status_name = isset($item["status_name"]) ? $item["status_name"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="<?= (isset($back) ? "go('".$back."');" : "window.history.back();") ?>" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Simpan" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/colours/process-form" method="POST">
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Name <sup class="color-red">*</sup></label>
                                        <label class="input">
                                            <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?php echo $name; ?>" />
                                        </label>
                                    </section>
                                    <section>
                                        <label class="label">Code <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <input name="code" id="code" type="text"  class="form-control" placeholder="Code" value="<?php echo $code; ?>" />
                                        </label>
                                    </section>
                                    <div class="row">
                                        <section class="col col-3">
                                            <label class="label">Red <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <input name="red" id="red" type="text"  class="form-control" placeholder="Red" value="<?php echo $red; ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="label">Green <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <input name="green" id="green" type="text"  class="form-control" placeholder="Green" value="<?php echo $green; ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="label">Blue <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <input name="blue" id="blue" type="text"  class="form-control" placeholder="Blue" value="<?php echo $blue; ?>" />
                                            </label>
                                        </section>
                                        <section class="col col-3">
                                            <label class="label">Hex code <sup class="color-red">*</sup></label>
                                            <label class="input">
                                                    <input name="hex_code" id="hex_code" type="text"  class="form-control" placeholder="Hex code" value="<?php echo $hex_code; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                    <section>
                                        <label class="label">Pick Color <sup class="color-red">*</sup></label>
                                        <label class="input">
                                                <div style="min-height:300px;" id="color_bg">
                                                    <div id="pick_color" class="demo-auto inl-bl" data-container="#pick_color" data-inline="true"></div>
                                                </div>
                                        </label>
                                    </section>
                                     <section>
                                        <label class="label">Show / Hide </label>
                                        <label class="input">
                                            <?php echo select_is_show('is_show', $is_show); ?>
                                        </label>
                                    </section>
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <section>
                                    <label class="label">Active</label>
                                    <label class="have_data">
                                        <div><?= $status_name ?></div>
                                    </label>
                                </section>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Deleted Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($deleted_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
