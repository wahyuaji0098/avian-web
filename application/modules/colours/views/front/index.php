<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>WARNA</span>
            </div>
            <div class="colcat-list">
                <a href="/palette/lists" class="colcat">
                    <div class="colcatpic">
                        <img src="/img/lists/colours/palette-list.jpg" />
                    </div>
                    <div class="colcatext">
                        <div class="colcatitle">
                            <h3>KARTU WARNA</h3>
                        </div>
                        <div class="colcatp">
                            <p>Lihat keseluruhan warna yang ada di database warna Avianbrands berdasarkan kartu warna.</p>
                        </div>
                    </div>
                </a>
                <a href="/colours/lists" class="colcat">
                    <div class="colcatpic">
                        <img src="/img/lists/colours/color-list.jpg" />
                    </div>
                    <div class="colcatext">
                        <div class="colcatitle">
                            <h3>KOLEKSI WARNA</h3>
                        </div>
                        <div class="colcatp">
                            <p>Lihat keseluruhan warna yang ada di database warna Avianbrands.</p>
                        </div>
                    </div>
                </a>
                <a href="/custom-colour" class="colcat">
                    <div class="colcatpic">
                        <img src="/img/lists/colours/custom-color.jpg" />
                    </div>
                    <div class="colcatext">
                        <div class="colcatitle">
                            <h3>RACIK WARNA</h3>
                        </div>
                        <div class="colcatp">
                            <p>Racik warna sesuai dengan apa yang Anda inginkan. Beberapa Toko Avian menyediakan fitur untuk membuat warna Anda sendiri (Custom Warna), jadi Anda tinggal menunjukan hasil racikan Anda ke Toko Avian tersebut.</p>
                        </div>
                    </div>
                </a>
                <a href="/download" class="colcat">
                    <div class="colcatpic">
                        <img src="/img/lists/colours/pick-color.jpg" />
                    </div>
                    <div class="colcatext">
                        <div class="colcatitle">
                            <h3>CARI WARNA</h3>
                        </div>
                        <div class="colcatp">
                            <p>Gunakan aplikasi smartphone Avian Brands untuk mencari warna dari foto / gambar yang Anda miliki.</p>
                        </div>
                    </div>
                </a>
                <div class="lead"></div>
            </div>
        </div>
    </div>
</div>
