<?php if ($this->input->get('q')) { ?>
    <script>
        var filter = <?php echo $this->input->get('q') ?>;
    </script>
<?php } else { ?>
    <script>
        var filter = 0;
    </script>
<?php } ?>

<div class="container">
    <div class="section section-crumb mobilenogap">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <a href="/colours">WARNA</a>
                <span>KOLEKSI WARNA</span>
            </div>
        </div>
    </div>
    <div class="section section-smargin">
        <div class="section_content">
            <div class="searchcontain">
                <div class="searchbox">
                    <input type="text" placeholder="Cari Nama Warna" id="search-colour" value="<?= $colours_search ?>"/>
                </div>
                <button class="btn" type="button" id="search-btn"/>Cari</button>
                <button class="btn btn-sml" type="button" id="clear-btn"/>Hapus</button>
            </div>
        </div>
    </div>
    <div class="section gridds">
        <div class="section_content griddscontent" id="colours-container">
            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <?php
                if (count($models) > 0):
                    foreach ($models as $model):
            ?>
            <div class="griddsitem gridcol width-2 height-3">
                <div class="colourbox trigger coltrigger" targid="colpop" colcode="<?= $model['code'] ?>" colid="<?= $model['id'] ?>"
                colexist="<?= (count($color_likebox) > 0 && array_search($model['id'],$color_likebox) !== FALSE) ? 1 : 0  ?>">
                    <div class="colourboxtint autopalette" colhex="<?= $model['hex_code'] ?>"></div>
                    <div class="colourboxcode"><?= $model['code'] ?></div>
                    <div class="colourboxname"><?= $model['name'] ?></div>
                </div>
            </div>
            <?php
                    endforeach;
                endif;
            ?>
        </div>
    </div>
    <div class="section section-thirds">
        <div class="section_content">
            <div class="superbig-btn" id="loadmore">LIHAT SELANJUTNYA</div>
        </div>
    </div>
</div>
<div class="modal-contain targetcolpop">
    <div class="modal-shade modal-shade-white trigger" targid="colpop"></div>
    <div class="modal-window modal-col colhextarg">
        <div class="modal-content">
            <div class="modal-close trigger" targid="colpop"></div>
            <div class="modal-colinfo">
                <div class="modal-colname colnametarg">
                    Colour Name
                </div>
                <div class="modal-colcode colcodetarg">
                    0000000
                </div>
            </div>
        </div>
    </div>
</div>
