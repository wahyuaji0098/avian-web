<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Merchant Controller.
 */
class Merchant_manager extends Baseadmin_Controller  {

    private $_title         = "Merchant";
    private $_title_page    = '<i class="fa-fw fa fa-shopping-bag"></i> Merchant ';
    private $_breadcrumb    = "<li><a href='".MANAGER_HOME."'>Home</a></li>";
    private $_active_page   = "Merchant";
    private $_back          = "/manager/merchant/lists";
    private $_js_path       = "/js/pages/merchant/manager/merchant/";
    private $_view_folder   = "merchant/manager/merchant/";
    private $_table         = "mst_merchant";
    private $_table_aliases = "mer";
    private $_pk            = "id";

    /**
	 * constructor.
	 */
    public function __construct() {
        parent::__construct();

    }

    //////////////////////////////// VIEWS //////////////////////////////////////

    /**
     * List Merchant
     */
    public function lists() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> List of Merchant</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Merchant</li>',
        );

        //set footer attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/datatables/jquery.dataTables.min.js",
                "/js/plugins/datatables/dataTables.bootstrap.min.js",
                "/js/plugins/datatable-responsive/datatables.responsive.min.js",
                $this->_js_path . "list.js",
            )
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Create Merchant
     */
    public function create() {
        //set header attribute.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Create Merchant</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Merchant</li>',
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js"
            ),
        );

        //load the views.
        $this->load->view(MANAGER_HEADER , $header);
        $this->load->view($this->_view_folder . 'create');
        $this->load->view(MANAGER_FOOTER , $footer);
    }

    /**
     * Edit an Merchant
     */
    public function edit ($id = null) {
        if ($id == 0) show_error('Page is not existing', 404);
        $this->_breadcrumb .= '<li><a href="/manager/merchant/lists">Merchant</a></li>';

        $data['item'] = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "find_by_pk" => array($id),
            "row_array"  => TRUE
        ))['datas'];

        //prepare header title.
        $header = array(
            "title"         => $this->_title,
            "title_page"    => $this->_title_page . '<span>> Merchant</span>',
            "active_page"   => $this->_active_page,
            "breadcrumb"    => $this->_breadcrumb . '<li>Merchant</li>',
            "back"          => "/manager/Merchant/lists",
        );

        //set footer & css attribute (additional script and css).
        $footer = array(
            "script" => array(
                "/js/plugins/tinymce/tinymce.min.js",
                $this->_js_path . "create.js",
            )
        );

        //load the view.
        $this->load->view(MANAGER_HEADER, $header);
        $this->load->view($this->_view_folder . 'create', $data);
        $this->load->view(MANAGER_FOOTER, $footer);
    }

    //////////////////////////////// RULES //////////////////////////////////////

    ////////////////////////////// AJAX CALL ////////////////////////////////////

    /**
     * Function to get list Merchant
     */
    public function list_all_data() {
        //must ajax and must get.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "GET") {
			exit('No direct script access allowed');
		}

        //sanitize and get inputed data
        $sort_col = sanitize_str_input($this->input->get("order")['0']['column'], "numeric");
        $sort_dir = sanitize_str_input($this->input->get("order")['0']['dir']);
        $limit    = sanitize_str_input($this->input->get("length"), "numeric");
        $start    = sanitize_str_input($this->input->get("start"), "numeric");
        $search   = sanitize_str_input($this->input->get("search")['value']);
        $filter   = $this->input->get("filter");

		$select = array('id','name','email','last_login_date','logo_image','is_banned','status');

        $column_sort = $select[$sort_col];
        
        //initialize.
        $data_filters = array();
        $conditions = array();

        if (count ($filter) > 0) {
            foreach ($filter as $key => $value) {
                $value = sanitize_str_input($value);
                switch ($key) {
                    case 'id':
                        if ($value != "") {
                            $data_filters['id'] = $value;
                        }
                        break;

                    case 'name':
                        if ($value != "") {
                            $data_filters['name'] = $value;
                        }
                        break;

                    case 'email':
                        if ($value != "") {
                            $data_filters['email'] = $value;
                        }
                        break;

                    case 'banned':
                        if ($value != "") {
                            $data_filters['is_banned'] = ($value == "inactive") ? 1 : 0;
                        }
                        break;

                    case 'status':
                        if ($value != "") {
                            $data_filters['status'] = ($value == "active") ? 1 : 0;
                        }
                        break;

                    case 'lld':
                        if ($value != "") {
                            $date = parse_date_range($value);
                            $conditions["cast(lld as date) <="] = $date['end'];
                            $conditions["cast(lld as date) >="] = $date['start'];
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        //get data
        $datas = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
            "select"          => $select,
            "order_by"        => array($column_sort => $sort_dir),
            "limit"           => $limit,
            "start"           => $start,
            "conditions"      => $conditions,
            "filter"          => $data_filters,
            "count_all_first" => TRUE
		));

        //get total rows
        $total_rows = $datas['total'];

        $output = array(
            "data"            => $datas['datas'],
            "draw"            => intval($this->input->get("draw")),
            "recordsTotal"    => $total_rows,
            "recordsFiltered" => $total_rows,
		);

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($output);
        exit;
    }

    /**
     * Method to process adding or editing via ajax post.
     */
    public function process_form() {
        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //set secure to true
        $this->_secure = true;

        //load form validation lib.
        $this->load->library('form_validation');

        //initial.
        $message['is_error']    = true;
        $message['error_msg']   = "";
        $message['redirect_to'] = "";

        //sanitize input (id is primary key, if from edit, it has value).
        $id                = sanitize_str_input($this->input->post('id'));

        $name              = sanitize_str_input($this->input->post('name'));
        $email             = sanitize_str_input($this->input->post('email'));
        $password          = $this->input->post('password');
        $conf_password     = $this->input->post("conf_password");
        $verification_code = $this->input->post('verification_code');
        $description       = $this->input->post('description');
        $additional_info   = $this->input->post('additional_info');
        $additional_link   = $this->input->post('additional_link');
        $data_image        = $this->input->post('data-image');

        if (!empty($conf_password)) {
            if ($conf_password != $password) {
                $message['error_msg'] = "Confirm password doesn't match.";
                echo json_encode($message);
                exit;
            }            
        }
        
        $arrayToDB = array(
            "name"              => $name,
            "email"             => $email,
            "password"          => $password,
            "verification_code" => $verification_code,
            "description"       => $description,
            "additional_info"   => $additional_info,
            "additional_link"   => $additional_link
        );

        $logo = $this->upload_image("logo", "upload/merchant", "image-file", $data_image, 640, 640, $id);

        // Begin transaction.
        $this->db->trans_begin();

        //load the model.
        $this->load->model('Merchant_model');

        // Insert or update?
        if ($id == "") {

            $valid_email = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                    "row_array"  => true,
                    "conditions" => array("email" => $email)
                ))['datas'];

            if (!empty($valid_email)) {
                $message['error_msg'] = 'Data E-Mail already exist.';
                echo json_encode($message);
                exit;
            }

            if (!empty($logo)) {
                $arrayToDB['logo_image'] = $logo;
            }

            // Insert to DB.
            $result = $this->Merchant_model->insert($arrayToDB);

            // Send email to user after user created.
            if ($result) {
                // Get content from view
                $content = $this->load->view('layout/email/login_detail', '', TRUE);
                $content = str_replace('%NAME%', $name, $content);
                $content = str_replace('%USERNAME%', $email, $content);
                $content = str_replace('%PASSWORD%', $password, $content);

                $mail = sendmail(array(
                    'subject' => SUBJECT_PASSWORD_INFO,
                    'message' => $content,
                    'to'      => array($email)
                ), "html");
            } 
            
        } else {
            $valid_email = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                    "row_array"  => true,
                    "conditions" => array(
                        "id !=" => $id,
                        "email" => $email
                    )
                ))['datas'];

            if (!empty($valid_email)) {
                $message['error_msg'] = 'Data E-Mail already exist.';
                echo json_encode($message);
                exit;
            }

            // Condition for update.
            $condition = array("id" => $id);
            $get_img = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->get_all_data(array(
                    "select"     => array("logo_image"),
                    "conditions" => $condition,
                    "row_array"  => TRUE
                )
            )["datas"];

            // update.
            if (!empty($logo) && isset($get_img['logo_image']) && !empty($get_img['logo_image'])) {
                unlink( FCPATH . $get_img['logo_image'] );
            }

            if (!empty($logo)) {
                $arrayToDB['logo_image'] = $logo;
            }
            // Update to DB.
            $result = $this->Merchant_model->update($arrayToDB, $condition);
        }

        // End transaction.
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            $message['error_msg'] = 'database operation failed.';
        } else {
            $this->db->trans_commit();
            $message['is_error'] = false;
            // success.
            // growler.
            $message['notif_title']   = "Good!";
            if (empty($id)) {
                $message['notif_message'] = "Merchant has been added.";
            } else {
                $message['notif_message'] = "Merchant has been updated.";
            }
            // redirected.
            $message['redirect_to'] = "/manager/merchant/lists";
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Hide an Merchant.
     */
    public function hide() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data merchant
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();
                //delete the data (deactivate)
                $condition = array($this->_pk => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "status" => 0 ),
                    $condition, 
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => FALSE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Merchant has been hide.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Show an Merchant.
     */
    public function show() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data merchant
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();
                //delete the data (deactivate)
                $condition = array($this->_pk => $id);
                $delete = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "status" => 1 ),
                    $condition, 
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => FALSE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Merchant has been show.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Banned an Merchant.
     */
    public function banned() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        // check first.
        if (!empty($id)) {

            //get data merchant
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                // begin transaction
                $this->db->trans_begin();
                // ban the data
                $condition = array($this->_pk => $id);
                $ban = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "is_banned" => 1 ),
                    $condition, 
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => FALSE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Merchant has been ban.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

    /**
     * Unbanned an Merchant.
     */
    public function unbanned() {

        //must ajax and must post.
        if (!$this->input->is_ajax_request() || $this->input->method(true) != "POST") {
            exit('No direct script access allowed');
        }

        //initial.
        $message['is_error']    = true;
        $message['redirect_to'] = "";
        $message['error_msg']   = "";

        //sanitize input (id is primary key).
        $id = sanitize_str_input($this->input->post('id'));

        $_model = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk);

        //check first.
        if (!empty($id)) {

            //get data merchant
            $data = $_model->get_all_data(array(
                "find_by_pk" => array($id),
                "row_array"  => TRUE
            ))['datas'];

            //no data is found with that ID.
            if (empty($data)) {
                $message['error_msg'] = 'Invalid ID.';
            } else {

                //begin transaction
                $this->db->trans_begin();
                // unban the data
                $condition = array($this->_pk => $id);
                $unban = $this->_dm->set_model($this->_table, $this->_table_aliases, $this->_pk)->update(
                    array( "is_banned" => 0 ),
                    $condition, 
                    array(
                        "is_direct"  => FALSE,
                        "is_version" => FALSE,
                        "ordering"   => FALSE
                    )
                );

                //end transaction.
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    //failed.
                    $message['error_msg'] = 'database operation failed';
                } else {
                    $this->db->trans_commit();
                    //success.
                    $message['is_error']  = false;
                    $message['error_msg'] = '';
                    //growler.
                    $message['notif_title']   = "Done!";
                    $message['notif_message'] = "Merchant has been unbanned.";
                    $message['redirect_to']   = "";
                }
            }
        } else {
            //id is not passed.
            $message['error_msg'] = 'Invalid ID.';
        }

        //encoding and returning.
        $this->output->set_content_type('application/json');
        echo json_encode($message);
        exit;
    }

}
