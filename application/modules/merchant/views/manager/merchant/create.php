<?php
    $id = isset($item["id"]) ? $item["id"] : "";
    $name  = isset($item["name"]) ? $item["name"] : "";
    $email = isset($item["email"]) ? $item["email"] : "";
    $logo_image  = isset($item["logo_image"]) ? $item["logo_image"] : "";
    $unique_code = isset($item["unique_code"]) ? $item["unique_code"] : "";
    $verification_code = isset($item["verification_code"]) ? $item["verification_code"] : "";

    $description     = isset($item["description"]) ? $item["description"] : "";
    $additional_info = isset($item["additional_info"]) ? $item["additional_info"] : "";
    $additional_link = isset($item["additional_link"]) ? $item["additional_link"] : "";
    
    $status_name  = isset($item["status_name"]) ? $item["status_name"] : "";
    $created_date = isset($item["created_date"]) ? $item["created_date"] : "";
    $updated_date = isset($item["updated_date"]) ? $item["updated_date"] : "";
    $last_login_date  = isset($item["last_login_date"]) ? $item["last_login_date"] : "";

    $btn_msg   = ($id == 0) ? "Create" : " Update";
    $title_msg = ($id == 0) ? "Create" : " Update";
    $data_edit = ($id == 0) ? 0 : 1;
?>
<!-- MAIN CONTENT -->
<div id="content">
    <div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark"><?= $title_page ?></h1>
		</div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 col-lg-offset-1 text-right">
			<h1>
                <button class="btn btn-warning back-button" onclick="window.location.href='/manager/merchant/lists';" title="Back" rel="tooltip" data-placement="left" data-original-title="Batal">
					<i class="fa fa-arrow-circle-left fa-lg"></i>
				</button>
				<button class="btn btn-primary submit-form" data-form-target="create-form" title="Save" rel="tooltip" data-placement="top" >
					<i class="fa fa-floppy-o fa-lg"></i>
				</button>
			</h1>
		</div>
	</div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <div class="row">
            <!-- NEW WIDGET ROW START -->
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-0"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-pencil-square-o"></i> </span>
                        <h2><?= $title_msg ?></h2>
                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="create-form" action="/manager/merchant/process-form" method="POST">
                            <header>Merchant form</header>
                            <?php if($id != 0): ?>
                                <input type="hidden" name="id" value="<?= $id ?>" />
                            <?php endif; ?>
                                <fieldset>
                                    <section>
                                        <label class="label">Name</label>
                                        <label class="input">
                                            <input name="name" id="name" type="text"  class="form-control" placeholder="Name" value="<?php echo $name; ?>" />
                                        </label>
                                    </section>
                                    <div class="row">
                                        <?php if($id != 0){ ?>
                                            <section class="col col-12">
                                                <label class="label">E-Mail</label>
                                                <label class="input">
                                                    <input name="email" id="email" type="text" class="form-control" placeholder="john@example.com" value="<?php echo $email; ?>" />
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Password</label>
                                                <label class="input">
                                                    <input name="password" id="password" type="password" class="form-control" placeholder="" value="" />
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Confirm Password</label>
                                                <label class="input">
                                                    <input name="conf_password" id="conf_password" type="password" class="form-control" placeholder="" value="" />
                                                </label>
                                            </section>
                                        <?php } else { ?>
                                            <section class="col col-6">
                                                <label class="label">E-Mail</label>
                                                <label class="input">
                                                    <input name="email" id="email" type="text" class="form-control" placeholder="john@example.com" value="<?php echo $email; ?>" />
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Password</label>
                                                <label class="input">
                                                    <input name="password" id="password" type="password" class="form-control" placeholder="" value="" />
                                                </label>
                                            </section>
                                        <?php } ?>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6 pull-right">
                                            <label class="label">Verification Code</label>
                                            <label class="input">
                                                <input name="verification_code" id="verification_code" type="text" maxlength="30" class="form-control" placeholder="" value="<?php echo $verification_code; ?>" />
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6 pull-right">
                                            <label class="label">Image View (640x640 px)</label>
                                            <div class="input">
                                                <div class="add-image-preview" id="preview-image">
                                                    <?php if($logo_image): ?>
                                                        <img src="<?= $logo_image ?>" height="100px"/>
                                                    <?php endif; ?>
                                                </div>
                                                <button type="button" class="btn btn-primary btn-sm" id="add-image" data-maxsize="<?= MAX_UPLOAD_IMAGE_SIZE ?>" data-maxwords="<?= WORDS_MAX_UPLOAD_IMAGE_SIZE ?>" data-edit="<?= $data_edit ?>"><?= ($logo_image != "") ? "Change" : "Add" ?> Image</button>
                                            </div>
                                        </section>
                                    </div><hr>
                                    <fieldset>
                                        <section>
                                            <label class="label">Description </label>
                                            <label class="input"> 
                                                <textarea name="description" id="description" class="form-control tinymce" rows="1" placeholder=" Untuk deskripsi dari Merchant, sebaiknya 70 karakter minimum."><?php echo $description; ?></textarea>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="label">Additional Info </label>
                                            <label class="input"> 
                                                <textarea name="additional_info" id="additional_info" class="form-control tinymce" rows="1" placeholder=" Additional Info untuk Merchant, sebaiknya 70 karakter minimum."><?php echo $additional_info; ?></textarea>
                                            </label>
                                        </section>
                                        <section>
                                            <label class="label">Additional Link </label>
                                            <label class="input"> 
                                                <textarea name="additional_link" id="additional_link" class="form-control tinymce" rows="1" placeholder=" Additional Link untuk Merchant."><?php echo $additional_link; ?></textarea>
                                            </label>
                                        </section>
                                    </fieldset>                                       
                                </fieldset>
                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>

            <?php if($id != 0): ?>
            <article class="col-sm-12 col-md-12 col-lg-12">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1"
                data-widget-editbutton="false"
                data-widget-deletebutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-eye"></i> </span>
                        <h2>Informasi Tambahan</h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <form class="smart-form" id="addon-form" method="post">


                            <fieldset>
                                <div class="row">
                                    <section class="col col-12">
                                        <label class="label">Active</label>
                                        <label class="have_data">
                                            <div><?= $status_name ?></div>
                                        </label>
                                    </section>
                                </div>
                                <div class="row">
                                    <section class="col col-4">
                                        <label class="label">Created Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($created_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Updated Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($updated_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                    <section class="col col-4">
                                        <label class="label">Last Login Date</label>
                                        <label class="have_data">
                                            <div><?= dateformatforview($last_login_date, "d F Y H:i:s") ?></div>
                                        </label>
                                    </section>
                                </div>
                            </fieldset>

                        </form>

                    </div>
                    <!-- end widget content -->


                </div>
                <!-- end widget div -->

            </article>
            <?php endif; ?>

        </div>
    </section> <!-- end widget grid -->
</div> <!-- END MAIN CONTENT -->
