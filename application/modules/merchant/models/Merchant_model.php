<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Merchant_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table       = 'mst_merchant';
        $this->_table_alias = 'mer';
        $this->_pk_field    = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    public function insert($datas, $extra_param = array("is_direct" => false))
    {
        if (isset($datas['password'])) {
			$options = [
				'cost' => 8,
			];
			$datas['password'] = password_hash($datas['password'], PASSWORD_BCRYPT, $options);
		}

        $datas['created_date'] = date("Y-m-d H:i:s");

        //add create date and update date
        $this->db->insert($this->_table, $datas);

        return $this->db->insert_id();
    }

    public function update($datas, $condition, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $options = [
                'cost' => 8,
            ];
            $datas['password'] = password_hash($datas['password'], PASSWORD_BCRYPT, $options);
        }

        $datas['updated_date'] = date("Y-m-d H:i:s");

        return $this->db->update($this->_table, $datas, $condition);
    }

}
