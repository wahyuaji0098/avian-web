<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Cron job functions is here.
 * mostly it's for getting data from windows server.
 */
class Cron extends MX_Controller  {

    private $_dm;
    private $_base_api_url;

    public function __construct() {
        parent::__construct();

        //load dynamic_model for easier database usage.
        $this->load->model("Dynamic_model");
        $this->_dm = $this->Dynamic_model;

        $this->_base_api_url = "http://36.66.214.244:81";
        //$this->_base_api_url = "http://avianvm.windows.idevelopit.net";
    }

    /**
     * this public method cron job is to get customer data.
     * since this data is just too big, this special job must be repeated more frequently.
     */
    public function get_customer () {
        if (!$this->input->is_cli_request()) {
            exit('No direct script access allowed');
        }

        //call api, set up method and url and api key
        $api_key    = API_KEY_AVIANVM;
        $method     = "GET";
        $url        = $this->_base_api_url."/api/v1/customer/data";

        //get latest version.
        $cus_last_ver = get_last_ver("dtb_store_v2");

        $data = array(
            "api_key" => $api_key,
            "cus_version" => $cus_last_ver,
            "mode" => "avian",
            "limiter" => 2500,
        );
        //call api
        $result = CallAPI($method, $url, $data);

        //decode result yang isinya json
        $result = json_decode($result, true);

        if ($result['result'] == "OK") {

            $data_customer   = $result['datas']['store'];

            //begin transaction.
            $this->db->trans_begin();

            //to dtb_store_v2.
            if (!empty($data_customer)) {
                if (count($data_customer) > 0) {
                    foreach ($data_customer as $key => $cus) {
                        $this->_dm->set_model("dtb_store_v2", "ds", "customer_id")->replace(array(
                            "customer_id"                   => $cus["customer_id"],
                            "kode_customer"                 => $cus["kode_customer"],
                            "nama_customer"                 => $cus["nama_customer"],
                            "latitude"                      => $cus["latitude"],
                            "longitude"                     => $cus["longitude"],
                            "punya_mesin_tinting"           => $cus["punya_mesin_tinting"],
                            "telepon"                       => $cus["telepon"],
                            "no_hp_1"                       => $cus["no_hp_1"],
                            "no_hp_2"                       => $cus["no_hp_2"],
                            "fax"                           => $cus["fax"],
                            "website"                       => $cus["website"],
                            "email"                         => $cus["email"],
                            "foto_toko_url"                 => $cus["foto_toko_url"],
                            "alamat"                        => $cus["alamat"],
                            "version"                       => $cus['version'],
                            "is_active"                     => $cus['is_active'],
                            "jual_pipa_power"               => $cus["jual_pipa_power"],
                            "jual_pipa_power_max"           => $cus["jual_pipa_power_max"],
                            "jual_fitting_power"            => $cus["jual_fitting_power"],
                            "jual_talang_power"             => $cus["jual_talang_power"],
                            "jual_fitting_talang_power"     => $cus["jual_fitting_talang_power"],
                            "jual_no_odor"                  => $cus["jual_no_odor"],
                            "jual_fres"                     => $cus["jual_fres"],
                            "jual_avitex_exterior"          => $cus["jual_avitex_exterior"],
                            "jual_boyo_plamir"              => $cus["jual_boyo_plamir"],
                            "jual_no_drop"                  => $cus["jual_no_drop"],
                            "jual_avitex"                   => $cus["jual_avitex"],
                            "jual_aries_gold"               => $cus["jual_aries_gold"],
                            "jual_aries"                    => $cus["jual_aries"],
                            "jual_sunguard"                 => $cus["jual_sunguard"],
                            "jual_supersilk"                => $cus["jual_supersilk"],
                            "jual_everglo"                  => $cus["jual_everglo"],
                            "jual_aquamatt"                 => $cus["jual_aquamatt"],
                            "jual_avitex_alkali_resisting_primer" => $cus["jual_avitex_alkali_resisting_primer"],
                            "jual_no_drop_107"              => $cus["jual_no_drop_107"],
                            "jual_no_drop_100"              => $cus["jual_no_drop_100"],
                            "jual_no_drop_bitumen_black"    => $cus["jual_no_drop_bitumen_black"],
                            "jual_absolute"                 => $cus["jual_absolute"],
                            "jual_avitex_roof"              => $cus["jual_avitex_roof"],
                            "jual_belmas_roof"              => $cus["jual_belmas_roof"],
                            "jual_yoko_roof"                => $cus["jual_yoko_roof"],
                            "jual_lem_putih_vip_pvac"       => $cus["jual_lem_putih_vip_pvac"],
                            "jual_lem_putih_max_pvac"       => $cus["jual_lem_putih_max_pvac"],
                            "jual_avian_road_line_paint"    => $cus["jual_avian_road_line_paint"],
                            "jual_wood_eco_woodstain"       => $cus["jual_wood_eco_woodstain"],
                            "jual_boyo_politur_vernis"      => $cus["jual_boyo_politur_vernis"],
                            "jual_tan_politur"              => $cus["jual_tan_politur"],
                            "jual_avian_hammertone"         => $cus["jual_avian_hammertone"],
                            "jual_suzuka_lacquer"           => $cus["jual_suzuka_lacquer"],
                            "jual_viplas"                   => $cus["jual_viplas"],
                            "jual_vip_paint_remover"        => $cus["jual_vip_paint_remover"],
                            "jual_avian_anti_fouling"       => $cus["jual_avian_anti_fouling"],
                            "jual_avian_lem_epoxy"          => $cus["jual_avian_lem_epoxy"],
                            "jual_avian_non_sag_epoxy"      => $cus["jual_avian_non_sag_epoxy"],
                            "jual_thinner_a_avia"           => $cus["jual_thinner_a_avia"],
                            "jual_lenkote_colorants"        => $cus["jual_lenkote_colorants"],
                            "jual_giant_mortar_220"         => $cus["jual_giant_mortar_220"],
                            "jual_giant_mortar_260"         => $cus["jual_giant_mortar_260"],
                            "jual_giant_mortar_270"         => $cus["jual_giant_mortar_270"],
                            "jual_giant_mortar_380"         => $cus["jual_giant_mortar_380"],
                            "jual_giant_mortar_480"         => $cus["jual_giant_mortar_480"],
                            "jual_platinum"                 => $cus["jual_platinum"],
                            "jual_avian"                    => $cus["jual_avian"],
                            "jual_yoko"                     => $cus["jual_yoko"],
                            "jual_belmas_zinchromate"       => $cus["jual_belmas_zinchromate"],
                            "jual_avian_zinchromate"        => $cus["jual_avian_zinchromate"],
                            "jual_yoko_loodmeni"            => $cus["jual_yoko_loodmeni"],
                            "jual_thinner_a_special_avia"   => $cus["jual_thinner_a_special_avia"],
                            "jual_yoko_yzermenie"           => $cus["jual_yoko_yzermenie"],
                            "jual_glovin"                   => $cus["jual_glovin"],
                            "jual_no_odor_wall_putty"       => $cus["jual_no_odor_wall_putty"],
                            "jual_lenkote_alkali_resisting_primer"  => $cus["jual_lenkote_alkali_resisting_primer"],
                            "jual_no_lumut_solvent_based"   => $cus["jual_no_lumut_solvent_based"],
                            "jual_no_drop_tinting"          => $cus["jual_no_drop_tinting"],
                            "jual_avian_1kg"                => $cus["jual_avian_1kg"],
                            "rating_avianbrands"            => $cus["rating_avianbrands"],
                        ), array("is_direct" => true));

                        //check if store exist in mst_store_addon
                        $exist = $this->_dm->set_model("mst_store_addon", "msa", "kode_customer")->get_all_data(array(
                            "conditions" => array(
                                "lower(kode_customer)" => strtolower($cus["kode_customer"]),
                            ),
                            //"row_array" => true,
                        ))['datas'];

                        //get last ver of mst_store_addon
                        $store_addon_last_ver = get_last_ver("mst_store_addon") + 1;

                        if (!empty($exist)) {
                            //update version only.
                            $this->_dm->set_model("mst_store_addon", "msa", "kode_customer")->update(array(
                                "version"                 => $store_addon_last_ver,
                            ), array(
                                "kode_customer"           => $cus["kode_customer"]
                            ));
                        } else{
                            //insert new row.
                            $mergedPhone = "";
                            if (!empty($cus["telepon"])) $mergedPhone = $cus["telepon"];
                            if (!empty($cus["no_hp_1"])) {
                                if (!empty($cus["telepon"])) $mergedPhone .= ", ";
                                $mergedPhone .= $cus["no_hp_1"];
                            }
                            if (!empty($cus["no_hp_2"])) {
                                if (!empty($cus["telepon"] || !empty($cus["no_hp_1"]))) $mergedPhone .= ", ";
                                $mergedPhone .= $cus["no_hp_2"];
                            }
                            $insertData = array(
                                "kode_customer"           => $cus["kode_customer"],
                                "pretty_url"              => url_title($cus["nama_customer"] . " " . substr($cus["kode_customer"], -5) ),
                                "version"                 => $store_addon_last_ver,
                                "phone"                   => $mergedPhone,
                                "fax"                     => $cus["fax"],
                                "email"                   => $cus["email"],
                                "website"                 => $cus["website"],
                            );
                            $this->_dm->set_model("mst_store_addon", "msa", "kode_customer")->insert($insertData);
                        }
                    }
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                echo "[cron] get customer failed.";
            } else {
                $this->db->trans_commit();
                echo "[cron] get customer success.";
            }
        }
    }
}
