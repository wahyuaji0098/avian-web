<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Terms Controller.
 */
class Terms extends Basepublic_Controller  {
    private $_view_folder = "terms/front/";

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        //get header page
		$page = get_page_detail('terms-condition');

        $header = array(
            'header'    => $page,
        );
        redirect(base_url());

        exit;
        //load the views.
        $this->load->view(FRONT_HEADER, $header);
        $this->load->view($this->_view_folder . 'index');
        $this->load->view(FRONT_FOOTER);
    }
}
