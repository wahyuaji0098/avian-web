<div class="container">
    <div class="section section-crumb">
        <div class="section_content">
            <div class="breadcrumbs">
                <a href="/">BERANDA</a>
                <span>SYARAT &amp; KETENTUAN</span>
            </div>
            <h2 class="section-title section-title-top">SYARAT &amp; KETENTUAN</h2>
            <div class="ps">
                <div>
                   Dengan mengakses dan menggunakan www.AvianBrands.com atau situs-situs lain Avian Brands Anda setuju dengan syarat dan ketentuan sebagai berikut:
                </div>
                <div>
                    Website ini, termasuk konten serta seleksi dan pengaturan isi dari masing-masing halaman atau dari koleksi halaman, yang dimiliki sepenuhnya oleh Avian Brands, kecuali dinyatakan lain. Seluruh hak cipta. Anda diperbolehkan untuk menyimpan kutipan dari situs Avian Brands dan mencetak salinan dari mereka untuk penggunaan pribadi, non-komersial saja. Jenis lain dari penggunaan, reproduksi, terjemahan, adaptasi, pengaturan, perubahan lain, distribusi atau penyimpanan situs ini dalam bentuk apapun dan dengan cara apapun, secara keseluruhan atau sebagian, tanpa izin tertulis dari Avian Brands dilarang. Avian Brands tidak bertanggung jawab atas isi dari website lain, yang tidak di bawah kendali Avian Brands di mana Anda mungkin telah mendapatkan akses ke website kami. Avian Brands tidak bertanggung jawab sehubungan materi pada website lain.
                </div>

                <div>
                    <span>Kebijakan Privasi</span>
                </div>
                <div>
                    Avian Brands menghargai semua kunjungan ke website kami dan kepentingan dalam layanan dan produk kami. Kami berusaha untuk membuat semua kunjungan ke situs kami adalah aman, dan kami menjamin bahwa informasi pribadi aman dengan kami. Setiap informasi pribadi anda yang diberikan kepada kami, akan kami gunakan untuk membantu anda.
                </div>
                <div>
                    Berikut ini adalah cara di mana kita menggunakan informasi ini:
                </div>
                <div>
                    Informasi dikumpulkan dan disimpan untuk membantu kami dalam memenuhi permintaan informasi dan menyediakan Anda dengan informasi yang relevan. Kunjungan ke situs kami dilacak untuk menilai bagian mana dari situs terbaik melayani pengunjung kami, dan membantu kami di masa depan. Kami tidak menjual salah satu informasi yang kami terima dari pengunjung ke situs kami. Harap dicatat bahwa website kami mungkin berisi link ke lainnya, situs non-Avian Brands. Avian Brands tidak bertanggung jawab atas praktik privasi dari situs-situs tersebut.
                </div>

                <div>
                    <span>Kebijakan Cookie </span>
                </div>

                <div>
                    Dengan menggunakan situs Avian Brands, Anda menyetujui penggunaan cookie sesuai dengan Kebijakan Cookie ini. Jika Anda tidak setuju untuk kami menggunakan cookies dengan cara ini, Anda harus menetapkan pengaturan browser Anda sesuai atau tidak menggunakan Situs Avian Brands. Jika Anda menonaktifkan cookie yang kita gunakan, ini dapat mempengaruhi pengalaman pengguna Anda saat di Situs Avian Brands.
                </div>

                <div>
                    Kebijakan Cookie ini berlaku untuk setiap situs web, halaman bermerek pada platform pihak ketiga (seperti Facebook atau YouTube), dan aplikasi diakses atau digunakan melalui website tersebut atau platform pihak ketiga ( "Situs Avian Brands") yang dioperasikan oleh atau atas nama Avian Brands.
                </div>

                <div>
                    Apa itu cookie? Cookies adalah file atau potongan informasi yang dapat disimpan pada komputer Anda atau perangkat internet diaktifkan lain, ketika Anda mengunjungi situs Avian Brands. Cookie biasanya akan berisi nama website dari mana cookie telah datang dari, "lifetime" dari cookie (yaitu berapa lama akan tetap pada perangkat Anda), dan Value, yang biasanya nomor unik yang dihasilkan secara acak.
                </div>

                <div>
                    Kami menggunakan cookies untuk membuat situs Avian Brands kita lebih mudah digunakan dan untuk lebih menyesuaikan situs Avian Brands dan produk dan layanan kami kepada kepentingan dan kebutuhan Anda. Cookies juga dapat digunakan untuk membantu meningkatkan aktivitas masa depan Anda dan pengalaman di situs kami. Kami juga menggunakan cookies untuk mengkompilasi anonim, statistik gabungan yang memungkinkan kita untuk memahami bagaimana orang menggunakan situs kami dan untuk membantu kami memperbaiki struktur dan konten.
                </div>

                <div>
                    Apa jenis cookies yang kita gunakan? Dua jenis cookies dapat digunakan pada situs Avian Brands - "session cookies" dan "persistent cookies". Session cookies adalah cookie sementara yang tetap pada perangkat Anda sampai Anda meninggalkan situs Avian Brands. Cookie persisten tetap pada perangkat Anda untuk lebih lama lagi atau sampai Anda secara manual menghapusnya (berapa lama cookie tetap pada perangkat Anda akan tergantung pada durasi atau "lifetime" dari cookie tertentu dan pengaturan browser Anda).
                </div>

                <div>
                    Beberapa halaman yang Anda kunjungi juga dapat mengumpulkan informasi menggunakan tag pixel (juga disebut clear gifs) yang dapat dipergunakan bersama dengan pihak ketiga yang langsung mendukung kegiatan promosi dan pengembangan website. Misalnya, situs informasi penggunaan tentang pengunjung ke situs Avian Brands dapat dibagi dengan agen periklanan pihak ketiga kami untuk lebih menargetkan iklan Internet banner di website kami. Informasi ini bisa dikaitkan dengan informasi pribadi Anda.
                </div>

                <div>
                    Apakah kita menggunakan cookies pihak ketiga? Kami menggunakan sejumlah pemasok yang juga dapat mengatur cookie pada perangkat Anda atas nama kami ketika Anda mengunjungi situs Avian Brands untuk memungkinkan mereka untuk memberikan layanan yang mereka sediakan.
                </div>

                <div>
                    Ketika Anda mengunjungi situs Avian Brands Anda mungkin menerima cookie dari situs web pihak ketiga atau domain. Kami berusaha untuk mengidentifikasi cookies ini sebelum mereka digunakan sehingga Anda dapat memutuskan apakah Anda ingin menerima mereka. Informasi lebih lanjut tentang cookie ini mungkin tersedia di situs web pihak ketiga yang bersangkutan.
                </div>

                <div>
                    Bagaimana saya bisa mengontrol atau menghapus cookies? Kebanyakan browser internet pada awalnya dibentuk untuk secara otomatis menerima cookie. Anda dapat mengubah pengaturan untuk memblokir cookie atau untuk memberitahukan Anda ketika cookie dikirim ke perangkat Anda. Ada sejumlah cara untuk mengelola cookie. Silakan lihat petunjuk browser atau bantuan layar untuk mempelajari lebih lanjut tentang cara menyesuaikan atau memodifikasi pengaturan browser Anda.
                </div>

                <div>
                    Jika Anda menonaktifkan cookie yang kita gunakan, ini dapat mempengaruhi pengalaman Anda saat di situs Avian Brands, misalnya Anda mungkin tidak dapat mengunjungi daerah-daerah tertentu dari situs Avian Brands atau Anda mungkin tidak menerima personalisedinformation ketika Anda mengunjungi situs Avian Brands.
                </div>

                <div>
                    Jika Anda menggunakan perangkat yang berbeda untuk melihat dan mengakses situs Avian Brands (misalnya komputer, smartphone, tablet dll) Anda akan perlu memastikan bahwa masing-masing browser pada setiap perangkat disesuaikan dengan preferensi cookies Anda.
                </div>

                <div>
                    <span>Sangkalan Tanggung Jawab</span>
                </div>
                <div>
                    Segala upaya dilakukan untuk menyediakan informasi yang akurat dan lengkap. Namun, kami tidak dapat menjamin bahwa tidak akan ada kesalahan. Avian Brands tidak membuat klaim, janji atau jaminan tentang keakuratan, kelengkapan, atau kecukupan isi dari situs ini dan menolak kewajiban atas ketimpangan, kesalahan dan kelalaian dalam isi situs ini.
                </div>
                <div>
                    Semua warna yang disajikan di situs dapat bervariasi tergantung kondisi pencahayaan, peralatan teknis, penggunaan yang berbeda dari web browser dan monitor, finish, tingkat gloss, jenis permukaan dan warna yang berdekatan. Untuk pertandingan warna penting kami merekomendasikan penggunaan sampel warna dicat fisik. Dalam beberapa situasi penyimpangan dapat terjadi antara sampel ditampilkan karena metode produksi, pigmen dll
                </div>
                <div>
                    Sehubungan dengan isi website Avian Brands, tbaik Avian Brands, atau afiliasinya, karyawan dan kontraktor memberikan jaminan, tersurat maupun tersirat atau hukum, termasuk tetapi tidak terbatas pada jaminan non-pelanggaran hak pihak ketiga, judul, dan jaminan diperjualbelikan dan kesesuaian untuk tujuan tertentu sehubungan dengan konten yang tersedia dari situs Avian Brands atau sumber Internet lainnya yang terhubung dari itu. Begitu juga Avian Brands maupun anak perusahaannya bertanggung jawab hukum atas langsung, tidak langsung atau lainnya kehilangan atau kerusakan apapun untuk akurasi, kelengkapan, atau kegunaan dari informasi, produk, atau proses diungkapkan di sini, atau kebebasan dari virus komputer , dan tidak menyatakan bahwa penggunaan informasi tersebut, produk, atau proses tidak akan melanggar hak milik pribadi.
                </div>
                <div>
                    Pemberitahuan ini akan diatur oleh dan ditafsirkan sesuai dengan hukum di Indonesia.
                </div>
                <div>
                    Jika ada ketentuan dari pemberitahuan ini akan melanggar hukum, batal atau karena alasan apapun tidak dapat dilaksanakan maka ketentuan tersebut akan dianggap terpisah dan tidak akan mempengaruhi keabsahan dan keberlakuan dari ketentuan lainnya.
                </div>

                <div>
                    <span>Updated 1 Mei 2016.</span>
                </div>
            </div>
        </div>
    </div>
</div>
