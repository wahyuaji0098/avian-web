<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Device_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = 'dtb_push_device';
        $this->_table_alias = 'dpd';
        $this->_pk_field = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    /*==============================================================================
     * API FUNCTION
     *==============================================================================*/
    //check api key exists or not.
    private function _key_exists($key) {
        return $this->db->where("api_key", $key)
                        ->count_all_results($this->_table) > 0;
    }

    /**
 	 * generate api key for login device.
 	 */
    public function generate_api_key() {
        do {
            // Generate unique_id
            $new_key = uniqid('',true);
        }
        while ($this->_key_exists($new_key));
        // Already in the DB? Fail. Try again

        return $new_key;
    }
}
