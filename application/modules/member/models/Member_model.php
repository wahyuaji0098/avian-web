<?php if (!defined("BASEPATH")) exit('No direct script access allowed');

class Member_model extends Base_Model {

    public function __construct() {
        parent::__construct();
        $this->_table = 'dtb_member';
        $this->_table_alias = 'dm';
        $this->_pk_field = 'id';
    }

    /**
     * extending _get_row function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_row($result)
    {
        return $result;
    }

    /**
     * extending _get_array function in base class.
     * see base_model for more info.
     */
    protected function _extend_get_array($result)
    {
        return $result;
    }

    /**
     * extending insert function in base class.
     * see base_model for more info.
     */
    protected function _extend_insert($datas)
    {
        //need to extend something?
    }

    /**
     * extending update function in base class.
     * see base_model for more info.
     */
    protected function _extend_update($datas, $condition)
    {
        //need to extend something?
    }

    /**
     * extending delete function in base class.
     * see base_model for more info.
     */
    protected function _extend_delete($condition)
    {

    }

    public function insert($datas, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $datas['password'] =  sha1( PASSWORD_FRONT . $this->db->escape_str($datas['password']));
		}

        $datas['created_date'] = date("Y-m-d H:i:s");

        //add create date and update date
        $this->db->insert($this->_table, $datas);

        return $this->db->insert_id();
    }

    public function update($datas, $condition, $extra_param = array())
    {
        if (isset($datas['password'])) {
            $datas['password'] =  sha1( PASSWORD_FRONT . $this->db->escape_str($datas['password']));
        }

        $datas['updated_date'] = date("Y-m-d H:i:s");

        return $this->db->update($this->_table, $datas, $condition);
    }


    /**
     * check username and password (for login).
     */
	public function check_login($username, $password) {

		//get data by username.
        $this->db->where("username" , $username);

		$user = $this->db->get($this->_table)->row_array();

		//if no user found, it's wrong then.
		if (!$user) {
			return false;
		}

		//check password.
		if ($user['password'] == sha1( PASSWORD_FRONT . $this->db->escape_str($password)) ) {
			return $user;
		}

		return false;
	}

    public function check_login_by_email($email, $password) {

        //get data by email.
        $this->db->where("email" , $email);

        $user = $this->db->get($this->_table)->row_array();

        //if no user found, it's wrong then.
        if (!$user) {
            return false;
        }

        //check password.
        if ($user['password'] == sha1( PASSWORD_FRONT . $this->db->escape_str($password)) ) {
            return $user;
        }

        return false;
    }

    public function check_password ($password, $password_conf) {
        if ($password == sha1(PASSWORD_FRONT.trim($password_conf))) {
            return true;
        }

        return false;
    }

    public function sendResetPassword($datas,$member_id) {
        $code = $this->_generate_link_conf_code($member_id);

        $link = base_url()."forgot-password/resets/".urlencode(base64_encode($code));

        // //butuh email html nya
        //get template email contact
        $content = $this->load->view('layout/email/front/template_forgot', '', true);
        $content = str_replace('%NAME%',$datas['name'],$content);
        $content = str_replace('%LINK_FORGOT%',$link,$content);
        $content = str_replace('%LOGO%',base_url().'img/ui/logo-head.png',$content);

        $mail = sendmail (array(
			'subject'			=> SUBJECT_RESET_PASSWORD,
			'message'			=> $content,
			'to'			    => array($datas['email']),
		), "html");

        return $mail;
    }

    public function sendmailRegister($datas) {
        //send to user
        $mail_user = sendmail (array(
            'subject'			=> SUBJECT_USER_REGISTRATION,
            'message'			=> 'Dear, '. $datas['name'] .'<br /><br />'.
                                    'Thank you for registering with us'.'<br /><br />'.
                                     'Best Regards, <br /><br />'.
                                     'Avian Brands',
            'to'				=> array($datas['email']),
        ));
    }

    /**
     * Check link forgot password
     */
    public function checkCode ($link) {
        $this->db->where("unique_code", $link);

        return $this->db->get($this->_table)->row_array();
    }

    /**
     * Generated link Confirmation code
     */
    public function _generate_link_conf_code ($member_id) {
        do {
            $generated_link = uniqid();
        } while ($this->checkCode ($generated_link));

        //update to member
        $this->db->update($this->_table,array(
            "unique_code" => $generated_link,
        ),array(
            "id" => $member_id,
        ));

        return $generated_link;
    }

    public function resetedPassword($datas) {
        $new_pass = substr(uniqid(),0,10);

		//change password and null the unique_code
		$this->update(array(
            "password" => $new_pass,
            "unique_code" => null,
        ),array(
            "id" => $datas['id'],
        ));

        // //butuh email html nya
		$content = "<p>Hello, ".$datas['name']."</p>".
					"<br/>".
					"<p>Your password has been changed : </p><br/>".
					"<p>Username : ". $datas['username'] . "</p>".
					"<p>Password : ". $new_pass . "</p>".
					"<br/><br/>".
					"<p>Have a nice day!!</p>";

        $mail = sendmail (array(
			'subject'			=> SUBJECT_RESETED_PASSWORD,
			'message'			=> $content,
			'to'			    => array($datas['email']),
		), "html");

        return $mail;
    }

    public function checkin ($member_id) {
        return $this->db->query("SELECT * FROM trs_member_point WHERE member_id = ? AND activity_code = 'login_daily' AND DATE(created_date) = CURDATE()", array($member_id))->result_array();
    }

}
